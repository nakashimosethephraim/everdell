#include "UI/EverdellDiscardNCardsPopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellHUD.h"
#include "GameData/EverdellCard.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Components/Border.h"
#include "Components/Image.h"
#include "Brushes/SlateImageBrush.h"
#include "Styling/SlateBrush.h"

#define FUNC_DEFINE_DISCARD_N_CARDS_POPUP_WIDGET_SETUP( ButtonNum )\
	{\
		constexpr int32 CardComparisonValue = (##ButtonNum - 1);\
		const auto* Card = PlayerState->GetHandCard(CardComparisonValue);\
		if (Card != nullptr)\
		{\
			DiscardOptionButton##ButtonNum##->OnClicked.AddDynamic(this, &UEverdellDiscardNCardsPopup::OnDiscardOptionButton##ButtonNum##Clicked);\
			DiscardOptionButton##ButtonNum##->SetBackgroundColor(WhiteColor);\
			DiscardOptionButton##ButtonNum##->SetColorAndOpacity(WhiteColor);\
			DiscardOptionButton##ButtonNum##->ToolTipText = FText::FromName(Card->CardName);\
			auto* CardTexture = static_cast<UTexture2D*>(StaticLoadObject(UTexture2D::StaticClass(), nullptr, *Card->CardTextureResourcePath.ToString()));\
			DiscardOptionButtonImage##ButtonNum##->SetBrushTintColor(WhiteColor);\
			DiscardOptionButtonImage##ButtonNum##->SetBrushFromTexture(CardTexture);\
			DiscardOptionButtonImage##ButtonNum##->SetOpacity(1.f);\
		}\
		else\
		{\
			DiscardOptionButton##ButtonNum##->SetBackgroundColor(InvisibleColor);\
			DiscardOptionButton##ButtonNum##->SetColorAndOpacity(InvisibleColor);\
			DiscardOptionButton##ButtonNum##->ToolTipText = FText::GetEmpty();\
			DiscardOptionButtonImage##ButtonNum##->SetBrush(FSlateImageBrush(\
				TEXT("NoImage"),\
				DefaultImageSize,\
				InvisibleColor,\
				ESlateBrushTileType::Type::NoTile,\
				ESlateBrushImageType::Type::NoImage\
			));\
			DiscardOptionButtonImage##ButtonNum##->SetBrushTintColor(InvisibleColor);\
			DiscardOptionButtonImage##ButtonNum##->SetOpacity(0.f);\
		}\
		DiscardOptionButtonBorder##ButtonNum##->SetBrushColor(InvisibleColor);\
	}

void UEverdellDiscardNCardsPopup::InitializeData(const FEverdellPopupData& PopupData)
{
	DefaultImageSize = FVector2D(50.f, 50.f);

	// Selection colors
	GreenColor = FLinearColor(0.041681f, 1.f, 0.f, 1.f);;

	// Text colors
	WhiteColor = FLinearColor(1.f, 1.f, 1.f, 1.f);
	BlackColor = FLinearColor(0.f, 0.f, 0.f, 1.f);

	// No color
	InvisibleColor = FLinearColor(1.f, 1.f, 1.f, 0.f);
	
	// Keep track of hand card
	for (int32 Index = 0; Index < 8; ++Index)
	{
		CardsToDiscard.Add(false);
	}

	DiscardLimit = PopupData.UpperLimit;
	Multiplier = PopupData.Multiplier;
	AfterDiscardDelegate = std::move(PopupData.AfterDiscardDelegate);

	DiscardCardCount = 0;
}

void UEverdellDiscardNCardsPopup::InitializeInputs()
{
	DiscardMessage->SetText(FText::FromString(FString::Printf(TEXT("Select up to %d %s to discard"), DiscardLimit, (DiscardLimit == 1 ? TEXT("card") : TEXT("cards")))));
	SubmitButton->OnClicked.AddDynamic(this, &UEverdellDiscardNCardsPopup::OnSubmitButtonClicked);

	REPEAT_POPUP_DECLARATION_N(8, FUNC_DEFINE_DISCARD_N_CARDS_POPUP_WIDGET_SETUP)
}

void UEverdellDiscardNCardsPopup::OnSubmitButtonClicked()
{
	HUD->ClosePopup();
	for (int32 Index = 0; Index < CardsToDiscard.Num(); ++Index)
	{
		if (CardsToDiscard[Index])
		{
			PlayerState->DiscardHandCard(Index);
		}
	}
	if (AfterDiscardDelegate.IsBound())
	{
		AfterDiscardDelegate.Execute(DiscardCardCount * Multiplier);
	}
}

void UEverdellDiscardNCardsPopup::HandleClicked(const int32 Index, UBorder* Border)
{
	if (DiscardCardCount < DiscardLimit && !CardsToDiscard[Index])
	{
		CardsToDiscard[Index] = true;
		Border->SetBrushColor(GreenColor);
		++DiscardCardCount;
	}
	else if (CardsToDiscard[Index] && DiscardCardCount > 0)
	{
		CardsToDiscard[Index] = false;
		Border->SetBrushColor(InvisibleColor);
		--DiscardCardCount;
	}
}

REPEAT_POPUP_DECLARATION_FOURPARAMS_N(8, FUNC_DEFINE_POPUP_CLICKABLE_CALLBACK_WITH_NUMBERED_ARG, UEverdellDiscardNCardsPopup, DiscardOptionButton, HandleClicked, Border)
