#include "UI/EverdellStorehousePopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellHUD.h"
#include "GameData/EverdellCard.h"
#include "Components/Button.h"

void UEverdellStorehousePopup::InitializeData(const FEverdellPopupData& PopupData)
{
	AssociatedCard = PopupData.AssociatedCard;
}

void UEverdellStorehousePopup::InitializeInputs()
{
	TwigOptionButton->OnClicked.AddDynamic(this, &UEverdellStorehousePopup::OnTwigOptionButtonClicked);
	ResinOptionButton->OnClicked.AddDynamic(this, &UEverdellStorehousePopup::OnResinOptionButtonClicked);
	PebbleOptionButton->OnClicked.AddDynamic(this, &UEverdellStorehousePopup::OnPebbleOptionButtonClicked);
	BerryOptionButton->OnClicked.AddDynamic(this, &UEverdellStorehousePopup::OnBerryOptionButtonClicked);
}

void UEverdellStorehousePopup::OnTwigOptionButtonClicked()
{
	AssociatedCard->ContainedTwigs += 3;
	HUD->ClosePopup();
}

void UEverdellStorehousePopup::OnResinOptionButtonClicked()
{
	AssociatedCard->ContainedResin += 2;
	HUD->ClosePopup();
}

void UEverdellStorehousePopup::OnPebbleOptionButtonClicked()
{
	AssociatedCard->ContainedPebbles += 1;
	HUD->ClosePopup();
}

void UEverdellStorehousePopup::OnBerryOptionButtonClicked()
{
	AssociatedCard->ContainedBerries += 2;
	HUD->ClosePopup();
}
