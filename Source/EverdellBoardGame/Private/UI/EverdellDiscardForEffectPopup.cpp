#include "UI/EverdellDiscardForEffectPopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellHUD.h"
#include "GameData/EverdellCard.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"

void UEverdellDiscardForEffectPopup::InitializeData(const FEverdellPopupData& PopupData)
{
	AssociatedCard = PopupData.AssociatedCard;
	CardToDiscard = PopupData.CardToDiscard;
}

void UEverdellDiscardForEffectPopup::InitializeInputs()
{
	NoDiscardButton->OnClicked.AddDynamic(this, &UEverdellDiscardForEffectPopup::OnNoDiscardButtonClicked);
	DiscardButton->OnClicked.AddDynamic(this, &UEverdellDiscardForEffectPopup::OnDiscardButtonClicked);
	DiscardButtonText->SetText(FText::FromName(CardToDiscard->CardName));
}

void UEverdellDiscardForEffectPopup::OnNoDiscardButtonClicked()
{
	HUD->ClosePopup();
	if (AssociatedCard->IsPlayable(PlayerState)) PlayerState->MoveToCityPayingCost(AssociatedCard, false, false);
}

void UEverdellDiscardForEffectPopup::OnDiscardButtonClicked()
{
	HUD->ClosePopup();
	PlayerState->DiscardCityCard(CardToDiscard->LocationLocalIndex);
	if (CardToDiscard->CanPerformActivatedEffect.IsBound() && CardToDiscard->PerformActivatedEffect.IsBound() && CardToDiscard->CanPerformActivatedEffect.Execute(GameState, HUD, nullptr, AssociatedCard))
	{
		CardToDiscard->PerformActivatedEffect.Execute(GameState, HUD, nullptr, AssociatedCard);
	}
}
