#include "UI/EverdellCardTooltip.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameData/EverdellCard.h"
#include "Components/TextBlock.h"
#include "Components/SlateWrapperTypes.h"

void UEverdellCardTooltip::SetContent(const UEverdellCard* Card)
{
	Content->SetText(GenerateContent(Card));
	SetVisibility(ESlateVisibility::Visible);
}

void UEverdellCardTooltip::ResetContent()
{
	Content->SetText(FText::FromString(TEXT("")));
	SetVisibility(ESlateVisibility::Hidden);
}

FText UEverdellCardTooltip::GenerateContent(const UEverdellCard* Card)
{
	TArray<FString> Contents;

	if (Card->ContainedTwigs > 0)
	{
		Contents.Add(FString::Printf(TEXT("%d %s"), Card->ContainedTwigs, (Card->ContainedTwigs == 1 ? TEXT("Twig") : TEXT("Twigs"))));
	}
	if (Card->ContainedResin > 0)
	{
		Contents.Add(FString::Printf(TEXT("%d Resin"), Card->ContainedResin));
	}
	if (Card->ContainedPebbles > 0)
	{
		Contents.Add(FString::Printf(TEXT("%d %s"), Card->ContainedPebbles, (Card->ContainedPebbles == 1 ? TEXT("Pebble") : TEXT("Pebbles"))));
	}
	if (Card->ContainedBerries > 0)
	{
		Contents.Add(FString::Printf(TEXT("%d %s"), Card->ContainedBerries, (Card->ContainedBerries == 1 ? TEXT("Berry") : TEXT("Berries"))));
	}
	if (Card->ContainedVictoryPoints > 0)
	{
		Contents.Add(FString::Printf(TEXT("%d %s"), Card->ContainedVictoryPoints, (Card->ContainedVictoryPoints == 1 ? TEXT("Point") : TEXT("Points"))));
	}

	return FText::FromString(FString::Join(Contents, TEXT("\n")));
}
