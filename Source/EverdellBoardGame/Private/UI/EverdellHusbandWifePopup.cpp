#include "UI/EverdellHusbandWifePopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellHUD.h"
#include "GameData/EverdellCard.h"
#include "Actors/Everdell3DCard.h"
#include "Actors/EverdellCardLocationActor.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"

void UEverdellHusbandWifePopup::InitializeData(const FEverdellPopupData& PopupData)
{
	AssociatedCard = PopupData.AssociatedCard;
	PossiblePlacementLocation = PopupData.PossiblePlacementLocation;
}

void UEverdellHusbandWifePopup::InitializeInputs()
{
	RegularPlayOptionButton->OnClicked.AddDynamic(this, &UEverdellHusbandWifePopup::OnRegularPlayOptionButtonClicked);
	CombineOptionButton->OnClicked.AddDynamic(this, &UEverdellHusbandWifePopup::OnCombineOptionButtonClicked);
	CombineOptionButtonText->SetText(FText::FromString(FString::Printf(TEXT("Combine with %s at city slot #%d"), *PossiblePlacementLocation->GetCard()->GetCard()->CardName.ToString(), PossiblePlacementLocation->LocationIndex + 1)));
}

void UEverdellHusbandWifePopup::OnRegularPlayOptionButtonClicked()
{
	HUD->ClosePopup();
	PlayerState->MoveToCity(AssociatedCard);
}

void UEverdellHusbandWifePopup::OnCombineOptionButtonClicked()
{
	HUD->ClosePopup();
	PlayerState->PairCardInCity(AssociatedCard, PossiblePlacementLocation);
}
