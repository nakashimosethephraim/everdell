#include "UI/EverdellCarnivalPopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellHUD.h"
#include "GameData/EverdellCardCategoryEnum.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"

void UEverdellCarnivalPopup::InitializeData(const FEverdellPopupData& PopupData)
{
	int32 GreenProductionCount = PlayerState->CountOfCardCategoryInCity(EEverdellCardCategory::GREEN_PRODUCTION) - 1;
	DrawCount = GreenProductionCount;
	AnyCount = static_cast<int32>(GreenProductionCount / 2);
}

void UEverdellCarnivalPopup::InitializeInputs()
{
	DrawOptionButton->OnClicked.AddDynamic(this, &UEverdellCarnivalPopup::OnDrawOptionButtonClicked);
	DrawOptionButtonText->SetText(FText::FromString(FString::Printf(TEXT("Draw %d Cards"), DrawCount)));

	GainAnyOptionButton->OnClicked.AddDynamic(this, &UEverdellCarnivalPopup::OnGainAnyOptionButtonClicked);
	GainAnyOptionButtonText->SetText(FText::FromString(FString::Printf(TEXT("Gain Any %d %s"), AnyCount, (AnyCount == 1 ? TEXT("Resource") : TEXT("Resources")))));
}

void UEverdellCarnivalPopup::OnDrawOptionButtonClicked()
{
	HUD->ClosePopup();
	PlayerState->AugmentAvailableCardDraw(DrawCount);
}

void UEverdellCarnivalPopup::OnGainAnyOptionButtonClicked()
{
	HUD->ClosePopup();
	HUD->OpenChooseAnyPopup(AnyCount);
}
