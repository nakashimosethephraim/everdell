#include "UI/EverdellCardPaymentPopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellHUD.h"
#include "GameData/EverdellCard.h"
#include "Actors/Everdell3DCard.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"

#define FUNC_DEFINE_CARD_PAYMENT_POPUP_WIDGET_SETUP( ButtonNum )\
	{\
		constexpr int32 FreeUpgradeOptionComparisonValue##ButtonNum = (##ButtonNum - 1);\
		if (FreeUpgradeOptions.Num() > FreeUpgradeOptionComparisonValue##ButtonNum##)\
		{\
			FreeUpgradeButton##ButtonNum##->OnClicked.AddDynamic(this, &UEverdellCardPaymentPopup::OnFreeUpgradeButton##ButtonNum##Clicked);\
			FreeUpgradeButtonTextBlock##ButtonNum##->SetText(\
				FText::FromString(FString::Printf(\
					TEXT("Free Upgrade: %s at city slot #%d"),\
					*FreeUpgradeOptions[FreeUpgradeOptionComparisonValue##ButtonNum##]->GetCard()->CardName.ToString(),\
					FreeUpgradeOptions[FreeUpgradeOptionComparisonValue##ButtonNum##]->GetCard()->LocationLocalIndex + 1\
				)\
			));\
			FreeUpgradeButton##ButtonNum##->SetBackgroundColor(GreenColor);\
			FreeUpgradeButton##ButtonNum##->SetColorAndOpacity(BlackColor);\
			FreeUpgradeButtonTextBlock##ButtonNum##->SetColorAndOpacity(BlackColor);\
		}\
		else\
		{\
			FreeUpgradeButton##ButtonNum##->SetBackgroundColor(InvisibleColor);\
			FreeUpgradeButton##ButtonNum##->SetColorAndOpacity(InvisibleColor);\
			FreeUpgradeButtonTextBlock##ButtonNum##->SetColorAndOpacity(InvisibleColor);\
		}\
	}

void UEverdellCardPaymentPopup::InitializeData(const FEverdellPopupData& PopupData)
{
	// Background color
	YellowColor = FLinearColor(1.f, 0.929942f, 0.f, 1.f);
	GreenColor = FLinearColor(0.041681f, 1.f, 0.f, 1.f);

	// Text colors
	WhiteColor = FLinearColor(1.f, 1.f, 1.f, 1.f);
	BlackColor = FLinearColor(0.f, 0.f, 0.f, 1.f);

	// No color
	InvisibleColor = FLinearColor(1.f, 1.f, 1.f, 0.f);

	AssociatedCard = PopupData.AssociatedCard;
	FreeUpgradeOptions = PopupData.FreeUpgradeOptions;
}

void UEverdellCardPaymentPopup::InitializeInputs()
{
	if (
		PlayerState->GetTwigCount() >= AssociatedCard->TwigCost &&
		PlayerState->GetResinCount() >= AssociatedCard->ResinCost &&
		PlayerState->GetPebbleCount() >= AssociatedCard->PebbleCost &&
		PlayerState->GetBerryCount() >= AssociatedCard->BerryCost
	)
	{
		PayCostButton->OnClicked.AddDynamic(this, &UEverdellCardPaymentPopup::OnPayCostButtonClicked);
		PayCostButton->SetBackgroundColor(YellowColor);
		PayCostButton->SetColorAndOpacity(BlackColor);
	}
	else
	{
		PayCostButton->SetBackgroundColor(InvisibleColor);
		PayCostButton->SetColorAndOpacity(InvisibleColor);
	}

	REPEAT_POPUP_DECLARATION_N(8, FUNC_DEFINE_CARD_PAYMENT_POPUP_WIDGET_SETUP)
}

void UEverdellCardPaymentPopup::OnPayCostButtonClicked()
{
	HUD->ClosePopup();
	PlayerState->MoveToCityPayingCost(AssociatedCard);
}

void UEverdellCardPaymentPopup::HandleFreeUpgrade(uint8 FreeUpgradeIndex)
{
	HUD->ClosePopup();
	PlayerState->MoveToCityWithFreeUpgrade(AssociatedCard, FreeUpgradeOptions[FreeUpgradeIndex]);
}

REPEAT_POPUP_DECLARATION_THREEPARAMS_N(8, FUNC_DEFINE_POPUP_CLICKABLE_CALLBACK, UEverdellCardPaymentPopup, FreeUpgradeButton, HandleFreeUpgrade)
