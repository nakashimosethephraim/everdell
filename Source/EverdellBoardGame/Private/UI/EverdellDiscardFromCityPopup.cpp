#include "UI/EverdellDiscardFromCityPopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellHUD.h"
#include "Actors/EverdellPlayerCity.h"
#include "Actors/Everdell3DCard.h"
#include "GameData/EverdellCard.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"

#define FUNC_DEFINE_DISCARD_FROM_CITY_POPUP_WIDGET_SETUP( ButtonNum )\
	{\
		constexpr int32 CardComparisonValue = (##ButtonNum - 1);\
		const auto* CityCard = PlayerState->GetCityCard(CardComparisonValue);\
		if (CanDiscardCard(CityCard))\
		{\
			CardButton##ButtonNum##->OnClicked.AddDynamic(this, &UEverdellDiscardFromCityPopup::OnCardButton##ButtonNum##Clicked);\
			CardButtonTextBlock##ButtonNum##->SetText(FText::FromName(CityCard->GetCard()->CardName));\
			CardButton##ButtonNum##->SetBackgroundColor(WhiteColor);\
			CardButton##ButtonNum##->SetColorAndOpacity(BlackColor);\
			CardButtonTextBlock##ButtonNum##->SetColorAndOpacity(BlackColor);\
		}\
		else\
		{\
			CardButton##ButtonNum##->SetBackgroundColor(InvisibleColor);\
			CardButton##ButtonNum##->SetColorAndOpacity(InvisibleColor);\
			CardButtonTextBlock##ButtonNum##->SetColorAndOpacity(InvisibleColor);\
		}\
	}

void UEverdellDiscardFromCityPopup::InitializeData(const FEverdellPopupData& PopupData)
{
	// Colors
	WhiteColor = FLinearColor(1.f, 1.f, 1.f, 1.f);
	BlackColor = FLinearColor(0.f, 0.f, 0.f, 1.f);

	// No color
	InvisibleColor = FLinearColor(255.f, 255.f, 255.f, 0.f);

	// Associated data
	AssociatedCard = PopupData.AssociatedCard;
	CardToPlay = PopupData.CardToPlay;
	bShouldMoveAssociatedCard = PopupData.bShouldMoveAssociatedCard;
	bShouldDiscardToAssociatedCard = PopupData.bShouldDiscardToAssociatedCard;
	bShouldGainResourcesAfterDiscard = PopupData.bShouldGainResourcesAfterDiscard;
	bCanDiscardCritters = PopupData.bCanDiscardCritters;
	bCanDiscardConstructions = PopupData.bCanDiscardConstructions;

	ValueAugmentScalar = PopupData.ValueAugmentScalar;
	AfterDiscardPopupScalar = PopupData.AfterDiscardPopupScalar;
	AfterCardChoicePopupScalar = PopupData.AfterCardChoicePopupScalar;

	ValueAugmentAfterDiscardDelegate = std::move(PopupData.ValueAugmentAfterDiscardDelegate);
	OpenPopupAfterDiscardDelegate = std::move(PopupData.OpenPopupAfterDiscardDelegate);
	OpenPopupAfterCardChoiceDelegate = std::move(PopupData.OpenPopupAfterCardChoiceDelegate);
}

void UEverdellDiscardFromCityPopup::InitializeInputs()
{
	NoDiscardButton->OnClicked.AddDynamic(this, &UEverdellDiscardFromCityPopup::OnNoDiscardButtonClicked);
	
	REPEAT_POPUP_DECLARATION_N(15, FUNC_DEFINE_DISCARD_FROM_CITY_POPUP_WIDGET_SETUP)
}

void UEverdellDiscardFromCityPopup::DiscardCard(int32 CityIndex)
{
	HUD->ClosePopup();

	UEverdellCard* CardToDiscard = PlayerState->GetCityCard(CityIndex)->GetCard();

	if (bShouldGainResourcesAfterDiscard)
	{
		PlayerState->SetTwigCount(PlayerState->GetTwigCount() + CardToDiscard->TwigCost);
		PlayerState->SetResinCount(PlayerState->GetResinCount() + CardToDiscard->ResinCost);
		PlayerState->SetPebbleCount(PlayerState->GetPebbleCount() + CardToDiscard->PebbleCost);
		PlayerState->SetBerryCount(PlayerState->GetBerryCount() + CardToDiscard->BerryCost);
	}

	PlayerState->DiscardCityCard(CityIndex, (bShouldDiscardToAssociatedCard ? AssociatedCard : nullptr));

	if (bShouldMoveAssociatedCard) PlayerState->MoveToCityPayingCost(AssociatedCard, false);

	if (ValueAugmentAfterDiscardDelegate.IsBound())
	{
		ValueAugmentAfterDiscardDelegate.Execute(ValueAugmentScalar);
	}

	if (OpenPopupAfterDiscardDelegate.IsBound())
	{
		OpenPopupAfterDiscardDelegate.Execute(AfterDiscardPopupScalar);
	}

	if (CardToPlay != nullptr && OpenPopupAfterCardChoiceDelegate.IsBound())
	{
		OpenPopupAfterCardChoiceDelegate.Execute(CardToPlay, AfterCardChoicePopupScalar);
	}
}

bool UEverdellDiscardFromCityPopup::CanDiscardCard(const AEverdell3DCard* Card)
{
	return (
		Card != nullptr && Card->HasCard() &&
		(bCanDiscardCritters || !Card->GetCard()->bIsCritter) &&
		(bCanDiscardConstructions || Card->GetCard()->bIsCritter) &&
		(bShouldMoveAssociatedCard || Card->GetCard()->LocationLocalIndex != AssociatedCard->LocationLocalIndex)
	);
}

void UEverdellDiscardFromCityPopup::OnNoDiscardButtonClicked()
{
	HUD->ClosePopup();

	if (CardToPlay != nullptr && CardToPlay->IsPlayable(PlayerState)) PlayerState->MoveToCityPayingCost(CardToPlay, false, false);
}

REPEAT_POPUP_DECLARATION_THREEPARAMS_N(15, FUNC_DEFINE_POPUP_CLICKABLE_CALLBACK, UEverdellDiscardFromCityPopup, CardButton, DiscardCard)
