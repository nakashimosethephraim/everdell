#include "UI/EverdellExchangeResourcesPopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellHUD.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Components/Slider.h"
#include "Kismet/KismetMathLibrary.h"

void UEverdellExchangeResourcesPopup::InitializeData(const FEverdellPopupData& PopupData)
{
	UpperLimit = PopupData.UpperLimit;
	CurrentBalance = 0;

	TwigValue = PlayerState->GetTwigCount();
	ResinValue = PlayerState->GetResinCount();
	PebbleValue = PlayerState->GetPebbleCount();
	BerryValue = PlayerState->GetBerryCount();
}

void UEverdellExchangeResourcesPopup::InitializeInputs()
{
	SetCurrentBalanceText();
	SetTwigSliderText();
	SetResinSliderText();
	SetPebbleSliderText();
	SetBerrySliderText();

	TwigSlider->SetMinValue(UKismetMathLibrary::Max(0, TwigValue - UpperLimit));
	TwigSlider->SetMaxValue(TwigValue + UpperLimit);
	TwigSlider->SetValue(TwigValue);
	TwigSlider->SetStepSize(1);
	TwigSlider->OnValueChanged.AddDynamic(this, &UEverdellExchangeResourcesPopup::OnTwigSliderValueChanged);

	ResinSlider->SetMinValue(UKismetMathLibrary::Max(0, ResinValue - UpperLimit));
	ResinSlider->SetMaxValue(ResinValue + UpperLimit);
	ResinSlider->SetValue(ResinValue);
	ResinSlider->SetStepSize(1);
	ResinSlider->OnValueChanged.AddDynamic(this, &UEverdellExchangeResourcesPopup::OnResinSliderValueChanged);

	PebbleSlider->SetMinValue(UKismetMathLibrary::Max(0, PebbleValue - UpperLimit));
	PebbleSlider->SetMaxValue(PebbleValue + UpperLimit);
	PebbleSlider->SetValue(PebbleValue);
	PebbleSlider->SetStepSize(1);
	PebbleSlider->OnValueChanged.AddDynamic(this, &UEverdellExchangeResourcesPopup::OnPebbleSliderValueChanged);

	BerrySlider->SetMinValue(UKismetMathLibrary::Max(0, BerryValue - UpperLimit));
	BerrySlider->SetMaxValue(BerryValue + UpperLimit);
	BerrySlider->SetValue(BerryValue);
	BerrySlider->SetStepSize(1);
	BerrySlider->OnValueChanged.AddDynamic(this, &UEverdellExchangeResourcesPopup::OnBerrySliderValueChanged);

	ExchangeButton->OnClicked.AddDynamic(this, &UEverdellExchangeResourcesPopup::OnExchangeButtonClicked);
}

void UEverdellExchangeResourcesPopup::SetCurrentBalanceText()
{
	CurrentBalanceText->SetText(FText::FromString(FString::Printf(TEXT("Current Balance: %d"), CurrentBalance)));
}

void UEverdellExchangeResourcesPopup::SetTwigSliderText()
{
	TwigText->SetText(FText::FromString(FString::Printf(TEXT("Twig Resources: %d"), TwigValue)));
}
void UEverdellExchangeResourcesPopup::SetResinSliderText()
{
	ResinText->SetText(FText::FromString(FString::Printf(TEXT("Resin Resources: %d"), ResinValue)));
}
void UEverdellExchangeResourcesPopup::SetPebbleSliderText()
{
	PebbleText->SetText(FText::FromString(FString::Printf(TEXT("Pebble Resources: %d"), PebbleValue)));
}
void UEverdellExchangeResourcesPopup::SetBerrySliderText()
{
	BerryText->SetText(FText::FromString(FString::Printf(TEXT("Berry Resources: %d"), BerryValue)));
}

void UEverdellExchangeResourcesPopup::OnExchangeButtonClicked()
{
	if (
		CurrentBalance == 0 &&
		((
			UKismetMathLibrary::Abs(PlayerState->GetTwigCount() - TwigValue) +
			UKismetMathLibrary::Abs(PlayerState->GetResinCount() - ResinValue) +
			UKismetMathLibrary::Abs(PlayerState->GetPebbleCount() - PebbleValue) +
			UKismetMathLibrary::Abs(PlayerState->GetBerryCount() - BerryValue)
		) <= (UpperLimit * 2))
	)
	{
		HUD->ClosePopup();

		PlayerState->SetTwigCount(TwigValue);
		PlayerState->SetResinCount(ResinValue);
		PlayerState->SetPebbleCount(PebbleValue);
		PlayerState->SetBerryCount(BerryValue);
	}
}

void UEverdellExchangeResourcesPopup::OnTwigSliderValueChanged(float Value)
{
	int32 Delta = static_cast<int32>(Value) - TwigValue;
	TwigValue = static_cast<int32>(Value);
	CurrentBalance += Delta;

	SetCurrentBalanceText();
	SetTwigSliderText();
}
void UEverdellExchangeResourcesPopup::OnResinSliderValueChanged(float Value)
{
	int32 Delta = static_cast<int32>(Value) - ResinValue;
	ResinValue = static_cast<int32>(Value);
	CurrentBalance += Delta;

	SetCurrentBalanceText();
	SetResinSliderText();
}
void UEverdellExchangeResourcesPopup::OnPebbleSliderValueChanged(float Value)
{
	int32 Delta = static_cast<int32>(Value) - PebbleValue;
	PebbleValue = static_cast<int32>(Value);
	CurrentBalance += Delta;

	SetCurrentBalanceText();
	SetPebbleSliderText();
}
void UEverdellExchangeResourcesPopup::OnBerrySliderValueChanged(float Value)
{
	int32 Delta = static_cast<int32>(Value) - BerryValue;
	BerryValue = static_cast<int32>(Value);
	CurrentBalance += Delta;

	SetCurrentBalanceText();
	SetBerrySliderText();
}
