#include "UI/EverdellPopupData.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellHUD.h"
#include "Actors/EverdellCardLocationActor.h"
#include "GameData/EverdellCard.h"
#include "Actors/Everdell3DCard.h"
#include "Actors/EverdellWorker.h"
#include "Actors/EverdellWorkerLocation.h"
#include "Blueprint/UserWidget.h"

void UEverdellPopupData::ClearData(FEverdellPopupData& PopupData)
{
	PopupData.MenuClass = nullptr;
	PopupData.MenuName = TEXT("");

	PopupData.AssociatedCard = nullptr;
	PopupData.CardToPlay = nullptr;
	PopupData.AssociatedCards.Empty();
	PopupData.PropertyName = TEXT("");
	PopupData.LowerLimit = 0;
	PopupData.UpperLimit = 0;
	PopupData.Multiplier = 1;
	PopupData.Reduction = 0;
	PopupData.ValueAugmentScalar = 0;
	PopupData.AfterDiscardPopupScalar = 0;
	PopupData.AfterCardChoicePopupScalar = 0;
	PopupData.CheckValueDelegate.Unbind();
	PopupData.ValueAugmentDelegate.Unbind();
	PopupData.AfterDiscardDelegate.Unbind();
	PopupData.ValueAugmentAfterDiscardDelegate.Unbind();
	PopupData.OpenPopupAfterDiscardDelegate.Unbind();
	PopupData.OpenPopupAfterCardChoiceDelegate.Unbind();
	PopupData.bShouldMoveAssociatedCard = false;
	PopupData.bShouldDiscardToAssociatedCard = false;
	PopupData.bShouldGainResourcesAfterDiscard = false;
	PopupData.bCanDiscardCritters = false;
	PopupData.bCanDiscardConstructions = false;

	PopupData.FreeUpgradeOptions.Empty();
	PopupData.PossiblePlacementLocation = nullptr;
	PopupData.bIncludeForestLocations = false;
	PopupData.bShouldPlayForFree = false;
	PopupData.bShouldDiscardOtherCards = false;
	PopupData.AfterCardChoiceDelegate.Unbind();
	PopupData.AfterMeadowDrawDelegate.Unbind();
	PopupData.ValidDelegateArray.Empty();
	PopupData.CardToDiscard = nullptr;
	PopupData.DeployedWorkers.Empty();
	PopupData.PreviousLocation = nullptr;
}
