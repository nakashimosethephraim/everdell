#include "UI/EverdellMoveWorkerPopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellHUD.h"
#include "Actors/EverdellWorkerLocation.h"
#include "GameData/EverdellLambdaWrapper.h"
#include "Blueprint/WidgetTree.h"
#include "Components/Button.h"
#include "Components/ButtonSlot.h"
#include "Components/TextBlock.h"
#include "Components/UniformGridPanel.h"
#include "Components/UniformGridSlot.h"
#include "Components/SlateWrapperTypes.h"
#include "Kismet/KismetMathLibrary.h"

void UEverdellMoveWorkerPopup::InitializeData(const FEverdellPopupData& PopupData)
{
	PreviousLocation = PopupData.PreviousLocation;

	// Text colors
	WhiteColor = FLinearColor(1.f, 1.f, 1.f, 1.f);
	BlackColor = FLinearColor(0.f, 0.f, 0.f, 1.f);

	ValidLocations = GameState->GetCurrentPlayer()->GetValidLocations(PreviousLocation);
	RowColCount = UKismetMathLibrary::FFloor(UKismetMathLibrary::Sqrt(ValidLocations.Num())) + 1;
}

void UEverdellMoveWorkerPopup::InitializeInputs()
{
	OptionsGrid->SetSlotPadding(FMargin(10.f));

	int32 LocationIndex = 0;
	for (int32 Row = 0; Row < RowColCount && LocationIndex < ValidLocations.Num(); ++Row)
	{
		for (int32 Col = 0; Col < RowColCount && LocationIndex < ValidLocations.Num(); ++Col)
		{
			// Get current location data
			auto* ValidLocation = ValidLocations[LocationIndex];
			++LocationIndex;

			// Set up button and interaction
			auto* DelegateButton = WidgetTree->ConstructWidget<UButton>(UButton::StaticClass());
			DelegateButton->SetBackgroundColor(WhiteColor);
			DelegateButton->SetColorAndOpacity(BlackColor);
			auto* NewLambda = NewObject<UEverdellLambdaWrapper>();
			NewLambda->LambdaFunction = [this, ValidLocation]
			{
				HUD->ClosePopup();
				ValidLocation->AddWorkerIfEffectExecutes();
				CleanUpWidgets();
			};
			Lambdas.Add(NewLambda);
			DelegateButton->OnClicked.AddDynamic(NewLambda, &UEverdellLambdaWrapper::Dispatch);

			// Set up button text
			auto* DelegateText = WidgetTree->ConstructWidget<UTextBlock>(UTextBlock::StaticClass());
			DelegateText->Font.Size = 12;
			DelegateText->SetJustification(ETextJustify::Center);
			DelegateText->SetAutoWrapText(true);
			DelegateText->SetText(FText::FromName(ValidLocation->GetLocationIdentifier()));
			DelegateText->SetColorAndOpacity(BlackColor);
			auto* DelegateTextSlot = static_cast<UButtonSlot*>(DelegateButton->AddChild(DelegateText));
			DelegateTextSlot->SetPadding(FMargin(4.f, 2.f, 4.f, 2.f));
			DelegateTextSlot->SetHorizontalAlignment(EHorizontalAlignment::HAlign_Center);
			DelegateTextSlot->SetVerticalAlignment(EVerticalAlignment::VAlign_Center);

			// Set up button slot dimensions
			auto* DelegateButtonSlot = OptionsGrid->AddChildToUniformGrid(DelegateButton, Row, Col);
			DelegateButtonSlot->SetHorizontalAlignment(EHorizontalAlignment::HAlign_Fill);
			DelegateButtonSlot->SetVerticalAlignment(EVerticalAlignment::VAlign_Fill);

			// Store widgets
			AllocatedWidgets.Add(TTuple<UButton*, UTextBlock*>(DelegateButton, DelegateText));
		}
	}
}

void UEverdellMoveWorkerPopup::CleanUpWidgets()
{
	for (const auto& AllocatedWidgetTuple : AllocatedWidgets)
	{
		OptionsGrid->RemoveChild(AllocatedWidgetTuple.Get<0>());
		WidgetTree->RemoveWidget(AllocatedWidgetTuple.Get<0>());
		AllocatedWidgetTuple.Get<0>()->RemoveChild(AllocatedWidgetTuple.Get<1>());

		if (AllocatedWidgetTuple.Get<0>()->ConditionalBeginDestroy())
		{
			AllocatedWidgetTuple.Get<0>()->BeginDestroy();
		}

		if (AllocatedWidgetTuple.Get<1>()->ConditionalBeginDestroy())
		{
			AllocatedWidgetTuple.Get<1>()->BeginDestroy();
		}
	}
	AllocatedWidgets.Empty();
}