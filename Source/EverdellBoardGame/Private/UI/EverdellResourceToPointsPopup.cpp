#include "UI/EverdellResourceToPointsPopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellHUD.h"
#include "GameData/EverdellCard.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Components/Slider.h"

void UEverdellResourceToPointsPopup::InitializeData(const FEverdellPopupData& PopupData)
{
	ResourceName = PopupData.PropertyName;

	Cost = PopupData.LowerLimit;
	MinCost = PopupData.LowerLimit;
	MaxCost = PopupData.UpperLimit;
	Rate = PopupData.Multiplier;

	CheckResourceCountDelegate = std::move(PopupData.CheckValueDelegate);
	AugmentResourceCountDelegate = std::move(PopupData.ValueAugmentDelegate);
}

void UEverdellResourceToPointsPopup::InitializeInputs()
{
	SetPointBenefitText();
	SetResourceCostSliderText();

	ResourceCostSlider->SetMinValue(MinCost);
	ResourceCostSlider->SetMaxValue(MaxCost);
	ResourceCostSlider->SetValue(Cost);
	ResourceCostSlider->SetStepSize(1);
	ResourceCostSlider->OnValueChanged.AddDynamic(this, &UEverdellResourceToPointsPopup::OnResourceCostSliderValueChanged);

	PayCostButton->OnClicked.AddDynamic(this, &UEverdellResourceToPointsPopup::OnPayCostButtonClicked);
}

void UEverdellResourceToPointsPopup::SetPointBenefitText()
{
	PointBenefitText->SetText(FText::FromString(FString::Printf(TEXT("Points Gained: %d"), Cost * Rate)));
}

void UEverdellResourceToPointsPopup::SetResourceCostSliderText()
{
	ResourceCostText->SetText(FText::FromString(FString::Printf(TEXT("%s Cost: %d"), *ResourceName.ToString(), Cost)));
}

void UEverdellResourceToPointsPopup::OnPayCostButtonClicked()
{
	if (CheckResourceCountDelegate.IsBound() && AugmentResourceCountDelegate.IsBound() && CheckResourceCountDelegate.Execute() >= Cost)
	{
		HUD->ClosePopup();

		AugmentResourceCountDelegate.Execute(-Cost);
		PlayerState->AugmentPointCount(Cost * Rate);
	}
}

void UEverdellResourceToPointsPopup::OnResourceCostSliderValueChanged(float Value)
{
	Cost = static_cast<int32>(Value);
	SetPointBenefitText();
	SetResourceCostSliderText();
}
