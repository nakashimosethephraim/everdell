#include "UI/EverdellSelectWorkerPopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellHUD.h"
#include "Actors/EverdellWorker.h"
#include "Actors/EverdellWorkerLocation.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"

#define FUNC_DEFINE_SELECT_WORKER_POPUP_WIDGET_SETUP( ButtonNum )\
	{\
		constexpr int32 WorkerComparisonValue = (##ButtonNum - 1);\
		if (DeployedWorkers.Num() > WorkerComparisonValue)\
		{\
			WorkerButton##ButtonNum##->OnClicked.AddDynamic(this, &UEverdellSelectWorkerPopup::OnWorkerButton##ButtonNum##Clicked);\
			WorkerButtonTextBlock##ButtonNum##->SetText(FText::FromName(DeployedWorkers[WorkerComparisonValue]->DeployedLocation->GetLocationIdentifier()));\
			WorkerButton##ButtonNum##->SetBackgroundColor(WhiteColor);\
			WorkerButton##ButtonNum##->SetColorAndOpacity(BlackColor);\
			WorkerButtonTextBlock##ButtonNum##->SetColorAndOpacity(BlackColor);\
		}\
		else\
		{\
			WorkerButton##ButtonNum##->SetBackgroundColor(InvisibleColor);\
			WorkerButton##ButtonNum##->SetColorAndOpacity(InvisibleColor);\
			WorkerButtonTextBlock##ButtonNum##->SetColorAndOpacity(InvisibleColor);\
		}\
	}

void UEverdellSelectWorkerPopup::InitializeData(const FEverdellPopupData& PopupData)
{
	DeployedWorkers = PopupData.DeployedWorkers;

	// Colors
	WhiteColor = FLinearColor(1.f, 1.f, 1.f, 1.f);
	BlackColor = FLinearColor(0.f, 0.f, 0.f, 1.f);

	// No color
	InvisibleColor = FLinearColor(255.f, 255.f, 255.f, 0.f);
}

void UEverdellSelectWorkerPopup::InitializeInputs()
{
	REPEAT_POPUP_DECLARATION_N(6, FUNC_DEFINE_SELECT_WORKER_POPUP_WIDGET_SETUP)
}

void UEverdellSelectWorkerPopup::HandleClicked(int32 Index)
{
	HUD->ClosePopup();
	auto* PreviousLocation = DeployedWorkers[Index]->DeployedLocation;
	PreviousLocation->RemoveWorker(DeployedWorkers[Index]);
	GameState->GetCurrentPlayer()->AddWorker(DeployedWorkers[Index], true);
	HUD->OpenMoveWorkerPopup(PreviousLocation);
}

REPEAT_POPUP_DECLARATION_THREEPARAMS_N(6, FUNC_DEFINE_POPUP_CLICKABLE_CALLBACK, UEverdellSelectWorkerPopup, WorkerButton, HandleClicked)
