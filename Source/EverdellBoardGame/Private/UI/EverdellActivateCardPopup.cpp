#include "UI/EverdellActivateCardPopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellHUD.h"
#include "Actors/EverdellCardLocationActor.h"
#include "Actors/Everdell3DCard.h"
#include "GameData/EverdellCard.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"

#define FUNC_DEFINE_ACTIVATE_CARD_POPUP_WIDGET_SETUP( ButtonNum )\
	{\
		constexpr int32 CityCardComparisonValue = (##ButtonNum - 1);\
		const auto* CityCard = PlayerState->GetCityCardLocation(CityCardComparisonValue);\
		if (const auto* Card = GetActivatableCard(CityCard))\
		{\
			CardButton##ButtonNum##->OnClicked.AddDynamic(this, &UEverdellActivateCardPopup::OnCardButton##ButtonNum##Clicked);\
			CardButtonTextBlock##ButtonNum##->SetText(FText::FromName(Card->CardName));\
			CardButton##ButtonNum##->SetBackgroundColor(WhiteColor);\
			CardButton##ButtonNum##->SetColorAndOpacity(BlackColor);\
			CardButtonTextBlock##ButtonNum##->SetColorAndOpacity(BlackColor);\
		}\
		else\
		{\
			CardButton##ButtonNum##->SetBackgroundColor(InvisibleColor);\
			CardButton##ButtonNum##->SetColorAndOpacity(InvisibleColor);\
			CardButtonTextBlock##ButtonNum##->SetColorAndOpacity(InvisibleColor);\
		}\
	}

void UEverdellActivateCardPopup::InitializeData(const FEverdellPopupData& PopupData)
{
	AssociatedCard = PopupData.AssociatedCard;

	// Colors
	WhiteColor = FLinearColor(1.f, 1.f, 1.f, 1.f);
	BlackColor = FLinearColor(0.f, 0.f, 0.f, 1.f);

	// No color
	InvisibleColor = FLinearColor(255.f, 255.f, 255.f, 0.f);
}

void UEverdellActivateCardPopup::InitializeInputs()
{
	REPEAT_POPUP_DECLARATION_N(15, FUNC_DEFINE_ACTIVATE_CARD_POPUP_WIDGET_SETUP)
}

void UEverdellActivateCardPopup::HandleClicked(int32 Index)
{
	HUD->ClosePopup();

	AEverdellCardLocationActor* CardLocation = PlayerState->GetCityCardLocation(Index);

	if (
		CardLocation->GetCard()->GetCard()->CardCategory == EEverdellCardCategory::GREEN_PRODUCTION &&
		CardLocation->GetCard()->GetCard()->CanPerformOnPlayEffect.IsBound() &&
		CardLocation->GetCard()->GetCard()->CanPerformOnPlayEffect.Execute(GameState, HUD, CardLocation, CardLocation->GetCard()->GetCard()) &&
		CardLocation->GetCard()->GetCard()->PerformPrepareForSeasonEffect.IsBound()
	)
	{
		CardLocation->GetCard()->GetCard()->PerformPrepareForSeasonEffect.Execute(GameState, HUD, CardLocation, CardLocation->GetCard()->GetCard());
	}
	else if (CardLocation->HasSecondaryCards())
	{
		for (int32 SecondaryCardIndex = 0; SecondaryCardIndex < CardLocation->GetSecondaryCardCount(); ++SecondaryCardIndex)
		{
			if (
				CardLocation->GetSecondaryCard(SecondaryCardIndex)->HasCard() &&
				CardLocation->GetSecondaryCard(SecondaryCardIndex)->GetCard()->CardCategory == EEverdellCardCategory::GREEN_PRODUCTION &&
				CardLocation->GetSecondaryCard(SecondaryCardIndex)->GetCard()->CanPerformOnPlayEffect.IsBound() &&
				CardLocation->GetSecondaryCard(SecondaryCardIndex)->GetCard()->CanPerformOnPlayEffect.Execute(GameState, HUD, CardLocation, CardLocation->GetSecondaryCard(SecondaryCardIndex)->GetCard()) &&
				CardLocation->GetSecondaryCard(SecondaryCardIndex)->GetCard()->PerformPrepareForSeasonEffect.IsBound()
			)
			{
				CardLocation->GetSecondaryCard(SecondaryCardIndex)->GetCard()->PerformPrepareForSeasonEffect.Execute(GameState, HUD, CardLocation, CardLocation->GetSecondaryCard(SecondaryCardIndex)->GetCard());
				break;
			}
		}
	}
}

const UEverdellCard* UEverdellActivateCardPopup::GetActivatableCard(const AEverdellCardLocationActor* CardLocation) const
{
	if (CardLocation != nullptr)
	{
		if (CardLocation->HasCard() && CardLocation->GetCard()->HasCard() && CardLocation->GetCard()->GetCard()->CardCategory == EEverdellCardCategory::GREEN_PRODUCTION &&
			CardLocation->GetCard()->GetCard()->LocationLocalIndex != AssociatedCard->LocationLocalIndex)
		{
			return CardLocation->GetCard()->GetCard();
		}
		else if (CardLocation->HasSecondaryCards())
		{
			for (int32 SecondaryCardIndex = 0; SecondaryCardIndex < CardLocation->GetSecondaryCardCount(); ++SecondaryCardIndex)
			{
				if (CardLocation->GetSecondaryCard(SecondaryCardIndex)->HasCard() && CardLocation->GetSecondaryCard(SecondaryCardIndex)->GetCard()->CardCategory == EEverdellCardCategory::GREEN_PRODUCTION &&
					CardLocation->GetSecondaryCard(SecondaryCardIndex)->GetCard()->LocationLocalIndex != AssociatedCard->LocationLocalIndex)
				{
					return CardLocation->GetSecondaryCard(SecondaryCardIndex)->GetCard();
				}
			}
		}
	}

	return nullptr;
}

REPEAT_POPUP_DECLARATION_THREEPARAMS_N(15, FUNC_DEFINE_POPUP_CLICKABLE_CALLBACK, UEverdellActivateCardPopup, CardButton, HandleClicked)
