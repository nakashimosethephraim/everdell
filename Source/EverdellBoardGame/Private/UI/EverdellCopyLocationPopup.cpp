#include "UI/EverdellCopyLocationPopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "GameManagement/EverdellHUD.h"
#include "Actors/EverdellLocation.h"
#include "Actors/EverdellForestLocationSlot.h"
#include "Actors/EverdellForestLocation.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"

#define FUNC_DEFINE_COPY_LOCATION_POPUP_WIDGET_SETUP( ButtonNum )\
	{\
		constexpr int32 LocationComparisonValue = (##ButtonNum - 1);\
		LocationButton##ButtonNum##->OnClicked.AddDynamic(this, &UEverdellCopyLocationPopup::OnLocationButton##ButtonNum##Clicked);\
		LocationButtonTextBlock##ButtonNum##->SetText(FText::FromName(GameState->BasicLocations[LocationComparisonValue]->GetLocationIdentifier()));\
	}

void UEverdellCopyLocationPopup::InitializeData(const FEverdellPopupData& PopupData)
{
	// Colors
	WhiteColor = FLinearColor(1.f, 1.f, 1.f, 1.f);
	BlackColor = FLinearColor(0.f, 0.f, 0.f, 1.f);

	// No color
	InvisibleColor = FLinearColor(1.f, 1.f, 1.f, 0.f);

	// Associated data
	bIncludeForestLocations = PopupData.bIncludeForestLocations;
}

void UEverdellCopyLocationPopup::InitializeInputs()
{
	REPEAT_POPUP_DECLARATION_N(8, FUNC_DEFINE_COPY_LOCATION_POPUP_WIDGET_SETUP)

	if (bIncludeForestLocations)
	{
		LocationButton9->OnClicked.AddDynamic(this, &UEverdellCopyLocationPopup::OnLocationButton9Clicked);
		LocationButtonTextBlock9->SetText(FText::FromName(GameState->ForestLocationSlots[0]->ForestLocation->GetLocationIdentifier()));
		LocationButton9->SetBackgroundColor(WhiteColor);
		LocationButton9->SetColorAndOpacity(BlackColor);
		LocationButtonTextBlock9->SetColorAndOpacity(BlackColor);

		LocationButton10->OnClicked.AddDynamic(this, &UEverdellCopyLocationPopup::OnLocationButton10Clicked);
		LocationButtonTextBlock10->SetText(FText::FromName(GameState->ForestLocationSlots[1]->ForestLocation->GetLocationIdentifier()));
		LocationButton10->SetBackgroundColor(WhiteColor);
		LocationButton10->SetColorAndOpacity(BlackColor);
		LocationButtonTextBlock10->SetColorAndOpacity(BlackColor);

		LocationButton11->OnClicked.AddDynamic(this, &UEverdellCopyLocationPopup::OnLocationButton11Clicked);
		LocationButtonTextBlock11->SetText(FText::FromName(GameState->ForestLocationSlots[2]->ForestLocation->GetLocationIdentifier()));
		LocationButton11->SetBackgroundColor(WhiteColor);
		LocationButton11->SetColorAndOpacity(BlackColor);
		LocationButtonTextBlock11->SetColorAndOpacity(BlackColor);

		LocationButton12->OnClicked.AddDynamic(this, &UEverdellCopyLocationPopup::OnLocationButton12Clicked);
		LocationButtonTextBlock12->SetText(FText::FromName(GameState->ForestLocationSlots[3]->ForestLocation->GetLocationIdentifier()));
		LocationButton12->SetBackgroundColor(WhiteColor);
		LocationButton12->SetColorAndOpacity(BlackColor);
		LocationButtonTextBlock12->SetColorAndOpacity(BlackColor);
	}
	else
	{
		LocationButton9->SetBackgroundColor(InvisibleColor);
		LocationButton9->SetColorAndOpacity(InvisibleColor);
		LocationButtonTextBlock9->SetColorAndOpacity(InvisibleColor);

		LocationButton10->SetBackgroundColor(InvisibleColor);
		LocationButton10->SetColorAndOpacity(InvisibleColor);
		LocationButtonTextBlock10->SetColorAndOpacity(InvisibleColor);

		LocationButton11->SetBackgroundColor(InvisibleColor);
		LocationButton11->SetColorAndOpacity(InvisibleColor);
		LocationButtonTextBlock11->SetColorAndOpacity(InvisibleColor);

		LocationButton12->SetBackgroundColor(InvisibleColor);
		LocationButton12->SetColorAndOpacity(InvisibleColor);
		LocationButtonTextBlock12->SetColorAndOpacity(InvisibleColor);
	}
}

void UEverdellCopyLocationPopup::HandleBasicLocation(int32 Index)
{
	HUD->ClosePopup();
	if (GameState->BasicLocations[Index]->PerformEffect.IsBound())
	{
		GameState->BasicLocations[Index]->PerformEffect.Execute(GameState, HUD, GameState->BasicLocations[Index], false);
	}
}

void UEverdellCopyLocationPopup::HandleForestLocation(int32 Index)
{
	HUD->ClosePopup();
	if (GameState->ForestLocationSlots[Index]->ForestLocation->PerformEffect.IsBound())
	{
		GameState->ForestLocationSlots[Index]->ForestLocation->PerformEffect.Execute(GameState, HUD, GameState->ForestLocationSlots[Index]->ForestLocation, false);
	}
}

REPEAT_POPUP_DECLARATION_THREEPARAMS_N(8, FUNC_DEFINE_POPUP_CLICKABLE_CALLBACK, UEverdellCopyLocationPopup, LocationButton, HandleBasicLocation)

void UEverdellCopyLocationPopup::OnLocationButton9Clicked()
{
	HandleForestLocation(0);
}

void UEverdellCopyLocationPopup::OnLocationButton10Clicked()
{
	HandleForestLocation(1);
}

void UEverdellCopyLocationPopup::OnLocationButton11Clicked()
{
	HandleForestLocation(2);
}

void UEverdellCopyLocationPopup::OnLocationButton12Clicked()
{
	HandleForestLocation(3);
}
