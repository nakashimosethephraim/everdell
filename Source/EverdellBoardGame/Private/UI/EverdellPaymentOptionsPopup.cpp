#include "UI/EverdellPaymentOptionsPopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellHUD.h"
#include "GameData/EverdellCard.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"

void UEverdellPaymentOptionsPopup::InitializeData(const FEverdellPopupData& PopupData)
{
	// Background color
	BrownColor = FLinearColor(0.129412f, 0.078431f, 0.f, 1.f);
	YellowColor = FLinearColor(1.f, 0.929942f, 0.f, 1.f);
	GreyColor = FLinearColor(0.3125f, 0.311349f, 0.307896, 1.f);
	PurpleColor = FLinearColor(0.479167f, 0.f, 0.420474f, 1.f);

	// Text colors
	WhiteColor = FLinearColor(1.f, 1.f, 1.f, 1.f);
	BlackColor = FLinearColor(0.f, 0.f, 0.f, 1.f);

	// No color
	InvisibleColor = FLinearColor(1.f, 1.f, 1.f, 0.f);

	// Associated data
	AssociatedCard = PopupData.AssociatedCard;
}

void UEverdellPaymentOptionsPopup::InitializeInputs()
{
	if (
		PlayerState->GetTwigCount() >= AssociatedCard->TwigCost &&
		PlayerState->GetResinCount() >= AssociatedCard->ResinCost &&
		PlayerState->GetPebbleCount() >= AssociatedCard->PebbleCost &&
		PlayerState->GetBerryCount() >= AssociatedCard->BerryCost
	)
	{
		DefaultCostOptionButton->OnClicked.AddDynamic(this, &UEverdellPaymentOptionsPopup::OnDefaultCostOptionButtonClicked);
		DefaultCostOptionButton->SetBackgroundColor(WhiteColor);
		DefaultCostOptionButton->SetColorAndOpacity(BlackColor);
	}
	else
	{
		DefaultCostOptionButton->SetBackgroundColor(InvisibleColor);
		DefaultCostOptionButton->SetColorAndOpacity(InvisibleColor);
	}

	if ((PlayerState->GetTwigCount() - AssociatedCard->TwigCost) > 0)
	{
		if (AssociatedCard->ResinCost > 0)
		{
			TwigForResinOptionButton->OnClicked.AddDynamic(this, &UEverdellPaymentOptionsPopup::OnTwigForResinOptionButtonClicked);
			TwigForResinOptionButtonText->SetText(CostsToDisplayText(AssociatedCard->TwigCost + 1, AssociatedCard->ResinCost - 1, AssociatedCard->PebbleCost, AssociatedCard->BerryCost));
			TwigForResinOptionButton->SetBackgroundColor(BrownColor);
			TwigForResinOptionButton->SetColorAndOpacity(WhiteColor);
			TwigForResinOptionButtonText->SetColorAndOpacity(WhiteColor);
		}
		else
		{
			TwigForResinOptionButton->SetBackgroundColor(InvisibleColor);
			TwigForResinOptionButton->SetColorAndOpacity(InvisibleColor);
			TwigForResinOptionButtonText->SetColorAndOpacity(InvisibleColor);
		}

		if (AssociatedCard->PebbleCost > 0)
		{
			TwigForPebbleOptionButton->OnClicked.AddDynamic(this, &UEverdellPaymentOptionsPopup::OnTwigForPebbleOptionButtonClicked);
			TwigForPebbleOptionButtonText->SetText(CostsToDisplayText(AssociatedCard->TwigCost + 1, AssociatedCard->ResinCost, AssociatedCard->PebbleCost - 1, AssociatedCard->BerryCost));
			TwigForPebbleOptionButton->SetBackgroundColor(BrownColor);
			TwigForPebbleOptionButton->SetColorAndOpacity(WhiteColor);
			TwigForPebbleOptionButtonText->SetColorAndOpacity(WhiteColor);
		}
		else
		{
			TwigForPebbleOptionButton->SetBackgroundColor(InvisibleColor);
			TwigForPebbleOptionButton->SetColorAndOpacity(InvisibleColor);
			TwigForPebbleOptionButtonText->SetColorAndOpacity(InvisibleColor);
		}

		if (AssociatedCard->BerryCost > 0)
		{
			TwigForBerryOptionButton->OnClicked.AddDynamic(this, &UEverdellPaymentOptionsPopup::OnTwigForBerryOptionButtonClicked);
			TwigForBerryOptionButtonText->SetText(CostsToDisplayText(AssociatedCard->TwigCost + 1, AssociatedCard->ResinCost, AssociatedCard->PebbleCost, AssociatedCard->BerryCost - 1));
			TwigForBerryOptionButton->SetBackgroundColor(BrownColor);
			TwigForBerryOptionButton->SetColorAndOpacity(WhiteColor);
			TwigForBerryOptionButtonText->SetColorAndOpacity(WhiteColor);
		}
		else
		{
			TwigForBerryOptionButton->SetBackgroundColor(InvisibleColor);
			TwigForBerryOptionButton->SetColorAndOpacity(InvisibleColor);
			TwigForBerryOptionButtonText->SetColorAndOpacity(InvisibleColor);
		}
	}
	else
	{
		TwigForResinOptionButton->SetBackgroundColor(InvisibleColor);
		TwigForResinOptionButton->SetColorAndOpacity(InvisibleColor);
		TwigForResinOptionButtonText->SetColorAndOpacity(InvisibleColor);

		TwigForPebbleOptionButton->SetBackgroundColor(InvisibleColor);
		TwigForPebbleOptionButton->SetColorAndOpacity(InvisibleColor);
		TwigForPebbleOptionButtonText->SetColorAndOpacity(InvisibleColor);

		TwigForBerryOptionButton->SetBackgroundColor(InvisibleColor);
		TwigForBerryOptionButton->SetColorAndOpacity(InvisibleColor);
		TwigForBerryOptionButtonText->SetColorAndOpacity(InvisibleColor);
	}

	if ((PlayerState->GetResinCount() - AssociatedCard->ResinCost) > 0)
	{
		if (AssociatedCard->TwigCost > 0)
		{
			ResinForTwigOptionButton->OnClicked.AddDynamic(this, &UEverdellPaymentOptionsPopup::OnResinForTwigOptionButtonClicked);
			ResinForTwigOptionButtonText->SetText(CostsToDisplayText(AssociatedCard->TwigCost - 1, AssociatedCard->ResinCost + 1, AssociatedCard->PebbleCost, AssociatedCard->BerryCost));
			ResinForTwigOptionButton->SetBackgroundColor(YellowColor);
			ResinForTwigOptionButton->SetColorAndOpacity(BlackColor);
			ResinForTwigOptionButtonText->SetColorAndOpacity(BlackColor);
		}
		else
		{
			ResinForTwigOptionButton->SetBackgroundColor(InvisibleColor);
			ResinForTwigOptionButton->SetColorAndOpacity(InvisibleColor);
			ResinForTwigOptionButtonText->SetColorAndOpacity(InvisibleColor);
		}

		if (AssociatedCard->PebbleCost > 0)
		{
			ResinForPebbleOptionButton->OnClicked.AddDynamic(this, &UEverdellPaymentOptionsPopup::OnResinForPebbleOptionButtonClicked);
			ResinForPebbleOptionButtonText->SetText(CostsToDisplayText(AssociatedCard->TwigCost, AssociatedCard->ResinCost + 1, AssociatedCard->PebbleCost - 1, AssociatedCard->BerryCost));
			ResinForPebbleOptionButton->SetBackgroundColor(YellowColor);
			ResinForPebbleOptionButton->SetColorAndOpacity(BlackColor);
			ResinForPebbleOptionButtonText->SetColorAndOpacity(BlackColor);
		}
		else
		{
			ResinForPebbleOptionButton->SetBackgroundColor(InvisibleColor);
			ResinForPebbleOptionButton->SetColorAndOpacity(InvisibleColor);
			ResinForPebbleOptionButtonText->SetColorAndOpacity(InvisibleColor);
		}

		if (AssociatedCard->BerryCost > 0)
		{
			ResinForBerryOptionButton->OnClicked.AddDynamic(this, &UEverdellPaymentOptionsPopup::OnResinForBerryOptionButtonClicked);
			ResinForBerryOptionButtonText->SetText(CostsToDisplayText(AssociatedCard->TwigCost, AssociatedCard->ResinCost + 1, AssociatedCard->PebbleCost, AssociatedCard->BerryCost - 1));
			ResinForBerryOptionButton->SetBackgroundColor(YellowColor);
			ResinForBerryOptionButton->SetColorAndOpacity(BlackColor);
			ResinForBerryOptionButtonText->SetColorAndOpacity(BlackColor);
		}
		else
		{
			ResinForBerryOptionButton->SetBackgroundColor(InvisibleColor);
			ResinForBerryOptionButton->SetColorAndOpacity(InvisibleColor);
			ResinForBerryOptionButtonText->SetColorAndOpacity(InvisibleColor);
		}
	}
	else
	{
		ResinForTwigOptionButton->SetBackgroundColor(InvisibleColor);
		ResinForTwigOptionButton->SetColorAndOpacity(InvisibleColor);
		ResinForTwigOptionButtonText->SetColorAndOpacity(InvisibleColor);

		ResinForPebbleOptionButton->SetBackgroundColor(InvisibleColor);
		ResinForPebbleOptionButton->SetColorAndOpacity(InvisibleColor);
		ResinForPebbleOptionButtonText->SetColorAndOpacity(InvisibleColor);

		ResinForBerryOptionButton->SetBackgroundColor(InvisibleColor);
		ResinForBerryOptionButton->SetColorAndOpacity(InvisibleColor);
		ResinForBerryOptionButtonText->SetColorAndOpacity(InvisibleColor);
	}

	if ((PlayerState->GetPebbleCount() - AssociatedCard->PebbleCost) > 0)
	{
		if (AssociatedCard->TwigCost > 0)
		{
			PebbleForTwigOptionButton->OnClicked.AddDynamic(this, &UEverdellPaymentOptionsPopup::OnPebbleForTwigOptionButtonClicked);
			PebbleForTwigOptionButtonText->SetText(CostsToDisplayText(AssociatedCard->TwigCost - 1, AssociatedCard->ResinCost, AssociatedCard->PebbleCost + 1, AssociatedCard->BerryCost));
			PebbleForTwigOptionButton->SetBackgroundColor(GreyColor);
			PebbleForTwigOptionButton->SetColorAndOpacity(WhiteColor);
			PebbleForTwigOptionButtonText->SetColorAndOpacity(WhiteColor);
		}
		else
		{
			PebbleForTwigOptionButton->SetBackgroundColor(InvisibleColor);
			PebbleForTwigOptionButton->SetColorAndOpacity(InvisibleColor);
			PebbleForTwigOptionButtonText->SetColorAndOpacity(InvisibleColor);
		}

		if (AssociatedCard->ResinCost > 0)
		{
			PebbleForResinOptionButton->OnClicked.AddDynamic(this, &UEverdellPaymentOptionsPopup::OnPebbleForResinOptionButtonClicked);
			PebbleForResinOptionButtonText->SetText(CostsToDisplayText(AssociatedCard->TwigCost, AssociatedCard->ResinCost - 1, AssociatedCard->PebbleCost + 1, AssociatedCard->BerryCost));
			PebbleForResinOptionButton->SetBackgroundColor(GreyColor);
			PebbleForResinOptionButton->SetColorAndOpacity(WhiteColor);
			PebbleForResinOptionButtonText->SetColorAndOpacity(WhiteColor);
		}
		else
		{
			PebbleForResinOptionButton->SetBackgroundColor(InvisibleColor);
			PebbleForResinOptionButton->SetColorAndOpacity(InvisibleColor);
			PebbleForResinOptionButtonText->SetColorAndOpacity(InvisibleColor);
		}

		if (AssociatedCard->BerryCost > 0)
		{
			PebbleForBerryOptionButton->OnClicked.AddDynamic(this, &UEverdellPaymentOptionsPopup::OnPebbleForBerryOptionButtonClicked);
			PebbleForBerryOptionButtonText->SetText(CostsToDisplayText(AssociatedCard->TwigCost, AssociatedCard->ResinCost, AssociatedCard->PebbleCost + 1, AssociatedCard->BerryCost - 1));
			PebbleForBerryOptionButton->SetBackgroundColor(GreyColor);
			PebbleForBerryOptionButton->SetColorAndOpacity(WhiteColor);
			PebbleForBerryOptionButtonText->SetColorAndOpacity(WhiteColor);
		}
		else
		{
			PebbleForBerryOptionButton->SetBackgroundColor(InvisibleColor);
			PebbleForBerryOptionButton->SetColorAndOpacity(InvisibleColor);
			PebbleForBerryOptionButtonText->SetColorAndOpacity(InvisibleColor);
		}
	}
	else
	{
		PebbleForTwigOptionButton->SetBackgroundColor(InvisibleColor);
		PebbleForTwigOptionButton->SetColorAndOpacity(InvisibleColor);
		PebbleForTwigOptionButtonText->SetColorAndOpacity(InvisibleColor);

		PebbleForResinOptionButton->SetBackgroundColor(InvisibleColor);
		PebbleForResinOptionButton->SetColorAndOpacity(InvisibleColor);
		PebbleForResinOptionButtonText->SetColorAndOpacity(InvisibleColor);

		PebbleForBerryOptionButton->SetBackgroundColor(InvisibleColor);
		PebbleForBerryOptionButton->SetColorAndOpacity(InvisibleColor);
		PebbleForBerryOptionButtonText->SetColorAndOpacity(InvisibleColor);
	}

	if ((PlayerState->GetBerryCount() - AssociatedCard->BerryCost) > 0)
	{
		if (AssociatedCard->TwigCost > 0)
		{
			BerryForTwigOptionButton->OnClicked.AddDynamic(this, &UEverdellPaymentOptionsPopup::OnBerryForTwigOptionButtonClicked);
			BerryForTwigOptionButtonText->SetText(CostsToDisplayText(AssociatedCard->TwigCost - 1, AssociatedCard->ResinCost, AssociatedCard->PebbleCost, AssociatedCard->BerryCost + 1));
			BerryForTwigOptionButton->SetBackgroundColor(PurpleColor);
			BerryForTwigOptionButton->SetColorAndOpacity(WhiteColor);
			BerryForTwigOptionButtonText->SetColorAndOpacity(WhiteColor);
		}
		else
		{
			BerryForTwigOptionButton->SetBackgroundColor(InvisibleColor);
			BerryForTwigOptionButton->SetColorAndOpacity(InvisibleColor);
			BerryForTwigOptionButtonText->SetColorAndOpacity(InvisibleColor);
		}

		if (AssociatedCard->ResinCost > 0)
		{
			BerryForResinOptionButton->OnClicked.AddDynamic(this, &UEverdellPaymentOptionsPopup::OnBerryForResinOptionButtonClicked);
			BerryForResinOptionButtonText->SetText(CostsToDisplayText(AssociatedCard->TwigCost, AssociatedCard->ResinCost - 1, AssociatedCard->PebbleCost, AssociatedCard->BerryCost + 1));
			BerryForResinOptionButton->SetBackgroundColor(PurpleColor);
			BerryForResinOptionButton->SetColorAndOpacity(WhiteColor);
			BerryForResinOptionButtonText->SetColorAndOpacity(WhiteColor);
		}
		else
		{
			BerryForResinOptionButton->SetBackgroundColor(InvisibleColor);
			BerryForResinOptionButton->SetColorAndOpacity(InvisibleColor);
			BerryForResinOptionButtonText->SetColorAndOpacity(InvisibleColor);
		}

		if (AssociatedCard->PebbleCost > 0)
		{
			BerryForPebbleOptionButton->OnClicked.AddDynamic(this, &UEverdellPaymentOptionsPopup::OnBerryForPebbleOptionButtonClicked);
			BerryForPebbleOptionButtonText->SetText(CostsToDisplayText(AssociatedCard->TwigCost, AssociatedCard->ResinCost, AssociatedCard->PebbleCost - 1, AssociatedCard->BerryCost + 1));
			BerryForPebbleOptionButton->SetBackgroundColor(PurpleColor);
			BerryForPebbleOptionButton->SetColorAndOpacity(WhiteColor);
			BerryForPebbleOptionButtonText->SetColorAndOpacity(WhiteColor);
		}
		else
		{
			BerryForPebbleOptionButton->SetBackgroundColor(InvisibleColor);
			BerryForPebbleOptionButton->SetColorAndOpacity(InvisibleColor);
			BerryForPebbleOptionButtonText->SetColorAndOpacity(InvisibleColor);
		}
	}
	else
	{
		BerryForTwigOptionButton->SetBackgroundColor(InvisibleColor);
		BerryForTwigOptionButton->SetColorAndOpacity(InvisibleColor);
		BerryForTwigOptionButtonText->SetColorAndOpacity(InvisibleColor);

		BerryForResinOptionButton->SetBackgroundColor(InvisibleColor);
		BerryForResinOptionButton->SetColorAndOpacity(InvisibleColor);
		BerryForResinOptionButtonText->SetColorAndOpacity(InvisibleColor);

		BerryForPebbleOptionButton->SetBackgroundColor(InvisibleColor);
		BerryForPebbleOptionButton->SetColorAndOpacity(InvisibleColor);
		BerryForPebbleOptionButtonText->SetColorAndOpacity(InvisibleColor);
	}
}

FText UEverdellPaymentOptionsPopup::CostsToDisplayText(int32 TwigCost, int32 ResinCost, int32 PebbleCost, int32 BerryCost)
{
	TArray<FString> Costs;

	if (TwigCost > 0)
	{
		FString TwigCostString = FString::Printf(TEXT("%d %s"), TwigCost, (TwigCost == 1 ? TEXT("Twig") : TEXT("Twigs")));
		Costs.Add(TwigCostString);
	}
	if (ResinCost > 0)
	{
		FString ResinCostString = FString::Printf(TEXT("%d %s"), ResinCost, TEXT("Resin"));
		Costs.Add(ResinCostString);
	}
	if (PebbleCost > 0)
	{
		FString PebbleCostString = FString::Printf(TEXT("%d %s"), PebbleCost, (PebbleCost == 1 ? TEXT("Pebble") : TEXT("Pebbles")));
		Costs.Add(PebbleCostString);
	}
	if (BerryCost > 0)
	{
		FString BerryCostString = FString::Printf(TEXT("%d %s"), BerryCost, (BerryCost == 1 ? TEXT("Berry") : TEXT("Berries")));
		Costs.Add(BerryCostString);
	}

	return FText::FromString(FString::Join(Costs, TEXT(", ")));
}

void UEverdellPaymentOptionsPopup::PayAssociatedCost(int32 TwigCostDelta, int32 ResinCostDelta, int32 PebbleCostDelta, int32 BerryCostDelta)
{
	HUD->ClosePopup();
	PlayerState->MoveToCityPayingExplicitCost(
		AssociatedCard,
		static_cast<int32>(AssociatedCard->TwigCost) + TwigCostDelta,
		static_cast<int32>(AssociatedCard->ResinCost) + ResinCostDelta,
		static_cast<int32>(AssociatedCard->PebbleCost) + PebbleCostDelta,
		static_cast<int32>(AssociatedCard->BerryCost) + BerryCostDelta
	);
}

void UEverdellPaymentOptionsPopup::OnDefaultCostOptionButtonClicked()
{
	PayAssociatedCost(0, 0, 0, 0);
}

void UEverdellPaymentOptionsPopup::OnTwigForResinOptionButtonClicked()
{
	PayAssociatedCost(1, -1, 0, 0);
}

void UEverdellPaymentOptionsPopup::OnTwigForPebbleOptionButtonClicked()
{
	PayAssociatedCost(1, 0, -1, 0);
}

void UEverdellPaymentOptionsPopup::OnTwigForBerryOptionButtonClicked()
{
	PayAssociatedCost(1, 0, 0, -1);
}

void UEverdellPaymentOptionsPopup::OnResinForTwigOptionButtonClicked()
{
	PayAssociatedCost(-1, 1, 0, 0);
}

void UEverdellPaymentOptionsPopup::OnResinForPebbleOptionButtonClicked()
{
	PayAssociatedCost(0, 1, -1, 0);
}

void UEverdellPaymentOptionsPopup::OnResinForBerryOptionButtonClicked()
{
	PayAssociatedCost(0, 1, 0, -1);
}

void UEverdellPaymentOptionsPopup::OnPebbleForTwigOptionButtonClicked()
{
	PayAssociatedCost(-1, 0, 1, 0);
}

void UEverdellPaymentOptionsPopup::OnPebbleForResinOptionButtonClicked()
{
	PayAssociatedCost(0, -1, 1, 0);
}

void UEverdellPaymentOptionsPopup::OnPebbleForBerryOptionButtonClicked()
{
	PayAssociatedCost(0, 0, 1, -1);
}

void UEverdellPaymentOptionsPopup::OnBerryForTwigOptionButtonClicked()
{
	PayAssociatedCost(-1, 0, 0, 1);
}

void UEverdellPaymentOptionsPopup::OnBerryForResinOptionButtonClicked()
{
	PayAssociatedCost(0, -1, 0, 1);
}

void UEverdellPaymentOptionsPopup::OnBerryForPebbleOptionButtonClicked()
{
	PayAssociatedCost(0, 0, -1, 1);
}
