#include "UI/EverdellDrawFromMeadowPopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "GameManagement/EverdellHUD.h"
#include "Actors/EverdellDeckActor.h"
#include "Actors/EverdellLocationGroup.h"
#include "Actors/Everdell3DCard.h"
#include "UI/EverdellUICard.h"
#include "GameData/EverdellCard.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"
#include "Kismet/KismetMathLibrary.h"

#define FUNC_DEFINE_DRAW_FROM_MEADOW_POPUP_WIDGET_SETUP( ButtonNum )\
	{\
		constexpr int32 CardComparisonValue = (##ButtonNum - 1);\
		DrawOptionButton##ButtonNum##->OnClicked.AddDynamic(this, &UEverdellDrawFromMeadowPopup::OnDrawOptionButton##ButtonNum##Clicked);\
		auto* Card = GameState->Meadow->GetCard(CardComparisonValue)->GetCard();\
		DrawOptionButton##ButtonNum##->SetToolTipText(FText::FromString(FString::Printf(TEXT("%s\n%s"), *Card->CardName.ToString(), *Card->CardDescription.ToString())));\
		auto* CardTexture = static_cast<UTexture2D*>(StaticLoadObject(UTexture2D::StaticClass(), nullptr, *Card->CardTextureResourcePath.ToString()));\
		DrawOptionButtonImage##ButtonNum##->SetBrushFromTexture(CardTexture);\
	}

void UEverdellDrawFromMeadowPopup::InitializeData(const FEverdellPopupData& PopupData)
{
	Reduction = PopupData.Reduction;
	RemainingDraw = UKismetMathLibrary::Min((PlayerState->GetHandCardLimit() - PlayerState->GetHandCardCount()), PopupData.UpperLimit);

	AfterCardChoiceDelegate = std::move(PopupData.AfterCardChoiceDelegate);
	AfterMeadowDrawDelegate = std::move(PopupData.AfterMeadowDrawDelegate);
}

void UEverdellDrawFromMeadowPopup::InitializeInputs()
{
	DrawText->SetText(FText::FromString(FString::Printf(TEXT("Draw %d more %s from the Meadow"), RemainingDraw, (RemainingDraw > 1 ? TEXT("cards") : TEXT("card")))));

	REPEAT_POPUP_DECLARATION_N(8, FUNC_DEFINE_DRAW_FROM_MEADOW_POPUP_WIDGET_SETUP)

	GameState->Deck->OnCardGeneratedDelegate.AddDynamic(this, &UEverdellDrawFromMeadowPopup::SetDataForIndex);
}

void UEverdellDrawFromMeadowPopup::HandleClicked(const int32 Index)
{
	--RemainingDraw;
	MoveCardToHand(GameState->Meadow->GetCard(Index)->GetCard());

	if (RemainingDraw <= 0)
	{
		HUD->ClosePopup();

		GameState->Deck->OnCardGeneratedDelegate.RemoveDynamic(this, &UEverdellDrawFromMeadowPopup::SetDataForIndex);

		if (DrawnCards.Num() > 1)
		{
			if (AfterMeadowDrawDelegate.IsBound())
			{
				AfterMeadowDrawDelegate.Execute(Reduction, DrawnCards, AfterCardChoiceDelegate);
			}
		}
		else if (DrawnCards.Num() == 1)
		{
			if (AfterCardChoiceDelegate.IsBound())
			{
				AfterCardChoiceDelegate.Execute(DrawnCards[0], Reduction);
			}
		}
	}
	else
	{
		DrawText->SetText(FText::FromString(FString::Printf(TEXT("Draw %d more %s from the Meadow"), RemainingDraw, (RemainingDraw > 1 ? TEXT("cards") : TEXT("card")))));
	}
}

void UEverdellDrawFromMeadowPopup::SetDataForIndex(const int32 Index)
{
	auto* Card = GameState->Meadow->GetCard(Index)->GetCard();
	FText NewToolTipText = FText::FromString(FString::Printf(TEXT("%s\n%s"), *Card->CardName.ToString(), *Card->CardDescription.ToString()));
	auto* CardTexture = static_cast<UTexture2D*>(StaticLoadObject(UTexture2D::StaticClass(), nullptr, *Card->CardTextureResourcePath.ToString()));
	switch (Index)
	{
	case 0:
		DrawOptionButton1->SetToolTipText(NewToolTipText);
		DrawOptionButtonImage1->SetBrushFromTexture(CardTexture);
		break;
	case 1:
		DrawOptionButton2->SetToolTipText(NewToolTipText);
		DrawOptionButtonImage2->SetBrushFromTexture(CardTexture);
		break;
	case 2:
		DrawOptionButton3->SetToolTipText(NewToolTipText);
		DrawOptionButtonImage3->SetBrushFromTexture(CardTexture);
		break;
	case 3:
		DrawOptionButton4->SetToolTipText(NewToolTipText);
		DrawOptionButtonImage4->SetBrushFromTexture(CardTexture);
		break;
	case 4:
		DrawOptionButton5->SetToolTipText(NewToolTipText);
		DrawOptionButtonImage5->SetBrushFromTexture(CardTexture);
		break;
	case 5:
		DrawOptionButton6->SetToolTipText(NewToolTipText);
		DrawOptionButtonImage6->SetBrushFromTexture(CardTexture);
		break;
	case 6:
		DrawOptionButton7->SetToolTipText(NewToolTipText);
		DrawOptionButtonImage7->SetBrushFromTexture(CardTexture);
		break;
	case 7:
		DrawOptionButton8->SetToolTipText(NewToolTipText);
		DrawOptionButtonImage8->SetBrushFromTexture(CardTexture);
		break;
	default:
		break;
	}
}

void UEverdellDrawFromMeadowPopup::MoveCardToHand(UEverdellCard* Card)
{
	AEverdell3DCard* Old3DCard = GameState->Meadow->ResetCard(Card->LocationLocalIndex);
	Old3DCard->ResetCard();
	Old3DCard->Destroy();
	Old3DCard = nullptr;

	PlayerState->SetHandCard(Card);

	if (Card->IsPlayableWithReducedCost(Reduction, PlayerState))
	{
		DrawnCards.Add(Card);
	}
}

REPEAT_POPUP_DECLARATION_THREEPARAMS_N(8, FUNC_DEFINE_POPUP_CLICKABLE_CALLBACK, UEverdellDrawFromMeadowPopup, DrawOptionButton, HandleClicked)
