#include "UI/EverdellChooseCardPopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "GameManagement/EverdellHUD.h"
#include "Actors/EverdellDiscardPile.h"
#include "Actors/Everdell3DCard.h"
#include "UI/EverdellUICard.h"
#include "GameData/EverdellCard.h"
#include "Components/Button.h"
#include "Components/Image.h"
#include "Brushes/SlateImageBrush.h"
#include "Styling/SlateBrush.h"

#define FUNC_DEFINE_CHOOSE_CARD_POPUP_WIDGET_SETUP( ButtonNum )\
	{\
		constexpr int32 CardComparisonValue = (##ButtonNum - 1);\
		if (AssociatedCards.Num() > CardComparisonValue && CanChooseCard(AssociatedCards[CardComparisonValue]))\
		{\
			PlayOptionButton##ButtonNum##->OnClicked.AddDynamic(this, &UEverdellChooseCardPopup::OnPlayOptionButton##ButtonNum##Clicked);\
			PlayOptionButton##ButtonNum##->SetBackgroundColor(WhiteColor);\
			PlayOptionButton##ButtonNum##->SetColorAndOpacity(WhiteColor);\
			PlayOptionButton##ButtonNum##->ToolTipText = FText::FromString(FString::Printf(TEXT("%s\n%s"), *AssociatedCards[CardComparisonValue]->CardName.ToString(), *AssociatedCards[CardComparisonValue]->CardDescription.ToString()));\
			auto* CardTexture = static_cast<UTexture2D*>(StaticLoadObject(UTexture2D::StaticClass(), nullptr, *AssociatedCards[CardComparisonValue]->CardTextureResourcePath.ToString()));\
			PlayOptionButtonImage##ButtonNum##->SetBrushTintColor(WhiteColor);\
			PlayOptionButtonImage##ButtonNum##->SetBrushFromTexture(CardTexture);\
			PlayOptionButtonImage##ButtonNum##->SetOpacity(1.f);\
		}\
		else\
		{\
			PlayOptionButton##ButtonNum##->SetBackgroundColor(InvisibleColor);\
			PlayOptionButton##ButtonNum##->SetColorAndOpacity(InvisibleColor);\
			PlayOptionButton##ButtonNum##->ToolTipText = FText::GetEmpty();\
			PlayOptionButtonImage##ButtonNum##->SetBrush(FSlateImageBrush(\
				TEXT("NoImage"),\
				DefaultImageSize,\
				InvisibleColor,\
				ESlateBrushTileType::Type::NoTile,\
				ESlateBrushImageType::Type::NoImage\
			));\
			PlayOptionButtonImage##ButtonNum##->SetBrushTintColor(InvisibleColor);\
			PlayOptionButtonImage##ButtonNum##->SetOpacity(0.f);\
		}\
	}

void UEverdellChooseCardPopup::InitializeData(const FEverdellPopupData& PopupData)
{
	DefaultImageSize = FVector2D(50.f, 50.f);

	Reduction = PopupData.Reduction;
	UpperLimit = PopupData.UpperLimit;
	AssociatedCards = PopupData.AssociatedCards;
	bShouldPlayForFree = PopupData.bShouldPlayForFree;
	bShouldDiscardOtherCards = PopupData.bShouldDiscardOtherCards;

	AfterCardChoiceDelegate = std::move(PopupData.AfterCardChoiceDelegate);

	// Colors
	WhiteColor = FLinearColor(1.f, 1.f, 1.f, 1.f);
	BlackColor = FLinearColor(0.f, 0.f, 0.f, 1.f);

	// No color
	InvisibleColor = FLinearColor(1.f, 1.f, 1.f, 0.f);
}

void UEverdellChooseCardPopup::InitializeInputs()
{
	REPEAT_POPUP_DECLARATION_N(8, FUNC_DEFINE_CHOOSE_CARD_POPUP_WIDGET_SETUP)
}

void UEverdellChooseCardPopup::HandleClicked(const int32 Index)
{
	HUD->ClosePopup();
	if (bShouldDiscardOtherCards)
	{
		for (int32 CardIndex = 0; CardIndex < AssociatedCards.Num(); ++CardIndex)
		{
			if (CardIndex != Index)
			{
				AssociatedCards[CardIndex]->CardLocation = EEverdellCardLocation::DISCARD_PILE;
				GameState->DiscardPile->PushCard(AssociatedCards[CardIndex]);
			}
		}
	}

	if (bShouldPlayForFree)
	{
		PlayerState->MoveToCityPayingExplicitCost(AssociatedCards[Index], 0, 0, 0, 0);
	}
	else if (AfterCardChoiceDelegate.IsBound())
	{
		AfterCardChoiceDelegate.Execute(AssociatedCards[Index], Reduction);
	}
}

bool UEverdellChooseCardPopup::CanChooseCard(const UEverdellCard* Card)
{
	if (Card == nullptr) return false;
	if (bShouldPlayForFree) return Card->DoesRespectBoardState(PlayerState) && (UpperLimit < 1 || Card->VictoryPoints <= UpperLimit);

	return Card->DoesRespectBoardState(PlayerState);
}

REPEAT_POPUP_DECLARATION_THREEPARAMS_N(8, FUNC_DEFINE_POPUP_CLICKABLE_CALLBACK, UEverdellChooseCardPopup, PlayOptionButton, HandleClicked)
