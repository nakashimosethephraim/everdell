#include "UI/EverdellRevealChoicePopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "GameManagement/EverdellHUD.h"
#include "Actors/EverdellDeckActor.h"
#include "Actors/EverdellDiscardPile.h"
#include "GameData/EverdellCard.h"
#include "Blueprint/WidgetTree.h"
#include "Components/Button.h"
#include "Components/ScaleBox.h"
#include "Components/ScaleBoxSlot.h"
#include "Components/HorizontalBox.h"
#include "Components/HorizontalBoxSlot.h"
#include "Components/Image.h"
#include "Brushes/SlateImageBrush.h"
#include "Styling/SlateBrush.h"

void UEverdellRevealChoicePopup::InitializeData(const FEverdellPopupData& PopupData)
{
	RemainingChoices = PopupData.UpperLimit;
	RevealedCardRespectsBoardState = false;
	RevealedCards.Empty();

	DefaultImageSize = FVector2D(50.f, 50.f);

	// Text colors
	WhiteColor = FLinearColor(1.f, 1.f, 1.f, 1.f);
	BlackColor = FLinearColor(0.f, 0.f, 0.f, 1.f);

	// No color
	InvisibleColor = FLinearColor(1.f, 1.f, 1.f, 0.f);
}

void UEverdellRevealChoicePopup::InitializeInputs()
{
	DeckOptionButton->OnClicked.AddDynamic(this, &UEverdellRevealChoicePopup::OnDeckOptionButtonClicked);
	HideDeckOptionButtonIfNotClickable();

	DiscardOptionButton->OnClicked.AddDynamic(this, &UEverdellRevealChoicePopup::OnDiscardOptionButtonClicked);
	HideDiscardOptionButtonIfNotClickable();

	for (int32 Index = 0; Index < RemainingChoices; ++Index)
	{
		// Set up scale boxes for filling
		auto* RevealedCardScaleBox = WidgetTree->ConstructWidget<UScaleBox>(UScaleBox::StaticClass());
		auto* RevealedCardScaleBoxSlot = OptionsBox->AddChildToHorizontalBox(RevealedCardScaleBox);
		RevealedCardScaleBoxSlot->SetPadding(FMargin(10.0));
		RevealedCardScaleBoxSlot->Size.Value = 1.f;
		RevealedCardScaleBoxSlot->Size.SizeRule = ESlateSizeRule::Fill;
		RevealedCardScaleBoxSlot->SetHorizontalAlignment(EHorizontalAlignment::HAlign_Fill);
		RevealedCardScaleBoxSlot->SetVerticalAlignment(EVerticalAlignment::VAlign_Fill);

		// Set up images for filling
		auto* RevealedCardImage = WidgetTree->ConstructWidget<UImage>(UImage::StaticClass());
		RevealedCardImage->SetBrush(FSlateImageBrush(
			TEXT("NoImage"),
			DefaultImageSize,
			InvisibleColor,
			ESlateBrushTileType::Type::NoTile,
			ESlateBrushImageType::Type::NoImage
		));
		RevealedCardImage->SetBrushTintColor(InvisibleColor);
		RevealedCardImage->SetOpacity(0.f);
		auto* RevealedCardImageSlot = static_cast<UScaleBoxSlot*>(RevealedCardScaleBox->AddChild(RevealedCardImage));
		RevealedCardImageSlot->SetHorizontalAlignment(EHorizontalAlignment::HAlign_Center);
		RevealedCardImageSlot->SetVerticalAlignment(EVerticalAlignment::VAlign_Center);

		// Store widgets
		AllocatedWidgets.Add(TPair<UScaleBox*,UImage*>(RevealedCardScaleBox, RevealedCardImage));
	}
}

void UEverdellRevealChoicePopup::CloseIfChoicesExhausted()
{
	if (RemainingChoices == 0)
	{
		HUD->ClosePopup();
		CleanUpWidgets();

		if (RevealedCardRespectsBoardState) HUD->OpenChooseFreeCardPopup(0, RevealedCards);
	}
}

void UEverdellRevealChoicePopup::SetImageTextureFromCard(const UEverdellCard* Card, UImage* Image)
{
	auto* CardTexture = static_cast<UTexture2D*>(StaticLoadObject(UTexture2D::StaticClass(), nullptr, *Card->CardTextureResourcePath.ToString()));
	Image->SetBrushTintColor(WhiteColor);
	Image->SetBrushFromTexture(CardTexture);
	Image->SetOpacity(1.f);
}

void UEverdellRevealChoicePopup::OnDeckOptionButtonClicked()
{
	if (CanClickDeckOptionButton())
	{
		RevealedCards.Add(GameState->Deck->RevealCard());
		RevealedCardRespectsBoardState |= RevealedCards.Top()->DoesRespectBoardState(PlayerState);
		SetImageTextureFromCard(RevealedCards.Top(), AllocatedWidgets[RevealedCards.Num() - 1].Value);
		HideDeckOptionButtonIfNotClickable();
		--RemainingChoices;
		CloseIfChoicesExhausted();
	}
}

void UEverdellRevealChoicePopup::HideDeckOptionButtonIfNotClickable()
{
	if (!CanClickDeckOptionButton())
	{
		DeckOptionButton->SetBackgroundColor(InvisibleColor);
		DeckOptionButton->SetColorAndOpacity(InvisibleColor);
	}
	else
	{
		DeckOptionButton->SetBackgroundColor(WhiteColor);
		DeckOptionButton->SetColorAndOpacity(BlackColor);
	}
}

bool UEverdellRevealChoicePopup::CanClickDeckOptionButton() const
{
	return GameState->Deck->HasCardsRemainingInDeck();
}

void UEverdellRevealChoicePopup::OnDiscardOptionButtonClicked()
{
	if (CanClickDiscardOptionButton())
	{
		RevealedCards.Add(GameState->DiscardPile->PopCard());
		RevealedCardRespectsBoardState |= RevealedCards.Top()->DoesRespectBoardState(PlayerState);
		SetImageTextureFromCard(RevealedCards.Top(), AllocatedWidgets[RevealedCards.Num() - 1].Value);
		HideDiscardOptionButtonIfNotClickable();
		--RemainingChoices;
		CloseIfChoicesExhausted();
	}
}

void UEverdellRevealChoicePopup::HideDiscardOptionButtonIfNotClickable()
{
	if (!CanClickDiscardOptionButton())
	{
		DiscardOptionButton->SetBackgroundColor(InvisibleColor);
		DiscardOptionButton->SetColorAndOpacity(InvisibleColor);
	}
	else
	{
		DiscardOptionButton->SetBackgroundColor(WhiteColor);
		DiscardOptionButton->SetColorAndOpacity(BlackColor);
	}
}

bool UEverdellRevealChoicePopup::CanClickDiscardOptionButton() const
{
	return GameState->DiscardPile->HasCards();
}

void UEverdellRevealChoicePopup::CleanUpWidgets()
{
	for (const auto& AllocatedWidgetPair : AllocatedWidgets)
	{
		AllocatedWidgetPair.Key->RemoveChild(AllocatedWidgetPair.Value);
		OptionsBox->RemoveChild(AllocatedWidgetPair.Key);

		WidgetTree->RemoveWidget(AllocatedWidgetPair.Key);
		WidgetTree->RemoveWidget(AllocatedWidgetPair.Value);

		if (AllocatedWidgetPair.Key->ConditionalBeginDestroy())
		{
			AllocatedWidgetPair.Key->BeginDestroy();
		}

		if (AllocatedWidgetPair.Value->ConditionalBeginDestroy())
		{
			AllocatedWidgetPair.Value->BeginDestroy();
		}
	}
	AllocatedWidgets.Empty();
}
