#include "UI/EverdellPlayForNLessPopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellHUD.h"
#include "GameData/EverdellCard.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Components/Slider.h"

void UEverdellPlayForNLessPopup::InitializeData(const FEverdellPopupData& PopupData)
{
	Reduction = PopupData.Reduction;

	AssociatedCard = PopupData.AssociatedCard;

	TwigCost = AssociatedCard->TwigCost;
	ResinCost = AssociatedCard->ResinCost;
	PebbleCost = AssociatedCard->PebbleCost;
	BerryCost = AssociatedCard->BerryCost;
}

void UEverdellPlayForNLessPopup::InitializeInputs()
{
	SetRemainingReductionText();
	SetTwigCostSliderText();
	SetResinCostSliderText();
	SetPebbleCostSliderText();
	SetBerryCostSliderText();

	TwigCostSlider->SetMinValue(0);
	TwigCostSlider->SetMaxValue(TwigCost);
	TwigCostSlider->SetValue(TwigCost);
	TwigCostSlider->SetStepSize(1);
	TwigCostSlider->OnValueChanged.AddDynamic(this, &UEverdellPlayForNLessPopup::OnTwigCostSliderValueChanged);

	ResinCostSlider->SetMinValue(0);
	ResinCostSlider->SetMaxValue(ResinCost);
	ResinCostSlider->SetValue(ResinCost);
	ResinCostSlider->SetStepSize(1);
	ResinCostSlider->OnValueChanged.AddDynamic(this, &UEverdellPlayForNLessPopup::OnResinCostSliderValueChanged);

	PebbleCostSlider->SetMinValue(0);
	PebbleCostSlider->SetMaxValue(PebbleCost);
	PebbleCostSlider->SetValue(PebbleCost);
	PebbleCostSlider->SetStepSize(1);
	PebbleCostSlider->OnValueChanged.AddDynamic(this, &UEverdellPlayForNLessPopup::OnPebbleCostSliderValueChanged);

	BerryCostSlider->SetMinValue(0);
	BerryCostSlider->SetMaxValue(BerryCost);
	BerryCostSlider->SetValue(BerryCost);
	BerryCostSlider->SetStepSize(1);
	BerryCostSlider->OnValueChanged.AddDynamic(this, &UEverdellPlayForNLessPopup::OnBerryCostSliderValueChanged);

	PayCostButton->OnClicked.AddDynamic(this, &UEverdellPlayForNLessPopup::OnPayCostButtonClicked);
}

void UEverdellPlayForNLessPopup::SetRemainingReductionText()
{
	RemainingReductionText->SetText(FText::FromString(FString::Printf(TEXT("Remaining Reduction: %d"), Reduction)));
}

void UEverdellPlayForNLessPopup::SetTwigCostSliderText()
{
	TwigCostText->SetText(FText::FromString(FString::Printf(TEXT("Twig Cost: %d"), TwigCost)));
}
void UEverdellPlayForNLessPopup::SetResinCostSliderText()
{
	ResinCostText->SetText(FText::FromString(FString::Printf(TEXT("Resin Cost: %d"), ResinCost)));
}
void UEverdellPlayForNLessPopup::SetPebbleCostSliderText()
{
	PebbleCostText->SetText(FText::FromString(FString::Printf(TEXT("Pebble Cost: %d"), PebbleCost)));
}
void UEverdellPlayForNLessPopup::SetBerryCostSliderText()
{
	BerryCostText->SetText(FText::FromString(FString::Printf(TEXT("Berry Cost: %d"), BerryCost)));
}

void UEverdellPlayForNLessPopup::OnPayCostButtonClicked()
{
	if (Reduction >= 0 && (
		PlayerState->GetTwigCount() >= TwigCost &&
		PlayerState->GetResinCount() >= ResinCost &&
		PlayerState->GetPebbleCount() >= PebbleCost &&
		PlayerState->GetBerryCount() >= BerryCost
	))
	{
		HUD->ClosePopup();
		PlayerState->MoveToCityPayingExplicitCost(AssociatedCard, TwigCost, ResinCost, PebbleCost, BerryCost);
	}
}

void UEverdellPlayForNLessPopup::OnTwigCostSliderValueChanged(float Value)
{
	int32 Delta = static_cast<int32>(Value) - TwigCost;
	TwigCost = static_cast<int32>(Value);
	Reduction += Delta;

	SetRemainingReductionText();
	SetTwigCostSliderText();
}
void UEverdellPlayForNLessPopup::OnResinCostSliderValueChanged(float Value)
{
	int32 Delta = static_cast<int32>(Value) - ResinCost;
	ResinCost = static_cast<int32>(Value);
	Reduction += Delta;

	SetRemainingReductionText();
	SetResinCostSliderText();
}
void UEverdellPlayForNLessPopup::OnPebbleCostSliderValueChanged(float Value)
{
	int32 Delta = static_cast<int32>(Value) - PebbleCost;
	PebbleCost = static_cast<int32>(Value);
	Reduction += Delta;

	SetRemainingReductionText();
	SetPebbleCostSliderText();
}
void UEverdellPlayForNLessPopup::OnBerryCostSliderValueChanged(float Value)
{
	int32 Delta = static_cast<int32>(Value) - BerryCost;
	BerryCost = static_cast<int32>(Value);
	Reduction += Delta;

	SetRemainingReductionText();
	SetBerryCostSliderText();
}
