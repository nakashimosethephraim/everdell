#include "UI/EverdellUICard.h"
#include "GameManagement/EverdellBoardGame.h"
#include "Components/HorizontalBox.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "UI/EverdellUserWidget.h"
#include "GameManagement/EverdellHUD.h"
#include "GameData/EverdellCard.h"
#include "Kismet/GameplayStatics.h"
#include "Brushes/SlateBorderBrush.h"
#include "Brushes/SlateImageBrush.h"
#include "Styling/SlateBrush.h"
#include "Components/Image.h"
#include "Components/Border.h"
#include "Input/Reply.h"

UEverdellUICard::UEverdellUICard(const FObjectInitializer& ObjectIn): UUserWidget(ObjectIn)
{
	bIsClicked = false;

	DefaultImageSize = FVector2D(50.f, 50.f);
	
	BorderDefaultColor = FLinearColor(0.f, 0.f, 0.f, 1.f);
	BorderHighlightColor = FLinearColor(90.f, 90.f, 255.f, 1.f);
	InvisibleColor = FLinearColor(255.f, 255.f, 255.f, 0.f);
	WhiteColor = FLinearColor(255.f, 255.f, 255.f, 1.f);

	DefaultMargin = FMargin(1.0, 1.0, 1.0, 1.0);
	
	CardTexture = nullptr;
	Card = nullptr;

	GameState = nullptr;
	HUD = nullptr;
}

bool UEverdellUICard::HasCard() const { return Card != nullptr; }

UEverdellCard* UEverdellUICard::GetCard() const { return Card; }

void UEverdellUICard::SetCard(UEverdellCard* NewCard)
{
	Card = NewCard;
	CardTexture = static_cast<UTexture2D*>(StaticLoadObject(UTexture2D::StaticClass(), nullptr, *Card->CardTextureResourcePath.ToString()));

	CardBorder->SetToolTipText(FText::FromName(Card->CardDescription));

	if (CardImage != nullptr)
	{
		CardImage->SetBrushTintColor(WhiteColor);
		CardImage->SetBrushFromTexture(CardTexture);
		CardImage->SetOpacity(1.f);
	}
}

UEverdellCard* UEverdellUICard::ResetCard()
{
	CardTexture = nullptr;

	CardBorder->SetToolTipText(FText::GetEmpty());

	if (CardImage != nullptr)
	{	
		CardImage->SetBrush(FSlateImageBrush(
			TEXT("NoImage"),
			DefaultImageSize,
			InvisibleColor,
			ESlateBrushTileType::Type::NoTile,
			ESlateBrushImageType::Type::NoImage
		));
		CardImage->SetBrushTintColor(InvisibleColor);
		CardImage->SetOpacity(0.f);
	}

	UEverdellCard* CardToReturn = Card;
	Card = nullptr;

	return CardToReturn;
}

void UEverdellUICard::NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	Super::NativeOnMouseEnter(InGeometry, InMouseEvent);

	if (CardBorder != nullptr)
	{
		CardBorder->SetBrush(FSlateBorderBrush(
			TEXT("ColoredBorder"),
			DefaultMargin,
			BorderHighlightColor,
			ESlateBrushImageType::Type::Linear
		));
	}
}

void UEverdellUICard::NativeOnMouseLeave(const FPointerEvent& InMouseEvent)
{
	Super::NativeOnMouseLeave(InMouseEvent);

	if (CardBorder != nullptr)
	{
		CardBorder->SetBrush(FSlateBorderBrush(
			TEXT("BlackBorder"),
			DefaultMargin,
			BorderDefaultColor,
			ESlateBrushImageType::Type::Linear
		));
	}
}

FReply UEverdellUICard::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);

	if (!bIsClicked && Card != nullptr && Card->CanInteract(GameState, HUD))
	{
		bIsClicked = true;
		Card->Interact(GameState, HUD);
	}
	bIsClicked = false;
	return FReply::Handled();
}
