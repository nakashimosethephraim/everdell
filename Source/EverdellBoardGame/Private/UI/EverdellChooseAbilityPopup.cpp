#include "UI/EverdellChooseAbilityPopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellHUD.h"
#include "GameData/EverdellLambdaWrapper.h"
#include "GameData/EverdellCard.h"
#include "Blueprint/WidgetTree.h"
#include "Components/Button.h"
#include "Components/ButtonSlot.h"
#include "Components/Spacer.h"
#include "Components/TextBlock.h"
#include "Components/VerticalBox.h"
#include "Components/VerticalBoxSlot.h"
#include "Components/SlateWrapperTypes.h"

void UEverdellChooseAbilityPopup::InitializeData(const FEverdellPopupData& PopupData)
{
	AssociatedCard = PopupData.AssociatedCard;
	ValidDelegateArray = PopupData.ValidDelegateArray;

	// Text colors
	WhiteColor = FLinearColor(1.f, 1.f, 1.f, 1.f);
	BlackColor = FLinearColor(0.f, 0.f, 0.f, 1.f);
}

void UEverdellChooseAbilityPopup::InitializeInputs()
{
	NoAbilityButton->OnClicked.AddDynamic(this, &UEverdellChooseAbilityPopup::OnNoAbilityButtonClicked);

	for (auto& ValidDelegate : ValidDelegateArray)
	{
		// Set up button and interaction
		auto* DelegateButton = WidgetTree->ConstructWidget<UButton>(UButton::StaticClass());
		DelegateButton->SetBackgroundColor(WhiteColor);
		DelegateButton->SetColorAndOpacity(BlackColor);
		auto* NewLambda = NewObject<UEverdellLambdaWrapper>();
		NewLambda->LambdaFunction = [this, ValidDelegate]
		{
			HUD->ClosePopup();
			ValidDelegate.Value.Execute(GameState, HUD, AssociatedCard, ValidDelegate.Key);
			CleanUpWidgets();
		};
		Lambdas.Add(NewLambda);
		DelegateButton->OnClicked.AddDynamic(NewLambda, &UEverdellLambdaWrapper::Dispatch);

		// Set up button text
		auto* DelegateText = WidgetTree->ConstructWidget<UTextBlock>(UTextBlock::StaticClass());
		DelegateText->Font.Size = 18;
		DelegateText->SetText(FText::FromName(ValidDelegate.Key->CardName));
		DelegateText->SetColorAndOpacity(BlackColor);
		auto* DelegateTextSlot = static_cast<UButtonSlot*>(DelegateButton->AddChild(DelegateText));
		DelegateTextSlot->SetPadding(FMargin(4.f, 2.f, 4.f, 2.f));
		DelegateTextSlot->SetHorizontalAlignment(EHorizontalAlignment::HAlign_Center);
		DelegateTextSlot->SetVerticalAlignment(EVerticalAlignment::VAlign_Center);

		// Set up button slot dimensions
		auto* DelegateButtonSlot = OptionsBox->AddChildToVerticalBox(DelegateButton);
		DelegateButtonSlot->SetPadding(FMargin(100.f, 0.f, 100.f, 0.f));
		DelegateButtonSlot->Size.Value = 1.f;
		DelegateButtonSlot->Size.SizeRule = ESlateSizeRule::Fill;
		DelegateButtonSlot->SetHorizontalAlignment(EHorizontalAlignment::HAlign_Fill);
		DelegateButtonSlot->SetVerticalAlignment(EVerticalAlignment::VAlign_Fill);

		// Set up spacer
		auto* DelegateSpacer = WidgetTree->ConstructWidget<USpacer>(USpacer::StaticClass());
		auto* DelegateSpacerSlot = OptionsBox->AddChildToVerticalBox(DelegateSpacer);
		DelegateSpacerSlot->Size.Value = 1.f;
		DelegateSpacerSlot->Size.SizeRule = ESlateSizeRule::Fill;
		DelegateSpacerSlot->SetHorizontalAlignment(EHorizontalAlignment::HAlign_Fill);
		DelegateSpacerSlot->SetVerticalAlignment(EVerticalAlignment::VAlign_Fill);

		// Store widgets
		AllocatedWidgets.Add(TTuple<UButton*, UTextBlock*, USpacer*>(DelegateButton, DelegateText, DelegateSpacer));
	}
}

void UEverdellChooseAbilityPopup::CleanUpWidgets()
{
	for (const auto& AllocatedWidgetTuple : AllocatedWidgets)
	{
		OptionsBox->RemoveChild(AllocatedWidgetTuple.Get<0>());
		OptionsBox->RemoveChild(AllocatedWidgetTuple.Get<2>());
		WidgetTree->RemoveWidget(AllocatedWidgetTuple.Get<0>());
		WidgetTree->RemoveWidget(AllocatedWidgetTuple.Get<2>());
		AllocatedWidgetTuple.Get<0>()->RemoveChild(AllocatedWidgetTuple.Get<1>());

		if (AllocatedWidgetTuple.Get<0>()->ConditionalBeginDestroy())
		{
			AllocatedWidgetTuple.Get<0>()->BeginDestroy();
		}

		if (AllocatedWidgetTuple.Get<1>()->ConditionalBeginDestroy())
		{
			AllocatedWidgetTuple.Get<1>()->BeginDestroy();
		}

		if (AllocatedWidgetTuple.Get<2>()->ConditionalBeginDestroy())
		{
			AllocatedWidgetTuple.Get<2>()->BeginDestroy();
		}
	}
	AllocatedWidgets.Empty();
}

void UEverdellChooseAbilityPopup::OnNoAbilityButtonClicked()
{
	HUD->ClosePopup();
	PlayerState->MoveToCityPayingCost(AssociatedCard, false, false);
	CleanUpWidgets();
}
