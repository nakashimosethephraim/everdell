#include "UI/EverdellChooseAnyPopup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellHUD.h"
#include "Components/Button.h"

void UEverdellChooseAnyPopup::InitializeData(const FEverdellPopupData& PopupData)
{
	RemainingChoices = PopupData.UpperLimit;
}

void UEverdellChooseAnyPopup::InitializeInputs()
{
	TwigOptionButton->OnClicked.AddDynamic(this, &UEverdellChooseAnyPopup::OnTwigOptionButtonClicked);
	ResinOptionButton->OnClicked.AddDynamic(this, &UEverdellChooseAnyPopup::OnResinOptionButtonClicked);
	PebbleOptionButton->OnClicked.AddDynamic(this, &UEverdellChooseAnyPopup::OnPebbleOptionButtonClicked);

	if (BerryOptionButton != nullptr)
	{
		BerryOptionButton->OnClicked.AddDynamic(this, &UEverdellChooseAnyPopup::OnBerryOptionButtonClicked);
	}
}

void UEverdellChooseAnyPopup::CloseIfChoicesExhausted()
{
	if (RemainingChoices == 0) HUD->ClosePopup();
}

void UEverdellChooseAnyPopup::OnTwigOptionButtonClicked()
{
	PlayerState->SetTwigCount(PlayerState->GetTwigCount() + 1);
	--RemainingChoices;
	CloseIfChoicesExhausted();
}

void UEverdellChooseAnyPopup::OnResinOptionButtonClicked()
{
	PlayerState->SetResinCount(PlayerState->GetResinCount() + 1);
	--RemainingChoices;
	CloseIfChoicesExhausted();
}

void UEverdellChooseAnyPopup::OnPebbleOptionButtonClicked()
{
	PlayerState->SetPebbleCount(PlayerState->GetPebbleCount() + 1);
	--RemainingChoices;
	CloseIfChoicesExhausted();
}

void UEverdellChooseAnyPopup::OnBerryOptionButtonClicked()
{
	PlayerState->SetBerryCount(PlayerState->GetBerryCount() + 1);
	--RemainingChoices;
	CloseIfChoicesExhausted();
}
