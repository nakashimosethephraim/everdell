#include "GameManagement/EverdellHUD.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "Actors/EverdellDeckActor.h"
#include "Actors/EverdellWorker.h"
#include "Actors/EverdellWorkerLocation.h"
#include "UI/EverdellUserWidget.h"
#include "UI/EverdellPopup.h"
#include "UI/EverdellCardTooltip.h"
#include "Actors/EverdellCardLocationActor.h"
#include "GameData/EverdellCard.h"
#include "Actors/Everdell3DCard.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/MenuAnchor.h"
#include "Components/CanvasPanelSlot.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/WidgetLayoutLibrary.h"
#include "Blueprint/WidgetBlueprintLibrary.h"

AEverdellHUD::AEverdellHUD(): AHUD()
{
	static ConstructorHelpers::FClassFinder<UEverdellUserWidget> WidgetBPClass(TEXT("/Game/Blueprints/WB_EverdellPlayerUI"));
	if (WidgetBPClass.Class != nullptr)
	{
		WidgetClass = WidgetBPClass.Class;
	}
	else
	{
		WidgetClass = UEverdellUserWidget::StaticClass();
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> ActivateCardWidgetPopupBPClass(TEXT("/Game/Blueprints/WB_EverdellActivateCardPopup"));
	if (ActivateCardWidgetPopupBPClass.Class != nullptr)
	{
		ActivateCardPopup = ActivateCardWidgetPopupBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> CarnivalWidgetPopupBPClass(TEXT("/Game/Blueprints/WB_EverdellCarnivalPopup"));
	if (CarnivalWidgetPopupBPClass.Class != nullptr)
	{
		CarnivalPopup = CarnivalWidgetPopupBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> ChooseAbilityWidgetPopupBPClass(TEXT("/Game/Blueprints/WB_EverdellChooseAbilityPopup"));
	if (ChooseAbilityWidgetPopupBPClass.Class != nullptr)
	{
		ChooseAbilityPopup = ChooseAbilityWidgetPopupBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> ChooseAnyWidgetPopupBPClass(TEXT("/Game/Blueprints/WB_EverdellChooseAnyPopup"));
	if (ChooseAnyWidgetPopupBPClass.Class != nullptr)
	{
		ChooseAnyPopup = ChooseAnyWidgetPopupBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> ChooseCardWidgetPopupBPClass(TEXT("/Game/Blueprints/WB_EverdellChooseCardPopup"));
	if (ChooseCardWidgetPopupBPClass.Class != nullptr)
	{
		ChooseCardPopup = ChooseCardWidgetPopupBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> ChooseConstructionPopupWidgetBPClass(TEXT("/Game/Blueprints/WB_EverdellChooseConstructionResourcePopup"));
	if (ChooseConstructionPopupWidgetBPClass.Class != nullptr)
	{
		ChooseConstructionResourcePopup = ChooseConstructionPopupWidgetBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> CopyLocationWidgetBPClass(TEXT("/Game/Blueprints/WB_EverdellCopyLocationPopup"));
	if (CopyLocationWidgetBPClass.Class != nullptr)
	{
		CopyLocationPopup = CopyLocationWidgetBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> DiscardForEffectWidgetBPClass(TEXT("/Game/Blueprints/WB_EverdellDiscardForEffectPopup"));
	if (DiscardForEffectWidgetBPClass.Class != nullptr)
	{
		DiscardForEffectPopup = DiscardForEffectWidgetBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> DiscardNCardsWidgetBPClass(TEXT("/Game/Blueprints/WB_EverdellDiscardNCardsPopup"));
	if (DiscardNCardsWidgetBPClass.Class != nullptr)
	{
		DiscardNCardsPopup = DiscardNCardsWidgetBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> DiscardFromCityPopupWidgetBPClass(TEXT("/Game/Blueprints/WB_EverdellDiscardFromCityPopup"));
	if (DiscardFromCityPopupWidgetBPClass.Class != nullptr)
	{
		DiscardFromCityPopup = DiscardFromCityPopupWidgetBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> DrawFromMeadowWidgetBPClass(TEXT("/Game/Blueprints/WB_EverdellDrawFromMeadowPopup"));
	if (DrawFromMeadowWidgetBPClass.Class != nullptr)
	{
		DrawFromMeadowPopup = DrawFromMeadowWidgetBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> ExchangeResourcesPopupWidgetBPClass(TEXT("/Game/Blueprints/WB_EverdellExchangeResourcesPopup"));
	if (ExchangeResourcesPopupWidgetBPClass.Class != nullptr)
	{
		ExchangeResourcesPopup = ExchangeResourcesPopupWidgetBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> HusbandWifePopupWidgetBPClass(TEXT("/Game/Blueprints/WB_EverdellHusbandWifePopup"));
	if (HusbandWifePopupWidgetBPClass.Class != nullptr)
	{
		HusbandWifePopup = HusbandWifePopupWidgetBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> MoveWorkerPopupWidgetBPClass(TEXT("/Game/Blueprints/WB_EverdellMoveWorkerPopup"));
	if (MoveWorkerPopupWidgetBPClass.Class != nullptr)
	{
		MoveWorkerPopup = MoveWorkerPopupWidgetBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> PaymentOptionsPopupWidgetBPClass(TEXT("/Game/Blueprints/WB_EverdellPaymentOptionsPopup"));
	if (PaymentOptionsPopupWidgetBPClass.Class != nullptr)
	{
		PaymentOptionsPopup = PaymentOptionsPopupWidgetBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> PayOrFreeUpgradePopupBPClass(TEXT("/Game/Blueprints/WB_EverdellCardPaymentPopup_1"));
	if (PayOrFreeUpgradePopupBPClass.Class != nullptr)
	{
		PayOrFreeUpgradePopup = PayOrFreeUpgradePopupBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> PlayForNLessPopupWidgetBPClass(TEXT("/Game/Blueprints/WB_EverdellPlayForNLessPopup"));
	if (PlayForNLessPopupWidgetBPClass.Class != nullptr)
	{
		PlayForNLessPopup = PlayForNLessPopupWidgetBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> ResourceToPointsPopupWidgetBPClass(TEXT("/Game/Blueprints/WB_EverdellResourceToPointsPopup"));
	if (ResourceToPointsPopupWidgetBPClass.Class != nullptr)
	{
		ResourceToPointsPopup = ResourceToPointsPopupWidgetBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> RevealChoicePopupWidgetBPClass(TEXT("/Game/Blueprints/WB_EverdellRevealChoicePopup"));
	if (RevealChoicePopupWidgetBPClass.Class != nullptr)
	{
		RevealChoicePopup = RevealChoicePopupWidgetBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> SelectWorkerPopupWidgetBPClass(TEXT("/Game/Blueprints/WB_EverdellSelectWorkerPopup"));
	if (SelectWorkerPopupWidgetBPClass.Class != nullptr)
	{
		SelectWorkerPopup = SelectWorkerPopupWidgetBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<UEverdellPopup> StorehousePopupWidgetBPClass(TEXT("/Game/Blueprints/WB_EverdellStorehousePopup"));
	if (StorehousePopupWidgetBPClass.Class != nullptr)
	{
		StorehousePopup = StorehousePopupWidgetBPClass.Class;
	}

	Widget = nullptr;
	bIsHoveringCard = false;

	GameState = nullptr;

	PrimaryActorTick.bCanEverTick = true;
}

void AEverdellHUD::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickEnabled(false);

	Widget = static_cast<UEverdellUserWidget*>(UEverdellUserWidget::CreateWidgetInstance(*GetWorld(), WidgetClass, FName("MainPlayerUI")));
	Widget->AddToViewport(0);
	Widget->CoreMenuAnchor->OnGetUserMenuContentEvent.BindDynamic(this, &AEverdellHUD::HandlePopupContent);

	GameState = static_cast<AEverdellGameStateBase*>(UGameplayStatics::GetGameState(GetWorld()));
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->InitializeUI(Widget);

	// Temporarily set all attributes high for testing
	//PlayerState->SetWorkerCount(4);
	PlayerState->SetTwigCount(10);
	PlayerState->SetResinCount(10);
	PlayerState->SetPebbleCount(10);
	PlayerState->SetBerryCount(10);
	PlayerState->SetPointCount(10);
	PlayerState->SetAvailableCardDraw(10);
}

FVector2D AEverdellHUD::GetMousePosition() const
{
	FVector2D Position = UWidgetLayoutLibrary::GetMousePositionOnViewport(GetWorld());
	Position.X += 5;
	Position.Y += 5;
	return Position;
}

void AEverdellHUD::Tick(float DeltaTime)
{
	bool bModifiedState = Widget->CoreMenuAnchor->IsOpen() && !PopupQueue.IsEmpty();

	if (!Widget->CoreMenuAnchor->IsOpen())
	{
		if (!PopupQueue.IsEmpty())
		{
			Widget->CoreMenuAnchor->Open(true);
			bModifiedState = true;
		}
	}

	if (bIsHoveringCard)
	{
		static_cast<UCanvasPanelSlot*>(Widget->CardTooltip->Slot)->SetPosition(GetMousePosition());
		bModifiedState = true;
	}

	if (!bModifiedState) SetActorTickEnabled(false);
}

void AEverdellHUD::OpenActivateCardPopup(UEverdellCard* AssociatedCard)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = ActivateCardPopup;
	PopupData.MenuName = TEXT("ActivateCardPopupMenu");
	PopupData.AssociatedCard = AssociatedCard;

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenCarnivalPopup()
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = CarnivalPopup;
	PopupData.MenuName = TEXT("CarnivalPopupMenu");

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenChooseAbilityPopup(UEverdellCard* AssociatedCard, const TArray<TPair<UEverdellCard*, FEverdellPerformBeforePaymentEffectSignature>>& ValidDelegateArray)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = ChooseAbilityPopup;
	PopupData.MenuName = TEXT("ChooseAbilityPopupMenu");
	PopupData.AssociatedCard = AssociatedCard;
	PopupData.ValidDelegateArray = ValidDelegateArray;

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenChooseAnyPopup(int32 TotalChoices)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = ChooseAnyPopup;
	PopupData.MenuName = TEXT("ChooseAnyPopupMenu");
	PopupData.UpperLimit = TotalChoices;
	
	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenChooseCardPopup(int32 Reduction, const TArray<UEverdellCard*>& AssociatedCards, const FEverdellAfterCardChoiceSignature& AfterCardChoiceDelegate)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = ChooseCardPopup;
	PopupData.MenuName = TEXT("ChooseCardPopupMenu");
	PopupData.Reduction = Reduction;
	PopupData.AssociatedCards = AssociatedCards;
	PopupData.UpperLimit = 0;
	PopupData.bShouldPlayForFree = false;
	PopupData.bShouldDiscardOtherCards = false;
	PopupData.AfterCardChoiceDelegate = AfterCardChoiceDelegate;

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenChooseCardPopupWithBoundDelegate(int32 Reduction, const TArray<UEverdellCard*>& AssociatedCards, const FEverdellAfterCardChoiceSignature& AfterCardChoiceDelegate)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = ChooseCardPopup;
	PopupData.MenuName = TEXT("ChooseCardPopupMenu");
	PopupData.Reduction = Reduction;
	PopupData.AssociatedCards = AssociatedCards;
	PopupData.UpperLimit = 0;
	PopupData.bShouldPlayForFree = false;
	PopupData.bShouldDiscardOtherCards = false;
	PopupData.AfterCardChoiceDelegate = AfterCardChoiceDelegate;

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenChooseFreeCardPopup(const int32 UpperLimit, const TArray<UEverdellCard*>& AssociatedCards)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = ChooseCardPopup;
	PopupData.MenuName = TEXT("ChooseCardPopupMenu");
	PopupData.AssociatedCards = AssociatedCards;
	PopupData.Reduction = 0;
	PopupData.UpperLimit = UpperLimit;
	PopupData.bShouldPlayForFree = true;
	PopupData.bShouldDiscardOtherCards = false;

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenChooseConstructionResourcePopup(int32 TotalChoices)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = ChooseConstructionResourcePopup;
	PopupData.MenuName = TEXT("ChooseConstructionResourcePopupMenu");
	PopupData.UpperLimit = TotalChoices;

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenCopyLocationPopup(bool bIncludeForestLocations)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = CopyLocationPopup;
	PopupData.MenuName = TEXT("CopyLocationPopupMenu");
	PopupData.bIncludeForestLocations = bIncludeForestLocations;

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenDiscardForEffectPopup(UEverdellCard* AssociatedCard, UEverdellCard* CardToDiscard)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = DiscardForEffectPopup;
	PopupData.MenuName = TEXT("DiscardForEffectPopupMenu");
	PopupData.AssociatedCard = AssociatedCard;
	PopupData.CardToDiscard = CardToDiscard;

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenDrawFromMeadowPopup(
	const int32 Reduction, const int32 RemainingDraw,
	FEverdellAfterCardChoiceSignature&& AfterCardChoiceDelegate, FEverdellAfterMeadowDrawSignature&& AfterMeadowDrawDelegate
)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = DrawFromMeadowPopup;
	PopupData.MenuName = TEXT("DrawFromMeadowPopupMenu");
	PopupData.Reduction = Reduction;
	PopupData.UpperLimit = RemainingDraw;
	PopupData.AfterCardChoiceDelegate = std::move(AfterCardChoiceDelegate);
	PopupData.AfterMeadowDrawDelegate = std::move(AfterMeadowDrawDelegate);

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenDiscardNCardsPopup(const int32 DiscardLimit, const int32 Multiplier, FEverdellAfterDiscardSignature&& AfterDiscardDelegate)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = DiscardNCardsPopup;
	PopupData.MenuName = TEXT("DiscardNCardsPopupMenu");
	PopupData.UpperLimit = DiscardLimit;
	PopupData.Multiplier = Multiplier;
	PopupData.AfterDiscardDelegate = std::move(AfterDiscardDelegate);

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenDungeonDiscardPopup(UEverdellCard* AssociatedCard, UEverdellCard* CardToPlay)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = DiscardFromCityPopup;
	PopupData.MenuName = TEXT("DiscardFromCityPopupMenu");
	PopupData.AssociatedCard = AssociatedCard;
	PopupData.CardToPlay = CardToPlay;
	PopupData.bShouldMoveAssociatedCard = false;
	PopupData.bShouldDiscardToAssociatedCard = true;
	PopupData.bShouldGainResourcesAfterDiscard = false;
	PopupData.bCanDiscardCritters = true;
	PopupData.bCanDiscardConstructions = false;

	PopupData.AfterCardChoicePopupScalar = 3;
	PopupData.OpenPopupAfterCardChoiceDelegate.BindUObject(this, &AEverdellHUD::OpenPlayForNLessPopup);

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenExchangeResourcesPopup(const int32 ExchangeLimit)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = ExchangeResourcesPopup;
	PopupData.MenuName = TEXT("ExchangeResourcesPopupMenu");
	PopupData.UpperLimit = ExchangeLimit;

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenHusbandWifePopup(UEverdellCard* AssociatedCard, AEverdellCardLocationActor* PossiblePlacementLocation)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = HusbandWifePopup;
	PopupData.MenuName = TEXT("HusbandWifePopupMenu");
	PopupData.AssociatedCard = AssociatedCard;
	PopupData.PossiblePlacementLocation = PossiblePlacementLocation;

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenMoveWorkerPopup(AEverdellWorkerLocation* PreviousLocation)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = MoveWorkerPopup;
	PopupData.MenuName = TEXT("MoveWorkerPopupMenu");
	PopupData.PreviousLocation = PreviousLocation;

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenPaymentOptionsPopup(UEverdellCard* AssociatedCard)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = PaymentOptionsPopup;
	PopupData.MenuName = TEXT("PaymentOptionsPopupMenu");
	PopupData.AssociatedCard = AssociatedCard;

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenPayOrFreeUpgradePopup(UEverdellCard* AssociatedCard, const TArray<AEverdell3DCard*>& FreeUpgradeOptions)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = PayOrFreeUpgradePopup;
	PopupData.MenuName = TEXT("CardPaymentPopupMenu");
	PopupData.AssociatedCard = AssociatedCard;
	PopupData.FreeUpgradeOptions = FreeUpgradeOptions;

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenPlayForNLessPopup(UEverdellCard* AssociatedCard, const int32 Reduction)
{
	if((AssociatedCard->TwigCost + AssociatedCard->ResinCost + AssociatedCard->PebbleCost + AssociatedCard->BerryCost) <= Reduction)
	{
		GameState->GetCurrentPlayer()->MoveToCityPayingExplicitCost(AssociatedCard, 0, 0, 0, 0);
	}
	else
	{
		FEverdellPopupData PopupData;
		PopupData.MenuClass = PlayForNLessPopup;
		PopupData.MenuName = TEXT("PlayForNLessPopupMenu");
		PopupData.AssociatedCard = AssociatedCard;
		PopupData.Reduction = Reduction;

		PopupQueue.Enqueue(PopupData);

		if (!IsActorTickEnabled()) SetActorTickEnabled(true);
	}
}

void AEverdellHUD::OpenPostalPigeonPopup()
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = ChooseCardPopup;
	PopupData.MenuName = TEXT("ChooseCardPopupMenu");
	PopupData.AssociatedCards = GameState->Deck->RevealCards(2);
	PopupData.Reduction = 0;
	PopupData.UpperLimit = 3;
	PopupData.bShouldPlayForFree = true;
	PopupData.bShouldDiscardOtherCards = true;

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenResourceToPointsPopup(
	const FName& ResourceName, const int32 MinCost, const int32 MaxCost, const int32 Rate,
	FEverdellCheckValueSignature&& CheckValueDelegate, FEverdellValueAugmentSignature&& ValueAugmentDelegate
)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = ResourceToPointsPopup;
	PopupData.MenuName = TEXT("ResourceToPointsPopupMenu");
	PopupData.PropertyName = ResourceName;
	PopupData.LowerLimit = MinCost;
	PopupData.UpperLimit = MaxCost;
	PopupData.Multiplier = Rate;

	PopupData.CheckValueDelegate = std::move(CheckValueDelegate);
	PopupData.ValueAugmentDelegate = std::move(ValueAugmentDelegate);

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenRevealChoicePopup(const int32 UpperLimit)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = RevealChoicePopup;
	PopupData.MenuName = TEXT("RevealChoicePopupMenu");
	PopupData.UpperLimit = UpperLimit;

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenRuinsDiscardPopup(UEverdellCard* AssociatedCard)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = DiscardFromCityPopup;
	PopupData.MenuName = TEXT("DiscardFromCityPopupMenu");
	PopupData.AssociatedCard = AssociatedCard;
	PopupData.bShouldMoveAssociatedCard = true;
	PopupData.bShouldDiscardToAssociatedCard = false;
	PopupData.bShouldGainResourcesAfterDiscard = true;
	PopupData.bCanDiscardCritters = false;
	PopupData.bCanDiscardConstructions = true;
	PopupData.ValueAugmentScalar = 2;
	PopupData.ValueAugmentAfterDiscardDelegate.BindUObject(GameState->GetCurrentPlayer(), &AEverdellPlayerState::AugmentAvailableCardDraw);

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenSelectWorkerPopup(const TArray<AEverdellWorker*>& DeployedWorkers)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = SelectWorkerPopup;
	PopupData.MenuName = TEXT("SelectWorkerPopupMenu");
	PopupData.DeployedWorkers = DeployedWorkers;

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenStorehousePopup(UEverdellCard* AssociatedCard)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = StorehousePopup;
	PopupData.MenuName = TEXT("StorehousePopupMenu");
	PopupData.AssociatedCard = AssociatedCard;

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::OpenUniversityDiscardPopup(UEverdellCard* AssociatedCard)
{
	FEverdellPopupData PopupData;
	PopupData.MenuClass = DiscardFromCityPopup;
	PopupData.MenuName = TEXT("DiscardFromCityPopupMenu");
	PopupData.AssociatedCard = AssociatedCard;
	PopupData.bShouldMoveAssociatedCard = false;
	PopupData.bShouldDiscardToAssociatedCard = false;
	PopupData.bShouldGainResourcesAfterDiscard = true;
	PopupData.bCanDiscardCritters = true;
	PopupData.bCanDiscardConstructions = true;

	PopupData.ValueAugmentScalar = 1;
	PopupData.ValueAugmentAfterDiscardDelegate.BindUObject(GameState->GetCurrentPlayer(), &AEverdellPlayerState::AugmentPointCount);
	PopupData.AfterDiscardPopupScalar = 1;
	PopupData.OpenPopupAfterDiscardDelegate.BindUObject(this, &AEverdellHUD::OpenChooseAnyPopup);

	PopupQueue.Enqueue(PopupData);

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::ClosePopup()
{
	Widget->CoreMenuAnchor->Close();
}

UUserWidget* AEverdellHUD::HandlePopupContent()
{
	FEverdellPopupData PopupData;
	PopupQueue.Dequeue(PopupData);

	UEverdellPopup* PopupWidget = static_cast<UEverdellPopup*>(UUserWidget::CreateWidgetInstance(*Widget->CoreMenuAnchor, PopupData.MenuClass, PopupData.MenuName));

	PopupWidget->PlayerState = GameState->GetCurrentPlayer();
	PopupWidget->GameState = GameState;
	PopupWidget->HUD = this;
	PopupWidget->InitializeData(PopupData);
	PopupWidget->InitializeInputs();
	
	return PopupWidget;
}

void AEverdellHUD::StartHover(UEverdellCard* Card)
{
	Widget->CardTooltip->SetContent(Card);
	bIsHoveringCard = true;

	if (!IsActorTickEnabled()) SetActorTickEnabled(true);
}

void AEverdellHUD::EndHover()
{
	bIsHoveringCard = false;
	Widget->CardTooltip->ResetContent();
}
