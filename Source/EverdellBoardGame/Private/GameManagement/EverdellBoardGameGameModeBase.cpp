#include "GameManagement/EverdellBoardGameGameModeBase.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerController.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "GameManagement/EverdellHUD.h"

AEverdellBoardGameGameModeBase::AEverdellBoardGameGameModeBase(): AGameModeBase()
{
	PlayerControllerClass = AEverdellPlayerController::StaticClass();
	PlayerStateClass = AEverdellPlayerState::StaticClass();
	HUDClass = AEverdellHUD::StaticClass();
	GameStateClass = AEverdellGameStateBase::StaticClass();
}
