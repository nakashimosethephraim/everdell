#include "GameManagement/EverdellBoardGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, EverdellBoardGame, "EverdellBoardGame" );

DEFINE_LOG_CATEGORY(LogEverdellBoardGame);
