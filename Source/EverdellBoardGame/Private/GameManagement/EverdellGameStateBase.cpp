#include "GameManagement/EverdellGameStateBase.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellAIController.h"
#include "GameFramework/Pawn.h"
#include "Actors/EverdellDeckActor.h"
#include "Actors/EverdellDiscardPile.h"
#include "Actors/EverdellLocationGroup.h"
#include "Actors/EverdellWorkerLocation.h"
#include "Actors/EverdellForestLocationSlot.h"
#include "Actors/EverdellForestLocation.h"
#include "Actors/EverdellCardWorkerLocation.h"
#include "GameData/EverdellData.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

#include "Actors/EverdellWorkerBL1Berry.h"
#include "Actors/EverdellWorkerBL1Berry1Card.h"
#include "Actors/EverdellWorkerBL1Pebble.h"
#include "Actors/EverdellWorkerBL1Resin1Card.h"
#include "Actors/EverdellWorkerBL2Cards1Point.h"
#include "Actors/EverdellWorkerBL2Resin.h"
#include "Actors/EverdellWorkerBL2Twigs1Card.h"
#include "Actors/EverdellWorkerBL3Twigs.h"

AEverdellGameStateBase::AEverdellGameStateBase(): AGameStateBase()
{
	AIController = nullptr;
	DefaultPawn = nullptr;
	CurrentPlayerIndex = 0;
	Meadow = nullptr;
	Deck = nullptr;
}

void AEverdellGameStateBase::BeginPlay()
{
	for (auto& PlayerState : PlayerArray)
	{
		auto* TypedPlayerState = static_cast<AEverdellPlayerState*>(PlayerState.Get());
		TypedPlayerState->InitializeCityAndWorkers(TEXT("PC"), TEXT("PCAuxiliary"));
	}

	AIController = static_cast<AEverdellAIController*>(GetWorld()->SpawnActor(AEverdellAIController::StaticClass()));
	auto* TypedAIState = static_cast<AEverdellPlayerState*>(AIController->PlayerState);
	TypedAIState->InitializeCityAndWorkers(TEXT("AI"), TEXT("AIAuxiliary"));
	AddPlayerState(AIController->PlayerState);

	DefaultPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), CurrentPlayerIndex);

	Meadow = static_cast<AEverdellLocationGroup*>(GetWorld()->SpawnActor(AEverdellLocationGroup::StaticClass()));
	Meadow->LocationTag = TEXT("Meadow");
	Meadow->InitializeLocations();

	for (uint32 LocationIndex = 0; LocationIndex < static_cast<uint32>(EverdellForestLocationData.Num()); ++LocationIndex)
	{
		LocationIndexPool.Add(LocationIndex);
	}

	TArray<AActor*> TempLocationSlots;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEverdellForestLocationSlot::StaticClass(), TempLocationSlots);

	for (auto* Location : TempLocationSlots)
	{
		ForestLocationSlots.Add(static_cast<AEverdellForestLocationSlot*>(Location));
	}

	ForestLocationSlots.Sort([](const AEverdellForestLocationSlot& Actor1, const AEverdellForestLocationSlot& Actor2) {
		return Actor1.SlotIndex < Actor2.SlotIndex;
	});

	for (auto* Location : ForestLocationSlots)
	{
		uint32 RandomPoolIndex = UKismetMathLibrary::RandomIntegerInRange(0, LocationIndexPool.Num() - 1);
		uint32 RandomIndex = LocationIndexPool[RandomPoolIndex];
		LocationIndexPool.RemoveAt(RandomPoolIndex);

		FVector LocationPosition = Location->GetActorLocation();
		FRotator LocationRotation = Location->GetActorRotation();
		AEverdellForestLocation* NewLocation = static_cast<AEverdellForestLocation*>(GetWorld()->SpawnActor(AEverdellForestLocation::StaticClass(), &LocationPosition, &LocationRotation));
		NewLocation->BuildFromData(EverdellForestLocationData[RandomIndex]);
		Location->ForestLocation = NewLocation;
	}

	// Obtain all basic locations
	BasicLocations.Add(static_cast<AEverdellWorkerLocation*>(UGameplayStatics::GetActorOfClass(GetWorld(), AEverdellWorkerBL1Berry::StaticClass())));
	BasicLocations.Add(static_cast<AEverdellWorkerLocation*>(UGameplayStatics::GetActorOfClass(GetWorld(), AEverdellWorkerBL1Berry1Card::StaticClass())));
	BasicLocations.Add(static_cast<AEverdellWorkerLocation*>(UGameplayStatics::GetActorOfClass(GetWorld(), AEverdellWorkerBL1Pebble::StaticClass())));
	BasicLocations.Add(static_cast<AEverdellWorkerLocation*>(UGameplayStatics::GetActorOfClass(GetWorld(), AEverdellWorkerBL1Resin1Card::StaticClass())));
	BasicLocations.Add(static_cast<AEverdellWorkerLocation*>(UGameplayStatics::GetActorOfClass(GetWorld(), AEverdellWorkerBL2Cards1Point::StaticClass())));
	BasicLocations.Add(static_cast<AEverdellWorkerLocation*>(UGameplayStatics::GetActorOfClass(GetWorld(), AEverdellWorkerBL2Resin::StaticClass())));
	BasicLocations.Add(static_cast<AEverdellWorkerLocation*>(UGameplayStatics::GetActorOfClass(GetWorld(), AEverdellWorkerBL2Twigs1Card::StaticClass())));
	BasicLocations.Add(static_cast<AEverdellWorkerLocation*>(UGameplayStatics::GetActorOfClass(GetWorld(), AEverdellWorkerBL3Twigs::StaticClass())));

	// Get deck and discard pile data
	Deck = static_cast<AEverdellDeckActor*>(UGameplayStatics::GetActorOfClass(GetWorld(), AEverdellDeckActor::StaticClass()));
	DiscardPile = static_cast<AEverdellDiscardPile*>(UGameplayStatics::GetActorOfClass(GetWorld(), AEverdellDiscardPile::StaticClass()));
}

TArray<AEverdellWorkerLocation*> AEverdellGameStateBase::GetViableLocations() const
{
	TArray<AEverdellWorkerLocation*> ViableLocations;

	for (auto* ForestLocationSlot : ForestLocationSlots)
	{
		if (ForestLocationSlot->ForestLocation->IsInteractable()) ViableLocations.Add(ForestLocationSlot->ForestLocation);
	}

	for (auto* BasicLocation : BasicLocations)
	{
		if (BasicLocation->IsInteractable()) ViableLocations.Add(BasicLocation);
	}

	TArray<AActor*> TempCardLocationSlots;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEverdellCardWorkerLocation::StaticClass(), TempCardLocationSlots);

	for (auto* CardLocation : TempCardLocationSlots)
	{
		auto* TypedCardLocation = static_cast<AEverdellCardWorkerLocation*>(CardLocation);
		if (TypedCardLocation->IsInteractable()) ViableLocations.Add(TypedCardLocation);
	}

	return ViableLocations;
}

int32 AEverdellGameStateBase::GetPlayerCount() const
{
	return PlayerArray.Num();
}

AEverdellPlayerState* AEverdellGameStateBase::GetCurrentPlayer() const
{
	return static_cast<AEverdellPlayerState*>(PlayerArray[CurrentPlayerIndex]);
}

void AEverdellGameStateBase::EndTurn()
{
	PlayerArray[CurrentPlayerIndex]->GetOwningController()->UnPossess();
	++CurrentPlayerIndex;
	if (CurrentPlayerIndex >= PlayerArray.Num()) CurrentPlayerIndex = 0;
	PlayerArray[CurrentPlayerIndex]->GetOwningController()->Possess(DefaultPawn);
}
