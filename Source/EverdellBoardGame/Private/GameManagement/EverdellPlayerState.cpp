#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellBoardGame.h"
#include "Actors/EverdellPlayerCity.h"
#include "GameManagement/EverdellHUD.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "GameManagement/EverdellPlayerController.h"
#include "Actors/EverdellDeckActor.h"
#include "Actors/EverdellDiscardPile.h"
#include "UI/EverdellUserWidget.h"
#include "UI/EverdellUICard.h"
#include "GameData/EverdellCard.h"
#include "Actors/Everdell3DCard.h"
#include "GameData/EverdellCardLocationEnum.h"
#include "GameData/EverdellAtomicWrapper.h"
#include "Actors/EverdellCardLocationActor.h"
#include "Actors/EverdellForestLocationSlot.h"
#include "Actors/EverdellForestLocation.h"
#include "Actors/EverdellWorkerSlotGroup.h"
#include "Actors/EverdellWorkerSlot.h"
#include "Actors/EverdellWorker.h"
#include "Kismet/GameplayStatics.h"
#include "Components/TextBlock.h"
#include "Components/MenuAnchor.h"

AEverdellPlayerState::AEverdellPlayerState(): APlayerState()
{
	CardsInHand = 0;
	NextAvailableIndex = 0;

	Season = EEverdellSeason::LATE_WINTER;

	PlayerTag = TEXT("");
	PlayerAuxiliaryTag = TEXT("");
	TwigCount = 0;
	ResinCount = 0;
	PebbleCount = 0;
	BerryCount = 0;
	PointCount = 0;
	AvailableCardDraw = 0;
	AvailableMeadowCardDraw = 0;

	HUD = nullptr;
	GameState = nullptr;
}

void AEverdellPlayerState::BeginPlay()
{
	if (auto* Controller = GetOwningController(); Controller != nullptr && Controller->GetClass()->IsChildOf(AEverdellPlayerController::StaticClass()))
	{
		HUD = static_cast<AEverdellHUD*>(GetPlayerController()->GetHUD());
	}
	GameState = static_cast<AEverdellGameStateBase*>(UGameplayStatics::GetGameState(GetWorld()));
}

void AEverdellPlayerState::InitializeCityAndWorkers(const FName& NewPlayerTag, const FName& NewPlayerAuxiliaryTag)
{
	PlayerTag = NewPlayerTag;
	PlayerAuxiliaryTag = NewPlayerAuxiliaryTag;

	City = static_cast<AEverdellPlayerCity*>(GetWorld()->SpawnActor(AEverdellPlayerCity::StaticClass()));
	City->LocationTag = PlayerTag;
	City->LocationAuxiliaryTag = PlayerAuxiliaryTag;
	City->InitializeLocations();

	WorkerSlots = static_cast<AEverdellWorkerSlotGroup*>(GetWorld()->SpawnActor(AEverdellWorkerSlotGroup::StaticClass()));
	WorkerSlots->SlotTag = PlayerTag;
	WorkerSlots->InitializeSlots();
	WorkerSlots->InitializeWithWorkers(Workers);
}

void AEverdellPlayerState::InitializeUI(UEverdellUserWidget* MainUI)
{
	if (auto* Controller = GetOwningController(); Controller != nullptr && Controller->GetClass()->IsChildOf(AEverdellPlayerController::StaticClass()))
	{
		AEverdellGameStateBase* TempGameState = static_cast<AEverdellGameStateBase*>(UGameplayStatics::GetGameState(GetWorld()));
		AEverdellHUD* TempHUD = static_cast<AEverdellHUD*>(GetPlayerController()->GetHUD());

		MainUI->HandCard1->GameState = TempGameState;
		MainUI->HandCard1->HUD = TempHUD;
		Hand.Add(MainUI->HandCard1);

		MainUI->HandCard2->GameState = TempGameState;
		MainUI->HandCard2->HUD = TempHUD;
		Hand.Add(MainUI->HandCard2);

		MainUI->HandCard3->GameState = TempGameState;
		MainUI->HandCard3->HUD = TempHUD;
		Hand.Add(MainUI->HandCard3);

		MainUI->HandCard4->GameState = TempGameState;
		MainUI->HandCard4->HUD = TempHUD;
		Hand.Add(MainUI->HandCard4);

		MainUI->HandCard5->GameState = TempGameState;
		MainUI->HandCard5->HUD = TempHUD;
		Hand.Add(MainUI->HandCard5);

		MainUI->HandCard6->GameState = TempGameState;
		MainUI->HandCard6->HUD = TempHUD;
		Hand.Add(MainUI->HandCard6);

		MainUI->HandCard7->GameState = TempGameState;
		MainUI->HandCard7->HUD = TempHUD;
		Hand.Add(MainUI->HandCard7);

		MainUI->HandCard8->GameState = TempGameState;
		MainUI->HandCard8->HUD = TempHUD;
		Hand.Add(MainUI->HandCard8);

		for (int32 CardIndex = 0; CardIndex < 5; ++CardIndex)
		{
			TempGameState->Deck->FillUICard();
		}
	}
}

void AEverdellPlayerState::SetTwigCount(const int32 NewTwigCount)
{
	TwigCount = NewTwigCount;
	if (HUD != nullptr) HUD->Widget->TwigCount->SetText(FText::FromString(FString::FromInt(TwigCount)));
}

void AEverdellPlayerState::AugmentTwigCount(const int32 TwigCountDelta)
{
	TwigCount += TwigCountDelta;
	if (HUD != nullptr) HUD->Widget->TwigCount->SetText(FText::FromString(FString::FromInt(TwigCount)));
}

void AEverdellPlayerState::SetResinCount(const int32 NewResinCount)
{
	ResinCount = NewResinCount;
	if (HUD != nullptr) HUD->Widget->ResinCount->SetText(FText::FromString(FString::FromInt(ResinCount)));
	UpdateScore();
}

void AEverdellPlayerState::AugmentResinCount(const int32 ResinCountDelta)
{
	ResinCount += ResinCountDelta;
	if (HUD != nullptr) HUD->Widget->ResinCount->SetText(FText::FromString(FString::FromInt(ResinCount)));
	UpdateScore();
}

void AEverdellPlayerState::SetPebbleCount(const int32 NewPebbleCount)
{
	PebbleCount = NewPebbleCount;
	if (HUD != nullptr) HUD->Widget->PebbleCount->SetText(FText::FromString(FString::FromInt(PebbleCount)));
	UpdateScore();
}

void AEverdellPlayerState::AugmentPebbleCount(const int32 PebbleCountDelta)
{
	PebbleCount += PebbleCountDelta;
	if (HUD != nullptr) HUD->Widget->PebbleCount->SetText(FText::FromString(FString::FromInt(PebbleCount)));
	UpdateScore();
}

void AEverdellPlayerState::SetBerryCount(const int32 NewBerryCount)
{
	BerryCount = NewBerryCount;
	if (HUD != nullptr) HUD->Widget->BerryCount->SetText(FText::FromString(FString::FromInt(BerryCount)));
}

void AEverdellPlayerState::AugmentBerryCount(const int32 BerryCountDelta)
{
	BerryCount += BerryCountDelta;
	if (HUD != nullptr) HUD->Widget->BerryCount->SetText(FText::FromString(FString::FromInt(BerryCount)));
}

void AEverdellPlayerState::SetPointCount(const int32 NewPointCount)
{
	PointCount = NewPointCount;
	if (HUD != nullptr) HUD->Widget->PointCount->SetText(FText::FromString(FString::FromInt(PointCount)));
	UpdateScore();
}

void AEverdellPlayerState::AugmentPointCount(const int32 PointCountDelta)
{
	PointCount += PointCountDelta;
	if (HUD != nullptr) HUD->Widget->PointCount->SetText(FText::FromString(FString::FromInt(PointCount)));
	UpdateScore();
}

void AEverdellPlayerState::SetAvailableCardDraw(const int32 NewAvailableCardDraw)
{
	AvailableCardDraw = NewAvailableCardDraw;
	if (HUD != nullptr) HUD->Widget->AvailableCardDraw->SetText(FText::FromString(FString::FromInt(AvailableCardDraw + AvailableMeadowCardDraw)));
}

void AEverdellPlayerState::AugmentAvailableCardDraw(const int32 AvailableCardDrawDelta)
{
	AvailableCardDraw += AvailableCardDrawDelta;
	if (HUD != nullptr) HUD->Widget->AvailableCardDraw->SetText(FText::FromString(FString::FromInt(AvailableCardDraw + AvailableMeadowCardDraw)));
}

void AEverdellPlayerState::SetAvailableMeadowCardDraw(const int32 NewAvailableMeadowCardDraw)
{
	AvailableMeadowCardDraw = NewAvailableMeadowCardDraw;
	if (HUD != nullptr) HUD->Widget->AvailableCardDraw->SetText(FText::FromString(FString::FromInt(AvailableCardDraw + AvailableMeadowCardDraw)));
}

void AEverdellPlayerState::AugmentAvailableMeadowCardDraw(const int32 AvailableMeadowCardDrawDelta)
{
	AvailableMeadowCardDraw += AvailableMeadowCardDrawDelta;
	if (HUD != nullptr) HUD->Widget->AvailableCardDraw->SetText(FText::FromString(FString::FromInt(AvailableCardDraw + AvailableMeadowCardDraw)));
}

void AEverdellPlayerState::DecrementAvailableCardDraw(const bool bFromMeadow)
{
	if (bFromMeadow && AvailableMeadowCardDraw > 0) --AvailableMeadowCardDraw;
	else if (AvailableCardDraw > 0) --AvailableCardDraw;

	if (HUD != nullptr) HUD->Widget->AvailableCardDraw->SetText(FText::FromString(FString::FromInt(AvailableCardDraw + AvailableMeadowCardDraw)));
}

int32 AEverdellPlayerState::GetHandCardCount() const
{
	return CardsInHand;
}

int32 AEverdellPlayerState::GetHandCardLimit() const
{
	return Hand.Num();
}

bool AEverdellPlayerState::CanReceiveHandCards() const
{
	return CardsInHand < Hand.Num();
}

bool AEverdellPlayerState::SetHandCard(UEverdellCard* Card)
{
	if (CardsInHand < Hand.Num())
	{
		Hand[NextAvailableIndex]->SetCard(Card);
		Card->CardLocation = EEverdellCardLocation::HAND;
		Card->LocationLocalIndex = NextAvailableIndex;

		++CardsInHand;
		++NextAvailableIndex;

		while (NextAvailableIndex < Hand.Num())
		{
			if (!Hand[NextAvailableIndex]->HasCard()) break;
			++NextAvailableIndex;
		}

		return true;
	}

	return false;
}

UEverdellCard* AEverdellPlayerState::GetHandCard(const int32 Index) const
{
	if (Index >= 0 && Index < Hand.Num() && Hand[Index]->HasCard())
	{
		return Hand[Index]->GetCard();
	}

	return nullptr;
}

UEverdellCard* AEverdellPlayerState::ResetHandCard(const int32 Index)
{
	if (Index >= 0 && Index < Hand.Num() && Hand[Index]->HasCard())
	{
		UEverdellCard* Card = Hand[Index]->ResetCard();
		
		--CardsInHand;
		if (Index < NextAvailableIndex) NextAvailableIndex = Index;

		return Card;
	}

	return nullptr;
}

UEverdellCard* AEverdellPlayerState::DiscardHandCard(const int32 Index)
{
	UEverdellCard* OldCard = ResetHandCard(Index);

	if (OldCard != nullptr)
	{
		OldCard->CardLocation = EEverdellCardLocation::DISCARD_PILE;
		GameState->DiscardPile->PushCard(OldCard);
	}
	
	return OldCard;
}

int32 AEverdellPlayerState::GetWorkerCount() const
{
	return WorkerSlots->GetWorkerCount();
}

bool AEverdellPlayerState::HasAvailableWorker() const
{
	return WorkerSlots->HasAvailableWorker();
}

AEverdellWorker* AEverdellPlayerState::GetNextAvailableWorker() const
{
	auto* Worker = WorkerSlots->GetNextAvailableWorker();
	if (HUD != nullptr) HUD->Widget->WorkerCount->SetText(FText::FromString(FString::FromInt(GetWorkerCount())));
	return Worker;
}

bool AEverdellPlayerState::AddWorker(AEverdellWorker* Worker, bool bReturningWorker)
{
	if (WorkerSlots->AddWorker(Worker))
	{
		if (!bReturningWorker) Workers.Add(Worker);
		if (HUD != nullptr) HUD->Widget->WorkerCount->SetText(FText::FromString(FString::FromInt(GetWorkerCount())));
		return true;
	}

	return false;
}

TArray<AEverdellWorker*> AEverdellPlayerState::GetDeployedWorkers() const
{
	TArray<AEverdellWorker*> DeployedWorkers;

	for (auto* Worker : Workers)
	{
		if (Worker != nullptr && Worker->bIsDeployed) DeployedWorkers.Add(Worker);
	}

	return DeployedWorkers;
}

TArray<AEverdellWorkerLocation*> AEverdellPlayerState::GetValidLocations(AEverdellWorkerLocation* LocationToExclude) const
{
	TArray<AActor*> TempLocations;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEverdellWorkerLocation::StaticClass(), TempLocations);

	TArray<AEverdellWorkerLocation*> ValidLocations;
	for (auto* Location : TempLocations)
	{
		AEverdellWorkerLocation* WorkerLocation = static_cast<AEverdellWorkerLocation*>(Location);
		if (WorkerLocation != LocationToExclude && WorkerLocation->IsInteractable())
		{
			ValidLocations.Add(WorkerLocation);
		}
	}

	return ValidLocations;
}

int32 AEverdellPlayerState::GetCityCardCount() const
{
	return City->GetCardCount();
}

bool AEverdellPlayerState::CanPlaceCityCards() const
{
	return City->CanPlaceCards();
}

bool AEverdellPlayerState::SetCityCard(AEverdell3DCard* CardActor)
{
	return City->SetCard(CardActor);
}

AEverdell3DCard* AEverdellPlayerState::GetCityCard(const int32 Index) const
{
	return City->GetCard(Index);
}

TArray<AEverdell3DCard*> AEverdellPlayerState::GetCityCardsOfType(const EEverdellCardType CardType) const
{
	return City->GetCardsOfType(CardType);
}

AEverdellCardLocationActor* AEverdellPlayerState::GetCityCardLocation(const int32 Index) const
{
	return City->GetCardLocation(Index);
}

UEverdellCard* AEverdellPlayerState::DiscardCityCard(const int32 Index, UEverdellCard* DiscardToCard)
{
	if (Index < GetCityCardCount())
	{
		AEverdell3DCard* Old3DCard = City->ResetCard(Index);
		if (Old3DCard != nullptr)
		{
			UEverdellCard* OldCard = Old3DCard->ResetCard();

			// Remove associated delegates if relevant
			const auto CardIndentifier = TPair<EEverdellCardType, int32>(OldCard->CardType, OldCard->LocationLocalIndex);
			if (BeforeCritterPaymentEventDelegates.Contains(CardIndentifier))
			{
				BeforeCritterPaymentEventDelegates.FindAndRemoveChecked(CardIndentifier);
			}
			if (BeforeConstructionPaymentEventDelegates.Contains(CardIndentifier))
			{
				BeforeConstructionPaymentEventDelegates.FindAndRemoveChecked(CardIndentifier);
			}
			if (AfterCritterEventDelegates.Contains(CardIndentifier))
			{
				AfterCritterEffect.Remove(AfterCritterEventDelegates.FindAndRemoveChecked(CardIndentifier));
			}
			if (AfterConstructionEventDelegates.Contains(CardIndentifier))
			{
				AfterConstructionEffect.Remove(AfterConstructionEventDelegates.FindAndRemoveChecked(CardIndentifier));
			}
			if (CheckIfPlayPossibleEventDelegates.Contains(CardIndentifier))
			{
				CheckIfPlayPossibleEffect.Remove(CheckIfPlayPossibleEventDelegates.FindAndRemoveChecked(CardIndentifier));
			}

			// Once discarded, free upgrades reset
			Old3DCard->SetFreeUpgradeUsed(false);

			if (DiscardToCard == nullptr)
			{
				// Initialize position before shifting
				Old3DCard->SetActorRelativeLocation(FVector(0, 0, 50));
				Old3DCard->SetActorRotation(FRotator(180, -90, 0));
				Old3DCard->SetActorRelativeScale3D(FVector(2, 2.75, 2));

				// Move to space above discard pile
				FVector DiscardPileLocation = GameState->DiscardPile->GetActorLocation();
				DiscardPileLocation.Z += 50;

				// Start movement
				Old3DCard->MoveToLocationWithRotation(std::move(DiscardPileLocation), Old3DCard->GetActorRotation(), true);

				if (OldCard != nullptr)
				{
					while (!OldCard->ContainedCards.IsEmpty())
					{
						auto* ContainedCard = OldCard->ContainedCards.Pop();
						ContainedCard->CardLocation = EEverdellCardLocation::DISCARD_PILE;
						GameState->DiscardPile->PushCard(ContainedCard);
					}

					OldCard->CardLocation = EEverdellCardLocation::DISCARD_PILE;
					GameState->DiscardPile->PushCard(OldCard);
				}
			}
			else if (OldCard != nullptr)
			{
				while (!OldCard->ContainedCards.IsEmpty())
				{
					auto* ContainedCard = OldCard->ContainedCards.Pop();
					ContainedCard->CardLocation = EEverdellCardLocation::CONTAINED_IN_CARD;
					DiscardToCard->ContainedCards.Add(ContainedCard);
				}

				OldCard->CardLocation = EEverdellCardLocation::CONTAINED_IN_CARD;
				DiscardToCard->ContainedCards.Add(OldCard);
			}

			if (OldCard != nullptr && OldCard->CanPerformDiscardEffect.IsBound() && OldCard->PerformDiscardEffect.IsBound() && OldCard->CanPerformDiscardEffect.Execute(GameState, HUD, nullptr, OldCard))
			{
				OldCard->PerformDiscardEffect.Execute(GameState, HUD, nullptr, OldCard);
			}

			UpdateScore();

			return OldCard;
		}
	}
	
	return nullptr;
}

int32 AEverdellPlayerState::CountOfCardInCity(const EEverdellCardType CardType) const
{
	return City->CountOfCard(CardType);
}

int32 AEverdellPlayerState::CountOfCardCategoryInCity(const EEverdellCardCategory CardCategory) const
{
	return City->CountOfCardCategory(CardCategory);
}

int32 AEverdellPlayerState::CountOfCrittersInCity() const
{
	return City->CountOfCritters();
}

int32 AEverdellPlayerState::CountOfConstructionsInCity() const
{
	return City->CountOfConstructions();
}

int32 AEverdellPlayerState::CountOfActivatableGreenProductionCardsInCity() const
{
	return City->CountOfActivatableGreenProductionCards(GameState, HUD);
}

void AEverdellPlayerState::MoveToCityPayingCost(UEverdellCard* Card, const bool bHandlePrePlayEffect, const bool bHandleBeforePaymentEffects)
{
	if (bHandlePrePlayEffect && Card->CanPerformPrePlayEffect.IsBound() && Card->PerformPrePlayEffect.IsBound())
	{
		if (Card->CanPerformPrePlayEffect.Execute(GameState, HUD, nullptr, Card))
		{
			Card->PerformPrePlayEffect.Execute(GameState, HUD, nullptr, Card);
		}
	}
	else
	{
		if (bHandleBeforePaymentEffects && Card->HasNonZeroCost())
		{
			TArray<TPair<EEverdellCardType, int32>> CardIDPairArray;
			if (Card->bIsCritter && !BeforeCritterPaymentEventDelegates.IsEmpty())
			{
				// Get all critter delegates for processing
				BeforeCritterPaymentEventDelegates.GenerateKeyArray(CardIDPairArray);
			}
			else if (!Card->bIsCritter && !BeforeConstructionPaymentEventDelegates.IsEmpty())
			{
				// Get all construction delegates for processing
				BeforeConstructionPaymentEventDelegates.GenerateKeyArray(CardIDPairArray);
			}

			if (!CardIDPairArray.IsEmpty())
			{
				// Collect all delegates that can be performed
				TArray<TPair<UEverdellCard*, FEverdellPerformBeforePaymentEffectSignature>> ValidDelegateArray;
				for (auto& CardIDPair : CardIDPairArray)
				{
					const TPair<FEverdellCanPerformBeforePaymentEffectSignature, FEverdellPerformBeforePaymentEffectSignature>& DelegatePair = (
						Card->bIsCritter ? BeforeCritterPaymentEventDelegates[CardIDPair] : BeforeConstructionPaymentEventDelegates[CardIDPair]
					);
					if (DelegatePair.Key.IsBound() && DelegatePair.Value.IsBound() && DelegatePair.Key.Execute(GameState, HUD, Card, City->GetCard(CardIDPair.Value)->GetCard()))
					{
						ValidDelegateArray.Add(TPair<UEverdellCard*, FEverdellPerformBeforePaymentEffectSignature>(City->GetCard(CardIDPair.Value)->GetCard(), DelegatePair.Value));
					}
				}

				// Only open decision popup if 2 or more registered
				if (ValidDelegateArray.Num() == 1)
				{
					ValidDelegateArray[0].Value.Execute(GameState, HUD, Card, ValidDelegateArray[0].Key);
					return;
				}
				else if (ValidDelegateArray.Num() > 1)
				{
					if (HUD != nullptr) HUD->OpenChooseAbilityPopup(Card, ValidDelegateArray);
					return;
				}
			}
		}

		SetTwigCount(TwigCount - Card->TwigCost);
		SetResinCount(ResinCount - Card->ResinCost);
		SetPebbleCount(PebbleCount - Card->PebbleCost);
		SetBerryCount(BerryCount - Card->BerryCost);

		DisambiguateAndMoveCard(Card);
	}
}

void AEverdellPlayerState::MoveToCityPayingExplicitCost(UEverdellCard* Card, const int32 TwigCost, const int32 ResinCost, const int32 PebbleCost, const int32 BerryCost)
{
	SetTwigCount(TwigCount - TwigCost);
	SetResinCount(ResinCount - ResinCost);
	SetPebbleCount(PebbleCount - PebbleCost);
	SetBerryCount(BerryCount - BerryCost);

	DisambiguateAndMoveCard(Card);
}

void AEverdellPlayerState::MoveToCityWithFreeUpgrade(UEverdellCard* Card, AEverdell3DCard* FreeUpgradeCard)
{
	FreeUpgradeCard->SetFreeUpgradeUsed(true);

	DisambiguateAndMoveCard(Card);
}

void AEverdellPlayerState::DisambiguateAndMoveCard(UEverdellCard* Card)
{
	AEverdellCardLocationActor* FoundLocation = nullptr;

	switch (Card->CardType)
	{
	case EEverdellCardType::HUSBAND:
		FoundLocation = City->GetNextAvailableCard(EEverdellCardType::WIFE);
		if (FoundLocation != nullptr)
		{
			if (HUD != nullptr) HUD->OpenHusbandWifePopup(Card, FoundLocation);
		}
		else MoveToCity(Card);
		break;
	case EEverdellCardType::WIFE:
		FoundLocation = City->GetNextAvailableCard(EEverdellCardType::HUSBAND);
		if (FoundLocation != nullptr)
		{
			if (HUD != nullptr) HUD->OpenHusbandWifePopup(Card, FoundLocation);
		}
		else MoveToCity(Card);
		break;
	case EEverdellCardType::SCURRBLE_CHAMPION:
		FoundLocation = City->GetNextAvailableCard(EEverdellCardType::SCURRBLE_CHAMPION);
		if (FoundLocation != nullptr) PairCardInCity(Card, FoundLocation);
		else MoveToCity(Card);
		break;
	default:
		MoveToCity(Card);
		break;
	}
}

void AEverdellPlayerState::MoveToCity(UEverdellCard* Card)
{
	AEverdellCardLocationActor* NextAvailableCardLocation = nullptr;
	if (Card->CardLocation == EEverdellCardLocation::HAND || Card->CardLocation == EEverdellCardLocation::NOT_GENERATED || Card->CardLocation == EEverdellCardLocation::DISCARD_PILE)
	{
		if (Card->CardLocation == EEverdellCardLocation::HAND) ResetHandCard(Card->LocationLocalIndex);

		Card->CardLocation = EEverdellCardLocation::CITY;
		Card->LocationLocalIndex = City->GetNextAvailableLocationIndex(Card->bRequiresSpace);

		NextAvailableCardLocation = City->GetNextAvailableLocation(Card->bRequiresSpace);
		FVector NewCardSpawnLocation = City->GetNextAvailablePlacementLocation(Card->bRequiresSpace);
		FRotator NewCardSpawnRotation = FRotator(0, -45, 0);
		AEverdell3DCard* New3DCard = static_cast<AEverdell3DCard*>(GetWorld()->SpawnActor(AEverdell3DCard::StaticClass(), &NewCardSpawnLocation, &NewCardSpawnRotation));
		New3DCard->SetCard(Card);
		New3DCard->SetActorRelativeScale3D(FVector(2.5, 2.5, 2.5));

		SetCityCard(New3DCard);
	}
	else if (Card->CardLocation == EEverdellCardLocation::MEADOW)
	{
		AEverdell3DCard* MeadowCard = GameState->Meadow->ResetCard(Card->LocationLocalIndex);
		MeadowCard->SetActorRelativeScale3D(FVector(2.5, 2.5, 2.5));

		Card->CardLocation = EEverdellCardLocation::CITY;
		Card->LocationLocalIndex = City->GetNextAvailableLocationIndex(Card->bRequiresSpace);

		NextAvailableCardLocation = City->GetNextAvailableLocation(Card->bRequiresSpace);
		FVector MeadowCardMoveLocation = City->GetNextAvailablePlacementLocation(Card->bRequiresSpace);
		FRotator MeadowCardMoveRotation = FRotator(0, -45, 0);
		MeadowCard->MoveToLocationWithRotation(std::move(MeadowCardMoveLocation), std::move(MeadowCardMoveRotation));

		SetCityCard(MeadowCard);
	}
	else return;

	if (Card->CanPerformOnPlayEffect.IsBound() && Card->PerformOnPlayEffect.IsBound() && Card->CanPerformOnPlayEffect.Execute(GameState, HUD, NextAvailableCardLocation, Card))
	{
		Card->PerformOnPlayEffect.Execute(GameState, HUD, NextAvailableCardLocation, Card);
	}

	if (Card->bIsCritter)
	{
		AfterCritterEffect.Broadcast(GameState, HUD);
	}
	else
	{
		AfterConstructionEffect.Broadcast(GameState, HUD);
	}

	if (Card->CanPerformTriggeredEffect.IsBound() && Card->PerformTriggeredEffect.IsBound() && Card->CanPerformTriggeredEffect.Execute(GameState, HUD, NextAvailableCardLocation, Card))
	{
		Card->PerformTriggeredEffect.Execute(GameState, HUD, NextAvailableCardLocation, Card);
	}

	UpdateScore();
}

void AEverdellPlayerState::PairCardInCity(UEverdellCard* Card, AEverdellCardLocationActor* PairLocation)
{
	if (Card->CardLocation == EEverdellCardLocation::HAND || Card->CardLocation == EEverdellCardLocation::NOT_GENERATED || Card->CardLocation == EEverdellCardLocation::DISCARD_PILE)
	{
		if (Card->CardLocation == EEverdellCardLocation::HAND) ResetHandCard(Card->LocationLocalIndex);

		Card->CardLocation = EEverdellCardLocation::CITY;
		Card->LocationLocalIndex = PairLocation->LocationIndex;

		FVector NewCardSpawnLocation = PairLocation->GetNextSecondaryCardPlacementLocation();
		FRotator NewCardSpawnRotation = FRotator(0, -45, 0);
		AEverdell3DCard* New3DCard = static_cast<AEverdell3DCard*>(GetWorld()->SpawnActor(AEverdell3DCard::StaticClass(), &NewCardSpawnLocation, &NewCardSpawnRotation));
		New3DCard->SetCard(Card);
		New3DCard->SetActorRelativeScale3D(FVector(2.5, 2.5, 2.5));

		PairLocation->AddSecondaryCard(New3DCard);
	}
	else if (Card->CardLocation == EEverdellCardLocation::MEADOW)
	{
		AEverdell3DCard* MeadowCard = GameState->Meadow->ResetCard(Card->LocationLocalIndex);
		MeadowCard->SetActorRelativeScale3D(FVector(2.5, 2.5, 2.5));

		Card->CardLocation = EEverdellCardLocation::CITY;
		Card->LocationLocalIndex = PairLocation->LocationIndex;

		FVector MeadowCardMoveLocation = PairLocation->GetNextSecondaryCardPlacementLocation();
		FRotator MeadowCardMoveRotation = FRotator(0, -45, 0);
		MeadowCard->MoveToLocationWithRotation(std::move(MeadowCardMoveLocation), std::move(MeadowCardMoveRotation));

		PairLocation->AddSecondaryCard(MeadowCard);
	}
	else return;

	if (Card->CanPerformOnPlayEffect.IsBound() && Card->PerformOnPlayEffect.IsBound() && Card->CanPerformOnPlayEffect.Execute(GameState, HUD, PairLocation, Card))
	{
		Card->PerformOnPlayEffect.Execute(GameState, HUD, PairLocation, Card);
	}

	AfterCritterEffect.Broadcast(GameState, HUD);

	UpdateScore();
}

void AEverdellPlayerState::UpdateScore()
{
	if (HUD != nullptr) HUD->Widget->CurrentScore->SetText(FText::FromString(FString::FromInt(City->GetCityScore(this, GameState, HUD) + PointCount)));
}
