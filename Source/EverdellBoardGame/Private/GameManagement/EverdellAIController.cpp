#include "GameManagement/EverdellAIController.h"

AEverdellAIController::AEverdellAIController() : AAIController()
{
	bWantsPlayerState = true;

	Tags.Add(TEXT("AI"));
	Tags.Add(TEXT("AIAuxiliary"));
}
