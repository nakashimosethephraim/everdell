#include "GameManagement/EverdellPlayerController.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellHUD.h"
#include "Actors/EverdellCameraActor.h"
#include "Kismet/GameplayStatics.h"

AEverdellPlayerController::AEverdellPlayerController(): APlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;
	CameraBlendTime = 0.25f;

	CameraTags.Add(TEXT("City"));
	CameraTags.Add(TEXT("Meadow"));
	CameraTags.Add(TEXT("OpponentCity"));
	CameraTags.Add(TEXT("ForestLocationsL"));
	CameraTags.Add(TEXT("ForestLocationsR"));
	CameraTags.Add(TEXT("BasicLocationsL"));
	CameraTags.Add(TEXT("DeckAndBasicLocationsC"));
	CameraTags.Add(TEXT("BasicLocationsR"));
	CameraTags.Add(TEXT("EverTree"));
	CameraTags.Add(TEXT("Journey"));
	CameraTags.Add(TEXT("Haven"));
}

void AEverdellPlayerController::BeginPlay()
{
	Super::BeginPlay();

	GetPawn()->SetActorHiddenInGame(true);

	for (auto& CameraName : CameraTags)
	{
		TArray<AActor*> FoundCameras;
		UGameplayStatics::GetAllActorsOfClassWithTag(GetWorld(), AEverdellCameraActor::StaticClass(), CameraName, FoundCameras);
		if (!FoundCameras.IsEmpty()) Cameras.Add(FoundCameras[0]);
	}

	if (!Cameras.IsEmpty()) SetViewTarget(Cameras[0]);
}

void AEverdellPlayerController::HandleCameraChange_Implementation(int CameraIndex)
{
	SetViewTargetWithBlend(Cameras[CameraIndex], CameraBlendTime, EViewTargetBlendFunction::VTBlend_Linear);
}

void AEverdellPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
}

void AEverdellPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
}
