#include "Actors/EverdellPlayerCity.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "GameManagement/EverdellHUD.h"
#include "Actors/EverdellDeckActor.h"
#include "Actors/EverdellCardLocationActor.h"
#include "Actors/Everdell3DCard.h"
#include "GameData/EverdellCard.h"
#include "Kismet/GameplayStatics.h"
#include "Async/Future.h"

int32 AEverdellPlayerCity::GetNextAvailableLocationIndex(bool bRequiresSpace) const
{
	return bRequiresSpace ? Super::GetNextAvailableLocationIndex(bRequiresSpace) : 0;
}

AEverdellCardLocationActor* AEverdellPlayerCity::GetNextAvailableLocation(bool bRequiresSpace) const
{
	return bRequiresSpace ? Super::GetNextAvailableLocation(bRequiresSpace) : AuxiliaryCardLocation;
}

FVector AEverdellPlayerCity::GetNextAvailablePlacementLocation(bool bRequiresSpace) const
{
	return bRequiresSpace ? Super::GetNextAvailablePlacementLocation(bRequiresSpace) : AuxiliaryCardLocation->GetNextCardPlacementLocation();
}

TArray<AEverdell3DCard*> AEverdellPlayerCity::GetUpgradeConstruction(EEverdellCardType CardType) const
{
	TArray<AEverdell3DCard*> FoundCards;
	for (auto* Location : Locations)
	{
		if (Location->HasCard() && Location->GetCard()->HasCard() && !Location->GetCard()->GetCard()->bFreeUpgradeUsed && (Location->GetCard()->GetCard()->FreeUpgradeOptions.Find(CardType) != INDEX_NONE || Location->GetCard()->GetCard()->FreeUpgradeOptions.Find(EEverdellCardType::ANY) != INDEX_NONE))
		{
			FoundCards.Add(Location->GetCard());
		}
	}

	return FoundCards;
}

AEverdellCardLocationActor* AEverdellPlayerCity::GetNextAvailableCard(EEverdellCardType CardType) const
{
	for (auto* Location : Locations)
	{
		if (Location->HasCard() && Location->GetCard()->HasCard() && Location->GetCard()->GetCard()->CardType == CardType && !Location->HasSecondaryCards())
		{
			return Location;
		}
	}

	return nullptr;
}

int32 AEverdellPlayerCity::GetCityScore(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD) const
{
	TArray<TFuture<int32>> LocationScores;
	for (auto* Location : Locations)
	{
		LocationScores.Add(Async(EAsyncExecution::ThreadPool, [=]()
			{
				return Location->TotalCardsScore(PlayerState, GameState, HUD);
			}
		));
	}

	LocationScores.Add(Async(EAsyncExecution::ThreadPool, [=]()
		{
			return AuxiliaryCardLocation->TotalCardsScore(PlayerState, GameState, HUD);
		}
	));

	int32 CityScore = 0;
	for (auto& LocationScore : LocationScores)
	{
		LocationScore.Wait();
		CityScore += LocationScore.Get();
	}	

	return CityScore;
}

void AEverdellPlayerCity::InitializeLocations()
{
	Super::InitializeLocations();

	TArray<AActor*> TempLocations;
	UGameplayStatics::GetAllActorsOfClassWithTag(GetWorld(), AEverdellCardLocationActor::StaticClass(), LocationAuxiliaryTag, TempLocations);

	if (!TempLocations.IsEmpty())
	{
		AuxiliaryCardLocation = static_cast<AEverdellCardLocationActor*>(TempLocations[0]);
	}
}

bool AEverdellPlayerCity::SetCard(AEverdell3DCard* CardActor)
{
	if (CardActor->HasCard() && !CardActor->GetCard()->bRequiresSpace)
	{
		AuxiliaryCardLocation->SetCardGeneric(CardActor);
		return true;
	}
	else
	{
		return Super::SetCard(CardActor);
	}
}

int32 AEverdellPlayerCity::GetCardCount() const
{
	return Super::GetCardCount() + AuxiliaryCardLocation->GetTotalCardCount();
}

int32 AEverdellPlayerCity::CountOfCard(EEverdellCardType CardType) const
{
	return Super::CountOfCard(CardType) + AuxiliaryCardLocation->TotalCountCardsOfType(CardType);
}

int32 AEverdellPlayerCity::CountOfCardCategory(EEverdellCardCategory CardCategory) const
{
	return Super::CountOfCardCategory(CardCategory) + AuxiliaryCardLocation->TotalCountCardsOfCategory(CardCategory);
}

int32 AEverdellPlayerCity::CountOfCritters() const
{
	return Super::CountOfCritters() + AuxiliaryCardLocation->TotalCountCritters();
}

int32 AEverdellPlayerCity::CountOfConstructions() const
{
	return Super::CountOfConstructions() + AuxiliaryCardLocation->TotalCountConstructions();
}

int32 AEverdellPlayerCity::CountOfCardWithProperties(bool bIsCritter, bool bIsUnique) const
{
	return Super::CountOfCardWithProperties(bIsCritter, bIsUnique) + AuxiliaryCardLocation->TotalCountSecondaryCardsWithProperties(bIsCritter, bIsUnique);
}
