#include "Actors/EverdellWorkerBL1Berry1Card.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameData/EverdellLocationEffects.h"

AEverdellWorkerBL1Berry1Card::AEverdellWorkerBL1Berry1Card() : AEverdellWorkerLocation()
{
	bIsOpenLocation = false;
	CanPerformEffect.BindLambda(&UEverdellLocationEffects::CanPerformEffectDefault);
	PerformEffect.BindLambda(&UEverdellLocationEffects::PerformEffectBasicLocation1BerryDraw1Card);
}
