#include "Actors/EverdellWorkerBL2Cards1Point.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameData/EverdellLocationEffects.h"

AEverdellWorkerBL2Cards1Point::AEverdellWorkerBL2Cards1Point() : AEverdellWorkerLocation()
{
	bIsOpenLocation = true;
	CanPerformEffect.BindLambda(&UEverdellLocationEffects::CanPerformEffectDefault);
	PerformEffect.BindLambda(&UEverdellLocationEffects::PerformEffectBasicLocationDraw2Cards1Point);
}
