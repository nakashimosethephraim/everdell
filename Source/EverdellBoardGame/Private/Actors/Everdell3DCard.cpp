#include "Actors/Everdell3DCard.h"
#include "GameManagement/EverdellBoardGame.h"
#include "Components/StaticMeshComponent.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "GameManagement/EverdellHUD.h"
#include "UI/EverdellUserWidget.h"
#include "Actors/EverdellCardWorkerLocation.h"
#include "GameData/EverdellCard.h"
#include "Kismet/GameplayStatics.h"
#include "Math/UnrealMathUtility.h"

AEverdell3DCard::AEverdell3DCard(): AActor()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> BaseMeshPlaneAsset(TEXT("/Engine/BasicShapes/Plane"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> BaseMeshCylinderAsset(TEXT("/Engine/BasicShapes/Cylinder"));
	static ConstructorHelpers::FObjectFinder<UMaterial> BackMaterialAsset(TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardBack_Mat.EverdellPlayableCardBack_Mat'"));
	static ConstructorHelpers::FObjectFinder<UMaterial> UpgradeTokenMaterialAsset(TEXT("Material'/Game/EverdellAssets/EverdellUsedToken_Mat.EverdellUsedToken_Mat'"));
	if (BaseMeshPlaneAsset.Succeeded() && BaseMeshCylinderAsset.Succeeded() && BackMaterialAsset.Succeeded() && UpgradeTokenMaterialAsset.Succeeded()) {
		CardFront = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CardFront"));
		CardFront->SetStaticMesh(BaseMeshPlaneAsset.Object);
		CardFront->OnBeginCursorOver.AddDynamic(this, &AEverdell3DCard::OnBeginMouseOver);
		CardFront->OnEndCursorOver.AddDynamic(this, &AEverdell3DCard::OnEndMouseOver);
		CardFront->OnClicked.AddDynamic(this, &AEverdell3DCard::OnMouseClicked);
		RootComponent = CardFront;

		CardBack = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CardBack"));
		CardBack->SetStaticMesh(BaseMeshPlaneAsset.Object);
		CardBack->SetupAttachment(RootComponent);
		CardBack->SetRelativeRotation(FRotator(180, 0, 0));
		CardBack->SetCastShadow(false);
		BackMaterial = BackMaterialAsset.Object;
		CardBack->SetMaterial(0, BackMaterial);

		UpgradeMarker = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("UpgradeMarker"));
		UpgradeMarker->SetStaticMesh(BaseMeshCylinderAsset.Object);
		UpgradeMarker->SetupAttachment(RootComponent);
		UpgradeMarker->SetRelativeLocation(FVector(41, 43, 0));
		UpgradeMarker->SetRelativeScale3D(FVector(0.135, 0.135, 0.02));
		UpgradeMarker->SetCastShadow(false);
		UpgradeTokenMaterial = UpgradeTokenMaterialAsset.Object;
		UpgradeMarker->SetMaterial(0, UpgradeTokenMaterial);
		UpgradeMarker->SetVisibility(false);
	}

	LocationScale = FVector(0.5, 0.5, 0.5);
	CenterLocationRelativePosition = FVector(2, 0, 0.1);
	FirstLocationRelativePosition = FVector(-16, 0, 0.1);
	SecondLocationRelativePosition = FVector(18, 0, 0.1);

	FrontMaterial = nullptr;

	GameState = nullptr;
	HUD = nullptr;
	Card = nullptr;
	bIsClicked = false;
	bIsHovered = false;
	bIsMoving = false;
	bShouldDestroyOnArrival = false;

	PrimaryActorTick.bCanEverTick = true;
}

void AEverdell3DCard::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickEnabled(false);

	GameState = static_cast<AEverdellGameStateBase*>(UGameplayStatics::GetGameState(GetWorld()));
	HUD = static_cast<AEverdellHUD*>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());

	FVector SpawnLocation = GetActorLocation();
	FRotator SpawnRotation = GetActorRotation();
	const FAttachmentTransformRules AttachmentRules(EAttachmentRule::KeepRelative, true);

	FirstLocation = static_cast<AEverdellCardWorkerLocation*>(GetWorld()->SpawnActor(AEverdellCardWorkerLocation::StaticClass(), &SpawnLocation, &SpawnRotation));
	FirstLocation->AttachToActor(this, AttachmentRules);
	FirstLocation->bIsSecondLocation = false;
	FirstLocation->SetActorScale3D(LocationScale);
	FirstLocation->SetActorEnableCollision(false);

	SecondLocation = static_cast<AEverdellCardWorkerLocation*>(GetWorld()->SpawnActor(AEverdellCardWorkerLocation::StaticClass(), &SpawnLocation, &SpawnRotation));
	SecondLocation->AttachToActor(this, AttachmentRules);
	SecondLocation->bIsSecondLocation = true;
	SecondLocation->SetActorScale3D(LocationScale);
	SecondLocation->SetActorEnableCollision(false);
}

void AEverdell3DCard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsMoving)
	{
		if (!TargetLocation.Equals(GetActorLocation(), 0.3) || !TargetRotation.Equals(GetActorRotation(), 0.3))
		{
			SetActorLocationAndRotation(
				FMath::VInterpTo(GetActorLocation(), TargetLocation, DeltaTime, 2.f),
				FMath::RInterpTo(GetActorRotation(), TargetRotation, DeltaTime, 2.f)
			);
		}
		else
		{
			bIsMoving = false;
			SetActorTickEnabled(false);

			if (bShouldDestroyOnArrival)
			{
				ResetCard();
				Destroy();
			}
			else if (Card != nullptr)
			{
				if (Card->LocationCount > 1)
				{
					FirstLocation->SetActorEnableCollision(Card->IsInCity());
					SecondLocation->SetActorEnableCollision(Card->IsInCity());
				}
				else if (Card->LocationCount == 1)
				{
					FirstLocation->SetActorEnableCollision(Card->IsInCity());
				}
			}
		}
	}
}

bool AEverdell3DCard::HasCard() const { return Card != nullptr; }

UEverdellCard* AEverdell3DCard::GetCard() const { return Card; }

void AEverdell3DCard::SetCard(UEverdellCard* NewCard)
{
	Card = NewCard;
	FrontMaterial = (UMaterial*)StaticLoadObject(UMaterial::StaticClass(), nullptr, *Card->CardMaterialResourcePath.ToString());
	CardFront->SetMaterial(0, FrontMaterial);

	if (Card->LocationCount > 1)
	{
		FirstLocation->SetCard(Card);
		FirstLocationRelativePosition.Y = Card->LocationYValue;
		FirstLocation->SetActorRelativeLocation(FirstLocationRelativePosition);
		FirstLocation->SetActorEnableCollision(Card->IsInCity());

		SecondLocation->SetCard(Card);
		SecondLocationRelativePosition.Y = Card->LocationYValue;
		SecondLocation->SetActorRelativeLocation(SecondLocationRelativePosition);
		SecondLocation->SetActorEnableCollision(Card->IsInCity());
	}
	else if (Card->LocationCount == 1)
	{
		FirstLocation->SetCard(Card);
		CenterLocationRelativePosition.Y = Card->LocationYValue;
		FirstLocation->SetActorRelativeLocation(CenterLocationRelativePosition);
		FirstLocation->SetActorEnableCollision(Card->IsInCity());
	}
}

UEverdellCard* AEverdell3DCard::ResetCard()
{
	FrontMaterial = nullptr;

	UEverdellCard* CardToReturn = nullptr;
	if (Card != nullptr)
	{
		if (Card->LocationCount > 1)
		{
			FirstLocation->ResetCard();
			FirstLocation->SetActorRelativeLocation(-FirstLocationRelativePosition);
			FirstLocationRelativePosition.Y = 0;
			FirstLocation->SetActorEnableCollision(false);

			SecondLocation->ResetCard();
			SecondLocation->SetActorRelativeLocation(-SecondLocationRelativePosition);
			SecondLocationRelativePosition.Y = 0;
			SecondLocation->SetActorEnableCollision(false);
		}
		else if (Card->LocationCount == 1)
		{
			FirstLocation->ResetCard();
			FirstLocation->SetActorRelativeLocation(-CenterLocationRelativePosition);
			CenterLocationRelativePosition.Y = 0;
			FirstLocation->SetActorEnableCollision(false);
		}

		CardToReturn = Card;
		Card = nullptr;
	}

	return CardToReturn;
}

void AEverdell3DCard::SetFreeUpgradeUsed(const bool bFreeUpgradeUsed)
{
	UpgradeMarker->SetVisibility(bFreeUpgradeUsed);
	if (Card != nullptr) Card->bFreeUpgradeUsed = bFreeUpgradeUsed;
}

void AEverdell3DCard::MoveToLocationWithRotation(FVector&& Location, FRotator&& Rotation, bool bDestroyOnArrival)
{
	TargetLocation = std::move(Location);
	TargetRotation = std::move(Rotation);

	SetActorTickEnabled(true);
	bIsMoving = true;
	bShouldDestroyOnArrival = bDestroyOnArrival;
}

void AEverdell3DCard::OnBeginMouseOver(UPrimitiveComponent* TouchedComponent)
{
	if (HasCard() && (GetCard()->CardLocation == EEverdellCardLocation::CITY || GetCard()->CardLocation == EEverdellCardLocation::OPPONENT_CITY) && GetCard()->HasContainedResources())
	{
		bIsHovered = true;
		HUD->StartHover(GetCard());
	}
}

void AEverdell3DCard::OnEndMouseOver(UPrimitiveComponent* TouchedComponent)
{
	if (bIsHovered)
	{
		HUD->EndHover();
	}
}

void AEverdell3DCard::OnMouseClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed)
{
	if (!bIsClicked && Card != nullptr && Card->CanInteract(GameState, HUD))
	{
		bIsClicked = true;
		Card->Interact(GameState, HUD);
	}
	bIsClicked = false;
}
