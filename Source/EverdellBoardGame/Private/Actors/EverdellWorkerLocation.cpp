#include "Actors/EverdellWorkerLocation.h"
#include "GameManagement/EverdellBoardGame.h"
#include "Actors/EverdellWorker.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "GameManagement/EverdellHUD.h"
#include "Kismet/GameplayStatics.h"

AEverdellWorkerLocation::AEverdellWorkerLocation() : AEverdellLocation()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMeshAsset(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube'"));
	if (CubeMeshAsset.Succeeded())
	{
		Collision = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Collision"));
		Collision->SetStaticMesh(CubeMeshAsset.Object);
		Collision->SetVisibleFlag(false);
		RootComponent = Collision;
		Collision->OnBeginCursorOver.AddDynamic(this, &AEverdellLocation::OnBeginMouseOver);
		Collision->OnEndCursorOver.AddDynamic(this, &AEverdellLocation::OnEndMouseOver);
		Collision->OnClicked.AddDynamic(this, &AEverdellLocation::OnMouseClicked);
		Collision->OnReleased.AddDynamic(this, &AEverdellLocation::OnMouseReleased);
	}

	GameState = nullptr;
	HUD = nullptr;
	bIsOpenLocation = false;
}

void AEverdellWorkerLocation::BeginPlay()
{
	Super::BeginPlay();

	GameState = static_cast<AEverdellGameStateBase*>(UGameplayStatics::GetGameState(GetWorld()));
	HUD = static_cast<AEverdellHUD*>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());
}

void AEverdellWorkerLocation::OnMouseClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed)
{
	Super::OnMouseClicked(TouchedComponent, ButtonPressed);
}

void AEverdellWorkerLocation::OnMouseReleased(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed)
{
	Super::OnMouseReleased(TouchedComponent, ButtonPressed);

	AddWorkerIfEffectExecutes();
}

bool AEverdellWorkerLocation::HasWorker() const
{
	return !Workers.IsEmpty();
}

int32 AEverdellWorkerLocation::WorkerCount() const
{
	return Workers.Num();
}

bool AEverdellWorkerLocation::CanAcceptWorkers() const
{
	return !HasWorker() || bIsOpenLocation;
}

void AEverdellWorkerLocation::AddWorker(AEverdellWorker* NewWorker)
{
	NewWorker->bIsDeployed = true;
	NewWorker->DeployedLocation = this;
	Workers.Add(NewWorker);
	NewWorker->MoveToLocationWithRotation(GetActorLocation(), NewWorker->GetActorRotation());
}

void AEverdellWorkerLocation::RemoveWorker(AEverdellWorker* Worker)
{
	Worker->bIsDeployed = false;
	Worker->DeployedLocation = nullptr;
	Workers.Remove(Worker);
}

void AEverdellWorkerLocation::AddWorkerIfEffectExecutes()
{
	if (CanAcceptWorkers() && CanPerformEffect.IsBound() && PerformEffect.IsBound() && CanPerformEffect.Execute(GameState, HUD, this, true))
	{
		AddWorker(PerformEffect.Execute(GameState, HUD, this, true));
	}
}

TArray<AEverdellWorker*> AEverdellWorkerLocation::ResetWorkers()
{
	TArray<AEverdellWorker*> OldWorkers = Workers.Array();
	Workers.Empty();
	return OldWorkers;
}

bool AEverdellWorkerLocation::IsInteractable() const
{
	return CanAcceptWorkers();
}
