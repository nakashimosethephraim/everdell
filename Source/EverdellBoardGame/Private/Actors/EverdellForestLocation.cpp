#include "Actors/EverdellForestLocation.h"
#include "GameManagement/EverdellBoardGame.h"
#include "Components/StaticMeshComponent.h"

AEverdellForestLocation::AEverdellForestLocation() : AEverdellWorkerLocation()
{
	bIsOpenLocation = false;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> BaseMeshPlaneAsset(TEXT("/Engine/BasicShapes/Plane"));
	static ConstructorHelpers::FObjectFinder<UMaterial> BackMaterialAsset(TEXT("Material'/Game/EverdellAssets/EverdellForestLocationBack_Mat.EverdellForestLocationBack_Mat'"));
	if (BaseMeshPlaneAsset.Succeeded() && BackMaterialAsset.Succeeded()) {
		CardFront = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CardFront"));
		CardFront->SetStaticMesh(BaseMeshPlaneAsset.Object);
		CardFront->SetupAttachment(RootComponent);
		CardFront->SetRelativeScale3D(FVector(5, 3, 1));
		CardFront->SetRelativeRotation(FRotator(0, -90, 0));
		CardFront->SetRelativeLocation(FVector(-100, -100, -5));

		CardBack = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CardBack"));
		CardBack->SetStaticMesh(BaseMeshPlaneAsset.Object);
		CardBack->SetupAttachment(RootComponent);
		CardBack->SetRelativeScale3D(FVector(5, 3, 1));
		CardBack->SetRelativeRotation(FRotator(180, -90, 0));
		CardBack->SetRelativeLocation(FVector(-100, -100, -5));
		CardBack->SetCastShadow(false);
		BackMaterial = BackMaterialAsset.Object;
		CardBack->SetMaterial(0, BackMaterial);
	}

	FrontMaterial = nullptr;
}

void AEverdellForestLocation::BuildFromData(const FEverdellForestLocationData& LocationData)
{
	LocationType = LocationData.LocationType;
	LocationName = LocationData.LocationName;
	FrontMaterial = (UMaterial*)StaticLoadObject(UMaterial::StaticClass(), nullptr, *LocationData.CardMaterialResourcePath.ToString());
	CardFront->SetMaterial(0, FrontMaterial);

	if (LocationData.CanPerformEffectType != nullptr)
	{
		CanPerformEffect.BindLambda(LocationData.CanPerformEffectType);
	}
	if (LocationData.PerformEffectType != nullptr)
	{
		PerformEffect.BindLambda(LocationData.PerformEffectType);
	}
}
