#include "Actors/EverdellWorkerBL3Twigs.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameData/EverdellLocationEffects.h"

AEverdellWorkerBL3Twigs::AEverdellWorkerBL3Twigs() : AEverdellWorkerLocation()
{
	bIsOpenLocation = false;
	CanPerformEffect.BindLambda(&UEverdellLocationEffects::CanPerformEffectDefault);
	PerformEffect.BindLambda(&UEverdellLocationEffects::PerformEffectBasicLocation3Twigs);
}
