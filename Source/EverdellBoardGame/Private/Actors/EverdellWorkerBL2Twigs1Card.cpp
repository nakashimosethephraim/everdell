#include "Actors/EverdellWorkerBL2Twigs1Card.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameData/EverdellLocationEffects.h"

AEverdellWorkerBL2Twigs1Card::AEverdellWorkerBL2Twigs1Card() : AEverdellWorkerLocation()
{
	bIsOpenLocation = true;
	CanPerformEffect.BindLambda(&UEverdellLocationEffects::CanPerformEffectDefault);
	PerformEffect.BindLambda(&UEverdellLocationEffects::PerformEffectBasicLocation2TwigsDraw1Card);
}
