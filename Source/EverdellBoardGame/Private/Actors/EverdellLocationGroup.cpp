#include "Actors/EverdellLocationGroup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "GameManagement/EverdellHUD.h"
#include "Actors/EverdellDeckActor.h"
#include "Actors/EverdellCardLocationActor.h"
#include "Actors/Everdell3DCard.h"
#include "GameData/EverdellCard.h"
#include "Kismet/GameplayStatics.h"

AEverdellLocationGroup::AEverdellLocationGroup() : AActor()
{
	CardsInGroup = 0;
	NextAvailableIndex = 0;
}

void AEverdellLocationGroup::InitializeLocations()
{
	TArray<AActor*> TempLocations;
	UGameplayStatics::GetAllActorsOfClassWithTag(GetWorld(), AEverdellCardLocationActor::StaticClass(), LocationTag, TempLocations);
	
	for (auto* Location : TempLocations)
	{
		Locations.Add(static_cast<AEverdellCardLocationActor*>(Location));
	}

	Locations.Sort([](const AEverdellCardLocationActor& Actor1, const AEverdellCardLocationActor& Actor2) {
		return Actor1.LocationIndex < Actor2.LocationIndex;
	});
}

int32 AEverdellLocationGroup::GetGroupSize() const
{
	return Locations.Num();
}

int32 AEverdellLocationGroup::GetCardCount() const
{
	return CardsInGroup;
}

int32 AEverdellLocationGroup::GetNextAvailableLocationIndex(bool bRequiresSpace) const
{
	return NextAvailableIndex;
}

AEverdellCardLocationActor* AEverdellLocationGroup::GetNextAvailableLocation(bool bRequiresSpace) const
{
	if (CanPlaceCards())
	{
		return Locations[NextAvailableIndex];
	}

	return nullptr;
}

FVector AEverdellLocationGroup::GetNextAvailablePlacementLocation(bool bRequiresSpace) const
{
	if (CanPlaceCards())
	{
		return Locations[NextAvailableIndex]->GetNextCardPlacementLocation();
	}

	return FVector(0, 0, 0);
}

bool AEverdellLocationGroup::CanPlaceCards() const
{
	return CardsInGroup < Locations.Num();
}

bool AEverdellLocationGroup::SetCard(AEverdell3DCard* CardActor)
{
	if (CardsInGroup < Locations.Num())
	{
		Locations[NextAvailableIndex]->SetCard(CardActor);

		++CardsInGroup;
		++NextAvailableIndex;

		while (NextAvailableIndex < Locations.Num())
		{
			if (!Locations[NextAvailableIndex]->HasCard()) break;
			++NextAvailableIndex;
		}

		return true;
	}

	return false;
}

AEverdell3DCard* AEverdellLocationGroup::GetCard(int32 Index) const
{
	if (Index < Locations.Num())
	{
		return Locations[Index]->GetCard();
	}

	return nullptr;
}

TArray<AEverdell3DCard*> AEverdellLocationGroup::GetCardsOfType(EEverdellCardType CardType) const
{
	TArray<AEverdell3DCard*> FoundCards;
	for (auto* Location : Locations)
	{
		if (Location->HasCard() && Location->GetCard()->HasCard() && Location->GetCard()->GetCard()->CardType == CardType)
		{
			FoundCards.Add(Location->GetCard());
		}
		FoundCards += Location->GetSecondaryCardsOfType(CardType);
	}

	return FoundCards;
}

AEverdellCardLocationActor* AEverdellLocationGroup::GetCardLocation(int32 Index) const
{
	if (Index < Locations.Num())
	{
		return Locations[Index];
	}

	return nullptr;
}

AEverdell3DCard* AEverdellLocationGroup::ResetCard(int32 Index)
{
	if (Index >= 0 && Index < Locations.Num() && Locations[Index]->HasCard())
	{
		AEverdell3DCard* card = Locations[Index]->ResetCard();

		--CardsInGroup;
		if (Index < NextAvailableIndex) NextAvailableIndex = Index;

		return card;
	}

	return nullptr;
}

int32 AEverdellLocationGroup::CountOfCard(EEverdellCardType CardType) const
{
	int32 TotalFoundCards = 0;
	for (auto* Location : Locations)
	{
		TotalFoundCards += static_cast<int32>(Location->HasCard() && Location->GetCard()->HasCard() && Location->GetCard()->GetCard()->CardType == CardType) + Location->CountOfSecondaryCardsOfType(CardType);
	}

	return TotalFoundCards;
}

int32 AEverdellLocationGroup::CountOfCardCategory(EEverdellCardCategory CardCategory) const
{
	int32 TotalFoundCards = 0;
	for (auto* Location : Locations)
	{
		TotalFoundCards += static_cast<int32>(Location->HasCard() && Location->GetCard()->HasCard() && Location->GetCard()->GetCard()->CardCategory == CardCategory) + Location->CountOfSecondaryCardsOfCategory(CardCategory);
	}

	return TotalFoundCards;
}

int32 AEverdellLocationGroup::CountOfCritters() const
{
	int32 TotalFoundCritters = 0;
	for (auto* Location : Locations)
	{
		TotalFoundCritters += static_cast<int32>(Location->HasCard() && Location->GetCard()->HasCard() && Location->GetCard()->GetCard()->bIsCritter) + Location->CountOfSecondaryCritters();
	}

	return TotalFoundCritters;
}

int32 AEverdellLocationGroup::CountOfConstructions() const
{
	int32 TotalFoundConstructions = 0;
	for (auto* Location : Locations)
	{
		TotalFoundConstructions += static_cast<int32>(Location->HasCard() && Location->GetCard()->HasCard() && !Location->GetCard()->GetCard()->bIsCritter);
	}

	return TotalFoundConstructions;
}

int32 AEverdellLocationGroup::CountOfCardWithProperties(bool bIsCritter, bool bIsUnique) const
{
	int32 TotalFoundCards = 0;
	for (auto* Location : Locations)
	{
		TotalFoundCards += static_cast<int32>(Location->HasCard() && Location->GetCard()->HasCard() && (Location->GetCard()->GetCard()->bIsCritter == bIsCritter) && (Location->GetCard()->GetCard()->bIsUnique == bIsUnique));
	}

	return TotalFoundCards;
}

int32 AEverdellLocationGroup::CountOfActivatableGreenProductionCards(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD) const
{
	int32 TotalFoundCards = 0;
	for (auto* Location : Locations)
	{
		TotalFoundCards += static_cast<int32>(
			Location->HasCard() && Location->GetCard()->HasCard() && Location->GetCard()->GetCard()->CardCategory == EEverdellCardCategory::GREEN_PRODUCTION &&
			Location->GetCard()->GetCard()->CanPerformOnPlayEffect.IsBound() &&
			Location->GetCard()->GetCard()->CanPerformOnPlayEffect.Execute(GameState, HUD, Location, Location->GetCard()->GetCard())
		) + Location->CountOfSecondaryActivatableGreenProductions(GameState, HUD);
	}

	return TotalFoundCards;
}
