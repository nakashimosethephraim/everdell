#include "Actors/EverdellWorkerBL1Berry.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameData/EverdellLocationEffects.h"

AEverdellWorkerBL1Berry::AEverdellWorkerBL1Berry() : AEverdellWorkerLocation()
{
	bIsOpenLocation = true;
	CanPerformEffect.BindLambda(&UEverdellLocationEffects::CanPerformEffectDefault);
	PerformEffect.BindLambda(&UEverdellLocationEffects::PerformEffectBasicLocation1Berry);
}
