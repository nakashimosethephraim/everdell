#include "Actors/EverdellCameraActor.h"
#include "GameManagement/EverdellBoardGame.h"
#include "Camera/CameraComponent.h"

AEverdellCameraActor::AEverdellCameraActor(): ACameraActor()
{
	AEverdellCameraActor::GetCameraComponent()->bConstrainAspectRatio = false;
}
