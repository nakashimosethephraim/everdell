#include "Actors/EverdellCardLocationActor.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "GameManagement/EverdellHUD.h"
#include "Actors/EverdellDeckActor.h"
#include "Actors/Everdell3DCard.h"
#include "GameData/EverdellCard.h"

AEverdellCardLocationActor::AEverdellCardLocationActor() : AEverdellLocation()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMeshAsset(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube'"));
	if (CubeMeshAsset.Succeeded())
	{
		Collision = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Collision"));
		Collision->SetStaticMesh(CubeMeshAsset.Object);
		Collision->SetWorldScale3D(FVector(1.5, 1.5, 1.5));
		Collision->SetRelativeScale3D(FVector(1.5, 1.5, 1.5));
		Collision->SetVisibleFlag(false);
		RootComponent = Collision;
		Collision->OnBeginCursorOver.AddDynamic(this, &AEverdellLocation::OnBeginMouseOver);
		Collision->OnEndCursorOver.AddDynamic(this, &AEverdellLocation::OnEndMouseOver);
		Collision->OnClicked.AddDynamic(this, &AEverdellLocation::OnMouseClicked);
		Collision->OnReleased.AddDynamic(this, &AEverdellLocation::OnMouseReleased);
	}

	Card = nullptr;
}

bool AEverdellCardLocationActor::HasCard() const
{
	return Card != nullptr;
}

AEverdell3DCard* AEverdellCardLocationActor::GetCard() const
{
	return Card;
}

void AEverdellCardLocationActor::SetCard(AEverdell3DCard* NewCard)
{
	Card = NewCard;
	SetActorEnableCollision(false);
}

AEverdell3DCard* AEverdellCardLocationActor::ResetCard()
{
	AEverdell3DCard* OldCard = Card;
	Card = nullptr;

	if (SecondaryCards.IsEmpty())
	{
		SetActorEnableCollision(true);
	}

	return OldCard;
}

bool AEverdellCardLocationActor::HasSecondaryCards() const
{
	return !SecondaryCards.IsEmpty();
}

AEverdell3DCard* AEverdellCardLocationActor::GetSecondaryCard(const int32 Index) const
{
	if (Index < SecondaryCards.Num())
	{
		return SecondaryCards[Index];
	}

	return nullptr;
}

void AEverdellCardLocationActor::AddSecondaryCard(AEverdell3DCard* NewCard)
{
	SecondaryCards.Add(NewCard);
	SetActorEnableCollision(false);
}

FVector AEverdellCardLocationActor::GetNextSecondaryCardPlacementLocation() const
{
	FVector PlacementLocation = GetActorLocation();
	PlacementLocation.X += 45 * (SecondaryCards.Num() + 1);
	PlacementLocation.Y += 20 * (SecondaryCards.Num() + 1);
	PlacementLocation.Z += 2 * (SecondaryCards.Num() + 1);
	return PlacementLocation;
}

TArray<AEverdell3DCard*> AEverdellCardLocationActor::ResetSecondaryCards()
{
	TArray<AEverdell3DCard*> OldCards = SecondaryCards;
	SecondaryCards.Empty();

	if (Card == nullptr)
	{
		SetActorEnableCollision(true);
	}

	return OldCards;
}

int32 AEverdellCardLocationActor::GetSecondaryCardCount() const
{
	return SecondaryCards.Num();
}

bool AEverdellCardLocationActor::HasSecondaryCardOfType(const EEverdellCardType CardType) const
{
	bool bFoundCardOfType = false;
	for (int32 Index = 0; Index < SecondaryCards.Num() && !bFoundCardOfType; ++Index)
	{
		bFoundCardOfType = SecondaryCards[Index]->HasCard() && SecondaryCards[Index]->GetCard()->CardType == CardType;
	}

	return bFoundCardOfType;
}

TArray<AEverdell3DCard*> AEverdellCardLocationActor::GetSecondaryCardsOfType(const EEverdellCardType CardType) const
{
	TArray<AEverdell3DCard*> CardsOfType;
	for (int32 Index = 0; Index < SecondaryCards.Num(); ++Index)
	{
		if (SecondaryCards[Index]->HasCard() && SecondaryCards[Index]->GetCard()->CardType == CardType)
		{
			CardsOfType.Add(SecondaryCards[Index]);
		}
	}

	return CardsOfType;
}

int32 AEverdellCardLocationActor::CountOfSecondaryCardsOfType(const EEverdellCardType CardType) const
{
	int32 CountOfCardOfType = 0;
	for (int32 Index = 0; Index < SecondaryCards.Num(); ++Index)
	{
		CountOfCardOfType += static_cast<int32>(SecondaryCards[Index]->HasCard() && SecondaryCards[Index]->GetCard()->CardType == CardType);
	}

	return CountOfCardOfType;
}

int32 AEverdellCardLocationActor::CountOfSecondaryCardsOfCategory(const EEverdellCardCategory CardCategory) const
{
	int32 CountOfCardOfCategory = 0;
	for (int32 Index = 0; Index < SecondaryCards.Num(); ++Index)
	{
		CountOfCardOfCategory += static_cast<int32>(SecondaryCards[Index]->HasCard() && SecondaryCards[Index]->GetCard()->CardCategory == CardCategory);
	}

	return CountOfCardOfCategory;
}

int32 AEverdellCardLocationActor::CountOfSecondaryCritters() const
{
	int32 CountOfCritters = 0;
	for (int32 Index = 0; Index < SecondaryCards.Num(); ++Index)
	{
		CountOfCritters += static_cast<int32>(SecondaryCards[Index]->HasCard() && SecondaryCards[Index]->GetCard()->bIsCritter);
	}

	return CountOfCritters;
}

int32 AEverdellCardLocationActor::CountOfSecondaryConstructions() const
{
	int32 CountOfConstructions = 0;
	for (int32 Index = 0; Index < SecondaryCards.Num(); ++Index)
	{
		CountOfConstructions += static_cast<int32>(SecondaryCards[Index]->HasCard() && !SecondaryCards[Index]->GetCard()->bIsCritter);
	}

	return CountOfConstructions;
}

int32 AEverdellCardLocationActor::CountOfSecondaryCardsWithProperties(const bool bIsCritter, const bool bIsUnique) const
{
	int32 CountOfCards = 0;
	for (int32 Index = 0; Index < SecondaryCards.Num(); ++Index)
	{
		CountOfCards += static_cast<int32>(SecondaryCards[Index]->HasCard() && (SecondaryCards[Index]->GetCard()->bIsCritter == bIsCritter) && (SecondaryCards[Index]->GetCard()->bIsUnique == bIsUnique));
	}

	return CountOfCards;
}

int32 AEverdellCardLocationActor::CountOfSecondaryActivatableGreenProductions(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD) const
{
	int32 CountOfSecondaryActivatableGreenProductions = 0;
	for (int32 Index = 0; Index < SecondaryCards.Num(); ++Index)
	{
		CountOfSecondaryActivatableGreenProductions += static_cast<int32>(
			SecondaryCards[Index]->HasCard() && SecondaryCards[Index]->GetCard()->CardCategory == EEverdellCardCategory::GREEN_PRODUCTION &&
			SecondaryCards[Index]->GetCard()->CanPerformOnPlayEffect.IsBound() &&
			SecondaryCards[Index]->GetCard()->CanPerformOnPlayEffect.Execute(GameState, HUD, this, SecondaryCards[Index]->GetCard())
		);
	}

	return CountOfSecondaryActivatableGreenProductions;
}

int32 AEverdellCardLocationActor::SecondaryCardsScore(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD) const
{
	int32 Score = 0;
	for (int32 Index = 0; Index < SecondaryCards.Num(); ++Index)
	{
		Score += SecondaryCards[Index]->GetCard()->GetCardScore(PlayerState, GameState, HUD, this);
	}

	return Score;
}

void AEverdellCardLocationActor::SetCardGeneric(AEverdell3DCard* NewCard)
{
	!HasCard() ? SetCard(NewCard) : AddSecondaryCard(NewCard);
}

int32 AEverdellCardLocationActor::GetTotalCardCount() const
{
	return static_cast<int32>(HasCard()) + GetSecondaryCardCount();
}

bool AEverdellCardLocationActor::HasCardOfType(const EEverdellCardType CardType) const
{
	return (HasCard() && GetCard()->HasCard() && GetCard()->GetCard()->CardType == CardType) || HasSecondaryCardOfType(CardType);
}

int32 AEverdellCardLocationActor::TotalCountCardsOfType(const EEverdellCardType CardType) const
{
	return static_cast<int32>(HasCard() && GetCard()->HasCard() && GetCard()->GetCard()->CardType == CardType) + CountOfSecondaryCardsOfType(CardType);
}

int32 AEverdellCardLocationActor::TotalCountCardsOfCategory(const EEverdellCardCategory CardCategory) const
{
	return static_cast<int32>(HasCard() && GetCard()->HasCard() && GetCard()->GetCard()->CardCategory == CardCategory) + CountOfSecondaryCardsOfCategory(CardCategory);
}

int32 AEverdellCardLocationActor::TotalCountCritters() const
{
	return static_cast<int32>(HasCard() && GetCard()->HasCard() && GetCard()->GetCard()->bIsCritter) + CountOfSecondaryCritters();
}

int32 AEverdellCardLocationActor::TotalCountConstructions() const
{
	return static_cast<int32>(HasCard() && GetCard()->HasCard() && !GetCard()->GetCard()->bIsCritter) + CountOfSecondaryConstructions();
}

int32 AEverdellCardLocationActor::TotalCountSecondaryCardsWithProperties(const bool bIsCritter, const bool bIsUnique) const
{
	return static_cast<int32>(HasCard() && GetCard()->HasCard() && (GetCard()->GetCard()->bIsCritter == bIsCritter) && (GetCard()->GetCard()->bIsUnique == bIsUnique)) +
		   CountOfSecondaryCardsWithProperties(bIsCritter, bIsUnique);
}

int32 AEverdellCardLocationActor::TotalCountActivatableGreenProductions(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD) const
{
	return static_cast<int32>(
		HasCard() && GetCard()->HasCard() && GetCard()->GetCard()->CardCategory == EEverdellCardCategory::GREEN_PRODUCTION &&
		GetCard()->GetCard()->CanPerformOnPlayEffect.IsBound() &&
		GetCard()->GetCard()->CanPerformOnPlayEffect.Execute(GameState, HUD, this, GetCard()->GetCard())
	) + CountOfSecondaryConstructions();
}

int32 AEverdellCardLocationActor::TotalCardsScore(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD) const
{
	return (HasCard() && GetCard()->HasCard() ? GetCard()->GetCard()->GetCardScore(PlayerState, GameState, HUD, this) : 0) +
		   SecondaryCardsScore(PlayerState, GameState, HUD);
}

FVector AEverdellCardLocationActor::GetNextCardPlacementLocation() const
{
	return !HasCard() ? GetActorLocation() : GetNextSecondaryCardPlacementLocation();
}

TArray<AEverdell3DCard*> AEverdellCardLocationActor::ResetCards()
{
	AEverdell3DCard* OldCard = Card;
	Card = nullptr;
	TArray<AEverdell3DCard*> OldCards = SecondaryCards;
	SecondaryCards.Empty();
	OldCards.Insert(OldCard, 0);

	SetActorEnableCollision(true);

	return OldCards;
}
