#include "Actors/EverdellWorkerSlot.h"
#include "GameManagement/EverdellBoardGame.h"
#include "Actors/EverdellWorker.h"

AEverdellWorkerSlot::AEverdellWorkerSlot() : AActor()
{
	Position = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Position"));
	Worker = nullptr;
	SlotIndex = 0;
}

bool AEverdellWorkerSlot::HasWorker() const
{
	return Worker != nullptr;
}

void AEverdellWorkerSlot::SetWorker(AEverdellWorker* NewWorker)
{
	Worker = NewWorker;
}

AEverdellWorker* AEverdellWorkerSlot::ResetWorker()
{
	auto* OldWorker = Worker;
	Worker = nullptr;
	return OldWorker;
}
