#include "Actors/EverdellWorkerSlotGroup.h"
#include "GameManagement/EverdellBoardGame.h"
#include "Actors/EverdellWorker.h"
#include "Actors/EverdellWorkerSlot.h"
#include "Kismet/GameplayStatics.h"

AEverdellWorkerSlotGroup::AEverdellWorkerSlotGroup() : AActor()
{
	WorkersInGroup = 0;
	NextAvailableIndex = -1;
	NextOpenIndex = 0;
}

void AEverdellWorkerSlotGroup::InitializeSlots()
{
	TArray<AActor*> TempSlots;
	UGameplayStatics::GetAllActorsOfClassWithTag(GetWorld(), AEverdellWorkerSlot::StaticClass(), SlotTag, TempSlots);

	for (auto* Slot : TempSlots)
	{
		Slots.Add(static_cast<AEverdellWorkerSlot*>(Slot));
	}

	Slots.Sort([](const AEverdellWorkerSlot& Actor1, const AEverdellWorkerSlot& Actor2) {
		return Actor1.SlotIndex < Actor2.SlotIndex;
	});

	for (int32 SlotIndex = 0; SlotIndex < Slots.Num(); ++SlotIndex)
	{
		if (Slots[SlotIndex]->HasWorker())
		{
			++WorkersInGroup;
			NextAvailableIndex = SlotIndex;
			NextOpenIndex = SlotIndex + 1;
		}
	}
}

int32 AEverdellWorkerSlotGroup::GetWorkerCount() const
{
	return WorkersInGroup;
}

bool AEverdellWorkerSlotGroup::HasAvailableWorker() const
{
	return WorkersInGroup > 0;
}

AEverdellWorker* AEverdellWorkerSlotGroup::GetNextAvailableWorker()
{
	if (HasAvailableWorker())
	{
		auto* Worker = Slots[NextAvailableIndex]->ResetWorker();

		--WorkersInGroup;
		NextOpenIndex = NextAvailableIndex;
		--NextAvailableIndex;

		return Worker;
	}

	return nullptr;
}

bool AEverdellWorkerSlotGroup::AddWorker(AEverdellWorker* Worker)
{
	if (WorkersInGroup < Slots.Num())
	{
		Slots[NextOpenIndex]->SetWorker(Worker);
		Worker->MoveToLocationWithRotation(Slots[NextOpenIndex]->GetActorLocation(), Worker->GetActorRotation());

		++WorkersInGroup;
		NextAvailableIndex = NextOpenIndex;
		++NextOpenIndex;

		while (NextOpenIndex < Slots.Num())
		{
			if (!Slots[NextOpenIndex]->HasWorker()) break;
			++NextOpenIndex;
		}

		return true;
	}

	return false;
}

void AEverdellWorkerSlotGroup::InitializeWithWorkers(TArray<AEverdellWorker*>& Workers) const
{
	for (const auto* Slot : Slots)
	{
		if (Slot->Worker != nullptr) Workers.Add(Slot->Worker);
	}
}
