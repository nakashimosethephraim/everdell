#include "Actors/EverdellWorkerBL1Pebble.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameData/EverdellLocationEffects.h"

AEverdellWorkerBL1Pebble::AEverdellWorkerBL1Pebble() : AEverdellWorkerLocation()
{
	bIsOpenLocation = false;
	CanPerformEffect.BindLambda(&UEverdellLocationEffects::CanPerformEffectDefault);
	PerformEffect.BindLambda(&UEverdellLocationEffects::PerformEffectBasicLocation1Pebble);
}
