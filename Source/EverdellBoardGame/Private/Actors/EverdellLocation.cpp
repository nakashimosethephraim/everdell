#include "Actors/EverdellLocation.h"
#include "GameManagement/EverdellBoardGame.h"
#include "NiagaraSystem.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"

AEverdellLocation::AEverdellLocation(): AActor()
{
	static ConstructorHelpers::FObjectFinder<UNiagaraSystem> LocationMouseOverEffectAsset(TEXT("NiagaraSystem'/Game/sA_PickupSet_1/Fx/NiagaraSystems/NS_Shield.NS_Shield'"));
	if (LocationMouseOverEffectAsset.Succeeded())
	{
		LocationMouseOverEffect = LocationMouseOverEffectAsset.Object;
	}

	static ConstructorHelpers::FObjectFinder<UNiagaraSystem> LocationClickEffectAsset(TEXT("NiagaraSystem'/Game/sA_PickupSet_1/Fx/NiagaraSystems/NS_CoinBurst.NS_CoinBurst'"));
	if (LocationClickEffectAsset.Succeeded())
	{
		LocationClickEffect = LocationClickEffectAsset.Object;
	}
}

void AEverdellLocation::BeginPlay()
{
	Super::BeginPlay();
}

void AEverdellLocation::OnBeginMouseOver(UPrimitiveComponent* TouchedComponent)
{
	if (IsInteractable())
	{
		SpawnedMouseOverEffect = UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, LocationMouseOverEffect, GetActorLocation(), FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
	}
}

void AEverdellLocation::OnEndMouseOver(UPrimitiveComponent* TouchedComponent)
{
	if (SpawnedMouseOverEffect != nullptr)
	{
		SpawnedMouseOverEffect->DestroyInstance();
		SpawnedMouseOverEffect = nullptr;
	}
}

void AEverdellLocation::OnMouseClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed)
{
	if (IsInteractable())
	{
		SpawnedClickEffect = UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, LocationClickEffect, GetActorLocation(), FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
	}
}

void AEverdellLocation::OnMouseReleased(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed)
{
	if (SpawnedClickEffect != nullptr)
	{
		SpawnedClickEffect->DestroyInstance();
		SpawnedClickEffect = nullptr;
	}
}
