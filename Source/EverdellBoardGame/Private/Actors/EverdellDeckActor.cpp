#include "Actors/EverdellDeckActor.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "Actors/EverdellLocationGroup.h"
#include "Actors/EverdellCardLocationActor.h"
#include "Actors/Everdell3DCard.h"
#include "UI/EverdellUICard.h"
#include "GameData/EverdellCard.h"
#include "GameData/EverdellData.h"
#include "GameData/EverdellCardLocationEnum.h"
#include "Materials/MaterialInstanceConstant.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

AEverdellDeckActor::AEverdellDeckActor(): AActor()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> BaseMeshPlaneAsset(TEXT("/Engine/BasicShapes/Plane"));
	static ConstructorHelpers::FObjectFinder<UMaterial> CardMaterialAsset(TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardBack_Mat.EverdellPlayableCardBack_Mat'"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMeshAsset(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube'"));
	static ConstructorHelpers::FObjectFinder<UMaterialInstanceConstant> CubeMaterialAsset(TEXT("MaterialInstanceConstant'/Game/Megascans/Surfaces/Drawing_Paper_vmcmfh1n/MI_Drawing_Paper_vmcmfh1n_4K.MI_Drawing_Paper_vmcmfh1n_4K'"));
	if (CubeMeshAsset.Succeeded() && CubeMaterialAsset.Succeeded() && BaseMeshPlaneAsset.Succeeded() && CardMaterialAsset.Succeeded())
	{
		Collision = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Collision"));
		Collision->SetStaticMesh(CubeMeshAsset.Object);
		Collision->SetWorldScale3D(FVector(2, 2.75, 1.15));
		Collision->SetRelativeScale3D(FVector(2, 2.75, 1.15));
		DeckMaterial = CubeMaterialAsset.Object;
		Collision->SetMaterial(0, DeckMaterial);
		RootComponent = Collision;

		TopCard = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TopCard"));
		TopCard->SetStaticMesh(BaseMeshPlaneAsset.Object);
		DeckCardMaterial = CardMaterialAsset.Object;
		TopCard->SetMaterial(0, DeckCardMaterial);
		TopCard->SetupAttachment(RootComponent);

		FVector Min, Max;
		Collision->GetLocalBounds(Min, Max);
		TopCard->SetRelativeLocation_Direct(FVector(0, 0, Max.Z + 0.25));

		NewCardSpawnLocation = FVector((Max.X + Min.X / 2), (Max.Y + Min.Y / 2), Max.Z + 0.5);
		NewCardSpawnRotation = FRotator(180, -90, 0);
	}

	GameState = nullptr;

	uint32 TotalInstanceIndex = 0;
	for (uint32 CardIndex = 0; CardIndex < static_cast<uint32>(EverdellCardData.Num()); ++CardIndex)
	{
		const auto& Data = EverdellCardData[CardIndex];
		for (uint8 CountIndex = 0; CountIndex < Data.CardCount; ++CountIndex)
		{
			CardIndexPool.Add(CardIndex);
			UniqueIdPool.Add(TotalInstanceIndex);
			TotalInstanceIndex++;
		}
	}

	bIsClicked = false;
	PrimaryActorTick.bCanEverTick = true;
}

UEverdellCard* AEverdellDeckActor::GenerateNewCard()
{
	if (!CardIndexPool.IsEmpty())
	{
		uint32 RandomPoolIndex = UKismetMathLibrary::RandomIntegerInRange(0, CardIndexPool.Num() - 1);
		uint32 RandomIndex = CardIndexPool[RandomPoolIndex];
		uint32 RandomUniqueId = UniqueIdPool[RandomPoolIndex];
		CardIndexPool.RemoveAt(RandomPoolIndex);
		UniqueIdPool.RemoveAt(RandomPoolIndex);

		UEverdellCard* NewCard = NewObject<UEverdellCard>(this, UEverdellCard::StaticClass());
		NewCard->UniqueId = RandomUniqueId;
		NewCard->BuildFromData(EverdellCardData[RandomIndex]);
		return NewCard;
	}
	
	return nullptr;
}

AEverdell3DCard* AEverdellDeckActor::Spawn3DCard()
{
	UEverdellCard* NewCard = GenerateNewCard();
	if (NewCard != nullptr)
	{
		AEverdell3DCard* New3DCard = static_cast<AEverdell3DCard*>(GetWorld()->SpawnActor(AEverdell3DCard::StaticClass(), &NewCardSpawnLocation, &NewCardSpawnRotation));
		New3DCard->SetCard(NewCard);
		New3DCard->SetActorRelativeScale3D(FVector(3, 3, 3));
		return New3DCard;
	}

	return nullptr;
}

bool AEverdellDeckActor::FillUICard()
{
	UEverdellCard* NewCard = GenerateNewCard();
	if (NewCard != nullptr)
	{
		return GameState->GetCurrentPlayer()->SetHandCard(NewCard);
	}

	return false;
}

UEverdellCard* AEverdellDeckActor::RevealCard()
{
	return GenerateNewCard();
}

TArray<UEverdellCard*> AEverdellDeckActor::RevealCards(const uint32 Count)
{
	TArray<UEverdellCard*> RevealedCards;
	for (uint32 Index = 0; Index < Count; ++Index)
	{
		UEverdellCard* NewCard = GenerateNewCard();
		if (NewCard != nullptr) RevealedCards.Add(NewCard);
	}

	return RevealedCards;
}

bool AEverdellDeckActor::HasCardsRemainingInDeck() const
{
	return !CardIndexPool.IsEmpty();
}

int32 AEverdellDeckActor::CardsRemainingInDeck() const
{
	return CardIndexPool.Num();
}

void AEverdellDeckActor::OnMouseClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed)
{
	if (!bIsClicked && GameState->GetCurrentPlayer()->CanReceiveHandCards() && GameState->GetCurrentPlayer()->GetAvailableCardDraw() > 0)
	{
		bIsClicked = true;
		FillUICard();
		GameState->GetCurrentPlayer()->DecrementAvailableCardDraw(false);
	}
	bIsClicked = false;
}

void AEverdellDeckActor::BeginPlay()
{
	Super::BeginPlay();

	GameState = static_cast<AEverdellGameStateBase*>(UGameplayStatics::GetGameState(GetWorld()));

	Collision->OnClicked.AddDynamic(this, &AEverdellDeckActor::OnMouseClicked);
	//Collision->OnBeginCursorOver.AddDynamic(this, &AEverdellDeckActor::OnBeginMouseOver);
	//Collision->OnEndCursorOver.AddDynamic(this, &AEverdellDeckActor::OnEndMouseOver);

	TopCard->OnClicked.AddDynamic(this, &AEverdellDeckActor::OnMouseClicked);
	//TopCard->OnBeginCursorOver.AddDynamic(this, &AEverdellDeckActor::OnBeginMouseOver);
	//TopCard->OnEndCursorOver.AddDynamic(this, &AEverdellDeckActor::OnEndMouseOver);
}

void AEverdellDeckActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!CardIndexPool.IsEmpty())
	{
		if (GameState->Meadow->CanPlaceCards())
		{
			AEverdell3DCard* New3DCard = Spawn3DCard();
			if (New3DCard != nullptr)
			{
				New3DCard->GetCard()->CardLocation = EEverdellCardLocation::MEADOW;
				New3DCard->GetCard()->LocationLocalIndex = GameState->Meadow->GetNextAvailableLocationIndex(New3DCard->GetCard()->bRequiresSpace);
				New3DCard->MoveToLocationWithRotation(GameState->Meadow->GetNextAvailablePlacementLocation(New3DCard->GetCard()->bRequiresSpace), FRotator(0, -90, 5));
				GameState->Meadow->SetCard(New3DCard);
				OnCardGeneratedDelegate.Broadcast(New3DCard->GetCard()->LocationLocalIndex);
			}
		}
	}
}

