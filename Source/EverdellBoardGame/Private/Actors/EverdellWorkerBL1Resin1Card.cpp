#include "Actors/EverdellWorkerBL1Resin1Card.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameData/EverdellLocationEffects.h"

AEverdellWorkerBL1Resin1Card::AEverdellWorkerBL1Resin1Card() : AEverdellWorkerLocation()
{
	bIsOpenLocation = true;
	CanPerformEffect.BindLambda(&UEverdellLocationEffects::CanPerformEffectDefault);
	PerformEffect.BindLambda(&UEverdellLocationEffects::PerformEffectBasicLocation1ResinDraw1Card);
}
