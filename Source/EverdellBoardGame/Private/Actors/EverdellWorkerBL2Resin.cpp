#include "Actors/EverdellWorkerBL2Resin.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameData/EverdellLocationEffects.h"

AEverdellWorkerBL2Resin::AEverdellWorkerBL2Resin() : AEverdellWorkerLocation()
{
	bIsOpenLocation = false;
	CanPerformEffect.BindLambda(&UEverdellLocationEffects::CanPerformEffectDefault);
	PerformEffect.BindLambda(&UEverdellLocationEffects::PerformEffectBasicLocation2Resin);
}
