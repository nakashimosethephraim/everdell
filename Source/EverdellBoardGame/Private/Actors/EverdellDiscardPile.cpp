#include "Actors/EverdellDiscardPile.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameData/EverdellCard.h"
#include "Materials/MaterialInstanceConstant.h"

AEverdellDiscardPile::AEverdellDiscardPile()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> BaseMeshPlaneAsset(TEXT("/Engine/BasicShapes/Plane"));
	static ConstructorHelpers::FObjectFinder<UMaterial> CardMaterialAsset(TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardBack_Mat.EverdellPlayableCardBack_Mat'"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMeshAsset(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube'"));
	static ConstructorHelpers::FObjectFinder<UMaterialInstanceConstant> CubeMaterialAsset(TEXT("MaterialInstanceConstant'/Game/Megascans/Surfaces/Drawing_Paper_vmcmfh1n/MI_Drawing_Paper_vmcmfh1n_4K.MI_Drawing_Paper_vmcmfh1n_4K'"));
	if (CubeMeshAsset.Succeeded() && CubeMaterialAsset.Succeeded() && BaseMeshPlaneAsset.Succeeded() && CardMaterialAsset.Succeeded())
	{
		Collision = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Collision"));
		Collision->SetStaticMesh(CubeMeshAsset.Object);
		Collision->SetWorldScale3D(FVector(2, 2.75, 0.5));
		Collision->SetRelativeScale3D(FVector(2, 2.75, 0.5));
		PileMaterial = CubeMaterialAsset.Object;
		Collision->SetMaterial(0, PileMaterial);
		RootComponent = Collision;

		TopCard = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TopCard"));
		TopCard->SetStaticMesh(BaseMeshPlaneAsset.Object);
		CardBackMaterial = CardMaterialAsset.Object;
		TopCard->SetMaterial(0, CardBackMaterial);
		TopCard->SetupAttachment(RootComponent);

		FVector Min, Max;
		Collision->GetLocalBounds(Min, Max);
		TopCard->SetRelativeLocation_Direct(FVector(0, 0, Max.Z + 0.25));
	}
}

bool AEverdellDiscardPile::HasCards() const
{
	return !DiscardedCards.IsEmpty();
}

int32 AEverdellDiscardPile::CardCount() const
{
	return DiscardedCards.Num();
}

void AEverdellDiscardPile::PushCard(UEverdellCard* Card)
{
	DiscardedCards.Push(Card);
}

UEverdellCard* AEverdellDiscardPile::PopCard()
{
	if (HasCards())
	{
		return DiscardedCards.Pop();
	}
	
	return nullptr;
}
