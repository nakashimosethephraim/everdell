#include "Actors/EverdellCardWorkerLocation.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameData/EverdellCard.h"

AEverdellCardWorkerLocation::AEverdellCardWorkerLocation() : AEverdellWorkerLocation()
{
	bIsOpenLocation = false;
	bIsSecondLocation = false;
	Card = nullptr;
}

void AEverdellCardWorkerLocation::SetCard(UEverdellCard* NewCard)
{
	if (Card != nullptr) ResetCard();

	Card = NewCard;

	if (Card->CanPerformWorkerPlacementEffectType != nullptr)
	{
		CanPerformEffect.BindLambda(Card->CanPerformWorkerPlacementEffectType);
	}
	if (Card->PerformWorkerPlacementEffectType != nullptr)
	{
		PerformEffect.BindLambda(Card->PerformWorkerPlacementEffectType);
	}
}

bool AEverdellCardWorkerLocation::HasCard() const { return Card != nullptr; }

UEverdellCard* AEverdellCardWorkerLocation::GetCard() const
{
	return Card;
}

UEverdellCard* AEverdellCardWorkerLocation::ResetCard()
{
	UEverdellCard* OldCard = Card;
	Card = nullptr;

	if (CanPerformEffect.IsBound()) CanPerformEffect.Unbind();
	if (PerformEffect.IsBound()) PerformEffect.Unbind();

	return OldCard;
}

bool AEverdellCardWorkerLocation::IsInteractable() const
{
	return Super::IsInteractable() && CanPerformEffect.IsBound() && CanPerformEffect.Execute(GameState, HUD, this, true);
}

FName AEverdellCardWorkerLocation::GetLocationIdentifier() const
{
	if (Card == nullptr)
	{
		return Super::GetLocationIdentifier();
	}

	return Card->CardName;
}
