#include "Actors/EverdellWorker.h"
#include "GameManagement/EverdellBoardGame.h"
#include "Actors/EverdellWorkerLocation.h"

AEverdellWorker::AEverdellWorker() : AActor()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMeshAsset(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube'"));
	if (CubeMeshAsset.Succeeded())
	{
		WorkerBody = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WorkerBody"));
		WorkerBody->SetStaticMesh(CubeMeshAsset.Object);
		WorkerBody->SetWorldScale3D(FVector(0.75, 0.75, 1));
		WorkerBody->SetRelativeScale3D(FVector(0.75, 0.75, 1));
		RootComponent = WorkerBody;
	}

	WorkerMaterial = nullptr;

	bIsMoving = false;
	bIsDeployed = false;

	DeployedLocation = nullptr;

	PrimaryActorTick.bCanEverTick = true;
}

void AEverdellWorker::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickEnabled(false);

	if (WorkerBody != nullptr && WorkerMaterial != nullptr)
	{
		WorkerBody->SetMaterial(0, WorkerMaterial);
	}
}

void AEverdellWorker::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsMoving)
	{
		if (!TargetLocation.Equals(GetActorLocation(), 0.3) || !TargetRotation.Equals(GetActorRotation(), 0.3))
		{
			SetActorLocationAndRotation(
				FMath::VInterpTo(GetActorLocation(), TargetLocation, DeltaTime, 3.f),
				FMath::RInterpTo(GetActorRotation(), TargetRotation, DeltaTime, 3.f)
			);
		}
		else
		{
			bIsMoving = false;
			SetActorTickEnabled(false);
		}
	}
}

void AEverdellWorker::MoveToLocationWithRotation(const FVector& Location, const FRotator& Rotation)
{
	TargetLocation = Location;
	TargetRotation = Rotation;

	SetActorTickEnabled(true);
	bIsMoving = true;
}
