#include "Actors/EverdellForestLocationSlot.h"
#include "GameManagement/EverdellBoardGame.h"
#include "Actors/EverdellForestLocation.h"

AEverdellForestLocationSlot::AEverdellForestLocationSlot() : AActor()
{
	Position = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Position"));
	ForestLocation = nullptr;
	SlotIndex = 0;
}

bool AEverdellForestLocationSlot::HasLocation() const
{
	return ForestLocation != nullptr;
}

void AEverdellForestLocationSlot::SetLocation(AEverdellForestLocation* NewLocation)
{
	ForestLocation = NewLocation;
}
