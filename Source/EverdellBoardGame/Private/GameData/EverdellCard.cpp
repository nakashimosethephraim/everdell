#include "GameData/EverdellCard.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "Actors/EverdellDeckActor.h"
#include "Actors/EverdellCardLocationActor.h"
#include "GameManagement/EverdellHUD.h"
#include "GameData/EverdellAtomicWrapper.h"
#include "Actors/Everdell3DCard.h"
#include "Actors/EverdellPlayerCity.h"
#include "Kismet/KismetMathLibrary.h"

UEverdellCard::UEverdellCard(): UObject()
{
	AtomicData = CreateDefaultSubobject<UEverdellAtomicWrapper>(TEXT("AtomicData"));
	AtomicData->Condition = false;
	AtomicData->Counter = 0;

	UniqueId = 0;
	CardCategory = EEverdellCardCategory::NONE;
	CardType = EEverdellCardType::NONE;
	CardName = TEXT("");
	CardDescription = TEXT("");

	CardLocation = EEverdellCardLocation::NOT_GENERATED;
	LocationLocalIndex = 0;

	TwigCost = 0;
	ResinCost = 0;
	PebbleCost = 0;
	BerryCost = 0;
	VictoryPoints = 0;

	ContainedTwigs = 0;
	ContainedResin = 0;
	ContainedPebbles = 0;
	ContainedBerries = 0;
	ContainedVictoryPoints = 0;
	ContainedCards.Empty();

	bIsCritter = false;
	bIsUnique = false;
	bRequiresSpace = true;
	bDiscardsBeforePlay = false;
	bPayToOpponent = false;
	bPlayToOpponentCity = false;
	bOpenToOpponent = false;
	bFreeUpgradeUsed = false;

	LocationCount = 0;
	LocationYValue = 0;

	RequiredForSecondCopy = EEverdellCardType::NONE;
	RequiredForSecondPlacementSlot = EEverdellCardType::NONE;

	CardTextureResourcePath = TEXT("");
	CardMaterialResourcePath = TEXT("");
}

void UEverdellCard::BuildFromData(const FEverdellCardData& CardData)
{
	CardCategory = CardData.CardCategory;
	CardType = CardData.CardType;
	CardName = CardData.CardName;
	CardDescription = CardData.CardDescription;

	TwigCost = CardData.TwigCost;
	ResinCost = CardData.ResinCost;
	PebbleCost = CardData.PebbleCost;
	BerryCost = CardData.BerryCost;
	VictoryPoints = CardData.VictoryPoints;

	bIsCritter = CardData.bIsCritter;
	bIsUnique = CardData.bIsUnique;
	bRequiresSpace = CardData.bRequiresSpace;
	bDiscardsBeforePlay = CardData.bDiscardsBeforePlay;
	bPayToOpponent = CardData.bPayToOpponent;
	bPlayToOpponentCity = CardData.bPlayToOpponentCity;
	bOpenToOpponent = CardData.bOpenToOpponent;

	LocationCount = CardData.LocationCount;
	LocationYValue = CardData.LocationYValue;

	FreeUpgradeOptions = CardData.FreeUpgradeOptions;

	RequiredForSecondCopy = CardData.RequiredForSecondCopy;
	RequiredForSecondPlacementSlot = CardData.RequiredForSecondPlacementSlot;

	CardTextureResourcePath = CardData.CardTextureResourcePath;
	CardMaterialResourcePath = CardData.CardMaterialResourcePath;

	if (CardData.CanPerformPrePlayEffectType != nullptr)
	{
		CanPerformPrePlayEffect.BindLambda(CardData.CanPerformPrePlayEffectType);
	}
	if (CardData.PerformPrePlayEffectType != nullptr)
	{
		PerformPrePlayEffect.BindLambda(CardData.PerformPrePlayEffectType);
	}
	
	if (CardData.CanPerformOnPlayEffectType != nullptr)
	{
		CanPerformOnPlayEffect.BindLambda(CardData.CanPerformOnPlayEffectType);
	}
	if (CardData.PerformOnPlayEffectType != nullptr)
	{
		PerformOnPlayEffect.BindLambda(CardData.PerformOnPlayEffectType);
	}

	if (CardData.CanPerformPrepareForSeasonEffectType != nullptr)
	{
		CanPerformPrepareForSeasonEffect.BindLambda(CardData.CanPerformPrepareForSeasonEffectType);
	}
	if (CardData.PerformPrepareForSeasonEffectType != nullptr)
	{
		PerformPrepareForSeasonEffect.BindLambda(CardData.PerformPrepareForSeasonEffectType);
	}

	if (CardData.CanPerformWorkerPlacementEffectType != nullptr)
	{
		CanPerformWorkerPlacementEffectType = CardData.CanPerformWorkerPlacementEffectType;
	}
	if (CardData.PerformWorkerPlacementEffectType != nullptr)
	{
		PerformWorkerPlacementEffectType = CardData.PerformWorkerPlacementEffectType;
	}

	if (CardData.CanPerformScoringEffectType != nullptr)
	{
		CanPerformScoringEffect.BindLambda(CardData.CanPerformScoringEffectType);
	}
	if (CardData.PerformScoringEffectType != nullptr)
	{
		PerformScoringEffect.BindLambda(CardData.PerformScoringEffectType);
	}

	if (CardData.CanPerformDiscardEffectType != nullptr)
	{
		CanPerformDiscardEffect.BindLambda(CardData.CanPerformDiscardEffectType);
	}
	if (CardData.PerformDiscardEffectType != nullptr)
	{
		PerformDiscardEffect.BindLambda(CardData.PerformDiscardEffectType);
	}

	if (CardData.CanPerformTriggeredEffectType != nullptr)
	{
		CanPerformTriggeredEffect.BindLambda(CardData.CanPerformTriggeredEffectType);
	}
	if (CardData.PerformTriggeredEffectType != nullptr)
	{
		PerformTriggeredEffect.BindLambda(CardData.PerformTriggeredEffectType);
	}

	if (CardData.CanPerformActivatedEffectType != nullptr)
	{
		CanPerformActivatedEffect.BindLambda(CardData.CanPerformActivatedEffectType);
	}
	if (CardData.PerformActivatedEffectType != nullptr)
	{
		PerformActivatedEffect.BindLambda(CardData.PerformActivatedEffectType);
	}
}

bool UEverdellCard::HasNonZeroCost() const
{
	return (TwigCost + ResinCost + PebbleCost + BerryCost) > 0;
}

bool UEverdellCard::HasContainedResources() const
{
	return (ContainedTwigs + ContainedResin + ContainedPebbles + ContainedBerries + ContainedVictoryPoints) > 0;
}

bool UEverdellCard::DoesRespectBoardState(const AEverdellGameStateBase* GameState) const
{
	return DoesRespectBoardState(GameState->GetCurrentPlayer());
}

bool UEverdellCard::DoesRespectBoardState(const AEverdellPlayerState* PlayerState) const
{
	return (!bRequiresSpace || bDiscardsBeforePlay || PlayerState->City->CanPlaceCards()) &&
		   (!bIsUnique || PlayerState->CountOfCardInCity(CardType) == 0);
}

bool UEverdellCard::IsPlayable(const AEverdellGameStateBase* GameState) const
{
	return IsPlayable(GameState->GetCurrentPlayer());
}

bool UEverdellCard::IsPlayable(const AEverdellPlayerState* PlayerState) const
{
	return (
		PlayerState->GetTwigCount() >= TwigCost &&
		PlayerState->GetResinCount() >= ResinCost &&
		PlayerState->GetPebbleCount() >= PebbleCost &&
		PlayerState->GetBerryCount() >= BerryCost
	);
}

bool UEverdellCard::IsPlayableWithReducedCost(const int32 ReducedCost, const AEverdellGameStateBase* GameState) const
{
	return IsPlayableWithReducedCost(ReducedCost, GameState->GetCurrentPlayer());
}

bool UEverdellCard::IsPlayableWithReducedCost(const int32 ReducedCost, const AEverdellPlayerState* PlayerState) const
{
	if (!IsPlayable(PlayerState))
	{
		return (
			(PlayerState->GetTwigCount() < TwigCost ? (TwigCost - PlayerState->GetTwigCount()) : 0) +
			(PlayerState->GetResinCount() < ResinCost ? (ResinCost - PlayerState->GetResinCount()) : 0) +
			(PlayerState->GetPebbleCount() < PebbleCost ? (PebbleCost - PlayerState->GetPebbleCount()) : 0) +
			(PlayerState->GetBerryCount() < BerryCost ? (BerryCost - PlayerState->GetBerryCount()) : 0)
		) <= ReducedCost;
	}

	return true;
}

bool UEverdellCard::IsPlayableWithSwappedCost(const int32 SwappedCost, const AEverdellGameStateBase* GameState) const
{
	return IsPlayableWithSwappedCost(SwappedCost, GameState->GetCurrentPlayer());
}

bool UEverdellCard::IsPlayableWithSwappedCost(const int32 SwappedCost, const AEverdellPlayerState* PlayerState) const
{
	if (!IsPlayable(PlayerState))
	{
		int32 Surplus = 0, Deficit = 0;
		(PlayerState->GetTwigCount() < TwigCost ? Deficit : Surplus) += (PlayerState->GetTwigCount() - TwigCost);
		(PlayerState->GetResinCount() < ResinCost ? Deficit : Surplus) += (PlayerState->GetResinCount() - ResinCost);
		(PlayerState->GetPebbleCount() < PebbleCost ? Deficit : Surplus) += (PlayerState->GetPebbleCount() - PebbleCost);
		(PlayerState->GetBerryCount() < BerryCost ? Deficit : Surplus) += (PlayerState->GetBerryCount() - BerryCost);
		Deficit = UKismetMathLibrary::Abs_Int(Deficit);

		return Surplus >= Deficit && Deficit <= SwappedCost;
	}

	return true;
}

bool UEverdellCard::IsInCity() const
{
	return CardLocation == EEverdellCardLocation::CITY || CardLocation == EEverdellCardLocation::OPPONENT_CITY;
}

int32 UEverdellCard::GetCardScore(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* Location) const
{
	int32 Score = VictoryPoints + ContainedVictoryPoints;

	if (CanPerformScoringEffect.IsBound() && PerformScoringEffect.IsBound() && CanPerformScoringEffect.Execute(PlayerState, GameState, HUD, Location, this))
	{
		Score += PerformScoringEffect.Execute(PlayerState, GameState, HUD, Location, this);
	}

	return Score;
}

bool UEverdellCard::CanInteract(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD) const
{
	const AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();

	// Initialize atomic data to track results from multicast delegate
	AtomicData->Condition = false;
	AtomicData->Counter = PlayerState->CheckIfPlayPossibleEventDelegates.Num();

	switch (CardLocation)
	{
	case EEverdellCardLocation::MEADOW:
		return PlayerState->GetTotalAvailableCardDraw() > 0 && PlayerState->CanReceiveHandCards();
	case EEverdellCardLocation::HAND:
		// Check if any registered delegates make play possible
		PlayerState->CheckIfPlayPossibleEffect.Broadcast(GameState, HUD, this, AtomicData);

		// Block until all delegates have responded
		while (AtomicData->Counter > 0) {}

		// Determine card playability
		return (!bRequiresSpace || bDiscardsBeforePlay || PlayerState->City->CanPlaceCards()) &&
			   (
				   (!bIsUnique || PlayerState->CountOfCardInCity(CardType) == 0) &&
				   (
					   (bIsCritter && !PlayerState->City->GetUpgradeConstruction(CardType).IsEmpty()) ||
					   IsPlayable(PlayerState) || AtomicData->Condition
				   )
			   );
	case EEverdellCardLocation::CITY:
		return false;
	case EEverdellCardLocation::OPPONENT_CITY:
		return false;
	case EEverdellCardLocation::NOT_GENERATED:
	default:
		return false;
	}
}

void UEverdellCard::Interact(AEverdellGameStateBase* GameState, AEverdellHUD* HUD)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	UEverdellCard* FoundConstruction = nullptr;
	AEverdell3DCard* Old3DCard = nullptr;

	TArray<AEverdell3DCard*> FreeUpgradeCards;

	switch (CardLocation)
	{
	case EEverdellCardLocation::MEADOW:
		Old3DCard = GameState->Meadow->ResetCard(LocationLocalIndex);
		Old3DCard->ResetCard();
		Old3DCard->Destroy();
		Old3DCard = nullptr;

		PlayerState->SetHandCard(this);
		PlayerState->DecrementAvailableCardDraw(true);
		break;
	case EEverdellCardLocation::HAND:
		if (bIsCritter)
		{
			FreeUpgradeCards = PlayerState->City->GetUpgradeConstruction(CardType);
		}

		if (!FreeUpgradeCards.IsEmpty())
		{
			HUD->OpenPayOrFreeUpgradePopup(this, FreeUpgradeCards);
		}
		else if (IsPlayable(PlayerState) || AtomicData->Condition)
		{
			PlayerState->MoveToCityPayingCost(this);
		}
		break;
	case EEverdellCardLocation::CITY:
		break;
	case EEverdellCardLocation::OPPONENT_CITY:
		break;
	case EEverdellCardLocation::NOT_GENERATED:
	default:
		return;
	}
}
