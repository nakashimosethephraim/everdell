#include "GameData/EverdellCardEffects.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "GameManagement/EverdellHUD.h"
#include "Actors/EverdellDeckActor.h"
#include "Actors/EverdellCardLocationActor.h"
#include "Actors/EverdellPlayerCity.h"
#include "Actors/Everdell3DCard.h"
#include "Actors/EverdellWorker.h"
#include "Actors/EverdellWorkerLocation.h"
#include "GameData/EverdellCard.h"
#include "GameData/EverdellCardCategoryEnum.h"
#include "GameData/EverdellCardTypeEnum.h"
#include "GameData/EverdellCardLocationEnum.h"
#include "GameData/EverdellAtomicWrapper.h"
#include "GameManagement/EverdellSeasonEnum.h"
#include "Kismet/KismetMathLibrary.h"

bool UEverdellCardEffects::CanPerformEffectDefault(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return false;
}

bool UEverdellCardEffects::CanPerformEffectOnPlay(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return AssociatedCard->CardLocation == EEverdellCardLocation::CITY;
}

bool UEverdellCardEffects::CanPerformEffectGreenProduction(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	const AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	return PlayerState->GetCurrentSeason() == EEverdellSeason::SPRING || PlayerState->GetCurrentSeason() == EEverdellSeason::AUTUMN;
}

bool UEverdellCardEffects::CanPerformEffectScoring(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return AssociatedCard->CardLocation == EEverdellCardLocation::CITY;
}

bool UEverdellCardEffects::CanPerformEffectTriggered(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return AssociatedCard->CardLocation == EEverdellCardLocation::CITY;
}

bool UEverdellCardEffects::CanPerformEffect_ChipSweep(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	const AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	return PlayerState->CountOfCardCategoryInCity(EEverdellCardCategory::GREEN_PRODUCTION) > 1;
}

bool UEverdellCardEffects::CanPerformEffectOnPlay_ChipSweep(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return CanPerformEffectOnPlay(GameState, HUD, CardLocation, AssociatedCard) &&
		   CanPerformEffect_ChipSweep(GameState, HUD, CardLocation, AssociatedCard);
}

bool UEverdellCardEffects::CanPerformEffectGreenProduction_ChipSweep(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return CanPerformEffectGreenProduction(GameState, HUD, CardLocation, AssociatedCard) &&
		   CanPerformEffect_ChipSweep(GameState, HUD, CardLocation, AssociatedCard);
}

bool UEverdellCardEffects::CanPerformActivatedEffect_Crane(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return CanPerformEffect_Crane(GameState, AssociatedCard);
}

bool UEverdellCardEffects::CanPerformEffect_Husband(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	const AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	return PlayerState->CountOfCardInCity(EEverdellCardType::FARM) > 0 &&
		   (CardLocation != nullptr && (
		       (CardLocation->HasCard() && CardLocation->GetCard()->HasCard() && CardLocation->GetCard()->GetCard()->CardType == EEverdellCardType::WIFE) ||
			   (CardLocation->HasSecondaryCards() && CardLocation->HasSecondaryCardOfType(EEverdellCardType::WIFE))
		   ));
}

bool UEverdellCardEffects::CanPerformEffectOnPlay_Husband(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return CanPerformEffectOnPlay(GameState, HUD, CardLocation, AssociatedCard) &&
		   CanPerformEffect_Husband(GameState, HUD, CardLocation, AssociatedCard);
}

bool UEverdellCardEffects::CanPerformEffectGreenProduction_Husband(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return CanPerformEffectGreenProduction(GameState, HUD, CardLocation, AssociatedCard) &&
		   CanPerformEffect_Husband(GameState, HUD, CardLocation, AssociatedCard);
}

bool UEverdellCardEffects::CanPerformActivatedEffect_Innkeeper(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return CanPerformEffect_Innkeeper(GameState, AssociatedCard);
}

bool UEverdellCardEffects::CanPerformEffect_Peddler(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	const AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	return (PlayerState->GetTwigCount() + PlayerState->GetResinCount() + PlayerState->GetPebbleCount() + PlayerState->GetBerryCount()) > 0;
}

bool UEverdellCardEffects::CanPerformEffectOnPlay_Peddler(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return CanPerformEffectOnPlay(GameState, HUD, CardLocation, AssociatedCard) &&
		   CanPerformEffect_Peddler(GameState, HUD, CardLocation, AssociatedCard);
}

bool UEverdellCardEffects::CanPerformEffectGreenProduction_Peddler(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return CanPerformEffectGreenProduction(GameState, HUD, CardLocation, AssociatedCard) &&
		   CanPerformEffect_Peddler(GameState, HUD, CardLocation, AssociatedCard);
}

bool UEverdellCardEffects::CanPerformEffect_Ruins(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return GameState->GetCurrentPlayer()->CountOfConstructionsInCity() > 0;
}

void UEverdellCardEffects::PerformEffectDefault(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
}

int32 UEverdellCardEffects::PerformEffect_Architect(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return UKismetMathLibrary::Min(6, PlayerState->GetResinCount() + PlayerState->GetPebbleCount());
}

void UEverdellCardEffects::PerformEffect_Bard(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	if (PlayerState->GetHandCardCount() > 0)
	{
		FEverdellAfterDiscardSignature AfterDiscardDelegate;
		AfterDiscardDelegate.BindUObject(PlayerState, &AEverdellPlayerState::AugmentPointCount);

		HUD->OpenDiscardNCardsPopup(5, 1, std::move(AfterDiscardDelegate));
	}
}

void UEverdellCardEffects::PerformEffect_BargeToad(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AugmentTwigCount(PlayerState->CountOfCardInCity(EEverdellCardType::FARM) * 2);
}

int32 UEverdellCardEffects::PerformEffect_Castle(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return PlayerState->City->CountOfCardWithProperties(false, false);
}

void UEverdellCardEffects::PerformEffect_Carnival(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	int32 GreenProductionCount = PlayerState->CountOfCardCategoryInCity(EEverdellCardCategory::GREEN_PRODUCTION) - 1;
	if (GreenProductionCount > 1)
	{
		HUD->OpenCarnivalPopup();
	}
	else if (GreenProductionCount == 1)
	{
		PlayerState->AugmentAvailableCardDraw(1);
	}
}

void UEverdellCardEffects::PerformEffect_ChipSweep(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	HUD->OpenActivateCardPopup(AssociatedCard);
}

void UEverdellCardEffects::PerformEffect_Courthouse(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AfterConstructionEventDelegates.Add(TPair<EEverdellCardType, int32>(EEverdellCardType::COURTHOUSE, CardLocation->LocationIndex), PlayerState->AfterConstructionEffect.AddLambda(&UEverdellCardEffects::AfterConstructionEffect_Courthouse));
}

void UEverdellCardEffects::PerformActivatedEffect_Crane(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	HUD->OpenPlayForNLessPopup(AssociatedCard, 3);
}

void UEverdellCardEffects::PerformTriggeredEffect_Crane(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	TPair<FEverdellCanPerformBeforePaymentEffectSignature, FEverdellPerformBeforePaymentEffectSignature> DelegatePair;
	DelegatePair.Key.BindLambda(&UEverdellCardEffects::CanPerformBeforePaymentEffect_Crane);
	DelegatePair.Value.BindLambda(&UEverdellCardEffects::PerformBeforePaymentEffect_Crane);

	PlayerState->BeforeConstructionPaymentEventDelegates.Add(TPair<EEverdellCardType, int32>(EEverdellCardType::CRANE, CardLocation->LocationIndex), std::move(DelegatePair));
	PlayerState->CheckIfPlayPossibleEventDelegates.Add(TPair<EEverdellCardType, int32>(EEverdellCardType::CRANE, CardLocation->LocationIndex), PlayerState->CheckIfPlayPossibleEffect.AddLambda(&UEverdellCardEffects::CanPlayUsingPaymentEffect_Crane));
}

void UEverdellCardEffects::PerformEffect_Doctor(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	FEverdellCheckValueSignature CheckValueDelegate;
	CheckValueDelegate.BindUObject(PlayerState, &AEverdellPlayerState::GetBerryCount);
	FEverdellValueAugmentSignature ValueAugmentDelegate;
	ValueAugmentDelegate.BindUObject(PlayerState, &AEverdellPlayerState::AugmentBerryCount);

	HUD->OpenResourceToPointsPopup(TEXT("Berry"), 0, 3, 1, std::move(CheckValueDelegate), std::move(ValueAugmentDelegate));
}

void UEverdellCardEffects::PerformEffect_Dungeon(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	TPair<FEverdellCanPerformBeforePaymentEffectSignature, FEverdellPerformBeforePaymentEffectSignature> DelegatePair;
	DelegatePair.Key.BindLambda(&UEverdellCardEffects::CanPerformBeforePaymentEffect_Dungeon);
	DelegatePair.Value.BindLambda(&UEverdellCardEffects::PerformBeforePaymentEffect_Dungeon);

	PlayerState->BeforeCritterPaymentEventDelegates.Add(TPair<EEverdellCardType, int32>(EEverdellCardType::DUNGEON, CardLocation->LocationIndex), DelegatePair);
	PlayerState->BeforeConstructionPaymentEventDelegates.Add(TPair<EEverdellCardType, int32>(EEverdellCardType::DUNGEON, CardLocation->LocationIndex), DelegatePair);
	PlayerState->CheckIfPlayPossibleEventDelegates.Add(TPair<EEverdellCardType, int32>(EEverdellCardType::DUNGEON, CardLocation->LocationIndex), PlayerState->CheckIfPlayPossibleEffect.AddLambda(&UEverdellCardEffects::CanPlayUsingPaymentEffect_Dungeon));
}

int32 UEverdellCardEffects::PerformEffect_EverTree(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return PlayerState->City->CountOfCardCategory(EEverdellCardCategory::PURPLE_PROPERITY);
}

void UEverdellCardEffects::PerformEffect_Fairgrounds(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	GameState->GetCurrentPlayer()->AugmentAvailableCardDraw(2);
}

void UEverdellCardEffects::PerformEffect_Farm(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	GameState->GetCurrentPlayer()->AugmentBerryCount(1);
}

void UEverdellCardEffects::PerformEffect_GeneralStore(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AugmentBerryCount(1 + (PlayerState->CountOfCardInCity(EEverdellCardType::FARM) > 0 ? 1 : 0));
}

void UEverdellCardEffects::PerformEffect_Historian(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AfterConstructionEventDelegates.Add(TPair<EEverdellCardType, int32>(EEverdellCardType::HISTORIAN, CardLocation->LocationIndex), PlayerState->AfterConstructionEffect.AddLambda(&UEverdellCardEffects::AfterCritterOrConstructionEffect_Historian));
	PlayerState->AfterCritterEventDelegates.Add(TPair<EEverdellCardType, int32>(EEverdellCardType::HISTORIAN, CardLocation->LocationIndex), PlayerState->AfterCritterEffect.AddLambda(&UEverdellCardEffects::AfterCritterOrConstructionEffect_Historian));
}

void UEverdellCardEffects::PerformEffect_Husband(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	HUD->OpenChooseAnyPopup(1);
}

void UEverdellCardEffects::PerformActivatedEffect_Innkeeper(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	GameState->GetCurrentPlayer()->MoveToCityPayingExplicitCost(AssociatedCard, AssociatedCard->TwigCost, AssociatedCard->ResinCost, AssociatedCard->PebbleCost, UKismetMathLibrary::Max((static_cast<int32>(AssociatedCard->BerryCost) - 3), 0));
}

void UEverdellCardEffects::PerformTriggeredEffect_Innkeeper(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	TPair<FEverdellCanPerformBeforePaymentEffectSignature, FEverdellPerformBeforePaymentEffectSignature> DelegatePair;
	DelegatePair.Key.BindLambda(&UEverdellCardEffects::CanPerformBeforePaymentEffect_Innkeeper);
	DelegatePair.Value.BindLambda(&UEverdellCardEffects::PerformBeforePaymentEffect_Innkeeper);

	PlayerState->BeforeCritterPaymentEventDelegates.Add(TPair<EEverdellCardType, int32>(EEverdellCardType::INNKEEPER, CardLocation->LocationIndex), std::move(DelegatePair));
	PlayerState->CheckIfPlayPossibleEventDelegates.Add(TPair<EEverdellCardType, int32>(EEverdellCardType::INNKEEPER, CardLocation->LocationIndex), PlayerState->CheckIfPlayPossibleEffect.AddLambda(&UEverdellCardEffects::CanPlayUsingPaymentEffect_Innkeeper));
}

void UEverdellCardEffects::PerformEffect_Judge(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	TPair<FEverdellCanPerformBeforePaymentEffectSignature, FEverdellPerformBeforePaymentEffectSignature> DelegatePair;
	DelegatePair.Key.BindLambda(&UEverdellCardEffects::CanPerformBeforePaymentEffect_Judge);
	DelegatePair.Value.BindLambda(&UEverdellCardEffects::PerformBeforePaymentEffect_Judge);

	PlayerState->BeforeCritterPaymentEventDelegates.Add(TPair<EEverdellCardType, int32>(EEverdellCardType::JUDGE, CardLocation->LocationIndex), DelegatePair);
	PlayerState->BeforeConstructionPaymentEventDelegates.Add(TPair<EEverdellCardType, int32>(EEverdellCardType::JUDGE, CardLocation->LocationIndex), DelegatePair);
	PlayerState->CheckIfPlayPossibleEventDelegates.Add(TPair<EEverdellCardType, int32>(EEverdellCardType::JUDGE, CardLocation->LocationIndex), PlayerState->CheckIfPlayPossibleEffect.AddLambda(&UEverdellCardEffects::CanPlayUsingPaymentEffect_Judge));
}

void UEverdellCardEffects::PerformEffect_Mine(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	GameState->GetCurrentPlayer()->AugmentPebbleCount(1);
}

int32 UEverdellCardEffects::PerformEffect_Palace(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return PlayerState->City->CountOfCardWithProperties(false, true);
}

void UEverdellCardEffects::PerformEffect_Peddler(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	HUD->OpenExchangeResourcesPopup(2);
}

void UEverdellCardEffects::PerformEffect_PostalPigeon(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	if (GameState->Deck->CardsRemainingInDeck() > 0)
	{
		HUD->OpenPostalPigeonPopup();
	}
}

void UEverdellCardEffects::PerformEffect_Ranger(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	const auto DeployedWorkers = GameState->GetCurrentPlayer()->GetDeployedWorkers();
	if (!DeployedWorkers.IsEmpty())
	{
		if (DeployedWorkers.Num() > 1) HUD->OpenSelectWorkerPopup(DeployedWorkers);
		else
		{
			auto* PreviousLocation = DeployedWorkers[0]->DeployedLocation;
			PreviousLocation->RemoveWorker(DeployedWorkers[0]);
			GameState->GetCurrentPlayer()->AddWorker(DeployedWorkers[0], true);
			HUD->OpenMoveWorkerPopup(PreviousLocation);
		}
	}
}

void UEverdellCardEffects::PerformEffect_ResinRefinery(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	GameState->GetCurrentPlayer()->AugmentResinCount(1);
}

void UEverdellCardEffects::PerformEffect_Ruins(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	HUD->OpenRuinsDiscardPopup(AssociatedCard);
}

int32 UEverdellCardEffects::PerformEffect_School(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return PlayerState->City->CountOfCardWithProperties(true, false);
}

int32 UEverdellCardEffects::PerformEffect_ScurrbleChampion(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return (CardLocation->TotalCountCardsOfType(EEverdellCardType::SCURRBLE_CHAMPION) - 1) * 2;
}

void UEverdellCardEffects::PerformEffect_Shopkeeper(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AfterCritterEventDelegates.Add(TPair<EEverdellCardType, int32>(EEverdellCardType::SHOPKEEPER, CardLocation->LocationIndex), PlayerState->AfterCritterEffect.AddLambda(&UEverdellCardEffects::AfterCritterEffect_Shopkeeper));
}

void UEverdellCardEffects::PerformEffect_Storehouse(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	HUD->OpenStorehousePopup(AssociatedCard);
}

int32 UEverdellCardEffects::PerformEffect_Theater(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return PlayerState->City->CountOfCardWithProperties(true, true);
}

void UEverdellCardEffects::PerformEffect_TwigBarge(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	GameState->GetCurrentPlayer()->AugmentTwigCount(2);
}

void UEverdellCardEffects::PerformEffect_Wanderer(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	GameState->GetCurrentPlayer()->AugmentAvailableCardDraw(3);
}

int32 UEverdellCardEffects::PerformEffect_Wife(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard)
{
	return (CardLocation != nullptr && (
		(CardLocation->HasCard() && CardLocation->GetCard()->HasCard() && CardLocation->GetCard()->GetCard()->CardType == EEverdellCardType::HUSBAND) ||
		(CardLocation->HasSecondaryCards() && CardLocation->HasSecondaryCardOfType(EEverdellCardType::HUSBAND))
	)) ? 3 : 0;
}

void UEverdellCardEffects::PerformEffect_Woodcarver(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	FEverdellCheckValueSignature CheckValueDelegate;
	CheckValueDelegate.BindUObject(PlayerState, &AEverdellPlayerState::GetTwigCount);
	FEverdellValueAugmentSignature ValueAugmentDelegate;
	ValueAugmentDelegate.BindUObject(PlayerState, &AEverdellPlayerState::AugmentTwigCount);

	HUD->OpenResourceToPointsPopup(TEXT("Twig"), 0, 3, 1, std::move(CheckValueDelegate), std::move(ValueAugmentDelegate));
}

void UEverdellCardEffects::AfterConstructionEffect_Courthouse(AEverdellGameStateBase* GameState, AEverdellHUD* HUD)
{
	HUD->OpenChooseConstructionResourcePopup(1);
}

bool UEverdellCardEffects::CanPerformBeforePaymentEffect_Crane(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const UEverdellCard* AssociatedCard, const UEverdellCard* CardToDiscard)
{
	return CanPerformEffect_Crane(GameState, AssociatedCard);
}

void UEverdellCardEffects::PerformBeforePaymentEffect_Crane(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, UEverdellCard* AssociatedCard, UEverdellCard* CardToDiscard)
{
	HUD->OpenDiscardForEffectPopup(AssociatedCard, CardToDiscard);
}

bool UEverdellCardEffects::CanPerformBeforePaymentEffect_Dungeon(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const UEverdellCard* AssociatedCard, const UEverdellCard* DungeonCard)
{
	return CanPerformEffect_Dungeon(GameState, AssociatedCard);
}

void UEverdellCardEffects::PerformBeforePaymentEffect_Dungeon(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, UEverdellCard* AssociatedCard, UEverdellCard* DungeonCard)
{
	HUD->OpenDungeonDiscardPopup(DungeonCard, AssociatedCard);
}

void UEverdellCardEffects::AfterCritterOrConstructionEffect_Historian(AEverdellGameStateBase* GameState, AEverdellHUD* HUD)
{
	GameState->GetCurrentPlayer()->AugmentAvailableCardDraw(1);
}

bool UEverdellCardEffects::CanPerformBeforePaymentEffect_Innkeeper(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const UEverdellCard* AssociatedCard, const UEverdellCard* CardToDiscard)
{
	return CanPerformEffect_Innkeeper(GameState, AssociatedCard);
}

void UEverdellCardEffects::PerformBeforePaymentEffect_Innkeeper(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, UEverdellCard* AssociatedCard, UEverdellCard* CardToDiscard)
{
	HUD->OpenDiscardForEffectPopup(AssociatedCard, CardToDiscard);
}

bool UEverdellCardEffects::CanPerformBeforePaymentEffect_Judge(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const UEverdellCard* AssociatedCard, const UEverdellCard* CardToDiscard)
{
	return true;
}

void UEverdellCardEffects::PerformBeforePaymentEffect_Judge(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, UEverdellCard* AssociatedCard, UEverdellCard* CardToDiscard)
{
	HUD->OpenPaymentOptionsPopup(AssociatedCard);
}

void UEverdellCardEffects::AfterCritterEffect_Shopkeeper(AEverdellGameStateBase* GameState, AEverdellHUD* HUD)
{
	GameState->GetCurrentPlayer()->AugmentBerryCount(1);
}

void UEverdellCardEffects::CanPlayUsingPaymentEffect_Crane(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const UEverdellCard* AssociatedCard, UEverdellAtomicWrapper* AtomicData)
{
	// Exchanges the value of CanPlay with `true` atomically if the expression evaluates to `true`
	// This has the effect of an atomic ||=
	if (!AssociatedCard->bIsCritter)
	{
		bool Default = false, Test = CanPerformEffect_Crane(GameState, AssociatedCard);
		AtomicData->Condition.compare_exchange_strong(Default, Test);
	}
	--AtomicData->Counter;
}

void UEverdellCardEffects::CanPlayUsingPaymentEffect_Dungeon(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const UEverdellCard* AssociatedCard, UEverdellAtomicWrapper* AtomicData)
{
	// Exchanges the value of CanPlay with `true` atomically if the expression evaluates to `true`
	// This has the effect of an atomic ||=
	bool Default = false, Test = CanPerformEffect_Dungeon(GameState, AssociatedCard);
	AtomicData->Condition.compare_exchange_strong(Default, Test);
	--AtomicData->Counter;
}

void UEverdellCardEffects::CanPlayUsingPaymentEffect_Innkeeper(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const UEverdellCard* AssociatedCard, UEverdellAtomicWrapper* AtomicData)
{
	// Exchanges the value of CanPlay with `true` atomically if the expression evaluates to `true`
	// This has the effect of an atomic ||=
	if (AssociatedCard->bIsCritter)
	{
		bool Default = false, Test = CanPerformEffect_Innkeeper(GameState, AssociatedCard);
		AtomicData->Condition.compare_exchange_strong(Default, Test);
	}
	--AtomicData->Counter;
}

void UEverdellCardEffects::CanPlayUsingPaymentEffect_Judge(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const UEverdellCard* AssociatedCard, UEverdellAtomicWrapper* AtomicData)
{
	// Exchanges the value of CanPlay with `true` atomically if the expression evaluates to `true`
	// This has the effect of an atomic ||=
	bool Default = false, Test = AssociatedCard->IsPlayableWithSwappedCost(1, GameState);
	AtomicData->Condition.compare_exchange_strong(Default, Test);
	--AtomicData->Counter;
}

bool UEverdellCardEffects::CanPerformEffect_Crane(const AEverdellGameStateBase* GameState, const UEverdellCard* AssociatedCard)
{
	return AssociatedCard->IsPlayableWithReducedCost(3, GameState);
}

bool UEverdellCardEffects::CanPerformEffect_Dungeon(const AEverdellGameStateBase* GameState, const UEverdellCard* AssociatedCard)
{
	const AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	if (PlayerState->CountOfCrittersInCity() < 1 || !AssociatedCard->IsPlayableWithReducedCost(3, PlayerState)) return false;

	auto DungeonCards = PlayerState->GetCityCardsOfType(EEverdellCardType::DUNGEON);
	if (DungeonCards.Num() == 1 && DungeonCards.Top()->HasCard())
	{
		auto* DungeonCard = DungeonCards.Top()->GetCard();
		return (
			DungeonCard->ContainedCards.IsEmpty() ||
			(DungeonCard->ContainedCards.Num() == 1 && PlayerState->CountOfCardInCity(EEverdellCardType::RANGER) > 0)
		);
	}
	
	return false;
}

bool UEverdellCardEffects::CanPerformEffect_Innkeeper(const AEverdellGameStateBase* GameState, const UEverdellCard* AssociatedCard)
{
	return GameState->GetCurrentPlayer()->GetBerryCount() >= (static_cast<int32>(AssociatedCard->BerryCost) - 3);
}
