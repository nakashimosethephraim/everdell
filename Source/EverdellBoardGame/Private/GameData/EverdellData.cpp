#include "GameData/EverdellData.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "GameManagement/EverdellHUD.h"
#include "GameData/EverdellCard.h"
#include "Actors/EverdellCardLocationActor.h"
#include "Actors/EverdellWorkerLocation.h"
#include "GameData/EverdellCardEffects.h"
#include "GameData/EverdellLocationEffects.h"
#include "Actors/EverdellWorker.h"

extern TArray<FEverdellCardData> EverdellCardData = {
	{
		EEverdellCardCategory::PURPLE_PROPERITY,
		EEverdellCardType::ARCHITECT,
		TEXT("Architect"),
		TEXT("1 Point for each of your unused Resin and Pebbles, to a maximum of 6"),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		4,     // Berries
		2,     // Victory Points
		true,  // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardArchitect.EverdellPlayableCardArchitect'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardArchitect_Mat.EverdellPlayableCardArchitect_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectScoring,
		&UEverdellCardEffects::PerformEffect_Architect,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::TAN_TRAVELER,
		EEverdellCardType::BARD,
		TEXT("Bard"),
		TEXT("You may discard up to 5 Cards to gain 1 Point each."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		3,     // Berries
		0,     // Victory Points
		true,  // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardBard.EverdellPlayableCardBard'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardBard_Mat.EverdellPlayableCardBard_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay,
		&UEverdellCardEffects::PerformEffect_Bard,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::BARGE_TOAD,
		TEXT("Barge Toad"),
		TEXT("Gain 2 Twigs for each Farm in your city."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		2,     // Berries
		1,     // Victory Points
		true,  // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardBargeToad.EverdellPlayableCardBargeToad'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardBargeToad_Mat.EverdellPlayableCardBargeToad_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay,
		&UEverdellCardEffects::PerformEffect_BargeToad,
		&UEverdellCardEffects::CanPerformEffectGreenProduction,
		&UEverdellCardEffects::PerformEffect_BargeToad,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::PURPLE_PROPERITY,
		EEverdellCardType::CASTLE,
		TEXT("Castle"),
		TEXT("1 Point for each Common Construction in your city."),
		2,     // Twigs
		3,     // Resin
		4,     // Pebbles
		0,     // Berries
		4,     // Victory Points
		false, // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::KING},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardCastle.EverdellPlayableCardCastle'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardCastle_Mat.EverdellPlayableCardCastle_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectScoring,
		&UEverdellCardEffects::PerformEffect_Castle,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::CARNIVAL,
		TEXT("Carnival"),
		TEXT("Draw 1 Card for each other Green Production in your city or gain 1 Any for every 2 other Green Production in your city."),
		2,     // Twigs
		0,     // Resin
		1,     // Pebbles
		0,     // Berries
		1,     // Victory Points
		false, // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::JUGGLER},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardCarnival.EverdellPlayableCardCarnival'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardCarnival_Mat.EverdellPlayableCardCarnival_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay,
		&UEverdellCardEffects::PerformEffect_Carnival,
		&UEverdellCardEffects::CanPerformEffectGreenProduction,
		&UEverdellCardEffects::PerformEffect_Carnival,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::RED_DESTINATION,
		EEverdellCardType::CEMETERY,
		TEXT("Cemetery"),
		TEXT("Reveal 4 Cards from the deck or discard pile and play 1 for free. Discard the others."),
		0,     // Twigs
		0,     // Resin
		2,     // Pebbles
		0,     // Berries
		0,     // Victory Points
		false, // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		2,     // Location Count
		22,    // Location Y Value
		{EEverdellCardType::UNDERTAKER},
		EEverdellCardType::NONE,
		EEverdellCardType::UNDERTAKER,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardCemetery.EverdellPlayableCardCemetery'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardCemetery_Mat.EverdellPlayableCardCemetery_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellLocationEffects::CanPerformEffect_Cemetery,
		&UEverdellLocationEffects::PerformEffect_Cemetery,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::RED_DESTINATION,
		EEverdellCardType::CHAPEL,
		TEXT("Chapel"),
		TEXT("Place 1 Point on this Chapel, then draw 2 Cards for each Point on this Chapel."),
		2,     // Twigs
		1,     // Resin
		1,     // Pebbles
		0,     // Berries
		2,     // Victory Points
		false, // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		1,     // Location Count
		20,    // Location Y Value
		{EEverdellCardType::SHEPHERD},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardChapel.EverdellPlayableCardChapel'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardChapel_Mat.EverdellPlayableCardChapel_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellLocationEffects::CanPerformEffect_Chapel,
		&UEverdellLocationEffects::PerformEffect_Chapel,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::CHIP_SWEEP,
		TEXT("Chip Sweep"),
		TEXT("Activate 1 Green Production in your city."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		3,     // Berries
		2,     // Victory Points
		true,  // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardChipSweep.EverdellPlayableCardChipSweep'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardChipSweep_Mat.EverdellPlayableCardChipSweep_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay_ChipSweep,
		&UEverdellCardEffects::PerformEffect_ChipSweep,
		&UEverdellCardEffects::CanPerformEffectGreenProduction_ChipSweep,
		&UEverdellCardEffects::PerformEffect_ChipSweep,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::BLUE_GOVERNANCE,
		EEverdellCardType::CLOCK_TOWER,
		TEXT("Clock Tower"),
		TEXT("When played, place 3 Points here. At the beginning of Preparing for Season, uou may pay 1 Point from here to activate 1 of the Basic or Forest locations where you have a worker deployed."),
		3,     // Twigs
		0,     // Resin
		1,     // Pebbles
		0,     // Berries
		0,     // Victory Points
		false, // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::HISTORIAN},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardClockTower.EverdellPlayableCardClockTower'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardClockTower_Mat.EverdellPlayableCardClockTower_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::BLUE_GOVERNANCE,
		EEverdellCardType::COURTHOUSE,
		TEXT("Courthouse"),
		TEXT("Gain 1 Twig or 1 Resin or 1 Pebble after you play a Construction."),
		1,     // Twigs
		1,     // Resin
		2,     // Pebbles
		0,     // Berries
		2,     // Victory Points
		false, // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::JUDGE},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardCourthouse.EverdellPlayableCardCourthouse'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardCourthouse_Mat.EverdellPlayableCardCourthouse_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectTriggered,
		&UEverdellCardEffects::PerformEffect_Courthouse,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::BLUE_GOVERNANCE,
		EEverdellCardType::CRANE,
		TEXT("Crane"),
		TEXT("When playing a Construction, you may discard this Crane from your city to play that Construction for 3 fewer Any."),
		0,     // Twigs
		0,     // Resin
		1,     // Pebbles
		0,     // Berries
		2,     // Victory Points
		false, // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::ARCHITECT},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardCrane.EverdellPlayableCardCrane'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardCrane_Mat.EverdellPlayableCardCrane_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectTriggered,
		&UEverdellCardEffects::PerformTriggeredEffect_Crane,
		&UEverdellCardEffects::CanPerformActivatedEffect_Crane,
		&UEverdellCardEffects::PerformActivatedEffect_Crane,
		3
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::DOCTOR,
		TEXT("Doctor"),
		TEXT("You may pay up to 3 Berries to gain 1 Point each."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		4,     // Berries
		4,     // Victory Points
		true,  // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardDoctor.EverdellPlayableCardDoctor'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardDoctor_Mat.EverdellPlayableCardDoctor_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay,
		&UEverdellCardEffects::PerformEffect_Doctor,
		&UEverdellCardEffects::CanPerformEffectGreenProduction,
		&UEverdellCardEffects::PerformEffect_Doctor,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::BLUE_GOVERNANCE,
		EEverdellCardType::DUNGEON,
		TEXT("Dungeon"),
		TEXT("When playing a Construction or Critter, you may place a Critter from your city face-down beneath this Dungeon to decrease the cost by 3 Any."),
		0,     // Twigs
		1,     // Resin
		2,     // Pebbles
		0,     // Berries
		0,     // Victory Points
		false, // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::RANGER},
		EEverdellCardType::NONE,
		EEverdellCardType::RANGER,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardDungeon.EverdellPlayableCardDungeon'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardDungeon_Mat.EverdellPlayableCardDungeon_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectTriggered,
		&UEverdellCardEffects::PerformEffect_Dungeon,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::PURPLE_PROPERITY,
		EEverdellCardType::EVER_TREE,
		TEXT("Ever Tree"),
		TEXT("1 Point for each Purple Prosperity in your city."),
		3,     // Twigs
		3,     // Resin
		3,     // Pebbles
		0,     // Berries
		5,     // Victory Points
		false, // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::ANY},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardEverTree.EverdellPlayableCardEverTree'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardEverTree_Mat.EverdellPlayableCardEverTree_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectScoring,
		&UEverdellCardEffects::PerformEffect_EverTree,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::FAIRGROUNDS,
		TEXT("Fairgrounds"),
		TEXT("Draw 2 Cards."),
		1,     // Twigs
		2,     // Resin
		1,     // Pebbles
		0,     // Berries
		3,     // Victory Points
		false, // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::FOOL},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardFairgrounds.EverdellPlayableCardFairgrounds'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardFairgrounds_Mat.EverdellPlayableCardFairgrounds_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay,
		&UEverdellCardEffects::PerformEffect_Fairgrounds,
		&UEverdellCardEffects::CanPerformEffectGreenProduction,
		&UEverdellCardEffects::PerformEffect_Fairgrounds,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::FARM,
		TEXT("Farm"),
		TEXT("Gain 1 Berry."),
		2,     // Twigs
		1,     // Resin
		0,     // Pebbles
		0,     // Berries
		1,     // Victory Points
		false, // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::HUSBAND, EEverdellCardType::WIFE},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardFarm.EverdellPlayableCardFarm'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardFarm_Mat.EverdellPlayableCardFarm_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay,
		&UEverdellCardEffects::PerformEffect_Farm,
		&UEverdellCardEffects::CanPerformEffectGreenProduction,
		&UEverdellCardEffects::PerformEffect_Farm,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		8
	},
	{
		EEverdellCardCategory::TAN_TRAVELER,
		EEverdellCardType::FOOL,
		TEXT("Fool"),
		TEXT("Play this Fool into an empty space in an opponent's city."),
		0, // Twigs
		0, // Resin
		0, // Pebbles
		3, // Berries
		-1, // Victory Points
		true,  // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		true,  // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardFool.EverdellPlayableCardFool'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardFool_Mat.EverdellPlayableCardFool_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::BLUE_GOVERNANCE,
		EEverdellCardType::GAZETTE,
		TEXT("Gazette"),
		TEXT("Place 1 Point here each time you achieve an Event.\n\nIf there are at least 3 Points here at the end of the game, gain 3 more Points."),
		0,     // Twigs
		2,     // Resin
		2,     // Pebbles
		0,     // Berries
		3,     // Victory Points
		false, // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::TOWN_CRIER},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardGazette.EverdellPlayableCardGazette'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardGazette_Mat.EverdellPlayableCardGazette_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::GENERAL_STORE,
		TEXT("General Store"),
		TEXT("Gain 1 Berry. If you have a Farm in your city, gain 1 additional Berry."),
		0,     // Twigs
		1,     // Resin
		1,     // Pebbles
		0,     // Berries
		1,     // Victory Points
		false, // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::SHOPKEEPER},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardGeneralStore.EverdellPlayableCardGeneralStore'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardGeneralStore_Mat.EverdellPlayableCardGeneralStore_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay,
		&UEverdellCardEffects::PerformEffect_GeneralStore,
		&UEverdellCardEffects::CanPerformEffectGreenProduction,
		&UEverdellCardEffects::PerformEffect_GeneralStore,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::BLUE_GOVERNANCE,
		EEverdellCardType::HISTORIAN,
		TEXT("Historian"),
		TEXT("Draw 1 Card after you play a Critter or Construction."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		2,     // Berries
		1,     // Victory Points
		true,  // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardHistorian.EverdellPlayableCardHistorian'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardHistorian_Mat.EverdellPlayableCardHistorian_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectTriggered,
		&UEverdellCardEffects::PerformEffect_Historian,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::HUSBAND,
		TEXT("Husband"),
		TEXT("Gain 1 Any if paired with a Wife and you have at least 1 Farm in your city."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		3,     // Berries
		2,     // Victory Points
		true,  // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardHusband.EverdellPlayableCardHusband'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardHusband_Mat.EverdellPlayableCardHusband_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay_Husband,
		&UEverdellCardEffects::PerformEffect_Husband,
		&UEverdellCardEffects::CanPerformEffectGreenProduction_Husband,
		&UEverdellCardEffects::PerformEffect_Husband,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		4
	},
	{
		EEverdellCardCategory::RED_DESTINATION,
		EEverdellCardType::INN,
		TEXT("Inn"),
		TEXT("Play a Critter or Construction from the Meadow for 3 fewer Any."),
		2,     // Twigs
		1,     // Resin
		0,     // Pebbles
		0,     // Berries
		2,     // Victory Points
		false, // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		true,  // Open to Opponent
		1,     // Location Count
		20,    // Location Y Value
		{EEverdellCardType::INNKEEPER},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardInn.EverdellPlayableCardInn'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardInn_Mat.EverdellPlayableCardInn_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellLocationEffects::CanPerformEffect_Inn,
		&UEverdellLocationEffects::PerformEffect_Inn,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::BLUE_GOVERNANCE,
		EEverdellCardType::INNKEEPER,
		TEXT("Innkeeper"),
		TEXT("When playing a Critter, you may discard this Innkeeper from your city to decrease the cost by 3 Berries."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		1,     // Berries
		1,     // Victory Points
		true,  // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardInnkeeper.EverdellPlayableCardInnkeeper'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardInnkeeper_Mat.EverdellPlayableCardInnkeeper_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectTriggered,
		&UEverdellCardEffects::PerformTriggeredEffect_Innkeeper,
		&UEverdellCardEffects::CanPerformActivatedEffect_Innkeeper,
		&UEverdellCardEffects::PerformActivatedEffect_Innkeeper,
		3
	},
	{
		EEverdellCardCategory::BLUE_GOVERNANCE,
		EEverdellCardType::JUDGE,
		TEXT("Judge"),
		TEXT("When you play a Critter or Construction, you may replace 1 Any with 1 Any."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		3,     // Berries
		2,     // Victory Points
		true,  // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardJudge.EverdellPlayableCardJudge'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardJudge_Mat.EverdellPlayableCardJudge_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectTriggered,
		&UEverdellCardEffects::PerformEffect_Judge,
		nullptr,
		nullptr,
		4
	},
	{
		EEverdellCardCategory::TAN_TRAVELER,
		EEverdellCardType::JUGGLER,
		TEXT("Juggler"),
		TEXT("Pay 3 Twigs to gain 4 Points or Pay 1 Twig to reveal 1 Card from the deck as many times as you like. If the total base value of the revealed cards is 6 or more, gain 6 Points. Discard all revealed cards."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		2,     // Berries
		0,     // Victory Points
		true,  // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardJuggler.EverdellPlayableCardJuggler'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardJuggler_Mat.EverdellPlayableCardJuggler_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::PURPLE_PROPERITY,
		EEverdellCardType::KING,
		TEXT("King"),
		TEXT("1 Point for each basic Event you achieved.\n2 Points for each special Event you achieved."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		6,     // Berries
		4,     // Victory Points
		true,  // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardKing.EverdellPlayableCardKing'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardKing_Mat.EverdellPlayableCardKing_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::RED_DESTINATION,
		EEverdellCardType::LOOKOUT,
		TEXT("Lookout"),
		TEXT("Copy any Basic or Forest location."),
		1,     // Twigs
		1,     // Resin
		1,     // Pebbles
		0,     // Berries
		2,     // Victory Points
		false, // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		1,     // Location Count
		20,    // Location Y Value
		{EEverdellCardType::WANDERER},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardLookout.EverdellPlayableCardLookout'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardLookout_Mat.EverdellPlayableCardLookout_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellLocationEffects::CanPerformCardLocationEffectDefault,
		&UEverdellLocationEffects::PerformEffect_Lookout,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::MINE,
		TEXT("Mine"),
		TEXT("Gain 1 Pebble."),
		1,     // Twigs
		1,     // Resin
		1,     // Pebbles
		0,     // Berries
		2,     // Victory Points
		false, // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::MINER_MOLE},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardMine.EverdellPlayableCardMine'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardMine_Mat.EverdellPlayableCardMine_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay,
		&UEverdellCardEffects::PerformEffect_Mine,
		&UEverdellCardEffects::CanPerformEffectGreenProduction,
		&UEverdellCardEffects::PerformEffect_Mine,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::MINER_MOLE,
		TEXT("Miner Mole"),
		TEXT("Copy 1 Green Production in an opponent's city."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		3,     // Berries
		1,     // Victory Points
		true,  // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardMinerMole.EverdellPlayableCardMinerMole'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardMinerMole_Mat.EverdellPlayableCardMinerMole_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::RED_DESTINATION,
		EEverdellCardType::MONASTERY,
		TEXT("Monastery"),
		TEXT("Give 1 Any to an opponent and gain 4 Points."),
		1,     // Twigs
		1,     // Resin
		1,     // Pebbles
		0,     // Berries
		1,     // Victory Points
		false, // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		2,     // Location Count
		22,    // Location Y Value
		{EEverdellCardType::MONK},
		EEverdellCardType::NONE,
		EEverdellCardType::MONK,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardMonastery.EverdellPlayableCardMonastery'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardMonastery_Mat.EverdellPlayableCardMonastery_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::MONK,
		TEXT("Monk"),
		TEXT("You may give up to 2 Berries to an opponent to gain 2 Points each.\n\nUnlocks second Monastery location."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		1,     // Berries
		0,     // Victory Points
		true,  // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardMonk.EverdellPlayableCardMonk'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardMonk_Mat.EverdellPlayableCardMonk_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::PURPLE_PROPERITY,
		EEverdellCardType::PALACE,
		TEXT("Palace"),
		TEXT("1 Point for each Unique Construction in your city."),
		2,     // Twigs
		3,     // Resin
		3,     // Pebbles
		0,     // Berries
		4,     // Victory Points
		false, // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::QUEEN},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardPalace.EverdellPlayableCardPalace'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardPalace_Mat.EverdellPlayableCardPalace_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectScoring,
		&UEverdellCardEffects::PerformEffect_Palace,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::PEDDLER,
		TEXT("Peddler"),
		TEXT("You may pay up to 2 Any to gain an equal amount of Any."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		2,     // Berries
		1,     // Victory Points
		true,  // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardPeddler.EverdellPlayableCardPeddler'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardPeddler_Mat.EverdellPlayableCardPeddler_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay_Peddler,
		&UEverdellCardEffects::PerformEffect_Peddler,
		&UEverdellCardEffects::CanPerformEffectGreenProduction_Peddler,
		&UEverdellCardEffects::PerformEffect_Peddler,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::TAN_TRAVELER,
		EEverdellCardType::POSTAL_PIGEON,
		TEXT("Postal Pigeon"),
		TEXT("Reveal 2 Cards. You may play 1 worth up to 3 Points for free. Discard the other."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		2,     // Berries
		0,     // Victory Points
		true,  // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardPostalPigeon.EverdellPlayableCardPostalPigeon'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardPostalPigeon_Mat.EverdellPlayableCardPostalPigeon_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay,
		&UEverdellCardEffects::PerformEffect_PostalPigeon,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::RED_DESTINATION,
		EEverdellCardType::POST_OFFICE,
		TEXT("Post Office"),
		TEXT("Give an opponent 2 Cards, then discard any number of Cards and draw up to your hand limit."),
		1,     // Twigs
		2,     // Resin
		0,     // Pebbles
		0,     // Berries
		2,     // Victory Points
		false, // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		true,  // Open to Opponent
		1,     // Location Count
		20,    // Location Y Value
		{EEverdellCardType::POSTAL_PIGEON},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardPostOffice.EverdellPlayableCardPostOffice'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardPostOffice_Mat.EverdellPlayableCardPostOffice_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::RED_DESTINATION,
		EEverdellCardType::QUEEN,
		TEXT("Queen"),
		TEXT("Play a Card worth up 3 Points for free."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		5,     // Berries
		4,     // Victory Points
		true,  // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		1,     // Location Count
		20,    // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardQueen.EverdellPlayableCardQueen'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardQueen_Mat.EverdellPlayableCardQueen_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellLocationEffects::CanPerformEffect_Queen,
		&UEverdellLocationEffects::PerformEffect_Queen,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::TAN_TRAVELER,
		EEverdellCardType::RANGER,
		TEXT("Ranger"),
		TEXT("Move 1 of your deployed workers to a new location.\n\nUnlocks a second Dungeon slot."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		2,     // Berries
		1,     // Victory Points
		true,  // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardRanger.EverdellPlayableCardRanger'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardRanger_Mat.EverdellPlayableCardRanger_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay,
		&UEverdellCardEffects::PerformEffect_Ranger,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::RESIN_REFINERY,
		TEXT("Resin Refinery"),
		TEXT("Gain 1 Resin."),
		0,     // Twigs
		1,     // Resin
		1,     // Pebbles
		0,     // Berries
		1,     // Victory Points
		false, // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::CHIP_SWEEP},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardResinRefinery.EverdellPlayableCardResinRefinery'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardResinRefinery_Mat.EverdellPlayableCardResinRefinery_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay,
		&UEverdellCardEffects::PerformEffect_ResinRefinery,
		&UEverdellCardEffects::CanPerformEffectGreenProduction,
		&UEverdellCardEffects::PerformEffect_ResinRefinery,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::TAN_TRAVELER,
		EEverdellCardType::RUINS,
		TEXT("Ruins"),
		TEXT("Discard a Construction from your city. Gain resources equal to that Construction's cost and draw 2 Cards."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		0,     // Berries
		0,     // Victory Points
		false, // Is Critter
		false, // Is Unique
		true,  // Requires Space
		true,  // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::PEDDLER},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardRuins.EverdellPlayableCardRuins'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardRuins_Mat.EverdellPlayableCardRuins_Mat'"),
		&UEverdellCardEffects::CanPerformEffect_Ruins,
		&UEverdellCardEffects::PerformEffect_Ruins,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::PURPLE_PROPERITY,
		EEverdellCardType::SCHOOL,
		TEXT("School"),
		TEXT("1 Point for each Common Critter in your city."),
		2,     // Twigs
		2,     // Resin
		0,     // Pebbles
		0,     // Berries
		2,     // Victory Points
		false, // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::TEACHER},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardSchool.EverdellPlayableCardSchool'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardSchool_Mat.EverdellPlayableCardSchool_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectScoring,
		&UEverdellCardEffects::PerformEffect_School,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::PURPLE_PROPERITY,
		EEverdellCardType::SCURRBLE_CHAMPION,
		TEXT("Scurrble Champion"),
		TEXT("2 Points for each other Scurrble Champion in your city."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		2,     // Berries
		2,     // Victory Points
		true,  // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardScurrbleChampion.EverdellPlayableCardScurrbleChampion'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardScurrbleChampion_Mat.EverdellPlayableCardScurrbleChampion_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectScoring,
		&UEverdellCardEffects::PerformEffect_ScurrbleChampion,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::RED_DESTINATION,
		EEverdellCardType::SCURRBLE_STADIUM,
		TEXT("Scurrble Stadium"),
		TEXT("Name a non-green card color and draw 4 Cards. If any of the drawn Cards match the named color, gain 3 Points."),
		1,     // Twigs
		1,     // Resin
		1,     // Pebbles
		0,     // Berries
		2,     // Victory Points
		false, // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		true,  // Open to Opponent
		1,     // Location Count
		20,    // Location Y Value
		{EEverdellCardType::SCURRBLE_CHAMPION},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardScurrbleStadium.EverdellPlayableCardScurrbleStadium'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardScurrbleStadium_Mat.EverdellPlayableCardScurrbleStadium_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::TAN_TRAVELER,
		EEverdellCardType::SHEPHERD,
		TEXT("Shepherd"),
		TEXT("Gain 3 Berries, then gain 1 Point for each Point on your Chapel."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		3,     // Berries
		1,     // Victory Points
		true,  // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		true,  // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardShepherd.EverdellPlayableCardShepherd'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardShepherd_Mat.EverdellPlayableCardShepherd_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::BLUE_GOVERNANCE,
		EEverdellCardType::SHOPKEEPER,
		TEXT("Shopkeeper"),
		TEXT("Gain 1 Berry after you play a Critter."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		2,     // Berries
		1,     // Victory Points
		true,  // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardShopkeeper.EverdellPlayableCardShopkeeper'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardShopkeeper_Mat.EverdellPlayableCardShopkeeper_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectTriggered,
		&UEverdellCardEffects::PerformEffect_Shopkeeper,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::STOREHOUSE,
		TEXT("Storehouse"),
		TEXT("Place either 3 Twigs, 2 Resin, 1 Pebble, or 2 Berries on this Storehouse from the supply.\n\nTake all resources on this card."),
		1,     // Twigs
		1,     // Resin
		1,     // Pebbles
		0,     // Berries
		2,     // Victory Points
		false, // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		1,     // Location Count
		30,    // Location Y Value
		{EEverdellCardType::WOODCARVER},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardStorehouse.EverdellPlayableCardStorehouse'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardStorehouse_Mat.EverdellPlayableCardStorehouse_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay,
		&UEverdellCardEffects::PerformEffect_Storehouse,
		&UEverdellCardEffects::CanPerformEffectGreenProduction,
		&UEverdellCardEffects::PerformEffect_Storehouse,
		&UEverdellLocationEffects::CanPerformEffect_Storehouse,
		&UEverdellLocationEffects::PerformEffect_Storehouse,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::TEACHER,
		TEXT("Teacher"),
		TEXT("Draw 2 Cards, keep 1, and give the other to an opponent."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		2,     // Berries
		2,     // Victory Points
		true,  // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardTeacher.EverdellPlayableCardTeacher'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardTeacher_Mat.EverdellPlayableCardTeacher_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::PURPLE_PROPERITY,
		EEverdellCardType::THEATER,
		TEXT("Theater"),
		TEXT("1 Point for each Unique Critter in your city."),
		3,     // Twigs
		1,     // Resin
		1,     // Pebbles
		0,     // Berries
		3,     // Victory Points
		false, // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::BARD},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardTheater.EverdellPlayableCardTheater'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardTheater_Mat.EverdellPlayableCardTheater_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectScoring,
		&UEverdellCardEffects::PerformEffect_Theater,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::TOWN_CRIER,
		TEXT("Town Crier"),
		TEXT("You may give 2 Cards to an opponent. If you do, gain 3 Points."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		2,     // Berries
		2,     // Victory Points
		true,  // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardTownCrier.EverdellPlayableCardTownCrier'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardTownCrier_Mat.EverdellPlayableCardTownCrier_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::TWIG_BARGE,
		TEXT("Twig Barge"),
		TEXT("Gain 2 Twigs."),
		1,     // Twigs
		0,     // Resin
		1,     // Pebbles
		0,     // Berries
		1,     // Victory Points
		false, // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{EEverdellCardType::BARGE_TOAD},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardTwigBarge.EverdellPlayableCardTwigBarge'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardTwigBarge_Mat.EverdellPlayableCardTwigBarge_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay,
		&UEverdellCardEffects::PerformEffect_TwigBarge,
		&UEverdellCardEffects::CanPerformEffectGreenProduction,
		&UEverdellCardEffects::PerformEffect_TwigBarge,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::TAN_TRAVELER,
		EEverdellCardType::UNDERTAKER,
		TEXT("Undertaker"),
		TEXT("Discard 3 Cards from the Meadow, replenish, then draw 1 Card from the Meadow.\n\nUnlocks second Cemetery location."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		2,     // Berries
		1,     // Victory Points
		true,  // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardUndertaker.EverdellPlayableCardUndertaker'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardUndertaker_Mat.EverdellPlayableCardUndertaker_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::RED_DESTINATION,
		EEverdellCardType::UNIVERSITY,
		TEXT("University"),
		TEXT("Discard a Critter or Construction from your city. Gain resources equal to that card's cost, then gain 1 Any and gain 1 Point."),
		0,     // Twigs
		1,     // Resin
		2,     // Pebbles
		0,     // Berries
		3,     // Victory Points
		false, // Is Critter
		true,  // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		1,     // Location Count
		20,    // Location Y Value
		{EEverdellCardType::DOCTOR},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardUniversity.EverdellPlayableCardUniversity'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardUniversity_Mat.EverdellPlayableCardUniversity_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellLocationEffects::CanPerformEffect_University,
		&UEverdellLocationEffects::PerformEffect_University,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		2
	},
	{
		EEverdellCardCategory::TAN_TRAVELER,
		EEverdellCardType::WANDERER,
		TEXT("Wanderer"),
		TEXT("Draw 3 Cards."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		2,     // Berries
		1,     // Victory Points
		true,  // Is Critter
		false, // Is Unique
		false, // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardWanderer.EverdellPlayableCardWanderer'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardWanderer_Mat.EverdellPlayableCardWanderer_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay,
		&UEverdellCardEffects::PerformEffect_Wanderer,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	},
	{
		EEverdellCardCategory::PURPLE_PROPERITY,
		EEverdellCardType::WIFE,
		TEXT("Wife"),
		TEXT("3 Points if paired with a Husband."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		2,     // Berries
		2,     // Victory Points
		true,  // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardWife.EverdellPlayableCardWife'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardWife_Mat.EverdellPlayableCardWife_Mat'"),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectScoring,
		&UEverdellCardEffects::PerformEffect_Wife,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		4
	},
	{
		EEverdellCardCategory::GREEN_PRODUCTION,
		EEverdellCardType::WOODCARVER,
		TEXT("Woodcarver"),
		TEXT("You may pay up to 3 Twigs to gain 1 Point each."),
		0,     // Twigs
		0,     // Resin
		0,     // Pebbles
		2,     // Berries
		2,     // Victory Points
		true,  // Is Critter
		false, // Is Unique
		true,  // Requires Space
		false, // Discards Before Play
		false, // Pay to Opponent
		false, // Play to Opponent City
		false, // Open to Opponent
		0,     // Location Count
		0,     // Location Y Value
		{},
		EEverdellCardType::NONE,
		EEverdellCardType::NONE,
		TEXT("Texture2D'/Game/EverdellAssets/EverdellPlayableCardWoodcarver.EverdellPlayableCardWoodcarver'"),
		TEXT("Material'/Game/EverdellAssets/EverdellPlayableCardWoodcarver_Mat.EverdellPlayableCardWoodcarver_Mat'"),
		nullptr,
		nullptr,
		&UEverdellCardEffects::CanPerformEffectOnPlay,
		&UEverdellCardEffects::PerformEffect_Woodcarver,
		&UEverdellCardEffects::CanPerformEffectGreenProduction,
		&UEverdellCardEffects::PerformEffect_Woodcarver,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		3
	}
};

extern TArray<FEverdellForestLocationData> EverdellForestLocationData = {
	{
		EEverdellForestLocationType::ONETWIG_ONERESIN_ONEBERRY,
		TEXT("Gain 1 Twig, 1 Resin, 1 Berry"),
		TEXT("Material'/Game/EverdellAssets/EverdellForestLocation1Twig1Resin1Berry_Mat.EverdellForestLocation1Twig1Resin1Berry_Mat'"),
		&UEverdellLocationEffects::CanPerformEffectDefault,
		&UEverdellLocationEffects::PerformEffectForestLocation1Twig1Resin1Berry
	},
	{
		EEverdellForestLocationType::TWOANY,
		TEXT("Gain 2 Any"),
		TEXT("Material'/Game/EverdellAssets/EverdellForestLocation2Any_Mat.EverdellForestLocation2Any_Mat'"),
		&UEverdellLocationEffects::CanPerformEffectDefault,
		&UEverdellLocationEffects::PerformEffectForestLocation2Any
	},
	{
		EEverdellForestLocationType::TWOBERRIES_DRAWONECARD,
		TEXT("Gain 2 Berries & Draw 1 Card"),
		TEXT("Material'/Game/EverdellAssets/EverdellForestLocation2BerriesDraw1Card_Mat.EverdellForestLocation2BerriesDraw1Card_Mat'"),
		&UEverdellLocationEffects::CanPerformEffectDefault,
		&UEverdellLocationEffects::PerformEffectForestLocation2BerriesDraw1Card
	},
	{
		EEverdellForestLocationType::TWORESIN_ONETWIG,
		TEXT("Gain 2 Resin, 1 Twig"),
		TEXT("Material'/Game/EverdellAssets/EverdellForestLocation2Resin1Twig_Mat.EverdellForestLocation2Resin1Twig_Mat'"),
		&UEverdellLocationEffects::CanPerformEffectDefault,
		&UEverdellLocationEffects::PerformEffectForestLocation2Resin1Twig
	},
	{
		EEverdellForestLocationType::THREEBERRIES,
		TEXT("Gain 3 Berries"),
		TEXT("Material'/Game/EverdellAssets/EverdellForestLocation3Berries_Mat.EverdellForestLocation3Berries_Mat'"),
		&UEverdellLocationEffects::CanPerformEffectDefault,
		&UEverdellLocationEffects::PerformEffectForestLocation3Berries
	},
	{
		EEverdellForestLocationType::COPYBASIC_DRAWONECARD,
		TEXT("Copy Basic Location & Draw 1 Card"),
		TEXT("Material'/Game/EverdellAssets/EverdellForestLocationCopyBasicDraw1Card_Mat.EverdellForestLocationCopyBasicDraw1Card_Mat'"),
		&UEverdellLocationEffects::CanPerformEffectDefault,
		&UEverdellLocationEffects::PerformEffectForestLocationCopyBasicDraw1Card
	},
	{
		EEverdellForestLocationType::DISCARDNCARDS_DRAW2NCARDS,
		TEXT("Discard N Cards & Draw 2N Cards"),
		TEXT("Material'/Game/EverdellAssets/EverdellForestLocationDiscardNCardsDraw2NCards_Mat.EverdellForestLocationDiscardNCardsDraw2NCards_Mat'"),
		&UEverdellLocationEffects::CanPerformEffectDefault,
		&UEverdellLocationEffects::PerformEffectForestLocationDiscardNCardsDraw2NCards
	},
	{
		EEverdellForestLocationType::DISCARDNCARDS_GAINNANY,
		TEXT("Discard N Cards & Gain N Any"),
		TEXT("Material'/Game/EverdellAssets/EverdellForestLocationDiscardNCardsGainNAny_Mat.EverdellForestLocationDiscardNCardsGainNAny_Mat'"),
		&UEverdellLocationEffects::CanPerformEffectDefault,
		&UEverdellLocationEffects::PerformEffectForestLocationDiscardNCardsGainNAny
	},
	{
		EEverdellForestLocationType::DRAWTWOCARDS_ONEANY,
		TEXT("Draw 2 Cards & Gain 1 Any"),
		TEXT("Material'/Game/EverdellAssets/EverdellForestLocationDraw2Cards1Any_Mat.EverdellForestLocationDraw2Cards1Any_Mat'"),
		&UEverdellLocationEffects::CanPerformEffectDefault,
		&UEverdellLocationEffects::PerformEffectForestLocationDraw2Cards1Any
	},
	{
		EEverdellForestLocationType::DRAWTWOMEADOWCARDS_PLAYFORONELESSANY,
		TEXT("Draw 2 Meadow Cards & Play 1 for 1 Less Any"),
		TEXT("Material'/Game/EverdellAssets/EverdellForestLocationDraw2MeadowCardsPlayFor1LessAny_Mat.EverdellForestLocationDraw2MeadowCardsPlayFor1LessAny_Mat'"),
		&UEverdellLocationEffects::CanPerformEffectDefault,
		&UEverdellLocationEffects::PerformEffectForestLocationDraw2MeadowCardsPlay1For1LessAny
	},
	{
		EEverdellForestLocationType::DRAWTHREECARDS_ONEPEBBLE,
		TEXT("Draw 3 Cards & Gain 1 Pebble"),
		TEXT("Material'/Game/EverdellAssets/EverdellForestLocationDraw3Cards1Pebble_Mat.EverdellForestLocationDraw3Cards1Pebble_Mat'"),
		&UEverdellLocationEffects::CanPerformEffectDefault,
		&UEverdellLocationEffects::PerformEffectForestLocationDraw3Cards1Pebble
	}
};
