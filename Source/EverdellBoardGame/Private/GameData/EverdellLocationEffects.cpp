#include "GameData/EverdellLocationEffects.h"
#include "GameManagement/EverdellBoardGame.h"
#include "GameManagement/EverdellPlayerState.h"
#include "GameManagement/EverdellGameStateBase.h"
#include "GameManagement/EverdellHUD.h"
#include "Actors/EverdellWorkerLocation.h"
#include "Actors/EverdellWorker.h"
#include "Actors/EverdellCardWorkerLocation.h"
#include "Actors/EverdellLocationGroup.h"
#include "Actors/Everdell3DCard.h"
#include "Actors/EverdellDeckActor.h"
#include "Actors/EverdellDiscardPile.h"
#include "GameData/EverdellCard.h"
#include "GameData/EverdellCardLocationEnum.h"

bool UEverdellLocationEffects::CanPerformEffectDefault(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	return Location->CanAcceptWorkers() && (!bShouldConsumeWorker || GameState->GetCurrentPlayer()->HasAvailableWorker());
}

bool UEverdellLocationEffects::CanPerformCardLocationEffectDefault(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	const AEverdellCardWorkerLocation* CardWorkerLocation = static_cast<const AEverdellCardWorkerLocation*>(Location);
	return CanPerformEffectDefault(GameState, HUD, Location, bShouldConsumeWorker) && CardWorkerLocation->HasCard() && (
		CardWorkerLocation->GetCard()->CardLocation == EEverdellCardLocation::CITY ||
		(CardWorkerLocation->GetCard()->CardLocation == EEverdellCardLocation::OPPONENT_CITY && CardWorkerLocation->GetCard()->bOpenToOpponent)
	);
}

bool UEverdellLocationEffects::CanPerformEffect_Cemetery(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	if (CanPerformCardLocationEffectDefault(GameState, HUD, Location, bShouldConsumeWorker) && (GameState->Deck->HasCardsRemainingInDeck() || GameState->DiscardPile->HasCards()))
	{
		return !static_cast<const AEverdellCardWorkerLocation*>(Location)->bIsSecondLocation || GameState->GetCurrentPlayer()->CountOfCardInCity(EEverdellCardType::UNDERTAKER) > 0;
	}

	return false;
}

bool UEverdellLocationEffects::CanPerformEffect_Chapel(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	return CanPerformCardLocationEffectDefault(GameState, HUD, Location, bShouldConsumeWorker) && GameState->GetCurrentPlayer()->GetPointCount() > 0;
}

bool UEverdellLocationEffects::CanPerformEffect_Inn(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	if (CanPerformCardLocationEffectDefault(GameState, HUD, Location, bShouldConsumeWorker))
	{
		TArray<UEverdellCard*> PossibleCards;
		return FindPossibleInnCardsFromMeadow(GameState, 3, PossibleCards);
	}
	
	return false;
}

bool UEverdellLocationEffects::CanPerformEffect_Queen(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	if (CanPerformCardLocationEffectDefault(GameState, HUD, Location, bShouldConsumeWorker))
	{
		TArray<UEverdellCard*> PossibleCards;
		return FindPossibleQueenCardsFromHand(GameState, 3, PossibleCards);
	}

	return false;
}

bool UEverdellLocationEffects::CanPerformEffect_Storehouse(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	const AEverdellCardWorkerLocation* CardWorkerLocation = static_cast<const AEverdellCardWorkerLocation*>(Location);
	return CanPerformCardLocationEffectDefault(GameState, HUD, Location, bShouldConsumeWorker) && CardWorkerLocation->GetCard()->HasContainedResources();
}

bool UEverdellLocationEffects::CanPerformEffect_University(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	const AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	return CanPerformCardLocationEffectDefault(GameState, HUD, Location, bShouldConsumeWorker) && (PlayerState->CountOfConstructionsInCity() > 1 || PlayerState->CountOfCrittersInCity() > 0);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectDefault(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	return HandleWorker(GameState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectBasicLocation1Berry(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AugmentBerryCount(1);
	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectBasicLocation1BerryDraw1Card(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AugmentBerryCount(1);
	PlayerState->AugmentAvailableCardDraw(1);
	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectBasicLocation1Pebble(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AugmentPebbleCount(1);
	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectBasicLocationDraw2Cards1Point(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AugmentAvailableCardDraw(2);
	PlayerState->AugmentPointCount(1);
	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectBasicLocation1ResinDraw1Card(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AugmentResinCount(1);
	PlayerState->AugmentAvailableCardDraw(1);
	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectBasicLocation2Resin(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AugmentResinCount(2);
	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectBasicLocation2TwigsDraw1Card(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AugmentTwigCount(2);
	PlayerState->AugmentAvailableCardDraw(1);
	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectBasicLocation3Twigs(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AugmentTwigCount(3);
	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectForestLocation1Twig1Resin1Berry(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AugmentResinCount(1);
	PlayerState->AugmentTwigCount(1);
	PlayerState->AugmentBerryCount(1);
	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectForestLocation2Any(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	HUD->OpenChooseAnyPopup(2);
	return HandleWorker(GameState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectForestLocation2BerriesDraw1Card(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AugmentBerryCount(2);
	PlayerState->AugmentAvailableCardDraw(1);
	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectForestLocation2Resin1Twig(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AugmentResinCount(2);
	PlayerState->AugmentTwigCount(1);
	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectForestLocation3Berries(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AugmentBerryCount(3);
	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectForestLocationCopyBasicDraw1Card(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	HUD->OpenCopyLocationPopup(false);
	PlayerState->AugmentAvailableCardDraw(1);
	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectForestLocationDiscardNCardsDraw2NCards(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	FEverdellAfterDiscardSignature AfterDiscardDelegate;
	AfterDiscardDelegate.BindUObject(PlayerState, &AEverdellPlayerState::AugmentAvailableCardDraw);

	HUD->OpenDiscardNCardsPopup(8, 2, std::move(AfterDiscardDelegate));
	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectForestLocationDiscardNCardsGainNAny(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	FEverdellAfterDiscardSignature AfterDiscardDelegate;
	AfterDiscardDelegate.BindUObject(HUD, &AEverdellHUD::OpenChooseAnyPopup);

	HUD->OpenDiscardNCardsPopup(3, 1, std::move(AfterDiscardDelegate));
	return HandleWorker(GameState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectForestLocationDraw2Cards1Any(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	HUD->OpenChooseAnyPopup(1);
	PlayerState->AugmentAvailableCardDraw(1);
	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectForestLocationDraw2MeadowCardsPlay1For1LessAny(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	FEverdellAfterCardChoiceSignature AfterCardChoiceDelegate;
	AfterCardChoiceDelegate.BindUObject(HUD, &AEverdellHUD::OpenPlayForNLessPopup);
	FEverdellAfterMeadowDrawSignature AfterMeadowDrawDelegate;
	AfterMeadowDrawDelegate.BindUObject(HUD, &AEverdellHUD::OpenChooseCardPopupWithBoundDelegate);

	HUD->OpenDrawFromMeadowPopup(1, 2, std::move(AfterCardChoiceDelegate), std::move(AfterMeadowDrawDelegate));
	return HandleWorker(GameState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffectForestLocationDraw3Cards1Pebble(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	PlayerState->AugmentAvailableCardDraw(3);
	PlayerState->AugmentPebbleCount(1);
	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffect_Cemetery(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	HUD->OpenRevealChoicePopup(4);
	return HandleWorker(GameState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffect_Chapel(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	AEverdellCardWorkerLocation* CardWorkerLocation = static_cast<AEverdellCardWorkerLocation*>(Location);
	PlayerState->SetPointCount(PlayerState->GetPointCount() - 1);
	CardWorkerLocation->GetCard()->ContainedVictoryPoints += 1;
	PlayerState->AugmentAvailableCardDraw(2 * CardWorkerLocation->GetCard()->ContainedVictoryPoints);
	PlayerState->UpdateScore();
	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffect_Inn(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	TArray<UEverdellCard*> PossibleCards;
	FindPossibleInnCardsFromMeadow(GameState, 3, PossibleCards);

	FEverdellAfterCardChoiceSignature AfterCardChoiceDelegate;
	AfterCardChoiceDelegate.BindUObject(HUD, &AEverdellHUD::OpenPlayForNLessPopup);

	HUD->OpenChooseCardPopup(3, PossibleCards, std::move(AfterCardChoiceDelegate));
	return HandleWorker(GameState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffect_Lookout(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	HUD->OpenCopyLocationPopup(true);
	return HandleWorker(GameState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffect_Queen(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	TArray<UEverdellCard*> PossibleCards;
	FindPossibleQueenCardsFromHand(GameState, 3, PossibleCards);

	HUD->OpenChooseFreeCardPopup(3, PossibleCards);
	return HandleWorker(GameState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffect_Storehouse(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	AEverdellCardWorkerLocation* CardWorkerLocation = static_cast<AEverdellCardWorkerLocation*>(Location);

	if (CardWorkerLocation->GetCard()->ContainedTwigs > 0)
	{
		PlayerState->AugmentTwigCount(CardWorkerLocation->GetCard()->ContainedTwigs);
		CardWorkerLocation->GetCard()->ContainedTwigs = 0;
	}
	if (CardWorkerLocation->GetCard()->ContainedResin > 0)
	{
		PlayerState->AugmentResinCount(CardWorkerLocation->GetCard()->ContainedResin);
		CardWorkerLocation->GetCard()->ContainedResin = 0;
	}
	if (CardWorkerLocation->GetCard()->ContainedPebbles > 0)
	{
		PlayerState->AugmentPebbleCount(CardWorkerLocation->GetCard()->ContainedPebbles);
		CardWorkerLocation->GetCard()->ContainedPebbles = 0;
	}
	if (CardWorkerLocation->GetCard()->ContainedBerries > 0)
	{
		PlayerState->AugmentBerryCount(CardWorkerLocation->GetCard()->ContainedBerries);
		CardWorkerLocation->GetCard()->ContainedBerries = 0;
	}

	return HandleWorker(PlayerState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::PerformEffect_University(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker)
{
	AEverdellCardWorkerLocation* CardWorkerLocation = static_cast<AEverdellCardWorkerLocation*>(Location);
	HUD->OpenUniversityDiscardPopup(CardWorkerLocation->GetCard());
	return HandleWorker(GameState, bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::HandleWorker(AEverdellGameStateBase* GameState, bool bShouldConsumeWorker)
{
	return HandleWorker(GameState->GetCurrentPlayer(), bShouldConsumeWorker);
}

AEverdellWorker* UEverdellLocationEffects::HandleWorker(AEverdellPlayerState* PlayerState, const bool bShouldConsumeWorker)
{
	return bShouldConsumeWorker ? PlayerState->GetNextAvailableWorker() : nullptr;
}

bool UEverdellLocationEffects::FindPossibleInnCardsFromMeadow(const AEverdellGameStateBase* GameState, const int32 Reduction, TArray<UEverdellCard*>& PossibleCards)
{
	const AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	bool FoundValidCard = false;
	for (int32 Index = 0; Index < GameState->Meadow->GetGroupSize(); ++Index)
	{
		AEverdell3DCard* MeadowCard = GameState->Meadow->GetCard(Index);
		UEverdellCard* Card = MeadowCard != nullptr ? MeadowCard->GetCard() : nullptr;
		if (Card != nullptr && (!Card->bIsUnique || PlayerState->CountOfCardInCity(Card->CardType) == 0) && Card->IsPlayableWithReducedCost(Reduction, PlayerState))
		{
			PossibleCards.Add(Card);
			FoundValidCard = true;
		}
		else PossibleCards.Add(nullptr);
	}

	return FoundValidCard;
}

bool UEverdellLocationEffects::FindPossibleQueenCardsFromHand(const AEverdellGameStateBase* GameState, const int32 MaxVictoryPoints, TArray<UEverdellCard*>& PossibleCards)
{
	const AEverdellPlayerState* PlayerState = GameState->GetCurrentPlayer();
	bool FoundValidCard = false;
	for (int32 Index = 0; Index < PlayerState->GetHandCardLimit(); ++Index)
	{
		UEverdellCard* Card = PlayerState->GetHandCard(Index);
		if (Card != nullptr && (!Card->bIsUnique || PlayerState->CountOfCardInCity(Card->CardType) == 0) && Card->VictoryPoints <= MaxVictoryPoints)
		{
			PossibleCards.Add(Card);
			FoundValidCard = true;
		}
		else PossibleCards.Add(nullptr);
	}

	return FoundValidCard;
}
