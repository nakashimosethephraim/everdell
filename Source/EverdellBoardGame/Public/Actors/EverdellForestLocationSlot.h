#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EverdellForestLocationSlot.generated.h"

class AEverdellForestLocation;

UCLASS(Blueprintable, BlueprintType)
class EVERDELLBOARDGAME_API AEverdellForestLocationSlot : public AActor
{
	GENERATED_BODY()

public:
	AEverdellForestLocationSlot();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* Position;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	AEverdellForestLocation* ForestLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	int32 SlotIndex;

	UFUNCTION()
	bool HasLocation() const;

	UFUNCTION()
	void SetLocation(AEverdellForestLocation* NewLocation);
};
