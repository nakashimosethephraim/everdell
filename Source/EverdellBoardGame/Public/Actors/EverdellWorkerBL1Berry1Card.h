#pragma once

#include "CoreMinimal.h"
#include "Actors/EverdellWorkerLocation.h"
#include "EverdellWorkerBL1Berry1Card.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class EVERDELLBOARDGAME_API AEverdellWorkerBL1Berry1Card : public AEverdellWorkerLocation
{
	GENERATED_BODY()

public:
	AEverdellWorkerBL1Berry1Card();

	virtual FName GetLocationIdentifier() const override { return TEXT("Gain 1 Berry & Draw 1 Card"); }
};
