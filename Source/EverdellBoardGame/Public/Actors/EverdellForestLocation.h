#pragma once

#include "CoreMinimal.h"
#include "Actors/EverdellWorkerLocation.h"
#include "GameData/EverdellData.h"
#include "GameData/EverdellForestLocationEnum.h"
#include "EverdellForestLocation.generated.h"

class UStaticMeshComponent;

/**
 * 
 */
UCLASS()
class EVERDELLBOARDGAME_API AEverdellForestLocation : public AEverdellWorkerLocation
{
	GENERATED_BODY()

	/** Meshes to display both front and back of card */
	UPROPERTY()
	UStaticMeshComponent* CardFront;
	UPROPERTY()
	UStaticMeshComponent* CardBack;

	/** Materials to display on front and back of card */
	UPROPERTY()
	UMaterialInterface* FrontMaterial;
	UPROPERTY()
	UMaterialInterface* BackMaterial;

	UPROPERTY()
	EEverdellForestLocationType LocationType;

	UPROPERTY()
	FName LocationName;

public:
	AEverdellForestLocation();

	UFUNCTION()
	void BuildFromData(const FEverdellForestLocationData& LocationData);

	virtual FName GetLocationIdentifier() const override { return LocationName; }
};
