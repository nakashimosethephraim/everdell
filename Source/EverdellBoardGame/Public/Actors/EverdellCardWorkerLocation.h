#pragma once

#include "CoreMinimal.h"
#include "Actors/EverdellWorkerLocation.h"
#include "EverdellCardWorkerLocation.generated.h"

class UEverdellCard;

/**
 * 
 */
UCLASS()
class EVERDELLBOARDGAME_API AEverdellCardWorkerLocation : public AEverdellWorkerLocation
{
	GENERATED_BODY()

	UPROPERTY()
	UEverdellCard* Card;

public:
	AEverdellCardWorkerLocation();

	UPROPERTY()
	uint8 bIsSecondLocation : 1;

	UFUNCTION()
	bool HasCard() const;

	UFUNCTION()
	void SetCard(UEverdellCard* NewCard);

	UFUNCTION()
	UEverdellCard* GetCard() const;

	UFUNCTION()
	UEverdellCard* ResetCard();

	virtual bool IsInteractable() const override;
	virtual FName GetLocationIdentifier() const override;
};
