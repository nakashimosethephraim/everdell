#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EverdellWorkerSlot.generated.h"

class AEverdellWorker;

UCLASS(Blueprintable, BlueprintType)
class EVERDELLBOARDGAME_API AEverdellWorkerSlot : public AActor
{
	GENERATED_BODY()

public:
	AEverdellWorkerSlot();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* Position;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	AEverdellWorker* Worker;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	int32 SlotIndex;

	UFUNCTION()
	bool HasWorker() const;

	UFUNCTION()
	void SetWorker(AEverdellWorker* NewWorker);

	UFUNCTION()
	AEverdellWorker* ResetWorker();
};
