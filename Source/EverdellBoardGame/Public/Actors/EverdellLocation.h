#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EverdellLocation.generated.h"

class UNiagaraSystem;
class UNiagaraComponent;

UCLASS(Blueprintable, BlueprintType)
class EVERDELLBOARDGAME_API AEverdellLocation : public AActor
{
	GENERATED_BODY()
	
public:
	AEverdellLocation();

protected:
	UPROPERTY()
	UNiagaraSystem* LocationMouseOverEffect;

	UPROPERTY()
	UNiagaraComponent* SpawnedMouseOverEffect;

	UPROPERTY()
	UNiagaraSystem* LocationClickEffect;

	UPROPERTY()
	UNiagaraComponent* SpawnedClickEffect;

	virtual void BeginPlay() override;

public:
	UFUNCTION()
	virtual bool IsInteractable() const { return true; }

	UFUNCTION()
	virtual FName GetLocationIdentifier() const { return TEXT("Everdell Location"); }

	UFUNCTION()
	virtual void OnBeginMouseOver(UPrimitiveComponent* TouchedComponent);

	UFUNCTION()
	virtual void OnEndMouseOver(UPrimitiveComponent* TouchedComponent);

	UFUNCTION()
	virtual void OnMouseClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed);

	UFUNCTION()
	virtual void OnMouseReleased(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed);
};
