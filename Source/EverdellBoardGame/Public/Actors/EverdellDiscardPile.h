#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EverdellDiscardPile.generated.h"

class UEverdellCard;

UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API AEverdellDiscardPile : public AActor
{
	GENERATED_BODY()

	UPROPERTY()
	UMaterialInterface* PileMaterial;

	UPROPERTY()
	UMaterialInterface* CardBackMaterial;

	UPROPERTY()
	TArray<UEverdellCard*> DiscardedCards;
	
public:
	AEverdellDiscardPile();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* Collision;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* TopCard;

	UFUNCTION()
	bool HasCards() const;

	UFUNCTION()
	int32 CardCount() const;

	UFUNCTION()
	void PushCard(UEverdellCard* Card);

	UFUNCTION()
	UEverdellCard* PopCard();
};
