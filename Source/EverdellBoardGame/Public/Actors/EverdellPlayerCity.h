#pragma once

#include "CoreMinimal.h"
#include "EverdellLocationGroup.h"
#include "GameData/EverdellCardTypeEnum.h"
#include "EverdellPlayerCity.generated.h"

class AEverdell3DCard;
class AEverdellCardLocationActor;
class AEverdellGameStateBase;
class AEverdellHUD;
class AEverdellDeckActor;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API AEverdellPlayerCity : public AEverdellLocationGroup
{
	GENERATED_BODY()

	UPROPERTY()
	AEverdellCardLocationActor* AuxiliaryCardLocation;

public:
	UPROPERTY()
	FName LocationAuxiliaryTag;

	UFUNCTION()
	TArray<AEverdell3DCard*> GetUpgradeConstruction(EEverdellCardType CardType) const;

	UFUNCTION()
	AEverdellCardLocationActor* GetNextAvailableCard(EEverdellCardType CardType) const;

	UFUNCTION()
	int32 GetCityScore(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD) const;

	virtual void InitializeLocations() override;

	virtual bool SetCard(AEverdell3DCard* CardActor) override;

	virtual int32 GetCardCount() const override;
	virtual int32 GetNextAvailableLocationIndex(bool bRequiresSpace) const override;
	virtual AEverdellCardLocationActor* GetNextAvailableLocation(bool bRequiresSpace) const override;
	virtual FVector GetNextAvailablePlacementLocation(bool bRequiresSpace) const override;

	virtual int32 CountOfCard(EEverdellCardType CardType) const override;
	virtual int32 CountOfCardCategory(EEverdellCardCategory CardCategory) const override;
	virtual int32 CountOfCritters() const override;
	virtual int32 CountOfConstructions() const override;
	virtual int32 CountOfCardWithProperties(bool bIsCritter, bool bIsUnique) const override;
};
