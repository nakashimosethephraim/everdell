#pragma once

#include "CoreMinimal.h"
#include "Actors/EverdellWorkerLocation.h"
#include "EverdellWorkerBL1Berry.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class EVERDELLBOARDGAME_API AEverdellWorkerBL1Berry : public AEverdellWorkerLocation
{
	GENERATED_BODY()

public:
	AEverdellWorkerBL1Berry();

	virtual FName GetLocationIdentifier() const override { return TEXT("Gain 1 Berry"); }
};
