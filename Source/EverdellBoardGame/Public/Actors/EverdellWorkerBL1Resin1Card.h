#pragma once

#include "CoreMinimal.h"
#include "Actors/EverdellWorkerLocation.h"
#include "EverdellWorkerBL1Resin1Card.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class EVERDELLBOARDGAME_API AEverdellWorkerBL1Resin1Card : public AEverdellWorkerLocation
{
	GENERATED_BODY()

public:
	AEverdellWorkerBL1Resin1Card();

	virtual FName GetLocationIdentifier() const override { return TEXT("Gain 1 Resin & Draw 1 Card"); }
};
