#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraActor.h"
#include "EverdellCameraActor.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class EVERDELLBOARDGAME_API AEverdellCameraActor : public ACameraActor
{
	GENERATED_BODY()

public:
	AEverdellCameraActor();
};
