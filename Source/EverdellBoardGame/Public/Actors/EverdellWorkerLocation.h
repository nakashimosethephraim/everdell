#pragma once

#include "CoreMinimal.h"
#include "Actors/EverdellLocation.h"
#include "EverdellWorkerLocation.generated.h"

class AEverdellWorker;
class AEverdellGameStateBase;
class AEverdellHUD;
class AEverdellWorkerLocation;

DECLARE_DELEGATE_RetVal_FourParams(bool, FEverdellCanPerformLocationEffectSignature, const AEverdellGameStateBase*, const AEverdellHUD*, const AEverdellWorkerLocation*, const bool);
DECLARE_DELEGATE_RetVal_FourParams(AEverdellWorker*, FEverdellPerformLocationEffectSignature, AEverdellGameStateBase*, AEverdellHUD*, AEverdellWorkerLocation*, const bool);

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class EVERDELLBOARDGAME_API AEverdellWorkerLocation : public AEverdellLocation
{
	GENERATED_BODY()

	UPROPERTY()
	TSet<AEverdellWorker*> Workers;

public:
	AEverdellWorkerLocation();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* Collision;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Constraints")
	uint8 bIsOpenLocation : 1;

	FEverdellCanPerformLocationEffectSignature CanPerformEffect;
	FEverdellPerformLocationEffectSignature PerformEffect;

	virtual void OnMouseClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed) override;
	virtual void OnMouseReleased(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed) override;

	UFUNCTION()
	bool HasWorker() const;

	UFUNCTION()
	int32 WorkerCount() const;

	UFUNCTION()
	bool CanAcceptWorkers() const;

	UFUNCTION()
	void AddWorker(AEverdellWorker* NewWorker);

	UFUNCTION()
	void RemoveWorker(AEverdellWorker* Worker);

	UFUNCTION()
	void AddWorkerIfEffectExecutes();

	UFUNCTION()
	TArray<AEverdellWorker*> ResetWorkers();

	virtual bool IsInteractable() const override;
	virtual FName GetLocationIdentifier() const override { return TEXT("Everdell Worker Location"); }

protected:
	virtual void BeginPlay() override;

	/** References to all core game data and functionality */
	UPROPERTY()
	AEverdellGameStateBase* GameState;
	UPROPERTY()
	AEverdellHUD* HUD;
};
