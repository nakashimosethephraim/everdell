#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameData/EverdellCardCategoryEnum.h"
#include "GameData/EverdellCardTypeEnum.h"
#include "EverdellLocationGroup.generated.h"

class AEverdellPlayerState;
class AEverdellGameStateBase;
class AEverdellHUD;
class AEverdellDeckActor;
class AEverdellCardLocationActor;
class AEverdell3DCard;

UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API AEverdellLocationGroup : public AActor
{
	GENERATED_BODY()

public:
	AEverdellLocationGroup();

	// Array of card locations representing grouping
	UPROPERTY(BlueprintReadOnly, Category = "Data")
	TArray<AEverdellCardLocationActor*> Locations;

	UPROPERTY(BlueprintReadOnly, Category = "Data")
	FName LocationTag;

	UFUNCTION()
	virtual void InitializeLocations();

	UFUNCTION()
	int32 GetGroupSize() const;
	UFUNCTION()
	virtual int32 GetCardCount() const;
	UFUNCTION()
	virtual int32 GetNextAvailableLocationIndex(bool bRequiresSpace) const;
	UFUNCTION()
	virtual AEverdellCardLocationActor* GetNextAvailableLocation(bool bRequiresSpace) const;
	UFUNCTION()
	virtual FVector GetNextAvailablePlacementLocation(bool bRequiresSpace) const;

	UFUNCTION()
	bool CanPlaceCards() const;
	UFUNCTION()
	virtual bool SetCard(AEverdell3DCard* CardActor);
	UFUNCTION()
	AEverdell3DCard* GetCard(int32 Index) const;
	UFUNCTION()
	TArray<AEverdell3DCard*> GetCardsOfType(EEverdellCardType CardType) const;
	UFUNCTION()
	AEverdellCardLocationActor* GetCardLocation(int32 Index) const;
	UFUNCTION()
	AEverdell3DCard* ResetCard(int32 Index);

	UFUNCTION()
	virtual int32 CountOfCard(EEverdellCardType CardType) const;
	UFUNCTION()
	virtual int32 CountOfCardCategory(EEverdellCardCategory CardCategory) const;
	UFUNCTION()
	virtual int32 CountOfCritters() const;
	UFUNCTION()
	virtual int32 CountOfConstructions() const;
	UFUNCTION()
	virtual int32 CountOfCardWithProperties(bool bIsCritter, bool bIsUnique) const;
	UFUNCTION()
	virtual int32 CountOfActivatableGreenProductionCards(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD) const;

protected:
	UPROPERTY()
	int32 CardsInGroup;

	UPROPERTY()
	int32 NextAvailableIndex;
};
