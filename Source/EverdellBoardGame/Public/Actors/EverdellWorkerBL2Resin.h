#pragma once

#include "CoreMinimal.h"
#include "Actors/EverdellWorkerLocation.h"
#include "EverdellWorkerBL2Resin.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class EVERDELLBOARDGAME_API AEverdellWorkerBL2Resin : public AEverdellWorkerLocation
{
	GENERATED_BODY()

public:
	AEverdellWorkerBL2Resin();

	virtual FName GetLocationIdentifier() const override { return TEXT("Gain 2 Resin"); }
};
