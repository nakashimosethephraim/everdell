#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EverdellWorkerSlotGroup.generated.h"

class AEverdellWorkerSlot;
class AEverdellWorker;

UCLASS()
class EVERDELLBOARDGAME_API AEverdellWorkerSlotGroup : public AActor
{
	GENERATED_BODY()
	
public:	
	AEverdellWorkerSlotGroup();

	// Array of card locations representing grouping
	UPROPERTY(BlueprintReadOnly, Category = "Data")
	TArray<AEverdellWorkerSlot*> Slots;

	UPROPERTY(BlueprintReadOnly, Category = "Data")
	FName SlotTag;

	UFUNCTION()
	void InitializeSlots();

	UFUNCTION()
	int32 GetWorkerCount() const;

	UFUNCTION()
	bool HasAvailableWorker() const;

	UFUNCTION()
	AEverdellWorker* GetNextAvailableWorker();

	UFUNCTION()
	bool AddWorker(AEverdellWorker* Worker);

	UFUNCTION()
	void InitializeWithWorkers(TArray<AEverdellWorker*>& Workers) const;

protected:
	UPROPERTY()
	int32 WorkersInGroup;

	UPROPERTY()
	int32 NextAvailableIndex;

	UPROPERTY()
	int32 NextOpenIndex;
};
