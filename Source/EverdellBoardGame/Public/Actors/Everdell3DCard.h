#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Everdell3DCard.generated.h"

class AEverdellGameStateBase;
class AEverdellHUD;
class AEverdellCardWorkerLocation;
class UEverdellCard;

/** Actor representing a card in 3D */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API AEverdell3DCard : public AActor
{
	GENERATED_BODY()

	/** Positions to place locations for card */
	FVector LocationScale;
	FVector CenterLocationRelativePosition;
	FVector FirstLocationRelativePosition;
	FVector SecondLocationRelativePosition;

	/** Meshes to display both front and back of card */
	UPROPERTY()
	UStaticMeshComponent* CardFront;
	UPROPERTY()
	UStaticMeshComponent* CardBack;
	UPROPERTY()
	UStaticMeshComponent* UpgradeMarker;

	/** Materials to display on front and back of card */
	UPROPERTY()
	UMaterialInterface* FrontMaterial;
	UPROPERTY()
	UMaterialInterface* BackMaterial;
	UPROPERTY()
	UMaterialInterface* UpgradeTokenMaterial;

	/** Locations to be used by cards that support worker placement */
	UPROPERTY()
	AEverdellCardWorkerLocation* FirstLocation;
	UPROPERTY()
	AEverdellCardWorkerLocation* SecondLocation;

	/** References to all core game data and functionality */
	UPROPERTY()
	AEverdellGameStateBase* GameState;
	UPROPERTY()
	AEverdellHUD* HUD;

	/** Card interface giving this card functionality, to be assigned at runtime */
	UPROPERTY()
	UEverdellCard* Card;
	
	/** Boolean state data for the 3D card */
	UPROPERTY()
	uint8 bIsClicked : 1;
	UPROPERTY()
	uint8 bIsHovered : 1;
	UPROPERTY()
	uint8 bIsMoving : 1;
	UPROPERTY()
	uint8 bShouldDestroyOnArrival : 1;

	/** Location and rotation state data used when moving from one position
	 *  to another 
	 */
	UPROPERTY()
	FVector TargetLocation;
	UPROPERTY()
	FRotator TargetRotation;

public:
	AEverdell3DCard();

	UFUNCTION()
	bool HasCard() const;

	UFUNCTION()
	UEverdellCard* GetCard() const;

	UFUNCTION()
	void SetCard(UEverdellCard* NewCard);

	UFUNCTION()
	UEverdellCard* ResetCard();

	UFUNCTION()
	void SetFreeUpgradeUsed(const bool bFreeUpgradeUsed);

	void MoveToLocationWithRotation(FVector&& Location, FRotator&& Rotation, bool bDestroyOnArrival = false);

	UFUNCTION()
	virtual void OnBeginMouseOver(UPrimitiveComponent* TouchedComponent);

	UFUNCTION()
	virtual void OnEndMouseOver(UPrimitiveComponent* TouchedComponent);

	UFUNCTION()
	virtual void OnMouseClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed);

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
};
