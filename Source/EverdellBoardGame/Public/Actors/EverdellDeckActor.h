#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EverdellDeckActor.generated.h"

class AEverdellPlayerState;
class AEverdellGameStateBase;
class AEverdell3DCard;
class UEverdellCard;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEverdellOnCardGenerated, int32, Index);

UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API AEverdellDeckActor : public AActor
{
	GENERATED_BODY()

	UPROPERTY()
	UMaterialInterface* DeckMaterial;

	UPROPERTY()
	UMaterialInterface* DeckCardMaterial;

	UPROPERTY()
	AEverdellGameStateBase* GameState;

	UPROPERTY()
	TArray<uint32> CardIndexPool;

	UPROPERTY()
	TArray<uint32> UniqueIdPool;

	UPROPERTY()
	FVector NewCardSpawnLocation;

	UPROPERTY()
	FRotator NewCardSpawnRotation;

	UPROPERTY()
	uint8 bIsClicked : 1;

	UFUNCTION()
	UEverdellCard* GenerateNewCard();

	UFUNCTION()
	AEverdell3DCard* Spawn3DCard();
	
public:
	AEverdellDeckActor();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* Collision;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* TopCard;

	FEverdellOnCardGenerated OnCardGeneratedDelegate;

	UFUNCTION()
	bool FillUICard();

	UFUNCTION()
	UEverdellCard* RevealCard();

	UFUNCTION()
	TArray<UEverdellCard*> RevealCards(const uint32 Count);

	UFUNCTION()
	bool HasCardsRemainingInDeck() const;

	UFUNCTION()
	int32 CardsRemainingInDeck() const;

	UFUNCTION()
	virtual void OnMouseClicked(UPrimitiveComponent* TouchedComponent, FKey ButtonPressed);

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
};
