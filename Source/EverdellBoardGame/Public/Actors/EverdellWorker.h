#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EverdellWorker.generated.h"

class AEverdellWorkerLocation;

UCLASS(Blueprintable, BlueprintType)
class EVERDELLBOARDGAME_API AEverdellWorker : public AActor
{
	GENERATED_BODY()

	UPROPERTY()
	uint8 bIsMoving : 1;

	/** Location and rotation state data used when moving from one position
	 *  to another
	 */
	UPROPERTY()
	FVector TargetLocation;
	UPROPERTY()
	FRotator TargetRotation;

public:
	AEverdellWorker();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* WorkerBody;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	UMaterialInterface* WorkerMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	uint8 bIsDeployed : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	AEverdellWorkerLocation* DeployedLocation;

	UFUNCTION()
	void MoveToLocationWithRotation(const FVector& Location, const FRotator& Rotation);

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
};
