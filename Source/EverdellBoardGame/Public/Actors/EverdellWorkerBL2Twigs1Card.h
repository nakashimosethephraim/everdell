#pragma once

#include "CoreMinimal.h"
#include "Actors/EverdellWorkerLocation.h"
#include "EverdellWorkerBL2Twigs1Card.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class EVERDELLBOARDGAME_API AEverdellWorkerBL2Twigs1Card : public AEverdellWorkerLocation
{
	GENERATED_BODY()

public:
	AEverdellWorkerBL2Twigs1Card();

	virtual FName GetLocationIdentifier() const override { return TEXT("Gain 2 Twigs & Draw 1 Card"); }
};
