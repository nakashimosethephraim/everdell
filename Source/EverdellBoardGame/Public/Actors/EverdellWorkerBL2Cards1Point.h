#pragma once

#include "CoreMinimal.h"
#include "Actors/EverdellWorkerLocation.h"
#include "EverdellWorkerBL2Cards1Point.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class EVERDELLBOARDGAME_API AEverdellWorkerBL2Cards1Point : public AEverdellWorkerLocation
{
	GENERATED_BODY()

public:
	AEverdellWorkerBL2Cards1Point();

	virtual FName GetLocationIdentifier() const override { return TEXT("Draw 2 Cards & Gain 1 Victory Point"); }
};
