#pragma once

#include "CoreMinimal.h"
#include "Actors/EverdellWorkerLocation.h"
#include "EverdellWorkerBL3Twigs.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class EVERDELLBOARDGAME_API AEverdellWorkerBL3Twigs : public AEverdellWorkerLocation
{
	GENERATED_BODY()

public:
	AEverdellWorkerBL3Twigs();

	virtual FName GetLocationIdentifier() const override { return TEXT("Gain 3 Twigs"); }
};
