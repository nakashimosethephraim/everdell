#pragma once

#include "CoreMinimal.h"
#include "Actors/EverdellLocation.h"
#include "GameData/EverdellCardCategoryEnum.h"
#include "GameData/EverdellCardTypeEnum.h"
#include "EverdellCardLocationActor.generated.h"

class AEverdell3DCard;
class AEverdellPlayerState;
class AEverdellGameStateBase;
class AEverdellHUD;
class AEverdellDeckActor;

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class EVERDELLBOARDGAME_API AEverdellCardLocationActor : public AEverdellLocation
{
	GENERATED_BODY()

	UPROPERTY()
	AEverdell3DCard* Card;

	UPROPERTY()
	TArray<AEverdell3DCard*> SecondaryCards;

public:
	AEverdellCardLocationActor();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* Collision;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	int32 LocationIndex;

	UFUNCTION()
	bool HasCard() const;
	UFUNCTION()
	AEverdell3DCard* GetCard() const;
	UFUNCTION()
	void SetCard(AEverdell3DCard* NewCard);
	UFUNCTION()
	AEverdell3DCard* ResetCard();

	UFUNCTION()
	bool HasSecondaryCards() const;
	UFUNCTION()
	AEverdell3DCard* GetSecondaryCard(const int32 Index) const;
	UFUNCTION()
	void AddSecondaryCard(AEverdell3DCard* NewCard);
	UFUNCTION()
	FVector GetNextSecondaryCardPlacementLocation() const;
	UFUNCTION()
	TArray<AEverdell3DCard*> ResetSecondaryCards();

	UFUNCTION()
	int32 GetSecondaryCardCount() const;
	UFUNCTION()
	bool HasSecondaryCardOfType(const EEverdellCardType CardType) const;
	UFUNCTION()
	TArray<AEverdell3DCard*> GetSecondaryCardsOfType(const EEverdellCardType CardType) const;
	UFUNCTION()
	int32 CountOfSecondaryCardsOfType(const EEverdellCardType CardType) const;
	UFUNCTION()
	int32 CountOfSecondaryCardsOfCategory(const EEverdellCardCategory CardCategory) const;
	UFUNCTION()
	int32 CountOfSecondaryCritters() const;
	UFUNCTION()
	int32 CountOfSecondaryConstructions() const;
	UFUNCTION()
	int32 CountOfSecondaryCardsWithProperties(const bool bIsCritter, const bool bIsUnique) const;
	UFUNCTION()
	int32 CountOfSecondaryActivatableGreenProductions(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD) const;
	UFUNCTION()
	int32 SecondaryCardsScore(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD) const;

	UFUNCTION()
	bool HasCards() const { return HasCard() || HasSecondaryCards(); }
	UFUNCTION()
	void SetCardGeneric(AEverdell3DCard* NewCard);
	UFUNCTION()
	int32 GetTotalCardCount() const;
	UFUNCTION()
	bool HasCardOfType(const EEverdellCardType CardType) const;
	UFUNCTION()
	int32 TotalCountCardsOfType(const EEverdellCardType CardType) const;
	UFUNCTION()
	int32 TotalCountCardsOfCategory(const EEverdellCardCategory CardCategory) const;
	UFUNCTION()
	int32 TotalCountCritters() const;
	UFUNCTION()
	int32 TotalCountConstructions() const;
	UFUNCTION()
	int32 TotalCountSecondaryCardsWithProperties(const bool bIsCritter, const bool bIsUnique) const;
	UFUNCTION()
	int32 TotalCountActivatableGreenProductions(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD) const;
	UFUNCTION()
	int32 TotalCardsScore(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD) const;
	UFUNCTION()
	FVector GetNextCardPlacementLocation() const;
	UFUNCTION()
	TArray<AEverdell3DCard*> ResetCards();

	virtual bool IsInteractable() const override { return Card == nullptr && SecondaryCards.IsEmpty(); }
};
