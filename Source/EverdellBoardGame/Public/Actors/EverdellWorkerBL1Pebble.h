#pragma once

#include "CoreMinimal.h"
#include "Actors/EverdellWorkerLocation.h"
#include "EverdellWorkerBL1Pebble.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class EVERDELLBOARDGAME_API AEverdellWorkerBL1Pebble : public AEverdellWorkerLocation
{
	GENERATED_BODY()

public:
	AEverdellWorkerBL1Pebble();

	virtual FName GetLocationIdentifier() const override { return TEXT("Gain 1 Pebble"); }
};
