#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "EverdellUserWidget.generated.h"

class UEverdellUICard;
class UTextBlock;
class UImage;
class UMenuAnchor;
class UEverdellCardTooltip;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellUserWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	// All cards in hand
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UEverdellUICard* HandCard1;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UEverdellUICard* HandCard2;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UEverdellUICard* HandCard3;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UEverdellUICard* HandCard4;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UEverdellUICard* HandCard5;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UEverdellUICard* HandCard6;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UEverdellUICard* HandCard7;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UEverdellUICard* HandCard8;

	// A text block representing the player's worker count
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* WorkerCount;

	// A text block representing the player's available card draw count
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* AvailableCardDraw;

	// A text block representing the player's total score
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CurrentScore;

	// A text block representing the player's twig count
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* TwigCount;

	// A text block representing the player's resin count
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* ResinCount;

	// A text block representing the player's pebble count
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* PebbleCount;

	// A text block representing the player's berry count
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* BerryCount;

	// A text block representing the player's point count
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* PointCount;

	// A menu anchor to place new popup widgets
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UMenuAnchor* CoreMenuAnchor;

	// A tooltip to show details about cards
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UEverdellCardTooltip* CardTooltip;
};
