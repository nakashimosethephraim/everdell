#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "EverdellPlayForNLessPopup.generated.h"

class UEverdellCard;
class UButton;
class UTextBlock;
class USlider;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellPlayForNLessPopup : public UEverdellPopup
{
	GENERATED_BODY()
	
	UPROPERTY()
	int32 Reduction;
	UPROPERTY()
	int32 TwigCost;
	UPROPERTY()
	int32 ResinCost;
	UPROPERTY()
	int32 PebbleCost;
	UPROPERTY()
	int32 BerryCost;

	UPROPERTY()
	UEverdellCard* AssociatedCard;

public:
	// Properties connected to blueprint subclass
	// Must be public to support blueprint access
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* RemainingReductionText;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* TwigCostText;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	USlider* TwigCostSlider;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* ResinCostText;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	USlider* ResinCostSlider;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* PebbleCostText;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	USlider* PebbleCostSlider;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* BerryCostText;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	USlider* BerryCostSlider;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* PayCostButton;

	// Override functions from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	UFUNCTION()
	void SetRemainingReductionText();
	UFUNCTION()
	void SetTwigCostSliderText();
	UFUNCTION()
	void SetResinCostSliderText();
	UFUNCTION()
	void SetPebbleCostSliderText();
	UFUNCTION()
	void SetBerryCostSliderText();

	UFUNCTION()
	void OnPayCostButtonClicked();

	UFUNCTION()
	void OnTwigCostSliderValueChanged(float Value);
	UFUNCTION()
	void OnResinCostSliderValueChanged(float Value);
	UFUNCTION()
	void OnPebbleCostSliderValueChanged(float Value);
	UFUNCTION()
	void OnBerryCostSliderValueChanged(float Value);
};
