#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "EverdellCardPaymentPopup.generated.h"

class UEverdellCard;
class AEverdell3DCard;
class UButton;
class UTextBlock;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellCardPaymentPopup : public UEverdellPopup
{
	GENERATED_BODY()

	// Unique properties filled in at runtime
	UPROPERTY()
	UEverdellCard* AssociatedCard;

	UPROPERTY()
	TArray<AEverdell3DCard*> FreeUpgradeOptions;

	UPROPERTY()
	FLinearColor YellowColor;
	UPROPERTY()
	FLinearColor GreenColor;
	UPROPERTY()
	FLinearColor WhiteColor;
	UPROPERTY()
	FLinearColor BlackColor;
	UPROPERTY()
	FLinearColor InvisibleColor;

public:
	// Properties connected to blueprint subclass
	// Must be public to support blueprint access
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* PayCostButton;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* FreeUpgradeButton1;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* FreeUpgradeButtonTextBlock1;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* FreeUpgradeButton2;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* FreeUpgradeButtonTextBlock2;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* FreeUpgradeButton3;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* FreeUpgradeButtonTextBlock3;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* FreeUpgradeButton4;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* FreeUpgradeButtonTextBlock4;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* FreeUpgradeButton5;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* FreeUpgradeButtonTextBlock5;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* FreeUpgradeButton6;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* FreeUpgradeButtonTextBlock6;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* FreeUpgradeButton7;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* FreeUpgradeButtonTextBlock7;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* FreeUpgradeButton8;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* FreeUpgradeButtonTextBlock8;

	// Override functions from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	// Button behavior defining functions
	UFUNCTION()
	void OnPayCostButtonClicked();

	UFUNCTION()
	void HandleFreeUpgrade(uint8 FreeUpgradeIndex);

	// The linter can't tell that these methods are defined
	// because they are defined within a macro
	UFUNCTION()
	void OnFreeUpgradeButton1Clicked();
	UFUNCTION()
	void OnFreeUpgradeButton2Clicked();
	UFUNCTION()
	void OnFreeUpgradeButton3Clicked();
	UFUNCTION()
	void OnFreeUpgradeButton4Clicked();
	UFUNCTION()
	void OnFreeUpgradeButton5Clicked();
	UFUNCTION()
	void OnFreeUpgradeButton6Clicked();
	UFUNCTION()
	void OnFreeUpgradeButton7Clicked();
	UFUNCTION()
	void OnFreeUpgradeButton8Clicked();
};
