#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "UI/EverdellPopupData.h"
#include "Components/SlateWrapperTypes.h"
#include "EverdellChooseCardPopup.generated.h"

class UEverdellCard;
class UButton;
class UImage;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellChooseCardPopup : public UEverdellPopup
{
	GENERATED_BODY()

	UPROPERTY()
	FVector2D DefaultImageSize;

	UPROPERTY()
	int32 Reduction;
	UPROPERTY()
	int32 UpperLimit;
	UPROPERTY()
	uint8 bShouldPlayForFree : 1;
	UPROPERTY()
	uint8 bShouldDiscardOtherCards : 1;
	UPROPERTY()
	TArray<UEverdellCard*> AssociatedCards;

	FEverdellAfterCardChoiceSignature AfterCardChoiceDelegate;

	UPROPERTY()
	FLinearColor WhiteColor;
	UPROPERTY()
	FLinearColor BlackColor;
	UPROPERTY()
	FLinearColor InvisibleColor;

public:
	// Properties connected to blueprint subclass
	// Must be public to support blueprint access
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* PlayOptionButton1;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* PlayOptionButtonImage1;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* PlayOptionButton2;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* PlayOptionButtonImage2;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* PlayOptionButton3;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* PlayOptionButtonImage3;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* PlayOptionButton4;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* PlayOptionButtonImage4;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* PlayOptionButton5;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* PlayOptionButtonImage5;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* PlayOptionButton6;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* PlayOptionButtonImage6;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* PlayOptionButton7;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* PlayOptionButtonImage7;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* PlayOptionButton8;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* PlayOptionButtonImage8;

	// Override functions from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	UFUNCTION()
	void HandleClicked(const int32 Index);

	UFUNCTION()
	bool CanChooseCard(const UEverdellCard* Card);

	// The linter can't tell that these methods are defined
	// because they are defined within a macro
	UFUNCTION()
	void OnPlayOptionButton1Clicked();
	UFUNCTION()
	void OnPlayOptionButton2Clicked();
	UFUNCTION()
	void OnPlayOptionButton3Clicked();
	UFUNCTION()
	void OnPlayOptionButton4Clicked();
	UFUNCTION()
	void OnPlayOptionButton5Clicked();
	UFUNCTION()
	void OnPlayOptionButton6Clicked();
	UFUNCTION()
	void OnPlayOptionButton7Clicked();
	UFUNCTION()
	void OnPlayOptionButton8Clicked();
};
