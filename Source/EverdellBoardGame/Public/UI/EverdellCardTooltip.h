#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "EverdellCardTooltip.generated.h"

class UEverdellCard;
class UTextBlock;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellCardTooltip : public UUserWidget
{
	GENERATED_BODY()

public:
	// A text block representing the tooltip content
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* Content;

	UFUNCTION()
	void SetContent(const UEverdellCard* Card);

	UFUNCTION()
	void ResetContent();

private:
	FText GenerateContent(const UEverdellCard* Card);
};
