#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "EverdellStorehousePopup.generated.h"

class UButton;
class UEverdellCard;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellStorehousePopup : public UEverdellPopup
{
	GENERATED_BODY()

	// Unique properties filled in at runtime
	UPROPERTY()
	UEverdellCard* AssociatedCard;

public:
	// Properties connected to blueprint subclass
	// Must be public to support blueprint access
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* TwigOptionButton;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* ResinOptionButton;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* PebbleOptionButton;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (OptionalBindWidget))
	UButton* BerryOptionButton;

	// Override functions from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	// Button behavior defining functions
	UFUNCTION()
	void OnTwigOptionButtonClicked();

	UFUNCTION()
	void OnResinOptionButtonClicked();

	UFUNCTION()
	void OnPebbleOptionButtonClicked();

	UFUNCTION()
	void OnBerryOptionButtonClicked();
};
