#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "EverdellHusbandWifePopup.generated.h"

class AEverdellCardLocationActor;
class UEverdellCard;
class UButton;
class UTextBlock;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellHusbandWifePopup : public UEverdellPopup
{
	GENERATED_BODY()

	// Unique properties filled in at runtime
	UPROPERTY()
	UEverdellCard* AssociatedCard;

	UPROPERTY()
	AEverdellCardLocationActor* PossiblePlacementLocation;

public:
	// Properties connected to blueprint subclass
	// Must be public to support blueprint access
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* RegularPlayOptionButton;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* CombineOptionButton;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CombineOptionButtonText;

	// Override functions from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	// Button behavior defining functions
	UFUNCTION()
	void OnRegularPlayOptionButtonClicked();

	UFUNCTION()
	void OnCombineOptionButtonClicked();
};
