#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "EverdellCarnivalPopup.generated.h"

class UButton;
class UTextBlock;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellCarnivalPopup : public UEverdellPopup
{
	GENERATED_BODY()

	UPROPERTY()
	int32 DrawCount;

	UPROPERTY()
	int32 AnyCount;

public:
	// Properties connected to blueprint subclass
	// Must be public to support blueprint access
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DrawOptionButton;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* DrawOptionButtonText;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* GainAnyOptionButton;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* GainAnyOptionButtonText;

	// Override function from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	UFUNCTION()
	void OnDrawOptionButtonClicked();

	UFUNCTION()
	void OnGainAnyOptionButtonClicked();
};
