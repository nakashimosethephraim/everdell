#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "EverdellDrawFromMeadowPopup.generated.h"

class UEverdellCard;
class UButton;
class UTextBlock;
class UImage;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellDrawFromMeadowPopup : public UEverdellPopup
{
	GENERATED_BODY()

	UPROPERTY()
	int32 Reduction;
	UPROPERTY()
	int32 RemainingDraw;
	UPROPERTY()
	TArray<UEverdellCard*> DrawnCards;

	FEverdellAfterCardChoiceSignature AfterCardChoiceDelegate;
	FEverdellAfterMeadowDrawSignature AfterMeadowDrawDelegate;

public:
	// Properties connected to blueprint subclass
	// Must be public to support blueprint access
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* DrawText;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DrawOptionButton1;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* DrawOptionButtonImage1;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DrawOptionButton2;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* DrawOptionButtonImage2;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DrawOptionButton3;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* DrawOptionButtonImage3;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DrawOptionButton4;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* DrawOptionButtonImage4;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DrawOptionButton5;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* DrawOptionButtonImage5;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DrawOptionButton6;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* DrawOptionButtonImage6;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DrawOptionButton7;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* DrawOptionButtonImage7;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DrawOptionButton8;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* DrawOptionButtonImage8;

	// Override functions from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	UFUNCTION()
	void HandleClicked(const int32 Index);

	UFUNCTION()
	void SetDataForIndex(const int32 Index);

	UFUNCTION()
	void MoveCardToHand(UEverdellCard* Card);

	// The linter can't tell that these methods are defined
	// because they are defined within a macro
	UFUNCTION()
	void OnDrawOptionButton1Clicked();
	UFUNCTION()
	void OnDrawOptionButton2Clicked();
	UFUNCTION()
	void OnDrawOptionButton3Clicked();
	UFUNCTION()
	void OnDrawOptionButton4Clicked();
	UFUNCTION()
	void OnDrawOptionButton5Clicked();
	UFUNCTION()
	void OnDrawOptionButton6Clicked();
	UFUNCTION()
	void OnDrawOptionButton7Clicked();
	UFUNCTION()
	void OnDrawOptionButton8Clicked();
};
