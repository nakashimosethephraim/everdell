#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "UI/EverdellPopupData.h"
#include "EverdellDiscardNCardsPopup.generated.h"

class UButton;
class UTextBlock;
class UImage;
class UBorder;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellDiscardNCardsPopup : public UEverdellPopup
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<bool> CardsToDiscard;

	UPROPERTY()
	int32 DiscardLimit;

	UPROPERTY()
	int32 Multiplier;

	UPROPERTY()
	int32 DiscardCardCount;

	FEverdellAfterDiscardSignature AfterDiscardDelegate;

	UPROPERTY()
	FVector2D DefaultImageSize;

	UPROPERTY()
	FLinearColor GreenColor;
	UPROPERTY()
	FLinearColor WhiteColor;
	UPROPERTY()
	FLinearColor BlackColor;
	UPROPERTY()
	FLinearColor InvisibleColor;

public:
	// Properties connected to blueprint subclass
	// Must be public to support blueprint access
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* DiscardMessage;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DiscardOptionButton1;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* DiscardOptionButtonImage1;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UBorder* DiscardOptionButtonBorder1;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DiscardOptionButton2;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* DiscardOptionButtonImage2;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UBorder* DiscardOptionButtonBorder2;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DiscardOptionButton3;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* DiscardOptionButtonImage3;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UBorder* DiscardOptionButtonBorder3;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DiscardOptionButton4;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* DiscardOptionButtonImage4;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UBorder* DiscardOptionButtonBorder4;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DiscardOptionButton5;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* DiscardOptionButtonImage5;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UBorder* DiscardOptionButtonBorder5;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DiscardOptionButton6;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* DiscardOptionButtonImage6;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UBorder* DiscardOptionButtonBorder6;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DiscardOptionButton7;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* DiscardOptionButtonImage7;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UBorder* DiscardOptionButtonBorder7;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DiscardOptionButton8;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* DiscardOptionButtonImage8;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UBorder* DiscardOptionButtonBorder8;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* SubmitButton;
	
	// Override functions from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	UFUNCTION()
	void OnSubmitButtonClicked();

	UFUNCTION()
	void HandleClicked(const int32 Index, UBorder* Border);

	// The linter can't tell that these methods are defined
	// because they are defined within a macro
	UFUNCTION()
	void OnDiscardOptionButton1Clicked();
	UFUNCTION()
	void OnDiscardOptionButton2Clicked();
	UFUNCTION()
	void OnDiscardOptionButton3Clicked();
	UFUNCTION()
	void OnDiscardOptionButton4Clicked();
	UFUNCTION()
	void OnDiscardOptionButton5Clicked();
	UFUNCTION()
	void OnDiscardOptionButton6Clicked();
	UFUNCTION()
	void OnDiscardOptionButton7Clicked();
	UFUNCTION()
	void OnDiscardOptionButton8Clicked();
};
