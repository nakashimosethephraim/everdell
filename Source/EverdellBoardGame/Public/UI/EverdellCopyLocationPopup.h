#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "EverdellCopyLocationPopup.generated.h"

class UButton;
class UTextBlock;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellCopyLocationPopup : public UEverdellPopup
{
	GENERATED_BODY()

	UPROPERTY()
	uint8 bIncludeForestLocations : 1;

	UPROPERTY()
	FLinearColor WhiteColor;
	UPROPERTY()
	FLinearColor BlackColor;
	UPROPERTY()
	FLinearColor InvisibleColor;

public:
	// Properties connected to blueprint subclass
	// Must be public to support blueprint access
	
	// Location buttons
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* LocationButton1;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* LocationButtonTextBlock1;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* LocationButton2;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* LocationButtonTextBlock2;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* LocationButton3;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* LocationButtonTextBlock3;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* LocationButton4;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* LocationButtonTextBlock4;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* LocationButton5;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* LocationButtonTextBlock5;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* LocationButton6;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* LocationButtonTextBlock6;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* LocationButton7;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* LocationButtonTextBlock7;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* LocationButton8;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* LocationButtonTextBlock8;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* LocationButton9;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* LocationButtonTextBlock9;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* LocationButton10;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* LocationButtonTextBlock10;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* LocationButton11;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* LocationButtonTextBlock11;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* LocationButton12;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* LocationButtonTextBlock12;
	
	// Override function from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	UFUNCTION()
	void HandleBasicLocation(int32 Index);
	UFUNCTION()
	void HandleForestLocation(int32 Index);

	// The linter can't tell that these methods are defined
	// because they are defined within a macro
	UFUNCTION()
	void OnLocationButton1Clicked();
	UFUNCTION()
	void OnLocationButton2Clicked();
	UFUNCTION()
	void OnLocationButton3Clicked();
	UFUNCTION()
	void OnLocationButton4Clicked();
	UFUNCTION()
	void OnLocationButton5Clicked();
	UFUNCTION()
	void OnLocationButton6Clicked();
	UFUNCTION()
	void OnLocationButton7Clicked();
	UFUNCTION()
	void OnLocationButton8Clicked();
	UFUNCTION()
	void OnLocationButton9Clicked();
	UFUNCTION()
	void OnLocationButton10Clicked();
	UFUNCTION()
	void OnLocationButton11Clicked();
	UFUNCTION()
	void OnLocationButton12Clicked();
};
