#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "EverdellSelectWorkerPopup.generated.h"

class AEverdellWorker;
class UButton;
class UTextBlock;

/**
 * 
 */
UCLASS()
class EVERDELLBOARDGAME_API UEverdellSelectWorkerPopup : public UEverdellPopup
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<AEverdellWorker*> DeployedWorkers;

	UPROPERTY()
	FLinearColor WhiteColor;
	UPROPERTY()
	FLinearColor BlackColor;
	UPROPERTY()
	FLinearColor InvisibleColor;

public:
	// Properties connected to blueprint subclass
	// Must be public to support blueprint access

	// Worker buttons
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* WorkerButton1;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* WorkerButtonTextBlock1;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* WorkerButton2;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* WorkerButtonTextBlock2;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* WorkerButton3;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* WorkerButtonTextBlock3;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* WorkerButton4;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* WorkerButtonTextBlock4;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* WorkerButton5;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* WorkerButtonTextBlock5;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* WorkerButton6;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* WorkerButtonTextBlock6;

	// Override functions from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	UFUNCTION()
	void HandleClicked(int32 Index);

	// The linter can't tell that these methods are defined
	// because they are defined within a macro
	UFUNCTION()
	void OnWorkerButton1Clicked();
	UFUNCTION()
	void OnWorkerButton2Clicked();
	UFUNCTION()
	void OnWorkerButton3Clicked();
	UFUNCTION()
	void OnWorkerButton4Clicked();
	UFUNCTION()
	void OnWorkerButton5Clicked();
	UFUNCTION()
	void OnWorkerButton6Clicked();
};
