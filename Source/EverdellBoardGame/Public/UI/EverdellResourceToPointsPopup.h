#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "EverdellResourceToPointsPopup.generated.h"

class UEverdellCard;
class UButton;
class UTextBlock;
class USlider;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellResourceToPointsPopup : public UEverdellPopup
{
	GENERATED_BODY()

	UPROPERTY()
	FName ResourceName;

	UPROPERTY()
	int Cost;
	UPROPERTY()
	int MinCost;
	UPROPERTY()
	int MaxCost;
	UPROPERTY()
	int Rate;

	FEverdellCheckValueSignature CheckResourceCountDelegate;
	FEverdellValueAugmentSignature AugmentResourceCountDelegate;

public:
	// Properties connected to blueprint subclass
	// Must be public to support blueprint access
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* PointBenefitText;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* ResourceCostText;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	USlider* ResourceCostSlider;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* PayCostButton;

	// Override functions from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	UFUNCTION()
	void SetPointBenefitText();
	UFUNCTION()
	void SetResourceCostSliderText();

	UFUNCTION()
	void OnPayCostButtonClicked();

	UFUNCTION()
	void OnResourceCostSliderValueChanged(float Value);
};
