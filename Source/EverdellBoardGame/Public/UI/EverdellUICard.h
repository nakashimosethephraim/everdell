#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Engine/EngineTypes.h"
#include "Components/SlateWrapperTypes.h"
#include "Layout/Margin.h"
#include "EverdellUICard.generated.h"

class AEverdellPlayerState;
class AEverdellGameStateBase;
class AEverdellHUD;
class UHorizontalBox;
class UEverdellCard;
class UImage;
class UBorder;
class FReply;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellUICard: public UUserWidget
{
	GENERATED_BODY()

	UPROPERTY()
	uint8 bIsClicked : 1;

	UPROPERTY()
	FMargin DefaultMargin;

	UPROPERTY()
	FVector2D DefaultImageSize;

	UPROPERTY()
	UTexture2D* CardTexture;

	UPROPERTY()
	UEverdellCard* Card;

public:
	UEverdellUICard(const FObjectInitializer& ObjectIn);

	UPROPERTY()
	AEverdellGameStateBase* GameState;

	UPROPERTY()
	AEverdellHUD* HUD;

	UPROPERTY()
	FLinearColor BorderDefaultColor;

	UPROPERTY()
	FLinearColor BorderHighlightColor;

	UPROPERTY()
	FLinearColor InvisibleColor;

	UPROPERTY()
	FLinearColor WhiteColor;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UBorder* CardBorder;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UImage* CardImage;

	UFUNCTION()
	bool HasCard() const;

	UFUNCTION()
	UEverdellCard* GetCard() const;

	UFUNCTION()
	void SetCard(UEverdellCard* NewCard);

	UFUNCTION()
	UEverdellCard* ResetCard();

protected:
	void NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
	void NativeOnMouseLeave(const FPointerEvent& InMouseEvent) override;
	FReply NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
};
