#pragma once

#include "CoreMinimal.h"
#include "GameManagement/EverdellPlayerState.h"
#include "EverdellPopupData.generated.h"

class UUserWidget;
class UEverdellCard;
class AEverdell3DCard;
class AEverdellCardLocationActor;
class AEverdellHUD;
class AEverdellWorker;
class AEverdellWorkerLocation;

DECLARE_DELEGATE_RetVal(int32, FEverdellCheckValueSignature);
DECLARE_DELEGATE_OneParam(FEverdellValueAugmentSignature, const int32);
DECLARE_DELEGATE_OneParam(FEverdellAfterDiscardSignature, const int32);
DECLARE_DELEGATE_TwoParams(FEverdellAfterCardChoiceSignature, UEverdellCard*, const int32);
DECLARE_DELEGATE_ThreeParams(FEverdellAfterMeadowDrawSignature, const int32, const TArray<UEverdellCard*>&, const FEverdellAfterCardChoiceSignature&);

/**
 *
 */
USTRUCT(BlueprintType)
struct FEverdellPopupData
{
	GENERATED_USTRUCT_BODY()

	// General utility data
	UPROPERTY()
	TSubclassOf<UUserWidget> MenuClass;
	UPROPERTY()
	FName MenuName;

	// Common data
	UPROPERTY()
	UEverdellCard* AssociatedCard;
	UPROPERTY()
	UEverdellCard* CardToPlay;
	UPROPERTY()
	TArray<UEverdellCard*> AssociatedCards;
	UPROPERTY()
	FName PropertyName;
	UPROPERTY()
	int32 LowerLimit;
	UPROPERTY()
	int32 UpperLimit;
	UPROPERTY()
	int32 Multiplier;
	UPROPERTY()
	int32 Reduction;
	UPROPERTY()
	int32 ValueAugmentScalar;
	UPROPERTY()
	int32 AfterDiscardPopupScalar;
	UPROPERTY()
	int32 AfterCardChoicePopupScalar;
	UPROPERTY()
	uint8 bShouldMoveAssociatedCard : 1;
	UPROPERTY()
	uint8 bShouldDiscardToAssociatedCard : 1;
	UPROPERTY()
	uint8 bShouldGainResourcesAfterDiscard : 1;
	UPROPERTY()
	uint8 bCanDiscardCritters : 1;
	UPROPERTY()
	uint8 bCanDiscardConstructions : 1;
	FEverdellCheckValueSignature CheckValueDelegate;
	FEverdellValueAugmentSignature ValueAugmentDelegate;
	FEverdellAfterDiscardSignature AfterDiscardDelegate;
	FEverdellAfterDiscardSignature ValueAugmentAfterDiscardDelegate;
	FEverdellAfterDiscardSignature OpenPopupAfterDiscardDelegate;
	FEverdellAfterCardChoiceSignature OpenPopupAfterCardChoiceDelegate;

	// Pay or free upgrade data
	UPROPERTY()
	TArray<AEverdell3DCard*> FreeUpgradeOptions;

	// Husband / wife data
	UPROPERTY()
	AEverdellCardLocationActor* PossiblePlacementLocation;

	// Choose location data
	UPROPERTY()
	uint8 bIncludeForestLocations : 1;

	// Choose card data
	UPROPERTY()
	uint8 bShouldPlayForFree : 1;
	UPROPERTY()
	uint8 bShouldDiscardOtherCards : 1;
	FEverdellAfterCardChoiceSignature AfterCardChoiceDelegate;

	// Draw Meadow card data
	FEverdellAfterMeadowDrawSignature AfterMeadowDrawDelegate;

	// Choose ability data
	TArray<TPair<UEverdellCard*, FEverdellPerformBeforePaymentEffectSignature>> ValidDelegateArray;

	// Discard for effect data
	UPROPERTY()
	UEverdellCard* CardToDiscard;

	// Select worker data
	UPROPERTY()
	TArray<AEverdellWorker*> DeployedWorkers;

	// Move worker data
	UPROPERTY()
	AEverdellWorkerLocation* PreviousLocation;
};

namespace UEverdellPopupData
{
	void ClearData(FEverdellPopupData& PopupData);
};
