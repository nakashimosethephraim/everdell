#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "UI/EverdellPopupData.h"
#include "EverdellActivateCardPopup.generated.h"

class UEverdellCard;
class AEverdellCardLocationActor;
class UButton;
class UTextBlock;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellActivateCardPopup : public UEverdellPopup
{
	GENERATED_BODY()

	UPROPERTY()
	UEverdellCard* AssociatedCard;

	UPROPERTY()
	FLinearColor WhiteColor;
	UPROPERTY()
	FLinearColor BlackColor;
	UPROPERTY()
	FLinearColor InvisibleColor;

public:
	// Properties connected to blueprint subclass
	// Must be public to support blueprint access

	// Card buttons
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* CardButton1;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CardButtonTextBlock1;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* CardButton2;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CardButtonTextBlock2;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* CardButton3;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CardButtonTextBlock3;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* CardButton4;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CardButtonTextBlock4;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* CardButton5;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CardButtonTextBlock5;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* CardButton6;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CardButtonTextBlock6;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* CardButton7;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CardButtonTextBlock7;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* CardButton8;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CardButtonTextBlock8;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* CardButton9;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CardButtonTextBlock9;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* CardButton10;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CardButtonTextBlock10;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* CardButton11;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CardButtonTextBlock11;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* CardButton12;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CardButtonTextBlock12;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* CardButton13;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CardButtonTextBlock13;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* CardButton14;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CardButtonTextBlock14;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* CardButton15;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CardButtonTextBlock15;

	// Override functions from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	UFUNCTION()
	void HandleClicked(int32 Index);

	UFUNCTION()
	const UEverdellCard* GetActivatableCard(const AEverdellCardLocationActor* CardLocation) const;

	// The linter can't tell that these methods are defined
	// because they are defined within a macro
	UFUNCTION()
	void OnCardButton1Clicked();
	UFUNCTION()
	void OnCardButton2Clicked();
	UFUNCTION()
	void OnCardButton3Clicked();
	UFUNCTION()
	void OnCardButton4Clicked();
	UFUNCTION()
	void OnCardButton5Clicked();
	UFUNCTION()
	void OnCardButton6Clicked();
	UFUNCTION()
	void OnCardButton7Clicked();
	UFUNCTION()
	void OnCardButton8Clicked();
	UFUNCTION()
	void OnCardButton9Clicked();
	UFUNCTION()
	void OnCardButton10Clicked();
	UFUNCTION()
	void OnCardButton11Clicked();
	UFUNCTION()
	void OnCardButton12Clicked();
	UFUNCTION()
	void OnCardButton13Clicked();
	UFUNCTION()
	void OnCardButton14Clicked();
	UFUNCTION()
	void OnCardButton15Clicked();
};
