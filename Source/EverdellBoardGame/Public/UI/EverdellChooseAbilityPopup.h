#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "EverdellChooseAbilityPopup.generated.h"

class UEverdellCard;
class UVerticalBox;
class UTextBlock;
class UButton;
class USpacer;
class UEverdellLambdaWrapper;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellChooseAbilityPopup : public UEverdellPopup
{
	GENERATED_BODY()

	UPROPERTY()
	UEverdellCard* AssociatedCard;

	UPROPERTY()
	TArray<UEverdellLambdaWrapper*> Lambdas;

	TArray<TPair<UEverdellCard*, FEverdellPerformBeforePaymentEffectSignature>> ValidDelegateArray;

	TArray<TTuple<UButton*, UTextBlock*, USpacer*>> AllocatedWidgets;

	UPROPERTY()
	FLinearColor BlackColor;
	UPROPERTY()
	FLinearColor WhiteColor;

public:
	// Properties connected to blueprint subclass
	// Must be public to support blueprint access
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* NoAbilityButton;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UVerticalBox* OptionsBox;

	// Override functions from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	void CleanUpWidgets();

	// Button behavior defining functions
	UFUNCTION()
	void OnNoAbilityButtonClicked();
};
