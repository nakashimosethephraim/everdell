#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "EverdellExchangeResourcesPopup.generated.h"

class UButton;
class UTextBlock;
class USlider;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellExchangeResourcesPopup : public UEverdellPopup
{
	GENERATED_BODY()

	UPROPERTY()
	int32 UpperLimit;
	UPROPERTY()
	int32 CurrentBalance;

	UPROPERTY()
	int32 TwigValue;
	UPROPERTY()
	int32 ResinValue;
	UPROPERTY()
	int32 PebbleValue;
	UPROPERTY()
	int32 BerryValue;

public:
	// Properties connected to blueprint subclass
	// Must be public to support blueprint access
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* CurrentBalanceText;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* TwigText;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	USlider* TwigSlider;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* ResinText;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	USlider* ResinSlider;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* PebbleText;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	USlider* PebbleSlider;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* BerryText;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	USlider* BerrySlider;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* ExchangeButton;

	// Override functions from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	UFUNCTION()
	void SetCurrentBalanceText();
	UFUNCTION()
	void SetTwigSliderText();
	UFUNCTION()
	void SetResinSliderText();
	UFUNCTION()
	void SetPebbleSliderText();
	UFUNCTION()
	void SetBerrySliderText();

	UFUNCTION()
	void OnExchangeButtonClicked();

	UFUNCTION()
	void OnTwigSliderValueChanged(float Value);
	UFUNCTION()
	void OnResinSliderValueChanged(float Value);
	UFUNCTION()
	void OnPebbleSliderValueChanged(float Value);
	UFUNCTION()
	void OnBerrySliderValueChanged(float Value);
	
};
