#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "EverdellRevealChoicePopup.generated.h"

class UButton;
class UImage;
class UScaleBox;
class UHorizontalBox;
class UEverdellCard;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellRevealChoicePopup : public UEverdellPopup
{
	GENERATED_BODY()

	// Unique properties filled in at runtime
	UPROPERTY()
	int32 RemainingChoices;
	UPROPERTY()
	uint8 RevealedCardRespectsBoardState: 1;

	UPROPERTY()
	TArray<UEverdellCard*> RevealedCards;

	TArray<TPair<UScaleBox*, UImage*>> AllocatedWidgets;

	UPROPERTY()
	FVector2D DefaultImageSize;

	UPROPERTY()
	FLinearColor BlackColor;
	UPROPERTY()
	FLinearColor WhiteColor;
	UPROPERTY()
	FLinearColor InvisibleColor;
	
public:
	// Properties connected to blueprint subclass
	// Must be public to support blueprint access
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DeckOptionButton;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DiscardOptionButton;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UHorizontalBox* OptionsBox;

	// Override functions from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	UFUNCTION()
	void CleanUpWidgets();

	// Button behavior defining functions
	UFUNCTION()
	void CloseIfChoicesExhausted();

	UFUNCTION()
	void SetImageTextureFromCard(const UEverdellCard* Card, UImage* Image);

	UFUNCTION()
	void OnDeckOptionButtonClicked();
	UFUNCTION()
	void HideDeckOptionButtonIfNotClickable();
	UFUNCTION()
	bool CanClickDeckOptionButton() const;

	UFUNCTION()
	void OnDiscardOptionButtonClicked();
	UFUNCTION()
	void HideDiscardOptionButtonIfNotClickable();
	UFUNCTION()
	bool CanClickDiscardOptionButton() const;
};
