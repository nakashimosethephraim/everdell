#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "EverdellDiscardForEffectPopup.generated.h"

class UEverdellCard;
class UButton;
class UTextBlock;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellDiscardForEffectPopup : public UEverdellPopup
{
	GENERATED_BODY()

	// Unique properties filled in at runtime
	UPROPERTY()
	UEverdellCard* AssociatedCard;

	UPROPERTY()
	UEverdellCard* CardToDiscard;

public:
	// Properties connected to blueprint subclass
	// Must be public to support blueprint access
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* NoDiscardButton;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DiscardButton;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* DiscardButtonText;

	// Override functions from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	// Button behavior defining functions
	UFUNCTION()
	void OnNoDiscardButtonClicked();

	UFUNCTION()
	void OnDiscardButtonClicked();
};
