#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "EverdellPaymentOptionsPopup.generated.h"

class UEverdellCard;
class UButton;
class UTextBlock;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API UEverdellPaymentOptionsPopup : public UEverdellPopup
{
	GENERATED_BODY()

	UPROPERTY()
	UEverdellCard* AssociatedCard;

	UPROPERTY()
	FLinearColor BrownColor;
	UPROPERTY()
	FLinearColor YellowColor;
	UPROPERTY()
	FLinearColor GreyColor;
	UPROPERTY()
	FLinearColor PurpleColor;
	UPROPERTY()
	FLinearColor WhiteColor;
	UPROPERTY()
	FLinearColor BlackColor;
	UPROPERTY()
	FLinearColor InvisibleColor;

public:
	// Properties connected to blueprint subclass
	// Must be public to support blueprint access

	// Default option
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* DefaultCostOptionButton;

	// Twig replacement buttons
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* TwigForResinOptionButton;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* TwigForResinOptionButtonText;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* TwigForPebbleOptionButton;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* TwigForPebbleOptionButtonText;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* TwigForBerryOptionButton;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* TwigForBerryOptionButtonText;

	// Resin replacement buttons
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* ResinForTwigOptionButton;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* ResinForTwigOptionButtonText;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* ResinForPebbleOptionButton;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* ResinForPebbleOptionButtonText;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* ResinForBerryOptionButton;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* ResinForBerryOptionButtonText;

	// Pebble replacement buttons
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* PebbleForTwigOptionButton;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* PebbleForTwigOptionButtonText;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* PebbleForResinOptionButton;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* PebbleForResinOptionButtonText;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* PebbleForBerryOptionButton;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* PebbleForBerryOptionButtonText;

	// Berry replacement buttons
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* BerryForTwigOptionButton;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* BerryForTwigOptionButtonText;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* BerryForResinOptionButton;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* BerryForResinOptionButtonText;

	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UButton* BerryForPebbleOptionButton;
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UTextBlock* BerryForPebbleOptionButtonText;

	// Override function from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	// Utility functions
	UFUNCTION()
	FText CostsToDisplayText(int32 TwigCost, int32 ResinCost, int32 PebbleCost, int32 BerryCost);
	UFUNCTION()
	void PayAssociatedCost(int32 TwigCostDelta, int32 ResinCostDelta, int32 PebbleCostDelta, int32 BerryCostDelta);

	// Button behavior defining functions
	UFUNCTION()
	void OnDefaultCostOptionButtonClicked();

	UFUNCTION()
	void OnTwigForResinOptionButtonClicked();
	UFUNCTION()
	void OnTwigForPebbleOptionButtonClicked();
	UFUNCTION()
	void OnTwigForBerryOptionButtonClicked();

	UFUNCTION()
	void OnResinForTwigOptionButtonClicked();
	UFUNCTION()
	void OnResinForPebbleOptionButtonClicked();
	UFUNCTION()
	void OnResinForBerryOptionButtonClicked();

	UFUNCTION()
	void OnPebbleForTwigOptionButtonClicked();
	UFUNCTION()
	void OnPebbleForResinOptionButtonClicked();
	UFUNCTION()
	void OnPebbleForBerryOptionButtonClicked();

	UFUNCTION()
	void OnBerryForTwigOptionButtonClicked();
	UFUNCTION()
	void OnBerryForResinOptionButtonClicked();
	UFUNCTION()
	void OnBerryForPebbleOptionButtonClicked();
};
