#pragma once

#include "CoreMinimal.h"
#include "UI/EverdellPopup.h"
#include "EverdellMoveWorkerPopup.generated.h"

class AEverdellWorkerLocation;
class UEverdellLambdaWrapper;
class UUniformGridPanel;
class UButton;
class UTextBlock;

/**
 * 
 */
UCLASS()
class EVERDELLBOARDGAME_API UEverdellMoveWorkerPopup : public UEverdellPopup
{
	GENERATED_BODY()

	UPROPERTY()
	int32 RowColCount;

	UPROPERTY()
	TArray<UEverdellLambdaWrapper*> Lambdas;

	TArray<TTuple<UButton*, UTextBlock*>> AllocatedWidgets;

	UPROPERTY()
	FLinearColor BlackColor;
	UPROPERTY()
	FLinearColor WhiteColor;
	
	UPROPERTY()
	AEverdellWorkerLocation* PreviousLocation;

	UPROPERTY()
	TArray<AEverdellWorkerLocation*> ValidLocations;

public:
	UPROPERTY(BlueprintReadWrite, Category = "Inner Widget Data", meta = (BindWidget))
	UUniformGridPanel* OptionsGrid;

	// Override functions from parent class
	virtual void InitializeData(const FEverdellPopupData& PopupData) override;
	virtual void InitializeInputs() override;

private:
	void CleanUpWidgets();
};
