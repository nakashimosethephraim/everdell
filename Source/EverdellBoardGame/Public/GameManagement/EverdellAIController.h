#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EverdellAIController.generated.h"

/**
 * 
 */
UCLASS()
class EVERDELLBOARDGAME_API AEverdellAIController : public AAIController
{
	GENERATED_BODY()

public:
	AEverdellAIController();
};
