#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EverdellBoardGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API AEverdellBoardGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AEverdellBoardGameGameModeBase();
};
