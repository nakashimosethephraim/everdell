#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "GameManagement/EverdellSeasonEnum.h"
#include "GameData/EverdellCardCategoryEnum.h"
#include "GameData/EverdellCardTypeEnum.h"
#include "EverdellPlayerState.generated.h"

class AEverdellHUD;
class AEverdellPlayerCity;
class AEverdellGameStateBase;
class AEverdellDeckActor;
class AEverdellDiscardPile;
class UEverdellUICard;
class UEverdellCard;
class AEverdell3DCard;
class AEverdellWorkerSlotGroup;
class AEverdellWorker;
class UEverdellAtomicWrapper;

DECLARE_MULTICAST_DELEGATE_FourParams(FEverdellCanPlayUsingPaymentEffectSignature, const AEverdellGameStateBase*, const AEverdellHUD*, const UEverdellCard*, UEverdellAtomicWrapper*);
DECLARE_DELEGATE_RetVal_FourParams(bool, FEverdellCanPerformBeforePaymentEffectSignature, const AEverdellGameStateBase*, const AEverdellHUD*, const UEverdellCard*, const UEverdellCard*);
DECLARE_DELEGATE_FourParams(FEverdellPerformBeforePaymentEffectSignature, AEverdellGameStateBase*, AEverdellHUD*, UEverdellCard*, UEverdellCard*);
DECLARE_MULTICAST_DELEGATE_TwoParams(FEverdellAfterCritterEffectSignature, AEverdellGameStateBase*, AEverdellHUD*);
DECLARE_MULTICAST_DELEGATE_TwoParams(FEverdellAfterConstructionEffectSignature, AEverdellGameStateBase*, AEverdellHUD*);

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API AEverdellPlayerState : public APlayerState
{
	GENERATED_BODY()

	UPROPERTY()
	int32 CardsInHand;

	UPROPERTY()
	int32 NextAvailableIndex;

	/** References to all core game data and functionality */
	UPROPERTY()
	AEverdellGameStateBase* GameState;
	UPROPERTY()
	AEverdellHUD* HUD;

	// Season
	UPROPERTY()
	EEverdellSeason Season;

	// Number of twigs available
	UPROPERTY()
	int32 TwigCount;

	// Number of resin available
	UPROPERTY()
	int32 ResinCount;

	// Number of pebbles available
	UPROPERTY()
	int32 PebbleCount;

	// Number of berries available
	UPROPERTY()
	int32 BerryCount;

	// Number of raw points accrued
	UPROPERTY()
	int32 PointCount;

	// Number of cards allowed to draw
	UPROPERTY()
	int32 AvailableCardDraw;

	// Number of meadow cards (only) allowed to draw
	UPROPERTY()
	int32 AvailableMeadowCardDraw;

public:
	AEverdellPlayerState();

	UPROPERTY(BlueprintReadOnly, Category = "Cards")
	TArray<UEverdellUICard*> Hand;

	// Available workers
	UPROPERTY(BlueprintReadOnly, Category = "Workers")
	AEverdellWorkerSlotGroup* WorkerSlots;

	// Total workers
	UPROPERTY(BlueprintReadOnly, Category = "Workers")
	TArray<AEverdellWorker*> Workers;

	// Player City
	UPROPERTY(BlueprintReadOnly, Category = "Locations")
	AEverdellPlayerCity* City;

	UPROPERTY(BlueprintReadOnly, Category = "Data")
	FName PlayerTag;
	UPROPERTY(BlueprintReadOnly, Category = "Data")
	FName PlayerAuxiliaryTag;

	// On play events
	TMap<TPair<EEverdellCardType, int32>, TPair<FEverdellCanPerformBeforePaymentEffectSignature, FEverdellPerformBeforePaymentEffectSignature>> BeforeCritterPaymentEventDelegates;
	TMap<TPair<EEverdellCardType, int32>, TPair<FEverdellCanPerformBeforePaymentEffectSignature, FEverdellPerformBeforePaymentEffectSignature>> BeforeConstructionPaymentEventDelegates;
	FEverdellAfterCritterEffectSignature AfterCritterEffect;
	FEverdellAfterConstructionEffectSignature AfterConstructionEffect;
	FEverdellCanPlayUsingPaymentEffectSignature CheckIfPlayPossibleEffect;

	// Event delegate storage
	TMap<TPair<EEverdellCardType, int32>, FDelegateHandle> AfterCritterEventDelegates;
	TMap<TPair<EEverdellCardType, int32>, FDelegateHandle> AfterConstructionEventDelegates;
	TMap<TPair<EEverdellCardType, int32>, FDelegateHandle> CheckIfPlayPossibleEventDelegates;

	// Season interface
	UFUNCTION()
	EEverdellSeason GetCurrentSeason() const { return Season; }
	UFUNCTION()
	void AdvanceSeason() { Season = static_cast<EEverdellSeason>(static_cast<uint8>(Season) + 1); }

	// Resource interface functions
	UFUNCTION()
	int32 GetTwigCount() const { return TwigCount; }
	UFUNCTION()
	void SetTwigCount(const int32 NewTwigCount);
	UFUNCTION()
	void AugmentTwigCount(const int32 TwigCountDelta);

	UFUNCTION()
	int32 GetResinCount() const { return ResinCount; }
	UFUNCTION()
	void SetResinCount(const int32 NewResinCount);
	UFUNCTION()
	void AugmentResinCount(const int32 ResinCountDelta);

	UFUNCTION()
	int32 GetPebbleCount() const { return PebbleCount; }
	UFUNCTION()
	void SetPebbleCount(const int32 NewPebbleCount);
	UFUNCTION()
	void AugmentPebbleCount(const int32 PebbleCountDelta);

	UFUNCTION()
	int32 GetBerryCount() const { return BerryCount; }
	UFUNCTION()
	void SetBerryCount(const int32 NewBerryCount);
	UFUNCTION()
	void AugmentBerryCount(const int32 BerryCountDelta);

	UFUNCTION()
	int32 GetPointCount() const { return PointCount; }
	UFUNCTION()
	void SetPointCount(const int32 NewPointCount);
	UFUNCTION()
	void AugmentPointCount(const int32 PointCountDelta);

	UFUNCTION()
	int32 GetAvailableCardDraw() const { return AvailableCardDraw; }
	UFUNCTION()
	void SetAvailableCardDraw(const int32 NewAvailableCardDraw);
	UFUNCTION()
	void AugmentAvailableCardDraw(const int32 AvailableCardDrawDelta);

	UFUNCTION()
	int32 GetAvailableMeadowCardDraw() const { return AvailableMeadowCardDraw; }
	UFUNCTION()
	void SetAvailableMeadowCardDraw(const int32 NewAvailableMeadowCardDraw);
	UFUNCTION()
	void AugmentAvailableMeadowCardDraw(const int32 AvailableMeadowCardDrawDelta);

	UFUNCTION()
	void DecrementAvailableCardDraw(const bool bFromMeadow);

	UFUNCTION()
	int32 GetTotalAvailableCardDraw() const { return AvailableCardDraw + AvailableMeadowCardDraw; }

	// Startup functions
	UFUNCTION()
	void InitializeCityAndWorkers(const FName& NewPlayerTag, const FName& NewPlayerAuxiliaryTag);

	// Hand interface functions
	UFUNCTION()
	void InitializeUI(UEverdellUserWidget* MainUI);

	UFUNCTION()
	int32 GetHandCardCount() const;

	UFUNCTION()
	int32 GetHandCardLimit() const;

	UFUNCTION()
	bool CanReceiveHandCards() const;

	UFUNCTION()
	bool SetHandCard(UEverdellCard* Card);

	UFUNCTION()
	UEverdellCard* GetHandCard(const int32 Index) const;

	UFUNCTION()
	UEverdellCard* ResetHandCard(const int32 Index);

	UFUNCTION()
	UEverdellCard* DiscardHandCard(const int32 Index);

	// Worker interface functions
	UFUNCTION()
	int32 GetWorkerCount() const;

	UFUNCTION()
	bool HasAvailableWorker() const;

	UFUNCTION()
	AEverdellWorker* GetNextAvailableWorker() const;

	UFUNCTION()
	bool AddWorker(AEverdellWorker* Worker, bool bReturningWorker = false);

	UFUNCTION()
	TArray<AEverdellWorker*> GetDeployedWorkers() const;

	UFUNCTION()
	TArray<AEverdellWorkerLocation*> GetValidLocations(AEverdellWorkerLocation* LocationToExclude = nullptr) const;

	// City interface functions
	UFUNCTION()
	int32 GetCityCardCount() const;

	UFUNCTION()
	bool CanPlaceCityCards() const;

	UFUNCTION()
	bool SetCityCard(AEverdell3DCard* CardActor);

	UFUNCTION()
	AEverdell3DCard* GetCityCard(const int32 Index) const;

	UFUNCTION()
	TArray<AEverdell3DCard*> GetCityCardsOfType(const EEverdellCardType CardType) const;

	UFUNCTION()
	AEverdellCardLocationActor* GetCityCardLocation(const int32 Index) const;

	UFUNCTION()
	UEverdellCard* DiscardCityCard(const int32 Index, UEverdellCard* DiscardToCard = nullptr);

	UFUNCTION()
	int32 CountOfCardInCity(const EEverdellCardType CardType) const;

	UFUNCTION()
	int32 CountOfCardCategoryInCity(const EEverdellCardCategory CardCategory) const;

	UFUNCTION()
	int32 CountOfCrittersInCity() const;

	UFUNCTION()
	int32 CountOfConstructionsInCity() const;

	UFUNCTION()
	int32 CountOfActivatableGreenProductionCardsInCity() const;

	// City / hand manipulation
	UFUNCTION()
	void MoveToCityPayingCost(UEverdellCard* Card, const bool bHandlePrePlayEffect = true, const bool bHandleBeforePaymentEffects = true);

	UFUNCTION()
	void MoveToCityPayingExplicitCost(UEverdellCard* Card, const int32 TwigCost, const int32 ResinCost, const int32 PebbleCost, const int32 BerryCost);

	UFUNCTION()
	void MoveToCityWithFreeUpgrade(UEverdellCard* Card, AEverdell3DCard* FreeUpgradeCard);

	UFUNCTION()
	void MoveToCity(UEverdellCard* Card);

	UFUNCTION()
	void PairCardInCity(UEverdellCard* Card, AEverdellCardLocationActor* PairLocation);

	UFUNCTION()
	void UpdateScore();

protected:
	virtual void BeginPlay() override;

private:
	UFUNCTION()
	void DisambiguateAndMoveCard(UEverdellCard* Card);	
};
