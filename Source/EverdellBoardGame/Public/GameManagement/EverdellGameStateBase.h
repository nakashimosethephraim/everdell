#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "EverdellGameStateBase.generated.h"

class APawn;
class AEverdellPlayerState;
class AEverdellAIController;
class AEverdellDeckActor;
class AEverdellDiscardPile;
class AEverdellLocationGroup;
class AEverdellWorkerLocation;
class AEverdellForestLocationSlot;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API AEverdellGameStateBase : public AGameStateBase
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<uint32> LocationIndexPool;
	
	// Keep reference to AI Controller
	UPROPERTY()
	AEverdellAIController* AIController;

	// Keep reference to default Pawn
	UPROPERTY()
	APawn* DefaultPawn;

	// Properties for managing player
	UPROPERTY()
	uint8 CurrentPlayerIndex;

public:
	AEverdellGameStateBase();

	// Meadow
	UPROPERTY(BlueprintReadOnly, Category = "Locations")
	AEverdellLocationGroup* Meadow;

	// Deck
	UPROPERTY()
	AEverdellDeckActor* Deck;

	// Discard pile
	UPROPERTY()
	AEverdellDiscardPile* DiscardPile;

	// Forest Locations
	TArray<AEverdellForestLocationSlot*> ForestLocationSlots;

	// Basic Locations
	TArray<AEverdellWorkerLocation*> BasicLocations;

	// Location utility
	UFUNCTION()
	TArray<AEverdellWorkerLocation*> GetViableLocations() const;

	// Player state interaction
	UFUNCTION()
	int32 GetPlayerCount() const;

	UFUNCTION()
	AEverdellPlayerState* GetCurrentPlayer() const;

	UFUNCTION()
	void EndTurn();

protected:
	virtual void BeginPlay() override;
};
