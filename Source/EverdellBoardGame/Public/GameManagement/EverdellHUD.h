#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "UI/EverdellPopupData.h"
#include "Containers/Queue.h"
#include "EverdellHUD.generated.h"

class UUserWidget;
class UEverdellUserWidget;
class UEverdellPopup;
class UEverdellCard;
class AEverdell3DCard;
class AEverdellGameStateBase;
class AEverdellDeckActor;
class AEverdellCardLocationActor;
class AEverdellWorker;
class AEverdellWorkerLocation;

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API AEverdellHUD : public AHUD
{
	GENERATED_BODY()

	// Is Hovering Card
	UPROPERTY()
	uint8 bIsHoveringCard : 1;

	// Queue for popups
	TQueue<FEverdellPopupData, EQueueMode::Mpsc> PopupQueue;

public:
	AEverdellHUD();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Widget UI")
	TSubclassOf<UEverdellUserWidget> WidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget UI")
	UEverdellUserWidget* Widget;

	// Reference to player information
	UPROPERTY()
	AEverdellGameStateBase* GameState;

	// Popup Classes
	UPROPERTY()
	TSubclassOf<UEverdellPopup> ActivateCardPopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> CarnivalPopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> ChooseAbilityPopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> ChooseAnyPopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> ChooseCardPopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> ChooseConstructionResourcePopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> CopyLocationPopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> DiscardForEffectPopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> DiscardFromCityPopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> DiscardNCardsPopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> DrawFromMeadowPopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> ExchangeResourcesPopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> HusbandWifePopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> MoveWorkerPopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> PaymentOptionsPopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> PayOrFreeUpgradePopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> PlayForNLessPopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> ResourceToPointsPopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> RevealChoicePopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> SelectWorkerPopup;
	UPROPERTY()
	TSubclassOf<UEverdellPopup> StorehousePopup;

	// Popup open functions
	UFUNCTION()
	void OpenActivateCardPopup(UEverdellCard* AssociatedCard);

	UFUNCTION()
	void OpenCarnivalPopup();

	void OpenChooseAbilityPopup(UEverdellCard* AssociatedCard, const TArray<TPair<UEverdellCard*, FEverdellPerformBeforePaymentEffectSignature>>& ValidDelegateArray);

	UFUNCTION()
	void OpenChooseAnyPopup(const int32 TotalChoices);

	void OpenChooseCardPopup(const int32 Reduction, const TArray<UEverdellCard*>& AssociatedCards, const FEverdellAfterCardChoiceSignature& AfterCardChoiceDelegate);

	void OpenChooseCardPopupWithBoundDelegate(const int32 Reduction, const TArray<UEverdellCard*>& AssociatedCards, const FEverdellAfterCardChoiceSignature& AfterCardChoiceDelegate);

	UFUNCTION()
	void OpenChooseFreeCardPopup(const int32 UpperLimit, const TArray<UEverdellCard*>& AssociatedCards);

	UFUNCTION()
	void OpenChooseConstructionResourcePopup(const int32 TotalChoices);

	UFUNCTION()
	void OpenCopyLocationPopup(const bool bIncludeForestLocations);

	UFUNCTION()
	void OpenDiscardForEffectPopup(UEverdellCard* AssociatedCard, UEverdellCard* CardToDiscard);

	void OpenDiscardNCardsPopup(const int32 DiscardLimit, const int32 Multiplier, FEverdellAfterDiscardSignature&& AfterDiscardDelegate);

	void OpenDrawFromMeadowPopup(
		const int32 Reduction, const int32 RemainingDraw,
		FEverdellAfterCardChoiceSignature&& AfterCardChoiceDelegate, FEverdellAfterMeadowDrawSignature&& AfterMeadowDrawDelegate
	);

	UFUNCTION()
	void OpenDungeonDiscardPopup(UEverdellCard* AssociatedCard, UEverdellCard* CardToPlay);

	UFUNCTION()
	void OpenExchangeResourcesPopup(const int32 ExchangeLimit);

	UFUNCTION()
	void OpenHusbandWifePopup(UEverdellCard* AssociatedCard, AEverdellCardLocationActor* PossiblePlacementLocation);

	UFUNCTION()
	void OpenMoveWorkerPopup(AEverdellWorkerLocation* PreviousLocation);

	UFUNCTION()
	void OpenPaymentOptionsPopup(UEverdellCard* AssociatedCard);

	UFUNCTION()
	void OpenPayOrFreeUpgradePopup(UEverdellCard* AssociatedCard, const TArray<AEverdell3DCard*>& FreeUpgradeOptions);

	UFUNCTION()
	void OpenPlayForNLessPopup(UEverdellCard* AssociatedCard, const int32 Reduction);

	UFUNCTION()
	void OpenPostalPigeonPopup();

	void OpenResourceToPointsPopup(
		const FName& ResourceName, const int32 MinCost, const int32 MaxCost, const int32 Rate,
		FEverdellCheckValueSignature&& CheckValueDelegate, FEverdellValueAugmentSignature&& ValueAugmentDelegate
	);

	UFUNCTION()
	void OpenRevealChoicePopup(const int32 UpperLimit);

	UFUNCTION()
	void OpenRuinsDiscardPopup(UEverdellCard* AssociatedCard);

	UFUNCTION()
	void OpenSelectWorkerPopup(const TArray<AEverdellWorker*>& DeployedWorkers);

	UFUNCTION()
	void OpenStorehousePopup(UEverdellCard* AssociatedCard);

	UFUNCTION()
	void OpenUniversityDiscardPopup(UEverdellCard* AssociatedCard);

	UFUNCTION()
	void ClosePopup();

	UFUNCTION()
	UUserWidget* HandlePopupContent();

	// Tooltip interaction functions
	UFUNCTION()
	void StartHover(UEverdellCard* Card);

	UFUNCTION()
	void EndHover();

private:
	UFUNCTION()
	FVector2D GetMousePosition() const;

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
};
