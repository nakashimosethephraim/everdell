#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "EverdellPlayerController.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class EVERDELLBOARDGAME_API AEverdellPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AEverdellPlayerController();

	// Array of cameras to select from
	UPROPERTY(BlueprintReadOnly, Category = "Target Cameras")
	TArray<AActor*> Cameras;

	// Array of cameras to select from
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Target Cameras")
	float CameraBlendTime;

	// Array of cameras to select from
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Handle Cameras")
	void HandleCameraChange(int CameraIndex);

protected:
	virtual void BeginPlay() override;
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;

private:
	UPROPERTY(EditDefaultsOnly)
	TArray<FName> CameraTags;
};
