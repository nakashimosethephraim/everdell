#pragma once

#include "CoreMinimal.h"
#include "EverdellSeasonEnum.generated.h"

UENUM(BlueprintType)
enum class EEverdellSeason : uint8 {
	LATE_WINTER	 UMETA(DisplayName = "LATE_WINTER"),
	SPRING		 UMETA(DisplayName = "SPRING"),
	SUMMER	     UMETA(DisplayName = "SUMMER"),
	AUTUMN	     UMETA(DisplayName = "AUTUMN"),
	EARLY_WINTER UMETA(DisplayName = "EARLY_WINTER")
};
