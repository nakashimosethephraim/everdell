#pragma once

#include "CoreMinimal.h"
#include "EverdellCardCategoryEnum.generated.h"

UENUM(BlueprintType)
enum class EEverdellCardCategory : uint8 {
	NONE			 UMETA(DisplayName = "NONE"),
	TAN_TRAVELER     UMETA(DisplayName = "TAN_TRAVELER"),
	GREEN_PRODUCTION UMETA(DisplayName = "GREEN_PRODUCTION"),
	RED_DESTINATION  UMETA(DisplayName = "RED_DESTINATION"),
	BLUE_GOVERNANCE  UMETA(DisplayName = "BLUE_GOVERNANCE"),
	PURPLE_PROPERITY UMETA(DisplayName = "PURPLE_PROPERITY")
};
