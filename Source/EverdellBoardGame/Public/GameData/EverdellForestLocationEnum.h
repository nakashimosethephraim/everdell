#pragma once

#include "CoreMinimal.h"
#include "EverdellForestLocationEnum.generated.h"

UENUM(BlueprintType)
enum class EEverdellForestLocationType : uint8 {
	NOT_GENERATED                        UMETA(DisplayName = "NOT_GENERATED"),
	ONETWIG_ONERESIN_ONEBERRY            UMETA(DisplayName = "ONETWIG_ONERESIN_ONEBERRY"),
	TWOANY                               UMETA(DisplayName = "TWOANY"),
	TWOBERRIES_DRAWONECARD               UMETA(DisplayName = "TWOBERRIES_DRAWONECARD"),
	TWORESIN_ONETWIG                     UMETA(DisplayName = "TWORESIN_ONETWIG"),
	THREEBERRIES                         UMETA(DisplayName = "THREEBERRIES"),
	COPYBASIC_DRAWONECARD                UMETA(DisplayName = "COPYBASIC_DRAWONECARD"),
	DISCARDNCARDS_DRAW2NCARDS            UMETA(DisplayName = "DISCARDNCARDS_DRAW2NCARDS"),
	DISCARDNCARDS_GAINNANY               UMETA(DisplayName = "DISCARDNCARDS_GAINNANY"),
	DRAWTWOCARDS_ONEANY                  UMETA(DisplayName = "DRAWTWOCARDS_ONEANY"),
	DRAWTWOMEADOWCARDS_PLAYFORONELESSANY UMETA(DisplayName = "DRAWTWOMEADOWCARDS_PLAYFORONELESSANY"),
	DRAWTHREECARDS_ONEPEBBLE             UMETA(DisplayName = "DRAWTHREECARDS_ONEPEBBLE")
};
