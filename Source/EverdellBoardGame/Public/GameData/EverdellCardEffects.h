#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "EverdellCardEffects.generated.h"

class AEverdellPlayerState;
class AEverdellGameStateBase;
class AEverdellHUD;
class AEverdellDeckActor;
class AEverdellCardLocationActor;
class UEverdellCard;
class UEverdellAtomicWrapper;

/**
 * 
 */
UCLASS()
class EVERDELLBOARDGAME_API UEverdellCardEffects : public UObject
{
	GENERATED_BODY()

public:
	// Generic card effect requirements
	UFUNCTION()
	static bool CanPerformEffectDefault(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformEffectOnPlay(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformEffectGreenProduction(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformEffectScoring(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformEffectTriggered(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformEffectOnPlay_ChipSweep(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformEffectGreenProduction_ChipSweep(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformActivatedEffect_Crane(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformEffectOnPlay_Husband(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformEffectGreenProduction_Husband(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformActivatedEffect_Innkeeper(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformEffectOnPlay_Peddler(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformEffectGreenProduction_Peddler(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformEffect_Ruins(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	
	// Default effects
	UFUNCTION()
	static void PerformEffectDefault(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);

	// Specific card effects
	UFUNCTION()
	static int32 PerformEffect_Architect(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Bard(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_BargeToad(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static int32 PerformEffect_Castle(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Carnival(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_ChipSweep(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Courthouse(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformActivatedEffect_Crane(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformTriggeredEffect_Crane(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Doctor(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Dungeon(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static int32 PerformEffect_EverTree(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Fairgrounds(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Farm(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_GeneralStore(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Historian(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Husband(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformActivatedEffect_Innkeeper(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformTriggeredEffect_Innkeeper(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Judge(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Mine(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static int32 PerformEffect_Palace(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Peddler(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_PostalPigeon(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Ranger(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_ResinRefinery(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Ruins(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static int32 PerformEffect_School(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static int32 PerformEffect_ScurrbleChampion(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Shopkeeper(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Storehouse(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static int32 PerformEffect_Theater(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_TwigBarge(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Wanderer(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);
	UFUNCTION()
	static int32 PerformEffect_Wife(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static void PerformEffect_Woodcarver(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellCardLocationActor* CardLocation, UEverdellCard* AssociatedCard);

	// Triggered effects
	UFUNCTION()
	static void AfterConstructionEffect_Courthouse(AEverdellGameStateBase* GameState, AEverdellHUD* HUD);
	UFUNCTION()
	static bool CanPerformBeforePaymentEffect_Crane(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const UEverdellCard* AssociatedCard, const UEverdellCard* CardToDiscard);
	UFUNCTION()
	static void PerformBeforePaymentEffect_Crane(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, UEverdellCard* AssociatedCard, UEverdellCard* CardToDiscard);
	UFUNCTION()
	static bool CanPerformBeforePaymentEffect_Dungeon(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const UEverdellCard* AssociatedCard, const UEverdellCard* DungeonCard);
	UFUNCTION()
	static void PerformBeforePaymentEffect_Dungeon(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, UEverdellCard* AssociatedCard, UEverdellCard* DungeonCard);
	UFUNCTION()
	static void AfterCritterOrConstructionEffect_Historian(AEverdellGameStateBase* GameState, AEverdellHUD* HUD);
	UFUNCTION()
	static bool CanPerformBeforePaymentEffect_Innkeeper(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const UEverdellCard* AssociatedCard, const UEverdellCard* CardToDiscard);
	UFUNCTION()
	static void PerformBeforePaymentEffect_Innkeeper(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, UEverdellCard* AssociatedCard, UEverdellCard* CardToDiscard);
	UFUNCTION()
	static bool CanPerformBeforePaymentEffect_Judge(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const UEverdellCard* AssociatedCard, const UEverdellCard* CardToDiscard);
	UFUNCTION()
	static void PerformBeforePaymentEffect_Judge(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, UEverdellCard* AssociatedCard, UEverdellCard* CardToDiscard);
	UFUNCTION()
	static void AfterCritterEffect_Shopkeeper(AEverdellGameStateBase* GameState, AEverdellHUD* HUD);

	// Check if play possible effects
	UFUNCTION()
	static void CanPlayUsingPaymentEffect_Crane(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const UEverdellCard* AssociatedCard, UEverdellAtomicWrapper* AtomicData);
	UFUNCTION()
	static void CanPlayUsingPaymentEffect_Dungeon(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const UEverdellCard* AssociatedCard, UEverdellAtomicWrapper* AtomicData);
	UFUNCTION()
	static void CanPlayUsingPaymentEffect_Innkeeper(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const UEverdellCard* AssociatedCard, UEverdellAtomicWrapper* AtomicData);
	UFUNCTION()
	static void CanPlayUsingPaymentEffect_Judge(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const UEverdellCard* AssociatedCard, UEverdellAtomicWrapper* AtomicData);

private:
	UFUNCTION()
	static bool CanPerformEffect_ChipSweep(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformEffect_Crane(const AEverdellGameStateBase* GameState, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformEffect_Dungeon(const AEverdellGameStateBase* GameState, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformEffect_Husband(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformEffect_Innkeeper(const AEverdellGameStateBase* GameState, const UEverdellCard* AssociatedCard);
	UFUNCTION()
	static bool CanPerformEffect_Peddler(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* CardLocation, const UEverdellCard* AssociatedCard);
};
