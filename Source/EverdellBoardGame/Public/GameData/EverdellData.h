#pragma once

#include "CoreMinimal.h"
#include "GameData/EverdellCardCategoryEnum.h"
#include "GameData/EverdellCardTypeEnum.h"
#include "GameData/EverdellForestLocationEnum.h"
#include "EverdellData.generated.h"

class AEverdellPlayerState;
class AEverdellGameStateBase;
class AEverdellHUD;
class AEverdellCardLocationActor;
class AEverdellWorkerLocation;
class AEverdellWorker;
class UEverdellCard;

using FEverdellCanPerformCardEffectFunction = TFunction<bool(const AEverdellGameStateBase*, const AEverdellHUD*, const AEverdellCardLocationActor*, const UEverdellCard*)>;
using FEverdellCanPerformCardScoringEffectFunction = TFunction<bool(const AEverdellPlayerState*, const AEverdellGameStateBase*, const AEverdellHUD*, const AEverdellCardLocationActor*, const UEverdellCard*)>;
using FEverdellPerformCardScoringEffectFunction = TFunction<int32(const AEverdellPlayerState*, const AEverdellGameStateBase*, const AEverdellHUD*, const AEverdellCardLocationActor*, const UEverdellCard*)>;
using FEverdellPerformCardEffectFunction = TFunction<void(AEverdellGameStateBase*, AEverdellHUD*, AEverdellCardLocationActor*, UEverdellCard*)>;

using FEverdellCanPerformLocationEffectFunction = TFunction<bool(const AEverdellGameStateBase*, const AEverdellHUD*, const AEverdellWorkerLocation*, const bool)>;
using FEverdellPerformLocationEffectFunction = TFunction<AEverdellWorker*(AEverdellGameStateBase*, AEverdellHUD*, AEverdellWorkerLocation*, const bool)>;

/**
 * 
 */
USTRUCT(BlueprintType)
struct FEverdellCardData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	EEverdellCardCategory CardCategory;

	UPROPERTY()
	EEverdellCardType CardType;

	UPROPERTY()
	FName CardName;

	UPROPERTY()
	FName CardDescription;

	UPROPERTY()
	uint8 TwigCost;

	UPROPERTY()
	uint8 ResinCost;

	UPROPERTY()
	uint8 PebbleCost;

	UPROPERTY()
	uint8 BerryCost;

	UPROPERTY()
	int8 VictoryPoints;

	UPROPERTY()
	uint8 bIsCritter : 1;

	UPROPERTY()
	uint8 bIsUnique : 1;

	UPROPERTY()
	uint8 bRequiresSpace : 1;

	UPROPERTY()
	uint8 bDiscardsBeforePlay : 1;

	UPROPERTY()
	uint8 bPayToOpponent : 1;

	UPROPERTY()
	uint8 bPlayToOpponentCity : 1;

	UPROPERTY()
	uint8 bOpenToOpponent : 1;

	UPROPERTY()
	uint8 LocationCount;

	UPROPERTY()
	int32 LocationYValue;

	UPROPERTY()
	TArray<EEverdellCardType> FreeUpgradeOptions;

	UPROPERTY()
	EEverdellCardType RequiredForSecondCopy;

	UPROPERTY()
	EEverdellCardType RequiredForSecondPlacementSlot;

	UPROPERTY()
	FName CardTextureResourcePath;

	UPROPERTY()
	FName CardMaterialResourcePath;

	FEverdellCanPerformCardEffectFunction CanPerformPrePlayEffectType;
	FEverdellPerformCardEffectFunction PerformPrePlayEffectType;

	FEverdellCanPerformCardEffectFunction CanPerformOnPlayEffectType;
	FEverdellPerformCardEffectFunction PerformOnPlayEffectType;

	FEverdellCanPerformCardEffectFunction CanPerformPrepareForSeasonEffectType;
	FEverdellPerformCardEffectFunction PerformPrepareForSeasonEffectType;

	FEverdellCanPerformLocationEffectFunction CanPerformWorkerPlacementEffectType;
	FEverdellPerformLocationEffectFunction PerformWorkerPlacementEffectType;

	FEverdellCanPerformCardScoringEffectFunction CanPerformScoringEffectType;
	FEverdellPerformCardScoringEffectFunction PerformScoringEffectType;

	FEverdellCanPerformCardEffectFunction CanPerformDiscardEffectType;
	FEverdellPerformCardEffectFunction PerformDiscardEffectType;

	FEverdellCanPerformCardEffectFunction CanPerformTriggeredEffectType;
	FEverdellPerformCardEffectFunction PerformTriggeredEffectType;

	FEverdellCanPerformCardEffectFunction CanPerformActivatedEffectType;
	FEverdellPerformCardEffectFunction PerformActivatedEffectType;

	UPROPERTY()
	uint8 CardCount;
};

/**
 *
 */
USTRUCT(BlueprintType)
struct FEverdellForestLocationData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	EEverdellForestLocationType LocationType;

	UPROPERTY()
	FName LocationName;

	UPROPERTY()
	FName CardMaterialResourcePath;

	FEverdellCanPerformLocationEffectFunction CanPerformEffectType;
	FEverdellPerformLocationEffectFunction PerformEffectType;
};

extern TArray<FEverdellCardData> EverdellCardData;
extern TArray<FEverdellForestLocationData> EverdellForestLocationData;
