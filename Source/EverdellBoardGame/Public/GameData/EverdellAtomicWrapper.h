#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include <atomic>
#include "EverdellAtomicWrapper.generated.h"

/**
 *
 */
UCLASS()
class EVERDELLBOARDGAME_API UEverdellAtomicWrapper : public UObject
{
	GENERATED_BODY()

public:
	std::atomic_bool Condition;
	std::atomic_int Counter;
};
