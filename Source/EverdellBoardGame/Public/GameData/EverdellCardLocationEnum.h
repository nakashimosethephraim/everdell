#pragma once

#include "CoreMinimal.h"
#include "EverdellCardLocationEnum.generated.h"

UENUM(BlueprintType)
enum class EEverdellCardLocation : uint8 {
	NOT_GENERATED      UMETA(DisplayName = "NOT_GENERATED"),
	MEADOW             UMETA(DisplayName = "MEADOW"),
	HAND               UMETA(DisplayName = "HAND"),
	CITY               UMETA(DisplayName = "CITY"),
	OPPONENT_CITY      UMETA(DisplayName = "OPPONENT_CITY"),
	DISCARD_PILE       UMETA(DisplayName = "DISCARD_PILE"),
	CONTAINED_IN_CARD  UMETA(DisplayName = "CONTAINED_IN_CARD")
};
