#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameData/EverdellData.h"
#include "GameData/EverdellCardCategoryEnum.h"
#include "GameData/EverdellCardLocationEnum.h"
#include "GameData/EverdellCardTypeEnum.h"
#include "EverdellCard.generated.h"

class AEverdellPlayerState;
class AEverdellGameStateBase;
class AEverdellHUD;
class AEverdellCardLocationActor;
class UEverdellCard;
class UEverdellAtomicWrapper;

DECLARE_DELEGATE_RetVal_FourParams(bool, FEverdellCanPerformCardEffectSignature, const AEverdellGameStateBase*, const AEverdellHUD*, const AEverdellCardLocationActor*, const UEverdellCard*);
DECLARE_DELEGATE_RetVal_FiveParams(bool, FEverdellCanPerformCardScoringEffectSignature, const AEverdellPlayerState*, const AEverdellGameStateBase*, const AEverdellHUD*, const AEverdellCardLocationActor*, const UEverdellCard*);
DECLARE_DELEGATE_RetVal_FiveParams(int32, FEverdellPerformCardScoringEffectSignature, const AEverdellPlayerState*, const AEverdellGameStateBase*, const AEverdellHUD*, const AEverdellCardLocationActor*, const UEverdellCard*);
DECLARE_DELEGATE_FourParams(FEverdellPerformCardEffectSignature, AEverdellGameStateBase*, AEverdellHUD*, AEverdellCardLocationActor*, UEverdellCard*);

using FEverdellCanPerformLocationEffectFunction = TFunction<bool(const AEverdellGameStateBase*, const AEverdellHUD*, const AEverdellWorkerLocation*, const bool)>;
using FEverdellPerformLocationEffectFunction = TFunction<AEverdellWorker* (AEverdellGameStateBase*, AEverdellHUD*, AEverdellWorkerLocation*, const bool)>;

/**
 * 
 */
UCLASS(BlueprintType)
class EVERDELLBOARDGAME_API UEverdellCard : public UObject
{
	GENERATED_BODY()

	UPROPERTY()
	UEverdellAtomicWrapper* AtomicData;

public:
	UEverdellCard();

	UPROPERTY(EditAnywhere, Category = "Identifiers")
	uint32 UniqueId;

	UPROPERTY(EditAnywhere, Category = "Identifiers")
	EEverdellCardCategory CardCategory;

	UPROPERTY(EditAnywhere, Category = "Identifiers")
	EEverdellCardType CardType;

	UPROPERTY(EditAnywhere, Category = "Identifiers")
	FName CardName;

	UPROPERTY(EditAnywhere, Category = "Identifiers")
	FName CardDescription;

	UPROPERTY(EditAnywhere, Category = "Location")
	EEverdellCardLocation CardLocation;

	UPROPERTY(EditAnywhere, Category = "Location")
	uint32 LocationLocalIndex;

	UPROPERTY(EditAnywhere, Category = "Costs")
	uint8 TwigCost;

	UPROPERTY(EditAnywhere, Category = "Costs")
	uint8 ResinCost;

	UPROPERTY(EditAnywhere, Category = "Costs")
	uint8 PebbleCost;

	UPROPERTY(EditAnywhere, Category = "Costs")
	uint8 BerryCost;

	UPROPERTY(EditAnywhere, Category = "Benefits")
	int8 VictoryPoints;

	UPROPERTY(EditAnywhere, Category = "ContainedResources")
	uint8 ContainedTwigs;

	UPROPERTY(EditAnywhere, Category = "ContainedResources")
	uint8 ContainedResin;

	UPROPERTY(EditAnywhere, Category = "ContainedResources")
	uint8 ContainedPebbles;

	UPROPERTY(EditAnywhere, Category = "ContainedResources")
	uint8 ContainedBerries;

	UPROPERTY(EditAnywhere, Category = "Benefits")
	int8 ContainedVictoryPoints;

	UPROPERTY(EditAnywhere, Category = "ContainedResources")
	TArray<UEverdellCard*> ContainedCards;

	UPROPERTY(EditAnywhere, Category = "Modifiers")
	uint8 bIsCritter : 1;

	UPROPERTY(EditAnywhere, Category = "Modifiers")
	uint8 bIsUnique : 1;

	UPROPERTY(EditAnywhere, Category = "Modifiers")
	uint8 bRequiresSpace : 1;

	UPROPERTY(EditAnywhere, Category = "Modifiers")
	uint8 bDiscardsBeforePlay : 1;

	UPROPERTY(EditAnywhere, Category = "Modifiers")
	uint8 bPayToOpponent : 1;

	UPROPERTY(EditAnywhere, Category = "Modifiers")
	uint8 bPlayToOpponentCity : 1;

	UPROPERTY(EditAnywhere, Category = "Modifiers")
	uint8 bOpenToOpponent : 1;

	UPROPERTY(EditAnywhere, Category = "Modifiers")
	uint8 LocationCount;

	UPROPERTY(EditAnywhere, Category = "Modifiers")
	int32 LocationYValue;

	UPROPERTY(EditAnywhere, Category = "Benefits")
	TArray<EEverdellCardType> FreeUpgradeOptions;

	UPROPERTY(EditAnywhere, Category = "Costs")
	uint8 bFreeUpgradeUsed : 1;

	UPROPERTY(EditAnywhere, Category = "Limitations")
	EEverdellCardType RequiredForSecondCopy;

	UPROPERTY(EditAnywhere, Category = "Limitations")
	EEverdellCardType RequiredForSecondPlacementSlot;

	UPROPERTY(EditAnywhere, Category = "VisualData")
	FName CardTextureResourcePath;

	UPROPERTY(EditAnywhere, Category = "VisualData")
	FName CardMaterialResourcePath;

	FEverdellCanPerformCardEffectSignature CanPerformPrePlayEffect;
	FEverdellPerformCardEffectSignature PerformPrePlayEffect;

	FEverdellCanPerformCardEffectSignature CanPerformOnPlayEffect;
	FEverdellPerformCardEffectSignature PerformOnPlayEffect;

	FEverdellCanPerformCardEffectSignature CanPerformPrepareForSeasonEffect;
	FEverdellPerformCardEffectSignature PerformPrepareForSeasonEffect;

	FEverdellCanPerformLocationEffectFunction CanPerformWorkerPlacementEffectType;
	FEverdellPerformLocationEffectFunction PerformWorkerPlacementEffectType;

	FEverdellCanPerformCardScoringEffectSignature CanPerformScoringEffect;
	FEverdellPerformCardScoringEffectSignature PerformScoringEffect;

	FEverdellCanPerformCardEffectSignature CanPerformDiscardEffect;
	FEverdellPerformCardEffectSignature PerformDiscardEffect;

	FEverdellCanPerformCardEffectSignature CanPerformTriggeredEffect;
	FEverdellPerformCardEffectSignature PerformTriggeredEffect;

	FEverdellCanPerformCardEffectSignature CanPerformActivatedEffect;
	FEverdellPerformCardEffectSignature PerformActivatedEffect;

	UFUNCTION()
	void BuildFromData(const FEverdellCardData& CardData);

	UFUNCTION()
	bool HasNonZeroCost() const;

	UFUNCTION()
	bool HasContainedResources() const;

	bool DoesRespectBoardState(const AEverdellGameStateBase* GameState) const;
	bool DoesRespectBoardState(const AEverdellPlayerState* PlayerState) const;

	bool IsPlayable(const AEverdellGameStateBase* GameState) const;
	bool IsPlayable(const AEverdellPlayerState* PlayerState) const;

	bool IsPlayableWithReducedCost(const int32 ReducedCost, const AEverdellGameStateBase* GameState) const;
	bool IsPlayableWithReducedCost(const int32 ReducedCost, const AEverdellPlayerState* PlayerState) const;

	bool IsPlayableWithSwappedCost(const int32 SwappedCost, const AEverdellGameStateBase* GameState) const;
	bool IsPlayableWithSwappedCost(const int32 SwappedCost, const AEverdellPlayerState* PlayerState) const;

	UFUNCTION()
	bool IsInCity() const;

	UFUNCTION()
	int32 GetCardScore(const AEverdellPlayerState* PlayerState, const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellCardLocationActor* Location) const;

	UFUNCTION()
	bool CanInteract(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD) const;

	UFUNCTION()
	void Interact(AEverdellGameStateBase* GameState, AEverdellHUD* HUD);
};
