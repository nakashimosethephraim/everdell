#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "EverdellLocationEffects.generated.h"

class AEverdellWorker;
class AEverdellGameStateBase;
class AEverdellHUD;
class AEverdellWorkerLocation;
class UEverdellCard;

/**
 * 
 */
UCLASS()
class EVERDELLBOARDGAME_API UEverdellLocationEffects : public UObject
{
	GENERATED_BODY()

public:
	// Generic location effect requirements
	UFUNCTION()
	static bool CanPerformEffectDefault(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);

	// Card location effect requirements
	UFUNCTION()
	static bool CanPerformCardLocationEffectDefault(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static bool CanPerformEffect_Cemetery(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static bool CanPerformEffect_Chapel(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static bool CanPerformEffect_Inn(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static bool CanPerformEffect_Queen(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static bool CanPerformEffect_Storehouse(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static bool CanPerformEffect_University(const AEverdellGameStateBase* GameState, const AEverdellHUD* HUD, const AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);

	// Default effects
	UFUNCTION()
	static AEverdellWorker* PerformEffectDefault(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);

	// Basic location effects
	UFUNCTION()
	static AEverdellWorker* PerformEffectBasicLocation1Berry(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffectBasicLocation1BerryDraw1Card(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffectBasicLocation1Pebble(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffectBasicLocationDraw2Cards1Point(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffectBasicLocation1ResinDraw1Card(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffectBasicLocation2Resin(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffectBasicLocation2TwigsDraw1Card(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffectBasicLocation3Twigs(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);

	// Forest location effects
	UFUNCTION()
	static AEverdellWorker* PerformEffectForestLocation1Twig1Resin1Berry(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffectForestLocation2Any(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffectForestLocation2BerriesDraw1Card(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffectForestLocation2Resin1Twig(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffectForestLocation3Berries(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffectForestLocationCopyBasicDraw1Card(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffectForestLocationDiscardNCardsDraw2NCards(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffectForestLocationDiscardNCardsGainNAny(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffectForestLocationDraw2Cards1Any(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffectForestLocationDraw2MeadowCardsPlay1For1LessAny(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffectForestLocationDraw3Cards1Pebble(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);

	// Card location effects
	UFUNCTION()
	static AEverdellWorker* PerformEffect_Cemetery(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffect_Chapel(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffect_Inn(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffect_Lookout(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffect_Queen(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffect_Storehouse(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);
	UFUNCTION()
	static AEverdellWorker* PerformEffect_University(AEverdellGameStateBase* GameState, AEverdellHUD* HUD, AEverdellWorkerLocation* Location, const bool bShouldConsumeWorker);

private:
	static AEverdellWorker* HandleWorker(AEverdellGameStateBase* GameState, bool bShouldConsumeWorker);
	static AEverdellWorker* HandleWorker(AEverdellPlayerState* PlayerState, bool bShouldConsumeWorker);

	UFUNCTION()
	static bool FindPossibleInnCardsFromMeadow(const AEverdellGameStateBase* GameState, const int32 Reduction, TArray<UEverdellCard*>& PossibleCards);

	UFUNCTION()
	static bool FindPossibleQueenCardsFromHand(const AEverdellGameStateBase* GameState, const int32 MaxVictoryPoints, TArray<UEverdellCard*>& PossibleCards);
};
