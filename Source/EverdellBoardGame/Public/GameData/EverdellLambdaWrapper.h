#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "EverdellLambdaWrapper.generated.h"

/**
 * 
 */
UCLASS()
class EVERDELLBOARDGAME_API UEverdellLambdaWrapper : public UObject
{
	GENERATED_BODY()

public:
	TFunction<void()> LambdaFunction;

	UFUNCTION()
	void Dispatch() { LambdaFunction(); }
};
